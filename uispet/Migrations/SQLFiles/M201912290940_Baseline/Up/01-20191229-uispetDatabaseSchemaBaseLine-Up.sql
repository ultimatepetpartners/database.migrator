﻿USE [master]
GO

/****** Object:  Database [uispet]    Script Date: 20/12/2019 12:46:54 ******/
CREATE DATABASE [uispet]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UISPET-2K', FILENAME = N'D:\SQLServer\MSSQL14.MSSQLSERVER\MSSQL\DATA\uispet.mdf' , SIZE = 76800000KB , MAXSIZE = UNLIMITED, FILEGROWTH = 5120000KB )
 LOG ON 
( NAME = N'UISPET-2K_log', FILENAME = N'E:\SQLServer\MSSQL14.MSSQLSERVER\MSSQL\Logs\uispet.ldf' , SIZE = 16897024KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024000KB )
GO

ALTER DATABASE [uispet] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [uispet].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO

ALTER DATABASE [uispet] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [uispet] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [uispet] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [uispet] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [uispet] SET ARITHABORT OFF 
GO

ALTER DATABASE [uispet] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [uispet] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [uispet] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [uispet] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [uispet] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [uispet] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [uispet] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [uispet] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [uispet] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [uispet] SET  DISABLE_BROKER 
GO

ALTER DATABASE [uispet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [uispet] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [uispet] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [uispet] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [uispet] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [uispet] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [uispet] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [uispet] SET RECOVERY BULK_LOGGED 
GO

ALTER DATABASE [uispet] SET  MULTI_USER 
GO

ALTER DATABASE [uispet] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [uispet] SET DB_CHAINING OFF 
GO

ALTER DATABASE [uispet] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [uispet] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [uispet] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [uispet] SET QUERY_STORE = ON
GO

ALTER DATABASE [uispet] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = ALL, SIZE_BASED_CLEANUP_MODE = AUTO)
GO

ALTER DATABASE [uispet] SET  READ_WRITE 
GO

USE uispet
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating certificates'
GO
-- Certificate [PaymentDetailsCertificate]
--   Authorization: dbo
--   Subject: Account Fields
--   Thumbprint: 0xd73e97fbce6e3ad1bc697721c64596edaa26cdd6
--   Start date: 2011-04-17 21:58:36Z
--   Expiry date: 2012-04-17 21:58:36Z
--   Issuer: Account Fields
--   Private key encryption: Encrypted by master key
--   Active for dialog: ON
-- This certificate cannot be synchronized.
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating symmetric keys'
GO
-- Symmetric key [PaymentDetailsSymmetricKey]
--   Authorization: dbo
--   Key algorithm: AES 256
--   Key length: 256
--   Key GUID: 2d084400-699f-4410-813a-c27a6c479497
--   Creation date: 2011-04-17 21:58:36Z
--   Modification date: 2011-04-17 21:58:36Z
-- This symmetric key cannot be synchronized.
IF @@ERROR <> 0 SET NOEXEC ON
GO
BEGIN TRANSACTION
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING')
CREATE LOGIN [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING] FOR LOGIN [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\ADMIN_MIS_UAT_Servers')
CREATE LOGIN [MARKERSTUDY\ADMIN_MIS_UAT_Servers] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\ADMIN_MIS_UAT_Servers] FOR LOGIN [MARKERSTUDY\ADMIN_MIS_UAT_Servers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\SVC_Mirobot')
CREATE LOGIN [MARKERSTUDY\SVC_Mirobot] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\SVC_Mirobot] FOR LOGIN [MARKERSTUDY\SVC_Mirobot]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\USR_GRP_Retail_MI')
CREATE LOGIN [MARKERSTUDY\USR_GRP_Retail_MI] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\USR_GRP_Retail_MI] FOR LOGIN [MARKERSTUDY\USR_GRP_Retail_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\USR_GRP_Ultimate_MI')
CREATE LOGIN [MARKERSTUDY\USR_GRP_Ultimate_MI] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\USR_GRP_Ultimate_MI] FOR LOGIN [MARKERSTUDY\USR_GRP_Ultimate_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''UltimateJobOwner''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [UltimateJobOwner] and mapping to the login [UltimateJobOwner]'
    CREATE USER [UltimateJobOwner] FOR LOGIN [UltimateJobOwner]
END
ELSE
BEGIN
    PRINT N'Creating user [UltimateJobOwner] without login'
    CREATE USER [UltimateJobOwner] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [UltimateMIJobOwner] WITHOUT LOGIN
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''a-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [a-user] and mapping to the login [a-user]'
    CREATE USER [a-user] FOR LOGIN [a-user]
END
ELSE
BEGIN
    PRINT N'Creating user [a-user] without login'
    CREATE USER [a-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''cvs-iframe''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [cvs-iframe] and mapping to the login [cvs-iframe]'
    CREATE USER [cvs-iframe] FOR LOGIN [cvs-iframe]
END
ELSE
BEGIN
    PRINT N'Creating user [cvs-iframe] without login'
    CREATE USER [cvs-iframe] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''h-framework-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [h-framework-user] and mapping to the login [h-framework-user]'
    CREATE USER [h-framework-user] FOR LOGIN [h-framework-user]
END
ELSE
BEGIN
    PRINT N'Creating user [h-framework-user] without login'
    CREATE USER [h-framework-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''h-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [h-user] and mapping to the login [h-user]'
    CREATE USER [h-user] FOR LOGIN [h-user]
END
ELSE
BEGIN
    PRINT N'Creating user [h-user] without login'
    CREATE USER [h-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''m-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [m-user] and mapping to the login [m-user]'
    CREATE USER [m-user] FOR LOGIN [m-user]
END
ELSE
BEGIN
    PRINT N'Creating user [m-user] without login'
    CREATE USER [m-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''p-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [p-user] and mapping to the login [p-user]'
    CREATE USER [p-user] FOR LOGIN [p-user]
END
ELSE
BEGIN
    PRINT N'Creating user [p-user] without login'
    CREATE USER [p-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''q-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [q-user] and mapping to the login [q-user]'
    CREATE USER [q-user] FOR LOGIN [q-user]
END
ELSE
BEGIN
    PRINT N'Creating user [q-user] without login'
    CREATE USER [q-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''quoteandbuy-asda''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [quoteandbuy-asda] and mapping to the login [quoteandbuy-asda]'
    CREATE USER [quoteandbuy-asda] FOR LOGIN [quoteandbuy-asda]
END
ELSE
BEGIN
    PRINT N'Creating user [quoteandbuy-asda] without login'
    CREATE USER [quoteandbuy-asda] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''s-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [s-user] and mapping to the login [s-user]'
    CREATE USER [s-user] FOR LOGIN [s-user]
END
ELSE
BEGIN
    PRINT N'Creating user [s-user] without login'
    CREATE USER [s-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''ssrs-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [ssrs-user] and mapping to the login [ssrs-user]'
    CREATE USER [ssrs-user] FOR LOGIN [ssrs-user]
END
ELSE
BEGIN
    PRINT N'Creating user [ssrs-user] without login'
    CREATE USER [ssrs-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [test-data-export-user] WITHOUT LOGIN
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''ult-admin''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [ult-admin] and mapping to the login [ult-admin]'
    CREATE USER [ult-admin] FOR LOGIN [ult-admin]
END
ELSE
BEGIN
    PRINT N'Creating user [ult-admin] without login'
    CREATE USER [ult-admin] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [upp-user] WITHOUT LOGIN
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''x-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [x-user] and mapping to the login [x-user]'
    CREATE USER [x-user] FOR LOGIN [x-user]
END
ELSE
BEGIN
    PRINT N'Creating user [x-user] without login'
    CREATE USER [x-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating role quoteandbuy'
GO
CREATE ROLE [quoteandbuy]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role quoteandbuy'
GO
ALTER ROLE [quoteandbuy] ADD MEMBER [quoteandbuy-asda]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datareader'
GO
ALTER ROLE [db_datareader] ADD MEMBER [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [MARKERSTUDY\SVC_Mirobot]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [MARKERSTUDY\USR_GRP_Retail_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [test-data-export-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datawriter'
GO
ALTER ROLE [db_datawriter] ADD MEMBER [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_owner'
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\ADMIN_MIS_UAT_Servers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\USR_GRP_Ultimate_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [UltimateJobOwner]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [UltimateMIJobOwner]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [upp-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [Aggregator]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Audit]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Core]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Correspondence]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Debenhams]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Pricing]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Quotes]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [RedGate]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [TestDataExport]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Voucher]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [products]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [q-user]
AUTHORIZATION [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [ult-admin]
AUTHORIZATION [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering schemas'
GO
ALTER AUTHORIZATION ON SCHEMA::[db_datareader] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead]'
GO
CREATE TABLE [dbo].[lead]
(
[id] [int] NOT NULL IDENTITY(65892, 1),
[product_id] [int] NOT NULL,
[title_id] [int] NULL,
[forename] [nvarchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [nvarchar] (32) COLLATE Latin1_General_CI_AS NULL,
[house] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[telephone] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email_address] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL,
[lead_status_id] [int] NULL CONSTRAINT [DF_lead_lead_status_id] DEFAULT ((100)),
[contact_date] [smalldatetime] NULL,
[contact_time] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[clinic_id] [int] NULL,
[clinic_vet_name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ip_address] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[lead_reason_id] [int] NULL,
[create_timestamp] [datetime] NOT NULL CONSTRAINT [DF__lead__create_tim__6A1BB7B0] DEFAULT (getdate()),
[update_timestamp] [datetime] NOT NULL CONSTRAINT [DF__lead__update_tim__6B0FDBE9] DEFAULT (getdate()),
[contact_attempt_count] [tinyint] NULL CONSTRAINT [DF_lead_contact_attempts] DEFAULT ((0)),
[lead_failure_reason_id] [int] NULL,
[quote_id] [int] NULL,
[email_sent] [tinyint] NULL,
[broker_referrer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[source] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[mobile_telephone] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[work_telephone] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[lead_time_id] [tinyint] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_lead] on [dbo].[lead]'
GO
ALTER TABLE [dbo].[lead] ADD CONSTRAINT [PK_lead] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_lead_forename_surname_7FFCF] on [dbo].[lead]'
GO
CREATE NONCLUSTERED INDEX [IDX_lead_forename_surname_7FFCF] ON [dbo].[lead] ([forename], [surname])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_lead_status_id] on [dbo].[lead]'
GO
CREATE NONCLUSTERED INDEX [idx_lead_status_id] ON [dbo].[lead] ([lead_status_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_lead_product_id_1D072] on [dbo].[lead]'
GO
CREATE NONCLUSTERED INDEX [IDX_lead_product_id_1D072] ON [dbo].[lead] ([product_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_id] on [dbo].[lead]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_id] ON [dbo].[lead] ([quote_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[lead]'
GO
CREATE TABLE [Audit].[lead]
(
[Id] [int] NOT NULL,
[LastUpdateDateTimeStamp] [datetime] NULL,
[DeletedDateTimeStamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[trgDel_lead] on [dbo].[lead]'
GO


CREATE TRIGGER [dbo].[trgDel_lead]
ON [dbo].[lead]
FOR DELETE
AS
BEGIN
SET NOCOUNT ON
MERGE [Audit].[lead] AS Tgt
USING deleted AS src
ON (Tgt.Id = Src.Id) 
WHEN NOT MATCHED BY TARGET 
    THEN INSERT(Id, [DeletedDateTimeStamp])
	VALUES (src.Id, GETDATE())
WHEN MATCHED 
    THEN UPDATE SET DeletedDateTimeStamp = GETDATE();
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_pet]'
GO
CREATE TABLE [dbo].[quote_pet]
(
[id] [int] NOT NULL IDENTITY(17822, 1),
[quote_id] [int] NOT NULL,
[pet_name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[animal_type_id] [int] NULL,
[product_gender_id] [int] NULL,
[breed_id] [int] NULL,
[purchase_price] [money] NULL,
[chip_number] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[colour] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[pet_DOB] [date] NULL,
[premium] [money] NULL,
[create_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__quote_pet__creat__320C68B7] DEFAULT (getdate()),
[update_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__quote_pet__updat__33008CF0] DEFAULT (getdate()),
[vet_visit] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[cover_id] [int] NULL,
[incidcent] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[incident] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[microchip] [bit] NULL CONSTRAINT [DF__quote_pet__micro__6E2152BE] DEFAULT ((0)),
[microchipnumber] [nvarchar] (32) COLLATE Latin1_General_CI_AS NULL,
[vetvisit] [bit] NULL,
[vetvisitdetails] [text] COLLATE Latin1_General_CI_AS NULL,
[accident] [bit] NULL,
[accidentdetails] [text] COLLATE Latin1_General_CI_AS NULL,
[exclusion] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[promo_discount] [decimal] (5, 2) NULL,
[multipet_discount] [decimal] (5, 2) NULL,
[offer_discount] [decimal] (5, 2) NULL,
[exclusion_review] [bit] NULL CONSTRAINT [DF_quote_pet_exclusion_review] DEFAULT ((0)),
[not_working] [bit] NULL,
[sick] [bit] NULL,
[kfconfirm] [bit] NULL,
[sickdetails] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[owned] [bit] NULL,
[kept_at_home] [bit] NULL,
[not_breeding] [bit] NULL,
[not_racing] [bit] NULL,
[not_hunting] [bit] NULL,
[neutered] [bit] NULL,
[agg_cover_id] [int] NULL,
[IPT_absolute] [money] NULL,
[commission_affinity_absolute] [money] NULL,
[commission_administrator_absolute] [money] NULL,
[commission_underwriter_absolute] [money] NULL,
[credit_charge_absolute] [money] NULL,
[helpline_charge_absolute] [money] NULL,
[tpl_absolute] [money] NULL,
[pricing_engine_id] [bigint] NOT NULL CONSTRAINT [DF_quote_pet_pricing_engine_id] DEFAULT ((1)),
[vol_excess] [money] NULL,
[promo_discount_absolute] [money] NULL,
[multipet_discount_absolute] [money] NULL,
[offer_discount_absolute] [money] NULL,
[annual_premium] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-id] on [dbo].[quote_pet]'
GO
CREATE CLUSTERED INDEX [idx-id] ON [dbo].[quote_pet] ([id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_quote_pet] on [dbo].[quote_pet]'
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [PK_quote_pet] PRIMARY KEY NONCLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_pet_cover_id_A921A] on [dbo].[quote_pet]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_pet_cover_id_A921A] ON [dbo].[quote_pet] ([cover_id]) INCLUDE ([quote_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_pet_exclusion_6485B] on [dbo].[quote_pet]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_pet_exclusion_6485B] ON [dbo].[quote_pet] ([exclusion]) INCLUDE ([quote_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_id] on [dbo].[quote_pet]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_id] ON [dbo].[quote_pet] ([quote_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PROPERCASE]'
GO

CREATE function [dbo].[PROPERCASE]
    (
    @inputString varchar(max)
    ) RETURNS varchar(max) As BEGIN


    -- Declarations -------------------------------------------------------------------------------
    declare @oldlen int
    DECLARE @Result varchar(2000)
    set @oldlen = -1
    SET @Result = ''

    -- Clean up input data ------------------------------------------------------------------------
    SET @inputString = ltrim(rtrim(COALESCE(@inputString, '')))

    while @oldlen <> len(@inputstring) begin
        set @oldlen = len(@inputstring)
        set @inputstring = replace(@inputstring, '  ', ' ')
        end

    -- Exceptions: Mc and Mac only forces 1st letter to be uppercase ------------------------------
    if not(@inputString like 'Mc%' or @inputString like 'Mac%')
		SET @inputString = lower(@inputString)

    -- Loop through the String --------------------------------------------------------------------
    SET @inputString = @inputString + ' '

    WHILE 1=1 BEGIN

        -- If no more spaces, exit loop
        IF PATINDEX('% %',@inputString) = 0 BREAK

        -- Special handing for two-letter words
        BEGIN

            SET @Result = @Result
                + UPPER(LEFT(@inputString, 1))
                + SUBSTRING(@inputString, 2, CHARINDEX(' ', @inputString) - 1)

            SET @inputString = SUBSTRING(@inputString, CHARINDEX(' ', @inputString) + 1, LEN(@inputString))

            END

    END

    SET @Result = substring(@Result, 1, LEN(@Result))

    -- Special Handling for Prefixes --------------------------------------------------------------
    set @Result = case 
                    when @Result like 'van %' 
                        then lower(substring(@Result,1,4)) + 
                            upper(substring(@Result,5,1)) + 
                            substring(@Result,6,len(@Result))
					/*
                    when @Result like 'Mac%' and len(@result)>6
                        then substring(@Result,1,3) + 
                            upper(substring(@Result,4,1)) + 
                            substring(@Result,5,len(@Result))

                    when @Result like 'Mc%' 
                        then substring(@Result,1,2) + 
                            upper(substring(@Result,3,1)) + 
                            substring(@Result,4,len(@Result))
					*/
                    when @Result like 'Mc%' or @Result like 'Mac%'
                        then upper(substring(@Result,1,1)) + 
                            substring(@Result,2,len(@Result))
					
                    when @Result like 'O''%' 
                        then substring(@Result,1,2) + 
                            upper(substring(@Result,3,1)) + 
                            substring(@Result,4,len(@Result))

                    else @Result
                    end

    -- Special handing for hyphenated names -------------------------------------------------------
    set @Result = case
                    when @Result like '%-%'
                        then substring(@Result,1,charindex('-',@Result)) +
                            upper(substring(@Result,charindex('-',@Result)+1,1)) +
                            substring(@Result,charindex('-',@Result)+2,len(@Result)) 
                    else @Result
                    end

--------------------------------------------
--select Result = @Result

    RETURN @Result

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[quote_pet_propercase_trg] on [dbo].[quote_pet]'
GO

CREATE TRIGGER [dbo].[quote_pet_propercase_trg]
   ON  [dbo].[quote_pet]
   AFTER INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    UPDATE quote_pet
	SET colour = dbo.PROPERCASE(i.colour),
		pet_name = dbo.PROPERCASE(i.pet_name)
	FROM quote_pet qp JOIN inserted i ON qp.id=i.id;
    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[lead_propercase_trg] on [dbo].[lead]'
GO

CREATE TRIGGER [dbo].[lead_propercase_trg]
   ON  [dbo].[lead]
   AFTER INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    UPDATE lead
	SET clinic_vet_name = dbo.PROPERCASE(i.clinic_vet_name),
		email_address = LOWER(i.email_address),
		forename = dbo.PROPERCASE(i.forename),
		house = dbo.PROPERCASE(i.house),
		postcode = UPPER(i.postcode),
		surname = dbo.PROPERCASE(i.surname)
	FROM lead l JOIN inserted i ON l.id=i.id;
    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_pet]'
GO
CREATE TABLE [dbo].[lead_pet]
(
[id] [int] NOT NULL IDENTITY(13137, 1),
[lead_id] [int] NOT NULL,
[pet_name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[animal_type_id] [int] NULL,
[product_gender_id] [int] NULL,
[breed_id] [int] NULL,
[purchase_price] [money] NULL,
[colour] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[pet_DOB] [smalldatetime] NULL,
[create_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__lead_pet__create__6EE06CCD] DEFAULT (getdate()),
[update_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__lead_pet__update__6FD49106] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [pk_lead_pet] on [dbo].[lead_pet]'
GO
ALTER TABLE [dbo].[lead_pet] ADD CONSTRAINT [pk_lead_pet] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_lead_id] on [dbo].[lead_pet]'
GO
CREATE NONCLUSTERED INDEX [idx_lead_id] ON [dbo].[lead_pet] ([lead_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[lead_pet_propercase_trg] on [dbo].[lead_pet]'
GO

CREATE TRIGGER [dbo].[lead_pet_propercase_trg]
   ON  [dbo].[lead_pet]
   AFTER INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    UPDATE lead_pet
	SET colour = dbo.PROPERCASE(i.colour),
		pet_name = dbo.PROPERCASE(i.pet_name)		
	FROM lead_pet lp JOIN inserted i ON lp.id=i.id;
    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote]'
GO
CREATE TABLE [dbo].[quote]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[quote_ref] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[title_id] [int] NULL,
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[DOB] [smalldatetime] NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[house_name_num] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[street] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[town] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[county] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[contact_num] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[quote_status_id] [int] NULL,
[cover_id] [int] NULL,
[premium] [money] NULL,
[create_timestamp] [smalldatetime] NOT NULL CONSTRAINT [timestamp1] DEFAULT (getdate()),
[update_timestamp] [smalldatetime] NOT NULL CONSTRAINT [timestamp2] DEFAULT (getdate()),
[product_excess_id] [int] NULL,
[occupation_id] [int] NULL,
[hear_about_us_id] [int] NULL,
[other_policies] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[motor_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[home_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[street2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[policy_start_date] [smalldatetime] NULL,
[agree_contact] [bit] NULL,
[agree_terms] [bit] NULL,
[payment_detail_id] [uniqueidentifier] NULL,
[VFV_clinic] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[VFV_ClinicID] [int] NULL,
[ReferrerID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientIpAddress] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[schedule_doc] [image] NULL,
[promo_code] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[email_sent] [tinyint] NULL CONSTRAINT [DF__quote__email_sen__57FD0775] DEFAULT ((0)),
[created_systuser_id] [int] NULL,
[modified_systuser_id] [int] NULL,
[converted_systuser_id] [int] NULL,
[min_cover_ordinal] [int] NULL,
[opt_out_marketing] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[agg_remote_ref] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[agree_preexisting_excl] [bit] NULL,
[agree_waiting_excl] [bit] NULL,
[agree_illvac_excl] [bit] NULL,
[agree_vicious_excl] [bit] NULL,
[enquiry_method_code] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[all_pet_names] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[confirm_excluded_breeds] [bit] NOT NULL CONSTRAINT [DF__quote__confirm_e__2ED0D4B0] DEFAULT ((0)),
[annual_premium] [money] NULL,
[agree_contact_phone] [bit] NULL,
[agree_contact_SMS] [bit] NULL,
[agree_contact_email] [bit] NULL,
[agree_contact_letter] [bit] NULL,
[campaign_code] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[EmailAddressConfirmation] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[is_multi_pet] [bit] NOT NULL CONSTRAINT [DF__quote__is_multi___1CE72E9F] DEFAULT ((0)),
[agree_contact_mmpportal] [bit] NULL,
[sent_by_post] [bit] NULL,
[agreeCoInsurance] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__quote__76969D2E] on [dbo].[quote]'
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [PK__quote__76969D2E] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_ClientIpAddress_F69BD] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_ClientIpAddress_F69BD] ON [dbo].[quote] ([ClientIpAddress]) INCLUDE ([id], [quote_ref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_ClientIpAddress_B5A0C] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_ClientIpAddress_B5A0C] ON [dbo].[quote] ([ClientIpAddress]) INCLUDE ([id], [quote_ref], [quote_status_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_create_timestamp] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [idx_create_timestamp] ON [dbo].[quote] ([create_timestamp]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_created_systuser_id_quote_status_id_create_timestamp_A8719] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_created_systuser_id_quote_status_id_create_timestamp_A8719] ON [dbo].[quote] ([created_systuser_id], [quote_status_id], [create_timestamp]) INCLUDE ([ClientIpAddress], [quote_ref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_forename_surname_F429C] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_forename_surname_F429C] ON [dbo].[quote] ([forename], [surname]) INCLUDE ([update_timestamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_id] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_id] ON [dbo].[quote] ([id]) INCLUDE ([promo_code]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_product_id_forename_surname_ReferrerID_B615C] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_product_id_forename_surname_ReferrerID_B615C] ON [dbo].[quote] ([product_id], [forename], [surname], [ReferrerID]) INCLUDE ([create_timestamp], [postcode], [quote_ref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_Quote_QuoteRef] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_Quote_QuoteRef] ON [dbo].[quote] ([quote_ref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_quote_status_id_email_sent_CEE3E] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_quote_status_id_email_sent_CEE3E] ON [dbo].[quote] ([quote_status_id], [email_sent]) INCLUDE ([agree_contact], [create_timestamp], [email], [id], [policy_start_date], [quote_ref], [surname], [update_timestamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_quote_status_id_quote_ref_email_create_timestamp_339A7] on [dbo].[quote]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_quote_status_id_quote_ref_email_create_timestamp_339A7] ON [dbo].[quote] ([quote_status_id], [quote_ref], [email], [create_timestamp]) INCLUDE ([agree_contact], [agree_contact_email], [agree_contact_letter], [agree_contact_phone], [agree_contact_SMS], [all_pet_names], [ClientIpAddress], [converted_systuser_id], [created_systuser_id], [id], [modified_systuser_id], [postcode], [premium], [ReferrerID], [surname])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[quote_propercase_trg] on [dbo].[quote]'
GO

CREATE TRIGGER [dbo].[quote_propercase_trg]
   ON  [dbo].[quote]
   AFTER INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    UPDATE quote
	SET county = dbo.PROPERCASE(i.county),
		email = LOWER(i.email),
		forename = dbo.PROPERCASE(i.forename),
		house_name_num = dbo.PROPERCASE(i.house_name_num),
		postcode = UPPER(i.postcode),
		street = dbo.PROPERCASE(i.street),
		street2 = dbo.PROPERCASE(i.street2),
		surname = dbo.PROPERCASE(i.surname),
		town = dbo.PROPERCASE(i.town)
	FROM quote q JOIN inserted i ON q.id=i.id;
    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[trgInsUpd_lead] on [dbo].[lead]'
GO



CREATE TRIGGER [dbo].[trgInsUpd_lead]
ON [dbo].[lead]
FOR INSERT, UPDATE
AS
BEGIN
SET NOCOUNT ON
MERGE [Audit].[lead] AS Tgt
USING inserted AS src
ON (Tgt.Id = Src.Id) 
WHEN NOT MATCHED BY TARGET 
    THEN INSERT(Id, [LastUpdateDateTimeStamp])
	VALUES (src.Id, GETDATE())
WHEN MATCHED 
    THEN UPDATE SET LastUpdateDateTimeStamp = GETDATE();
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover]'
GO
CREATE TABLE [dbo].[cover]
(
[id] [int] NOT NULL,
[product_id] [int] NOT NULL,
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[ordinal] [int] NOT NULL,
[active] [int] NULL,
[offer_discount] [decimal] (5, 2) NULL CONSTRAINT [DF_cover_offer_discount] DEFAULT ((0)),
[multipet_discount] [decimal] (5, 2) NULL CONSTRAINT [DF_cover_multipet_discount] DEFAULT ((0)),
[affinity_code] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[underwriter] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[renewable] [bit] NULL,
[cover_effective_date] [datetime] NULL,
[cover_inactive_date] [datetime] NULL,
[animal_type_id] [int] NULL,
[policy_period] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__cover__policy_pe__1FAE8CB1] DEFAULT ('year'),
[policy_length] [int] NOT NULL CONSTRAINT [DF__cover__policy_le__20A2B0EA] DEFAULT ((1)),
[email_ordinal] [int] NULL,
[cover_type] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[IsCoinsurance] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__cover__12FDD1B2] on [dbo].[cover]'
GO
ALTER TABLE [dbo].[cover] ADD CONSTRAINT [PK__cover__12FDD1B2] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-id] on [dbo].[cover]'
GO
CREATE NONCLUSTERED INDEX [idx-id] ON [dbo].[cover] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-underwriter] on [dbo].[cover]'
GO
CREATE NONCLUSTERED INDEX [idx-underwriter] ON [dbo].[cover] ([underwriter])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[Breakdown-8-8-18]'
GO
CREATE TABLE [Pricing].[Breakdown-8-8-18]
(
[Id] [uniqueidentifier] NOT NULL,
[SourceId] [int] NOT NULL,
[Identifier] [int] NULL,
[CoverId] [int] NOT NULL,
[Premium] [decimal] (10, 2) NOT NULL,
[PricingEngineId] [bigint] NOT NULL,
[SequenceId] [varchar] (36) COLLATE Latin1_General_CI_AS NOT NULL,
[Breakdown] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Created] [datetime] NOT NULL,
[Uploaded] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [[PK_Pricing.Breakdown-8-8-18]] on [Pricing].[Breakdown-8-8-18]'
GO
ALTER TABLE [Pricing].[Breakdown-8-8-18] ADD CONSTRAINT [[PK_Pricing.Breakdown-8-8-18]]] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_Breakdown_SourceId_Identifier] on [Pricing].[Breakdown-8-8-18]'
GO
CREATE NONCLUSTERED INDEX [IDX_Breakdown_SourceId_Identifier] ON [Pricing].[Breakdown-8-8-18] ([SourceId], [Identifier]) INCLUDE ([Created], [Id], [Premium])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_Breakdown_Uploaded] on [Pricing].[Breakdown-8-8-18]'
GO
CREATE NONCLUSTERED INDEX [IDX_Breakdown_Uploaded] ON [Pricing].[Breakdown-8-8-18] ([Uploaded])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_engine]'
GO
CREATE TABLE [dbo].[pricing_engine]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Description] [nchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_engine] on [dbo].[pricing_engine]'
GO
ALTER TABLE [dbo].[pricing_engine] ADD CONSTRAINT [PK_pricing_engine] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[Source]'
GO
CREATE TABLE [Pricing].[Source]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Pricing.Source] on [Pricing].[Source]'
GO
ALTER TABLE [Pricing].[Source] ADD CONSTRAINT [PK_Pricing.Source] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[Voucher]'
GO
CREATE TABLE [Voucher].[Voucher]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[Code] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Created] [datetime2] NOT NULL CONSTRAINT [DF__Voucher__Created__03275C9C] DEFAULT (getdate()),
[Amount] [decimal] (6, 2) NOT NULL,
[ValidFrom] [datetime2] NOT NULL,
[ExpireAfter] [datetime2] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__Voucher__3214EC07961AE48D] on [Voucher].[Voucher]'
GO
ALTER TABLE [Voucher].[Voucher] ADD CONSTRAINT [PK__Voucher__3214EC07961AE48D] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [Voucher].[Voucher]'
GO
ALTER TABLE [Voucher].[Voucher] ADD CONSTRAINT [UC_CampaignIdCode] UNIQUE NONCLUSTERED  ([CampaignId], [Code])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[Allocation]'
GO
CREATE TABLE [Voucher].[Allocation]
(
[Guid] [uniqueidentifier] NOT NULL,
[VoucherId] [int] NOT NULL,
[Created] [datetime2] NOT NULL CONSTRAINT [DF__Allocatio__Creat__0AC87E64] DEFAULT (getdate()),
[EntityType] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[EntityIdentifier] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [Voucher].[Allocation]'
GO
ALTER TABLE [Voucher].[Allocation] ADD CONSTRAINT [UC_Guid] UNIQUE NONCLUSTERED  ([Guid])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [Voucher].[Allocation]'
GO
ALTER TABLE [Voucher].[Allocation] ADD CONSTRAINT [UC_VoucherIdEntityType] UNIQUE NONCLUSTERED  ([VoucherId], [EntityType])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[Property]'
GO
CREATE TABLE [Voucher].[Property]
(
[VoucherId] [int] NOT NULL,
[Key] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [Voucher].[Property]'
GO
ALTER TABLE [Voucher].[Property] ADD CONSTRAINT [UC_VoucherIdKey] UNIQUE NONCLUSTERED  ([VoucherId], [Key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QuoteEmailQueue]'
GO
CREATE TABLE [dbo].[QuoteEmailQueue]
(
[QuoteId] [int] NOT NULL,
[Created] [datetime] NOT NULL CONSTRAINT [DF__QuoteEmai__Creat__7F21C18E] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__QuoteEma__AF9688C1F38F121B] on [dbo].[QuoteEmailQueue]'
GO
ALTER TABLE [dbo].[QuoteEmailQueue] ADD CONSTRAINT [PK__QuoteEma__AF9688C1F38F121B] PRIMARY KEY CLUSTERED  ([QuoteId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[Campaign]'
GO
CREATE TABLE [Voucher].[Campaign]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Created] [datetime2] NOT NULL CONSTRAINT [DF__Campaign__Create__7F56CBB8] DEFAULT (getdate()),
[StartOn] [datetime2] NOT NULL,
[EndAfter] [datetime2] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__Campaign__3214EC07B44F7B3D] on [Voucher].[Campaign]'
GO
ALTER TABLE [Voucher].[Campaign] ADD CONSTRAINT [PK__Campaign__3214EC07B44F7B3D] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[UnallocatedVouchers]'
GO
CREATE TABLE [Voucher].[UnallocatedVouchers]
(
[Id] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[Code] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Created] [datetime2] NOT NULL CONSTRAINT [DF__Unallocat__Creat__42D7CD5D] DEFAULT (getdate()),
[Amount] [decimal] (6, 2) NOT NULL,
[ValidFrom] [datetime2] NOT NULL,
[ExpireAfter] [datetime2] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__Unalloca__3214EC071E0CF239] on [Voucher].[UnallocatedVouchers]'
GO
ALTER TABLE [Voucher].[UnallocatedVouchers] ADD CONSTRAINT [PK__Unalloca__3214EC071E0CF239] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [Voucher].[UnallocatedVouchers]'
GO
ALTER TABLE [Voucher].[UnallocatedVouchers] ADD CONSTRAINT [UCCampaignIdCode] UNIQUE NONCLUSTERED  ([CampaignId], [Code])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Debenhams].[VoucherConfig]'
GO
CREATE TABLE [Debenhams].[VoucherConfig]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[ReferrerId] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value] [decimal] (5, 2) NOT NULL,
[DaysAfterInception] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__VoucherC__3214EC0703C0BB6B] on [Debenhams].[VoucherConfig]'
GO
ALTER TABLE [Debenhams].[VoucherConfig] ADD CONSTRAINT [PK__VoucherC__3214EC0703C0BB6B] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator]'
GO
CREATE TABLE [dbo].[aggregator]
(
[id] [int] NOT NULL IDENTITY(2314, 1),
[source] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[title] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[quote_ref] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[contact_num_day] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[contact_num_eve] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[status_id] [int] NULL,
[create_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__aggregato__creat__0F4D3C5F] DEFAULT (getdate()),
[update_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__aggregato__updat__10416098] DEFAULT (getdate()),
[policy_start_date] [date] NULL,
[client_ip_address] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[promo_code] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[quote_id] [int] NULL,
[lead_id] [int] NULL,
[date_of_birth] [date] NULL,
[house] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[address1] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address2] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address3] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address4] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[remote_reference] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[suffix_id] [int] NULL,
[opt_out_marketing] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[all_pet_names] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator] on [dbo].[aggregator]'
GO
ALTER TABLE [dbo].[aggregator] ADD CONSTRAINT [PK_aggregator] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_aggregator_affinity_create_timestamp_AE43C] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [IDX_aggregator_affinity_create_timestamp_AE43C] ON [dbo].[aggregator] ([affinity], [create_timestamp]) INCLUDE ([address4], [forename], [id], [policy_start_date], [source], [status_id], [surname])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_aggregator_affinity_forename_surname_77D65] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [IDX_aggregator_affinity_forename_surname_77D65] ON [dbo].[aggregator] ([affinity], [forename], [surname]) INCLUDE ([create_timestamp], [postcode], [quote_ref], [source])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_aggregator_forename_surname_6B5C5] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [IDX_aggregator_forename_surname_6B5C5] ON [dbo].[aggregator] ([forename], [surname]) INCLUDE ([affinity], [create_timestamp], [postcode], [quote_ref], [source])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_aggregator_forename_surname_create_timestamp_address4_AEB07] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [IDX_aggregator_forename_surname_create_timestamp_address4_AEB07] ON [dbo].[aggregator] ([forename], [surname], [create_timestamp], [address4]) INCLUDE ([affinity], [id], [policy_start_date], [source], [status_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_lead_id] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx_lead_id] ON [dbo].[aggregator] ([lead_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_id] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_id] ON [dbo].[aggregator] ([quote_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_ref] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_ref] ON [dbo].[aggregator] ([quote_ref]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-timestamp] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx-timestamp] ON [dbo].[aggregator] ([quote_ref], [create_timestamp]) INCLUDE ([id], [postcode], [surname])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_source] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx_source] ON [dbo].[aggregator] ([source]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_suffix_id] on [dbo].[aggregator]'
GO
CREATE NONCLUSTERED INDEX [idx_suffix_id] ON [dbo].[aggregator] ([suffix_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_pet]'
GO
CREATE TABLE [dbo].[aggregator_pet]
(
[id] [int] NOT NULL IDENTITY(17822, 1),
[aggregator_id] [int] NOT NULL,
[pet_name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[animal_type] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[sex] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[breed_id] [int] NULL,
[purchase_price] [money] NULL,
[colour] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[pet_DOB] [date] NULL,
[create_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__aggregato__creat__1F83A428] DEFAULT (getdate()),
[update_timestamp] [smalldatetime] NOT NULL CONSTRAINT [DF__aggregato__updat__2077C861] DEFAULT (getdate()),
[microchip] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[original_breed_id] [int] NULL,
[vetvisit] [bit] NULL,
[not_working] [bit] NULL,
[sick] [bit] NULL,
[owned] [bit] NULL,
[kept_at_home] [bit] NULL,
[not_breeding] [bit] NULL,
[not_racing] [bit] NULL,
[not_hunting] [bit] NULL,
[neutered] [bit] NULL,
[pricing_engine_id] [bigint] NULL CONSTRAINT [DF__aggregato__prici__2E66C05C] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator_pet] on [dbo].[aggregator_pet]'
GO
ALTER TABLE [dbo].[aggregator_pet] ADD CONSTRAINT [PK_aggregator_pet] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_aggregator_id] on [dbo].[aggregator_pet]'
GO
CREATE NONCLUSTERED INDEX [idx_aggregator_id] ON [dbo].[aggregator_pet] ([aggregator_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_aggregator_pet_pet_name_create_timestamp_CEFF2] on [dbo].[aggregator_pet]'
GO
CREATE NONCLUSTERED INDEX [IDX_aggregator_pet_pet_name_create_timestamp_CEFF2] ON [dbo].[aggregator_pet] ([pet_name], [create_timestamp]) INCLUDE ([aggregator_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_archive]'
GO
CREATE TABLE [dbo].[aggregator_archive]
(
[id] [int] NOT NULL IDENTITY(2314, 1),
[source] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[title] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[quote_ref] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[contact_num_day] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[contact_num_eve] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[status_id] [int] NULL,
[create_timestamp] [smalldatetime] NOT NULL,
[update_timestamp] [smalldatetime] NOT NULL,
[policy_start_date] [date] NULL,
[client_ip_address] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[promo_code] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[quote_id] [int] NULL,
[lead_id] [int] NULL,
[date_of_birth] [date] NULL,
[house] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[address1] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address2] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address3] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address4] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[remote_reference] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[suffix_id] [int] NULL,
[opt_out_marketing] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[all_pet_names] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator_archive] on [dbo].[aggregator_archive]'
GO
ALTER TABLE [dbo].[aggregator_archive] ADD CONSTRAINT [PK_aggregator_archive] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-create-timestamp] on [dbo].[aggregator_archive]'
GO
CREATE NONCLUSTERED INDEX [idx-create-timestamp] ON [dbo].[aggregator_archive] ([create_timestamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_timestamp2] on [dbo].[aggregator_archive]'
GO
CREATE NONCLUSTERED INDEX [idx_timestamp2] ON [dbo].[aggregator_archive] ([create_timestamp]) INCLUDE ([lead_id], [quote_id], [quote_ref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-lead-id] on [dbo].[aggregator_archive]'
GO
CREATE NONCLUSTERED INDEX [idx-lead-id] ON [dbo].[aggregator_archive] ([lead_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-quote-id] on [dbo].[aggregator_archive]'
GO
CREATE NONCLUSTERED INDEX [idx-quote-id] ON [dbo].[aggregator_archive] ([quote_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-surname] on [dbo].[aggregator_archive]'
GO
CREATE NONCLUSTERED INDEX [idx-surname] ON [dbo].[aggregator_archive] ([surname]) INCLUDE ([create_timestamp], [id], [postcode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[animal_type]'
GO
CREATE TABLE [dbo].[animal_type]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__animal_type__023D5A04] on [dbo].[animal_type]'
GO
ALTER TABLE [dbo].[animal_type] ADD CONSTRAINT [PK__animal_type__023D5A04] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product]'
GO
CREATE TABLE [dbo].[product]
(
[id] [int] NOT NULL,
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address1] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address3] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[town] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[county] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[tel] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[web_address] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[admin_email] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[quote_expiration_days] [int] NULL,
[policy_document] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[claim_form] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[DD_orig_ref] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[show_marketing_panel] [bit] NULL,
[cust_services_num] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[multi_pet] [bit] NULL,
[affinity_code] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[prod_prefix] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[welcome_text] [text] COLLATE Latin1_General_CI_AS NULL,
[footer_text] [text] COLLATE Latin1_General_CI_AS NULL,
[commission] [float] NULL,
[commission_20110801] [float] NULL,
[renewals_num] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[mmp_web_address] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[filter_covers_by_cg_min_ordinal] [bit] NOT NULL CONSTRAINT [DF__product__filter___2DDCB077] DEFAULT ((0)),
[surcharge_threshold] [decimal] (5, 2) NULL,
[Own_Brand_Ind] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__product__3F466844] on [dbo].[product]'
GO
ALTER TABLE [dbo].[product] ADD CONSTRAINT [PK__product__3F466844] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [products].[BaseSite]'
GO
CREATE TABLE [products].[BaseSite]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NOT NULL,
[SystEnvironmentID] [int] NOT NULL,
[SiteTypeID] [int] NOT NULL,
[SiteAddress] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BaseSite] on [products].[BaseSite]'
GO
ALTER TABLE [products].[BaseSite] ADD CONSTRAINT [PK_BaseSite] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SiteType]'
GO
CREATE TABLE [dbo].[SiteType]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SiteType] on [dbo].[SiteType]'
GO
ALTER TABLE [dbo].[SiteType] ADD CONSTRAINT [PK_SiteType] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SystEnvironment]'
GO
CREATE TABLE [dbo].[SystEnvironment]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Environment] on [dbo].[SystEnvironment]'
GO
ALTER TABLE [dbo].[SystEnvironment] ADD CONSTRAINT [PK_Environment] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breed]'
GO
CREATE TABLE [dbo].[breed]
(
[id] [int] NOT NULL,
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[animal_type_id] [int] NOT NULL,
[breed_category_id] [int] NULL,
[map_to_breed_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__breed__145C0A3F] on [dbo].[breed]'
GO
ALTER TABLE [dbo].[breed] ADD CONSTRAINT [PK__breed__145C0A3F] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breed_category]'
GO
CREATE TABLE [dbo].[breed_category]
(
[id] [int] NOT NULL,
[description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[animal_type_id] [int] NULL,
[default_breed_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__breed_ca__3213E83F1DA648AE] on [dbo].[breed_category]'
GO
ALTER TABLE [dbo].[breed_category] ADD CONSTRAINT [PK__breed_ca__3213E83F1DA648AE] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic_group]'
GO
CREATE TABLE [dbo].[clinic_group]
(
[id] [int] NOT NULL,
[description] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[referrer_code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[service_profile] [int] NULL,
[active] [bit] NULL CONSTRAINT [DF__clinic_gr__activ__38A457AD] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__clinic_g__3213E83F2823D721] on [dbo].[clinic_group]'
GO
ALTER TABLE [dbo].[clinic_group] ADD CONSTRAINT [PK__clinic_g__3213E83F2823D721] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic]'
GO
CREATE TABLE [dbo].[clinic]
(
[id] [int] NOT NULL,
[product_id] [int] NOT NULL,
[description] [nvarchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[area_id] [int] NOT NULL,
[status] [tinyint] NULL CONSTRAINT [DF__clinic__status__53385258] DEFAULT ((1)),
[clinic_group_id] [int] NULL,
[renewal_area_id] [int] NULL,
[claim_vet_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__clinic__52442E1F] on [dbo].[clinic]'
GO
ALTER TABLE [dbo].[clinic] ADD CONSTRAINT [PK__clinic__52442E1F] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discount]'
GO
CREATE TABLE [dbo].[discount]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[code] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[description] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[start_date] [date] NULL,
[end_date] [date] NULL,
[staff] [bit] NOT NULL CONSTRAINT [DF__discount__staff__5AD97420] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_discount] on [dbo].[discount]'
GO
ALTER TABLE [dbo].[discount] ADD CONSTRAINT [PK_discount] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_discount_uniqueness]'
GO
CREATE FUNCTION [dbo].[cover_discount_uniqueness](@cover_id int, @discount_id int)
RETURNS INT
AS
BEGIN
      declare @ReturnValue INT = 0
      declare @code nvarchar(10);
      
      select @code = code
      from discount d
      where d.id = @discount_id;
      
      select @ReturnValue = COUNT(*)
      from discount d 
            join cover_discount cd on d.id = cd.discount_id
            join cover c on cd.cover_id = c.id
      where c.id = @cover_id
            and (CURRENT_TIMESTAMP between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))
            and d.code = @code;
            
      RETURN      @ReturnValue
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_discount]'
GO
CREATE TABLE [dbo].[cover_discount]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[cover_id] [int] NULL,
[discount_id] [int] NULL,
[discount] [decimal] (5, 2) NULL,
[funded_by] [int] NULL,
[persist_at_renewal] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cover_discount] on [dbo].[cover_discount]'
GO
ALTER TABLE [dbo].[cover_discount] ADD CONSTRAINT [PK_cover_discount] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit]'
GO
CREATE TABLE [dbo].[cover_benefit]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[section_id] [int] NOT NULL,
[cover_id] [int] NOT NULL,
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[excess] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[max_ben] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[section_num] [varchar] (8) COLLATE Latin1_General_CI_AS NULL,
[alternative_description] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[help_content] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__cover_benefit__15DA3E5D] on [dbo].[cover_benefit]'
GO
ALTER TABLE [dbo].[cover_benefit] ADD CONSTRAINT [PK__cover_benefit__15DA3E5D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_cover_benefit_cover_id_93A38] on [dbo].[cover_benefit]'
GO
CREATE NONCLUSTERED INDEX [IDX_cover_benefit_cover_id_93A38] ON [dbo].[cover_benefit] ([cover_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_cover_benefit_section_id_71048] on [dbo].[cover_benefit]'
GO
CREATE NONCLUSTERED INDEX [IDX_cover_benefit_section_id_71048] ON [dbo].[cover_benefit] ([section_id]) INCLUDE ([alternative_description], [cover_id], [description], [excess], [help_content])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[title]'
GO
CREATE TABLE [dbo].[title]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__title__0425A276] on [dbo].[title]'
GO
ALTER TABLE [dbo].[title] ADD CONSTRAINT [PK__title__0425A276] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[customer]'
GO
CREATE TABLE [dbo].[customer]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[title_id] [int] NOT NULL,
[forename] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[surname] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[tel] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[mobile] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[address1] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[town] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[county] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (32) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__customer__182C9B23] on [dbo].[customer]'
GO
ALTER TABLE [dbo].[customer] ADD CONSTRAINT [PK__customer__182C9B23] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dd_service_organisation]'
GO
CREATE TABLE [dbo].[dd_service_organisation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[url_logo] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[footer] [text] COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dd_organisation] on [dbo].[dd_service_organisation]'
GO
ALTER TABLE [dbo].[dd_service_organisation] ADD CONSTRAINT [PK_dd_organisation] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dd_service]'
GO
CREATE TABLE [dbo].[dd_service]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[dd_service_organisation_id] [int] NOT NULL,
[service_number] [varchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[service_bank_description] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[waiting_days] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dd_service] on [dbo].[dd_service]'
GO
ALTER TABLE [dbo].[dd_service] ADD CONSTRAINT [PK_dd_service] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dd_service_product]'
GO
CREATE TABLE [dbo].[dd_service_product]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[dd_service_id] [int] NOT NULL,
[valid_from_date] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dd_service_product] on [dbo].[dd_service_product]'
GO
ALTER TABLE [dbo].[dd_service_product] ADD CONSTRAINT [PK_dd_service_product] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_GRN]'
GO
CREATE TABLE [dbo].[pricing_matrix_GRN]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__pricing_matrix_G__56D3D912] on [dbo].[pricing_matrix_GRN]'
GO
ALTER TABLE [dbo].[pricing_matrix_GRN] ADD CONSTRAINT [PK__pricing_matrix_G__56D3D912] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_request_log]'
GO
CREATE TABLE [dbo].[iv_request_log]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[iv_profile_id] [int] NOT NULL,
[authentication_id] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[quote_id] [int] NULL,
[band_text] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[score] [int] NULL,
[forename] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[date_of_birth] [date] NULL,
[building_number] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[city] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[country] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[street] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[region] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[requested_date] [datetime] NULL,
[match_level_api] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_iv_request_log] on [dbo].[iv_request_log]'
GO
ALTER TABLE [dbo].[iv_request_log] ADD CONSTRAINT [PK_iv_request_log] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_iv_request_log_forename_surname_date_of_birth_postcode_requested_date_2DD81] on [dbo].[iv_request_log]'
GO
CREATE NONCLUSTERED INDEX [IDX_iv_request_log_forename_surname_date_of_birth_postcode_requested_date_2DD81] ON [dbo].[iv_request_log] ([forename], [surname], [date_of_birth], [postcode], [requested_date])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_iv_request_log_quote_id_64832] on [dbo].[iv_request_log]'
GO
CREATE NONCLUSTERED INDEX [IDX_iv_request_log_quote_id_64832] ON [dbo].[iv_request_log] ([quote_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_iv_request_log_requested_date_025E8] on [dbo].[iv_request_log]'
GO
CREATE NONCLUSTERED INDEX [IDX_iv_request_log_requested_date_025E8] ON [dbo].[iv_request_log] ([requested_date]) INCLUDE ([quote_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_decision_result]'
GO
CREATE TABLE [dbo].[iv_decision_result]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[quote_id] [int] NOT NULL,
[iv_decision_match_type_id] [int] NOT NULL,
[band_pass] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[policy_id] [int] NULL,
[iv_request_log_id] [int] NULL,
[create_timestamp] [datetime] NULL CONSTRAINT [DF__iv_decisi__creat__40EF84EB] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_iv_decision_result] on [dbo].[iv_decision_result]'
GO
ALTER TABLE [dbo].[iv_decision_result] ADD CONSTRAINT [PK_iv_decision_result] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[max_benefit]'
GO
CREATE TABLE [dbo].[max_benefit]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[affinity_code] [varchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[vets_fees] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[claiming_limitation] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[excess] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[compl_alt_treatment] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[clinical_diet] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[death_illness_accident] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[boarding_kennels_catteries] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[travel_quarantine] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[replacement_healthcare_cert] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[travel_repeat_tick_worming] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[third_party_liability] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[quarantine] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[healthcare_certificate] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[flea_tick_worm] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__max_benefit__6DCC4D03] on [dbo].[max_benefit]'
GO
ALTER TABLE [dbo].[max_benefit] ADD CONSTRAINT [PK__max_benefit__6DCC4D03] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[card_type]'
GO
CREATE TABLE [dbo].[card_type]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[value] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__card_type__28B808A7] on [dbo].[card_type]'
GO
ALTER TABLE [dbo].[card_type] ADD CONSTRAINT [PK__card_type__28B808A7] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_detail]'
GO
CREATE TABLE [dbo].[payment_detail]
(
[id] [uniqueidentifier] NOT NULL,
[payment_type_id] [int] NOT NULL,
[card_type_id] [int] NULL,
[ClientIPAddress] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[post_result] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[create_date] [smalldatetime] NOT NULL CONSTRAINT [DF__payment_d__creat__0E04126B] DEFAULT (getdate()),
[update_date] [smalldatetime] NOT NULL CONSTRAINT [DF__payment_d__updat__0EF836A4] DEFAULT (getdate()),
[amount] [decimal] (10, 2) NULL,
[enc_card_number] [varbinary] (256) NULL,
[enc_card_name] [varbinary] (256) NULL,
[enc_valid_from] [varbinary] (52) NULL,
[enc_valid_to] [varbinary] (52) NULL,
[enc_account_number] [varbinary] (256) NULL,
[enc_account_name] [varbinary] (256) NULL,
[enc_sort_code] [varbinary] (256) NULL,
[enc_issue_number] [varbinary] (256) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__payment_detail__7908F585] on [dbo].[payment_detail]'
GO
ALTER TABLE [dbo].[payment_detail] ADD CONSTRAINT [PK__payment_detail__7908F585] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_type]'
GO
CREATE TABLE [dbo].[payment_type]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__payment_type__66603565] on [dbo].[payment_type]'
GO
ALTER TABLE [dbo].[payment_type] ADD CONSTRAINT [PK__payment_type__66603565] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ApplicationType]'
GO
CREATE TABLE [dbo].[ApplicationType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__Applicat__3214EC07D15FE6CB] on [dbo].[ApplicationType]'
GO
ALTER TABLE [dbo].[ApplicationType] ADD CONSTRAINT [PK__Applicat__3214EC07D15FE6CB] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PaymentProviderCallBack]'
GO
CREATE TABLE [dbo].[PaymentProviderCallBack]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ApplicationTypeID] [int] NOT NULL,
[ProductPaymentProviderConfigId] [int] NOT NULL,
[UrlNotification] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[UrlSuccess] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[UrlFail] [varchar] (256) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__PaymentP__3214EC07530182CC] on [dbo].[PaymentProviderCallBack]'
GO
ALTER TABLE [dbo].[PaymentProviderCallBack] ADD CONSTRAINT [PK__PaymentP__3214EC07530182CC] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ProductPaymentProviderConfig]'
GO
CREATE TABLE [dbo].[ProductPaymentProviderConfig]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[PaymentProviderId] [int] NOT NULL,
[PaymentProviderDetailId] [int] NOT NULL,
[ActiveDate] [datetime] NOT NULL,
[InactiveDate] [datetime] NULL,
[ApplySurcharge] [bit] NOT NULL CONSTRAINT [DF__ProductPa__Apply__4A0EDAD1] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__ProductP__3214EC072FBA3946] on [dbo].[ProductPaymentProviderConfig]'
GO
ALTER TABLE [dbo].[ProductPaymentProviderConfig] ADD CONSTRAINT [PK__ProductP__3214EC072FBA3946] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[Breakdown]'
GO
CREATE TABLE [Pricing].[Breakdown]
(
[Id] [uniqueidentifier] NOT NULL,
[SourceId] [int] NOT NULL,
[Identifier] [int] NULL,
[CoverId] [int] NOT NULL,
[Premium] [decimal] (10, 2) NOT NULL,
[PricingEngineId] [bigint] NOT NULL,
[SequenceId] [varchar] (36) COLLATE Latin1_General_CI_AS NOT NULL,
[Breakdown] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Created] [datetime] NOT NULL,
[Uploaded] [bit] NOT NULL,
[AssetID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Pricing.Breakdown] on [Pricing].[Breakdown]'
GO
ALTER TABLE [Pricing].[Breakdown] ADD CONSTRAINT [PK_Pricing.Breakdown] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Breakdown_SequenceID] on [Pricing].[Breakdown]'
GO
CREATE NONCLUSTERED INDEX [IX_Breakdown_SequenceID] ON [Pricing].[Breakdown] ([SequenceId]) INCLUDE ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[BreakdownAssociation]'
GO
CREATE TABLE [Pricing].[BreakdownAssociation]
(
[BreakdownId] [uniqueidentifier] NOT NULL,
[SourceId] [int] NOT NULL,
[Identifier] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Pricing.BreakdownAssociation] on [Pricing].[BreakdownAssociation]'
GO
ALTER TABLE [Pricing].[BreakdownAssociation] ADD CONSTRAINT [PK_Pricing.BreakdownAssociation] PRIMARY KEY CLUSTERED  ([BreakdownId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_gender]'
GO
CREATE TABLE [dbo].[product_gender]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[gender_desc] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__product_gender__440B1D61] on [dbo].[product_gender]'
GO
ALTER TABLE [dbo].[product_gender] ADD CONSTRAINT [PK__product_gender__440B1D61] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_payment_type]'
GO
CREATE TABLE [dbo].[product_payment_type]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[payment_type_id] [int] NOT NULL,
[start_date] [date] NULL,
[end_date] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_payment_type] on [dbo].[product_payment_type]'
GO
ALTER TABLE [dbo].[product_payment_type] ADD CONSTRAINT [PK_product_payment_type] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[postcode]'
GO
CREATE TABLE [dbo].[postcode]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[ipt_percentage] [float] NOT NULL CONSTRAINT [DF_postcode_ipt_percentage] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__postcode__4AB81AF0] on [dbo].[postcode]'
GO
ALTER TABLE [dbo].[postcode] ADD CONSTRAINT [PK__postcode__4AB81AF0] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_postcode_score]'
GO
CREATE TABLE [dbo].[product_postcode_score]
(
[product_id] [int] NOT NULL,
[postcode_id] [int] NOT NULL,
[rating] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_postcode_score] on [dbo].[product_postcode_score]'
GO
ALTER TABLE [dbo].[product_postcode_score] ADD CONSTRAINT [PK_product_postcode_score] PRIMARY KEY CLUSTERED  ([product_id], [postcode_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_pricing_engine]'
GO
CREATE TABLE [dbo].[product_pricing_engine]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[pricing_engine_id] [bigint] NOT NULL,
[product_id] [int] NOT NULL,
[new_business_effective_date] [date] NOT NULL,
[renewals_effective_date] [date] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_pricing_engine] on [dbo].[product_pricing_engine]'
GO
ALTER TABLE [dbo].[product_pricing_engine] ADD CONSTRAINT [PK_product_pricing_engine] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_site_configuration]'
GO
CREATE TABLE [dbo].[product_site_configuration]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[sagepay_url_success] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[sagepay_url_fail] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[start_date] [date] NULL,
[end_date] [date] NULL,
[sagepay_notification_url] [varchar] (256) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_site_configuration] on [dbo].[product_site_configuration]'
GO
ALTER TABLE [dbo].[product_site_configuration] ADD CONSTRAINT [PK_product_site_configuration] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_underwriting_check]'
GO
CREATE TABLE [dbo].[product_underwriting_check]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[action_id] [int] NOT NULL,
[page_id] [int] NOT NULL,
[response] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__product_underwri__40058253] on [dbo].[product_underwriting_check]'
GO
ALTER TABLE [dbo].[product_underwriting_check] ADD CONSTRAINT [PK__product_underwri__40058253] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_underwriting_check_quote]'
GO
CREATE TABLE [dbo].[product_underwriting_check_quote]
(
[product_underwriting_check_id] [int] NOT NULL,
[quote_id] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PaymentProvider]'
GO
CREATE TABLE [dbo].[PaymentProvider]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__PaymentP__3214EC078E354D6A] on [dbo].[PaymentProvider]'
GO
ALTER TABLE [dbo].[PaymentProvider] ADD CONSTRAINT [PK__PaymentP__3214EC078E354D6A] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PaymentProviderDetail]'
GO
CREATE TABLE [dbo].[PaymentProviderDetail]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[VendorName] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[EndPointUrl] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__PaymentP__3214EC0737E9E85C] on [dbo].[PaymentProviderDetail]'
GO
ALTER TABLE [dbo].[PaymentProviderDetail] ADD CONSTRAINT [PK__PaymentP__3214EC0737E9E85C] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [products].[ProductQBActionMethod]'
GO
CREATE TABLE [products].[ProductQBActionMethod]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NOT NULL,
[QBActionMethodID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_ProductQBActionMethod] on [products].[ProductQBActionMethod]'
GO
ALTER TABLE [products].[ProductQBActionMethod] ADD CONSTRAINT [PK_ProductQBActionMethod] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QBActionMethod]'
GO
CREATE TABLE [dbo].[QBActionMethod]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ControlAddress] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_QBActionMethod] on [dbo].[QBActionMethod]'
GO
ALTER TABLE [dbo].[QBActionMethod] ADD CONSTRAINT [PK_QBActionMethod] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[systuser]'
GO
CREATE TABLE [dbo].[systuser]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[title_id] [int] NULL,
[username] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL,
[password] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL,
[password_prompt] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[time_stamp] [smalldatetime] NOT NULL CONSTRAINT [DF_systuser_time_stamp] DEFAULT (getdate()),
[user_status] [bit] NULL CONSTRAINT [DF_systuser_enabled] DEFAULT (1),
[email_address] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[systgroup_id] [int] NOT NULL,
[pw_date] [smalldatetime] NOT NULL CONSTRAINT [DF_systuser_pwdate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__systuser__0BC6C43E] on [dbo].[systuser]'
GO
ALTER TABLE [dbo].[systuser] ADD CONSTRAINT [PK__systuser__0BC6C43E] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pw_history]'
GO
CREATE TABLE [dbo].[pw_history]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[systuser_id] [int] NOT NULL,
[password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[time_stamp] [smalldatetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__pw_history__1273C1CD] on [dbo].[pw_history]'
GO
ALTER TABLE [dbo].[pw_history] ADD CONSTRAINT [PK__pw_history__1273C1CD] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_optional_question]'
GO
CREATE TABLE [dbo].[quote_optional_question]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[quoteId] [int] NOT NULL,
[coverForIllness] [bit] NULL,
[coverForOngoingTreatment] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_optional_question_id] on [dbo].[quote_optional_question]'
GO
CREATE CLUSTERED INDEX [idx_quote_optional_question_id] ON [dbo].[quote_optional_question] ([id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_quote_optional_question] on [dbo].[quote_optional_question]'
GO
ALTER TABLE [dbo].[quote_optional_question] ADD CONSTRAINT [PK_quote_optional_question] PRIMARY KEY NONCLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_optional_question_quoteId] on [dbo].[quote_optional_question]'
GO
CREATE NONCLUSTERED INDEX [idx_quote_optional_question_quoteId] ON [dbo].[quote_optional_question] ([quoteId]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[quote_optional_question]'
GO
ALTER TABLE [dbo].[quote_optional_question] ADD CONSTRAINT [UC_quote_optional_question_quoteId] UNIQUE NONCLUSTERED  ([quoteId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_payment_detail]'
GO
CREATE TABLE [dbo].[quote_payment_detail]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[quote_id] [int] NOT NULL,
[enc_account_number] [varbinary] (256) NULL,
[enc_account_name] [varbinary] (256) NULL,
[enc_sort_code] [varbinary] (256) NULL,
[timestamp] [datetime] NULL CONSTRAINT [DF__quote_pay__times__53CD4F35] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_quote_payment_detail] on [dbo].[quote_payment_detail]'
GO
ALTER TABLE [dbo].[quote_payment_detail] ADD CONSTRAINT [PK_quote_payment_detail] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_status]'
GO
CREATE TABLE [dbo].[quote_status]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__quote_status__019E3B86] on [dbo].[quote_status]'
GO
ALTER TABLE [dbo].[quote_status] ADD CONSTRAINT [PK__quote_status__019E3B86] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QuoteListExclusionType]'
GO
CREATE TABLE [dbo].[QuoteListExclusionType]
(
[Id] [int] NOT NULL,
[Description] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_QuoteListExclusionType] on [dbo].[QuoteListExclusionType]'
GO
ALTER TABLE [dbo].[QuoteListExclusionType] ADD CONSTRAINT [PK_QuoteListExclusionType] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QuoteListExclusion]'
GO
CREATE TABLE [dbo].[QuoteListExclusion]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Value] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL,
[QuoteListExclusionTypeId] [int] NOT NULL,
[FurtherDetail] [varchar] (256) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_QuoteListExclusion] on [dbo].[QuoteListExclusion]'
GO
ALTER TABLE [dbo].[QuoteListExclusion] ADD CONSTRAINT [PK_QuoteListExclusion] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_premium_override]'
GO
CREATE TABLE [dbo].[renewal_premium_override]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[policy_id] [int] NULL,
[renewal_year] [int] NULL,
[premium] [decimal] (10, 2) NULL,
[cover_id] [int] NULL,
[systuser_id] [int] NULL,
[datetimestamp] [datetime] NULL,
[cover_discount_id] [int] NULL,
[pricing_engine_id] [bigint] NOT NULL CONSTRAINT [DF__renewal_p__prici__0717E911] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_renewal_premium_override] on [dbo].[renewal_premium_override]'
GO
ALTER TABLE [dbo].[renewal_premium_override] ADD CONSTRAINT [PK_renewal_premium_override] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_policy_id_renewal_year] on [dbo].[renewal_premium_override]'
GO
CREATE NONCLUSTERED INDEX [idx_policy_id_renewal_year] ON [dbo].[renewal_premium_override] ([policy_id], [renewal_year])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sagepay_transaction]'
GO
CREATE TABLE [dbo].[sagepay_transaction]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[quote_id] [int] NULL,
[policy_id] [int] NULL,
[VendorTxId] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[VPSTxId] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[SecurityKey] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[create_timestamp] [datetime] NULL CONSTRAINT [DF__sagepay_t__creat__23FE4082] DEFAULT (getdate()),
[ProductPaymentProviderConfigId] [int] NULL,
[ApplicationTypeId] [int] NOT NULL CONSTRAINT [DF__sagepay_t__Appli__6F405F80] DEFAULT ((1)),
[TransactionAmount] [decimal] (10, 2) NOT NULL CONSTRAINT [DF__sagepay_t__Trans__7405149D] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_sagepay_transaction] on [dbo].[sagepay_transaction]'
GO
ALTER TABLE [dbo].[sagepay_transaction] ADD CONSTRAINT [PK_sagepay_transaction] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[section]'
GO
CREATE TABLE [dbo].[section]
(
[id] [int] NOT NULL,
[product_id] [int] NOT NULL,
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[ordinal] [int] NOT NULL,
[section_id] [int] NULL,
[alternative_description] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[help_content] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__section__0A688BB1] on [dbo].[section]'
GO
ALTER TABLE [dbo].[section] ADD CONSTRAINT [PK__section__0A688BB1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[schedule_section_config]'
GO
CREATE TABLE [dbo].[schedule_section_config]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SectionId] [int] NOT NULL,
[AffinityCode] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ScheduleDescription] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_ScheduleSectionConfig] on [dbo].[schedule_section_config]'
GO
ALTER TABLE [dbo].[schedule_section_config] ADD CONSTRAINT [PK_ScheduleSectionConfig] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[surcharge]'
GO
CREATE TABLE [dbo].[surcharge]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[card_type_id] [int] NOT NULL,
[surcharge_value] [decimal] (5, 2) NULL,
[is_percentage_value] [bit] NULL,
[start_date] [date] NULL,
[end_date] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_surcharge] on [dbo].[surcharge]'
GO
ALTER TABLE [dbo].[surcharge] ADD CONSTRAINT [PK_surcharge] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[systuser_logon_history]'
GO
CREATE TABLE [dbo].[systuser_logon_history]
(
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL,
[username] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ip_address] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[time_stamp] [smalldatetime] NOT NULL CONSTRAINT [DF__systuser___time___108B795B] DEFAULT (getdate()),
[systuser_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[systgroup]'
GO
CREATE TABLE [dbo].[systgroup]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__systgroup__76CBA758] on [dbo].[systgroup]'
GO
ALTER TABLE [dbo].[systgroup] ADD CONSTRAINT [PK__systgroup__76CBA758] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[underwriting_check]'
GO
CREATE TABLE [dbo].[underwriting_check]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[product_id] [int] NULL,
[page_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__underwriting_che__0B91BA14] on [dbo].[underwriting_check]'
GO
ALTER TABLE [dbo].[underwriting_check] ADD CONSTRAINT [PK__underwriting_che__0B91BA14] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[zenith_breed_mapping]'
GO
CREATE TABLE [dbo].[zenith_breed_mapping]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[upp_breed_id] [int] NOT NULL,
[zenith_breed_id] [int] NOT NULL,
[cover_id] [int] NOT NULL,
[effective_date] [date] NOT NULL,
[sales_type] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__zenith_br__sales__34F3C25A] DEFAULT ('NB')
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_zenith_breed_mapping] on [dbo].[zenith_breed_mapping]'
GO
ALTER TABLE [dbo].[zenith_breed_mapping] ADD CONSTRAINT [PK_zenith_breed_mapping] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_cover_id_sales_type_995E6] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_cover_id_sales_type_995E6] ON [dbo].[zenith_breed_mapping] ([cover_id], [sales_type]) INCLUDE ([upp_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_cover_id_sales_type_692A9] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_cover_id_sales_type_692A9] ON [dbo].[zenith_breed_mapping] ([cover_id], [sales_type]) INCLUDE ([effective_date], [upp_breed_id], [zenith_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_sales_type_62B91] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_sales_type_62B91] ON [dbo].[zenith_breed_mapping] ([sales_type]) INCLUDE ([cover_id], [upp_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_upp_breed_id_D4A3D] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_upp_breed_id_D4A3D] ON [dbo].[zenith_breed_mapping] ([upp_breed_id]) INCLUDE ([cover_id], [effective_date], [id], [sales_type], [zenith_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_upp_breed_id_cover_id_effective_date_9E745] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_upp_breed_id_cover_id_effective_date_9E745] ON [dbo].[zenith_breed_mapping] ([upp_breed_id], [cover_id], [effective_date]) INCLUDE ([id], [zenith_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_zenith_breed_id_68F8B] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_zenith_breed_id_68F8B] ON [dbo].[zenith_breed_mapping] ([zenith_breed_id]) INCLUDE ([cover_id], [effective_date], [id], [sales_type], [upp_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_zenith_breed_id_9C26F] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_zenith_breed_id_9C26F] ON [dbo].[zenith_breed_mapping] ([zenith_breed_id]) INCLUDE ([cover_id], [effective_date], [sales_type], [upp_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_zenith_breed_mapping_zenith_breed_id_cover_id_sales_type_DD6A1] on [dbo].[zenith_breed_mapping]'
GO
CREATE NONCLUSTERED INDEX [IDX_zenith_breed_mapping_zenith_breed_id_cover_id_sales_type_DD6A1] ON [dbo].[zenith_breed_mapping] ([zenith_breed_id], [cover_id], [sales_type]) INCLUDE ([effective_date], [upp_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[zenith_breed_mapping3]'
GO
CREATE TABLE [dbo].[zenith_breed_mapping3]
(
[upp_breed_id] [int] NOT NULL,
[zenith_breed_id] [int] NOT NULL,
[effective_date] [date] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_zenith_breed_mapping3] on [dbo].[zenith_breed_mapping3]'
GO
ALTER TABLE [dbo].[zenith_breed_mapping3] ADD CONSTRAINT [PK_zenith_breed_mapping3] PRIMARY KEY CLUSTERED  ([upp_breed_id], [zenith_breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_time]'
GO
CREATE TABLE [dbo].[lead_time]
(
[id] [tinyint] NOT NULL,
[start_time] [time] (0) NOT NULL,
[end_time] [time] (0) NOT NULL,
[time_description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__lead_tim__3213E83F4AC307E8] on [dbo].[lead_time]'
GO
ALTER TABLE [dbo].[lead_time] ADD CONSTRAINT [PK__lead_tim__3213E83F4AC307E8] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[minder]'
GO
CREATE TABLE [dbo].[minder]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[started] [bigint] NULL,
[ended] [bigint] NULL,
[ext] [int] NULL,
[field] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[duration] [int] NULL CONSTRAINT [DF_minder_duration] DEFAULT ((0)),
[datetimestamp] [datetime] NULL CONSTRAINT [DF_minder_datetimestamp] DEFAULT (getdate()),
[qref] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_minder] on [dbo].[minder]'
GO
ALTER TABLE [dbo].[minder] ADD CONSTRAINT [PK_minder] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_minder_ended_ext_qref_3C182] on [dbo].[minder]'
GO
CREATE NONCLUSTERED INDEX [IDX_minder_ended_ext_qref_3C182] ON [dbo].[minder] ([ended], [ext], [qref])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[minderL_prc]'
GO

CREATE PROCEDURE [dbo].[minderL_prc]  (@start bigint) 
AS

SET NOCOUNT ON

	select top 100 [started], ext, field, duration from minder where [started] > @start order by [started]

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_schedule_get_signatureS_prc]'
GO



CREATE PROCEDURE [dbo].[policy_schedule_get_signatureS_prc] (@affinityCode VARCHAR(10))
AS
BEGIN

	DECLARE @productID INT = (SELECT TOP 1 ID FROM dbo.product WHERE affinity_code = LEFT(@affinityCode,3))
	,@endDisclaimer VARCHAR(500)
	,@signature_title_html VARCHAR(200)

	--Fix for missing affinities
	IF(ISNULL(@productID,0) = 0)
	BEGIN
		SELECT @productID = CASE LEFT(@affinityCode,3)
			WHEN 'PAH' THEN 32
			WHEN 'PUH' THEN 26
			WHEN 'UIZ' THEN 23
			END
	END

	
	IF(@productID IN (23, 26, 32, 33)) --Our brands
	BEGIN
		SET @endDisclaimer = 'For this Policy Schedule to be valid it must be signed and dated and the wording must be attached. Please inform Insurance Factory Limited immediately if this is not the case.'			
		SET @signature_title_html = 'Managing Director'
	END
	ELSE
	IF @productID = 35
	BEGIN	
		SET @signature_title_html = ''
		SET @endDisclaimer = 'For this Policy Schedule to be valid it must be signed and dated and the wording must be attached. Please inform Insurance Factory Limited immediately if this is not the case.'
	END
	ELSE
	BEGIN
		SET @endDisclaimer = 'For this Policy Schedule to be valid it must be signed and dated and the wording must be attached. Please inform Insurance Factory Limited immediately if this is not the case.'
	END

	SELECT --u.company_logo_url
		'' AS company_logo_url
		,u.signature_name_html
		,ISNULL(@signature_title_html,u.signature_title_html) AS signature_title_html
		,u.signature_image_url
		,u.underwriter_code
	
		-- it will decide if show or not the message below the underwriter's signature:
		-- For this Policy Schedule to be valid it must be signed and dated and the wording must be attached.  Please inform Insurance Factory Limited immediately if this is not the case.
		,CASE WHEN u.signature_image_url IS NOT NULL 
			THEN 'true'
			ELSE 'false'
		END AS show_footer_note
	
		,CONVERT(VARCHAR(11),GETDATE(),113) AS date_printed
		,@endDisclaimer AS end_disclaimer
	FROM uispetmis..affinity a
	INNER JOIN uispetmis..underwriter u ON a.underwriter = u.underwriter_code
	WHERE a.AffinityCode = @affinityCode
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[leads_today_countS_prc]'
GO


CREATE PROCEDURE [dbo].[leads_today_countS_prc]
AS
BEGIN

select isnull(sum(case when DATEDIFF(day, getdate(), contact_date) = 0 then 1 else 0 end),0)
		, isnull(sum(case when DATEDIFF(day, getdate(), contact_date) < 0 then 1 else 0 end),0)
	from lead l
		join aggregator a on a.lead_id = l.id
	where lead_status_id = 100 
	and a.source != 'GOCOMP'

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_VSR]'
GO
CREATE TABLE [dbo].[pricing_matrix_VSR]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[area_1] [money] NOT NULL,
[area_2] [money] NOT NULL,
[area_3] [money] NOT NULL,
[area_4] [money] NOT NULL,
[area_5] [money] NOT NULL,
[area_6] [money] NOT NULL,
[area_101] [money] NULL,
[area_102] [money] NULL,
[area_103] [money] NULL,
[area_104] [money] NULL,
[area_105] [money] NULL,
[area_106] [money] NULL,
[area_0] [money] NULL,
[area_7] [money] NULL,
[area_100] [money] NULL,
[area_107] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_VSR] on [dbo].[pricing_matrix_VSR]'
GO
ALTER TABLE [dbo].[pricing_matrix_VSR] ADD CONSTRAINT [PK_pricing_matrix_VSR] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_introductory_discount]'
GO
CREATE TABLE [dbo].[cover_introductory_discount]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[cover_id] [int] NULL,
[discount] [decimal] (5, 2) NULL,
[funded_by] [int] NULL,
[effective_date] [date] NULL,
[inactive_date] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cover_introctory_discount] on [dbo].[cover_introductory_discount]'
GO
ALTER TABLE [dbo].[cover_introductory_discount] ADD CONSTRAINT [PK_cover_introctory_discount] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_introductory_discount_by_product]'
GO

CREATE FUNCTION [dbo].[get_introductory_discount_by_product]
(	
	@applicable_date date = null,
	@product_id int 
)
RETURNS @Result TABLE 
(
	discount decimal(10,2),
	cover_id int
)
AS
BEGIN
	
	--this procedure will return all discounts grouped by cover.
	if @applicable_date is null
	begin
		set @applicable_date = GETDATE();
	end

	insert into @Result (discount,cover_id)
	select ISNULL(SUM(cid.discount),0) as discount ,cid.cover_id
	from cover_introductory_discount cid
	inner join cover c on c.id = cid.cover_id
	where c.product_id = @product_id
	and c.cover_inactive_date is null
	and @applicable_date between cid.effective_date and isnull(cid.inactive_date,'2050-01-01')
	group by cid.cover_id;
	
	return
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_coinsurance_and_excess]'
GO

CREATE FUNCTION [dbo].[get_coinsurance_and_excess](@affinity_code varchar(5), @pet_DOB smalldatetime, @policy_start_date smalldatetime) 
RETURNS @Result TABLE
(
       coinsurance float,
       excess money
)
AS
BEGIN
       declare @age_years int;
       select @age_years = (
              SELECT (
                     CONVERT(int, CONVERT(char(8), @policy_start_date, 112))
                           -
                     CONVERT(char(8), @pet_DOB, 112)
              )
              / 10000
       )
       
       insert into @Result(coinsurance, excess)
              select top 1
                           ISNULL(ader.pct_copayment,0),
                           ade.min_excess_amt
              from   uispetmis..affinity_document ad
                           join uispetmis..affinity_document_excess ade on ade.affinity_document_id = ad.id
                           left join uispetmis..affinity_document_excess_rule ader on ader.affinity_document_XS_id = ade.id
              where ad.affinity_code = @affinity_code
                       AND ad.valid_from = 
                                    (SELECT MAX(ad2.valid_from) FROM uispetmis..affinity_document ad2 
                                                WHERE ad.affinity_code = ad2.affinity_code AND ad2.valid_from <= @policy_start_date)
--                     AND ISNULL(ader.rule_value,0) <= @age_years
                       AND ( (rule_condition IS NULL) OR (rule_condition = 1  AND ader.rule_value <= @age_years) OR (rule_condition = 3  AND ader.rule_value > @age_years))
              ORDER BY ISNULL(ader.rule_value,0) DESC, ISNULL(ade.[default], 0) DESC;
              
       return;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quick_quote_calculate_VSR_U_prc]'
GO
CREATE PROCEDURE [dbo].[quick_quote_calculate_VSR_U_prc]
	@clinic_id int,
	@product_id int,
	@quote_pet_id int,
	@pet_count int,
	@promo_code nvarchar(10),
	@policy_start_date date,
	@name varchar(32),
	@pet_DOB date,
	@animal_type_id int,
	@cover_id int,
	@offer_discount decimal(5,2),
	@multipet_discount decimal(5,2),
	@promo_discount decimal(5,2),
	@ipt_factor float,
	@age_rating varchar(16),
	@breed_rating int,
	@area_rating int,
	@min_cover_ordinal int,
	@sel_cover_only bit
AS
BEGIN
	--create a table variable with all the discounts for this product and the supplied promo code
	declare @promo table (cover_id int, discount decimal(5,2), staff bit)
	insert into @promo
	select cd.cover_id, discount, staff
	from cover_discount cd 
		join discount d on d.id = cd.discount_id
	where d.code = @promo_code
	AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))

	declare @area_id int
	if @clinic_id is null
		select @area_id = @area_rating;
	else
		select @area_id = area_id from clinic where id = @clinic_id and product_id = @product_id

	if @policy_start_date < '2014-07-14'
		select @area_id = @area_id + 100

	/* INTRODUCTORY DISCOUNT */
	declare @coverIntroductoryDiscount table(discount decimal(10,2), cover_id int);
	--check if its not renewal
	if not exists(select * from uispetmis..POLICY p where p.quote_pet_id = @quote_pet_id)
	begin
		insert into @coverIntroductoryDiscount (discount, cover_id)
		select discount, cover_id
		from uispet..get_introductory_discount_by_product(GETDATE(), @product_id);
	end
		
	--OK, have all the factors, let's look up the value
	SELECT	@name pet
		,@quote_pet_id pet_id
		,@animal_type_id animal_type_id
		,c.id cover_id,
		convert(decimal(10,2), 
			case @area_id 
				when 0 then [area_0]
				when 1 then [area_1]
				when 2 then [area_2]
				when 3 then [area_3]
				when 4 then [area_4]
				when 5 then [area_5]
				when 6 then [area_6]
				when 7 then [area_7]
				when 100 then [area_100]
				when 101 then [area_101]
				when 102 then [area_102]
				when 103 then [area_103]
				when 104 then [area_104]
				when 105 then [area_105]
				when 106 then [area_106]
				when 107 then [area_107] 
			end
			* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
			* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
			* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
			* @ipt_factor)
			as premium
		,case when @cover_id = c.id then 1 else 0 end selected
		,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
		,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
		,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
		,c.[description]
		,@product_id product_id
		,gcae.coinsurance coinsurance
		,gcae.excess excess
	FROM	pricing_matrix_vsr
		JOIN	cover c ON cover_id = c.id
		LEFT JOIN	@promo p on p.cover_id = c.id
		CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
		left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
	WHERE	animal_type_id = @animal_type_id 
	AND		age	= @age_rating 
	AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
	and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
	and c.ordinal >= @min_cover_ordinal
	and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
	order by ordinal
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_statusU_prc]'
GO

CREATE PROCEDURE [dbo].[quote_statusU_prc]  (@id INT, @quote_status_id INT, @systuser_id int = NULL)
		
AS

SET NOCOUNT ON

		--Only status that will be passed in is 3(paid) or 4(expired)
		--May need to do more depending on the value... 

		UPDATE	quote
		SET		quote_status_id = @quote_status_id
			,modified_systuser_id = ISNULL(modified_systuser_id, @systuser_id)	--only update if not previously populated
			,converted_systuser_id = @systuser_id --always update most recent user in case converted to policy
		WHERE	id = @id

				
			
SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_pet_charity]'
GO
CREATE TABLE [dbo].[quote_pet_charity]
(
[quote_pet_id] [int] NULL,
[charity_id] [int] NULL,
[donation] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_quote_pet_id] on [dbo].[quote_pet_charity]'
GO
CREATE CLUSTERED INDEX [idx_quote_pet_id] ON [dbo].[quote_pet_charity] ([quote_pet_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QuotePurgeD_prc]'
GO
CREATE procedure [dbo].[QuotePurgeD_prc] @quote_ref varchar(20)

as

/*
Created	on		26.3.2015 by	T. Pullen

This procedure deletes data from all tables containing quote information, for a single quote.
Rows are deleted in correct order to satisfy referential integrity.
Deletion is done in one transaction, all of which is rolled back if errors occur, in which caes
error number and message are returned.
*/

set nocount on

begin try

begin transaction

delete ea
from uispetmis..email_activity ea
inner join uispetmis..email e
on e.tracking_id = ea.email_tracking_id
inner join uispetmis..diary d 
on d.diaryId = e.diary_Id
inner join quote q
on q.id = d.quote_id 
where quote_ref = @quote_ref

delete ead
from uispetmis..email_attachment_link ead
inner join uispetmis..email e
on e.id = ead.email_id
inner join uispetmis..diary d 
on d.diaryId = e.diary_Id
inner join quote q
on q.id = d.quote_id 
where quote_ref = @quote_ref

delete eal
from uispetmis..email_attachment_link eal
inner join uispetmis..email e
on e.id = eal.email_id
inner join uispetmis..diary d 
on d.diaryId = e.diary_Id
inner join quote q
on q.id = d.quote_id 
where quote_ref = @quote_ref

delete e
from uispetmis..email e
inner join uispetmis..diary d 
on d.diaryId = e.diary_Id
inner join quote q
on q.id = d.quote_id 
where quote_ref = @quote_ref

delete d 
from uispetmis..diary d
inner join quote q
on q.id = d.quote_id 
where quote_ref = @quote_ref

delete a
from aggregator a
inner join quote q
on q.id = a.quote_id
where q.quote_ref  = @quote_ref

delete qpc
from quote_pet_charity qpc
inner join quote_pet qp 
on qpc.quote_pet_id = qp.id
inner join quote q
on q.id = qp.quote_id
where q.quote_ref  = @quote_ref

delete qp
from quote_pet qp
inner join quote q
on q.id = qp.quote_id
where q.quote_ref  = @quote_ref

delete i
from iv_decision_result i
inner join quote q
on q.id = i.quote_id
where q.quote_ref  = @quote_ref

delete from quote where quote_ref =  @quote_ref

commit transaction

end try

begin catch
    SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_MESSAGE() AS ErrorMessage;
        
       rollback transaction
end catch
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sales_ip_address]'
GO
CREATE TABLE [dbo].[sales_ip_address]
(
[ip_address] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_sales_ip_address] on [dbo].[sales_ip_address]'
GO
CREATE CLUSTERED INDEX [idx_sales_ip_address] ON [dbo].[sales_ip_address] ([ip_address])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[report_data_dump_prc]'
GO



CREATE PROCEDURE [dbo].[report_data_dump_prc]
(@status varchar(12) = '', @affinity varchar(3) = '', @from smalldatetime = '2008-01-01', @to smalldatetime = '2030-12-31')
AS
BEGIN
	SET NOCOUNT ON;

IF(@from > @to) return

select @to = DATEADD(n,-1,DATEADD(d,1,@to))

select ISNULL(policynumber, quote_ref) collate Latin1_General_CI_AS [quote_ref]
	,case when quote_status_id = 6 then 'Declined'
		when p.cancelleddate < @to then 'Cancelled'
		when p.policyid is not null then 'Policy' 
		 else 'Quote' end [outcome]
	,ISNULL(cl.description,'') collate Latin1_General_CI_AS [practice]
	,ISNULL(cg.description,'') collate Latin1_General_CI_AS [vet group]
	,ISNULL(c.description,'') collate Latin1_General_CI_AS [product]
	,replace(isnull(qp.premium,''), '0.00','') [premium]
	,LOWER(pet_name) collate Latin1_General_CI_AS [pet name]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,case when p.policyid is not null then
		ISNULL(convert(varchar, q.update_timestamp, 103),'')  
	else '' end collate Latin1_General_CI_AS  [policy sold date]
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') [inception date]

	,convert(varchar, q.policy_start_date, 103) collate Latin1_General_CI_AS [policy start date]

	,ISNULL(convert(varchar, cancelleddate, 103),'') collate Latin1_General_CI_AS [cancellation date]
	,ISNULL(convert(varchar, p.cancellation_instruction_date, 103),'') [cancellation instruction date]
	,ISNULL(cr.description,'') collate Latin1_General_CI_AS [cancel reason]

	,'' [lapse effective date]
	,'' [lapse instruction date]

	,at.description collate Latin1_General_CI_AS [species]
	,pg.gender_desc collate Latin1_General_CI_AS [gender]
	,b.description collate Latin1_General_CI_AS [breed]
	,convert(int, qp.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') collate Latin1_General_CI_AS chip
	,ISNULL(qp.colour,'') collate Latin1_General_CI_AS colour
	,convert(varchar, qp.pet_dob, 103) collate Latin1_General_CI_AS [pet dob]
	,ISNULL(convert(varchar(255), qp.vetvisitdetails),'') collate Latin1_General_CI_AS vetvisit
	,ISNULL(convert(varchar(255), qp.accidentdetails),'') collate Latin1_General_CI_AS accident
	,ISNULL(p.exclusion,'') collate Latin1_General_CI_AS exclusion
	,ISNULL(t.description,'') collate Latin1_General_CI_AS title
	,ISNULL(q.forename,'') collate Latin1_General_CI_AS firstname
	,ISNULL(q.surname,'') collate Latin1_General_CI_AS lastname
	,ISNULL(convert(varchar, q.dob, 103),'') collate Latin1_General_CI_AS dob
	
	,q.house_name_num +' '+ISNULL(q.street,'') collate Latin1_General_CI_AS address1
	,q.street2 collate Latin1_General_CI_AS address2
	,q.town collate Latin1_General_CI_AS address3
	,q.county collate Latin1_General_CI_AS address4
	,'' address5
	,q.postcode collate Latin1_General_CI_AS postcode
	,ISNULL(q.contact_num,'') collate Latin1_General_CI_AS telephone
	,q.email collate Latin1_General_CI_AS email
	,ISNULL(convert(varchar, qp.offer_discount),'') collate Latin1_General_CI_AS offer
	,ISNULL(convert(varchar, qp.multipet_discount),'') collate Latin1_General_CI_AS multipet
	,case when s.ip_address is null then 'Y' else 'N' end collate Latin1_General_CI_AS internet
	,case when d.staff = 1 then 'Y' else 'N' end [staff]
	,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end [contactable]
from uispet..quote_pet qp 
	join uispet..quote q on q.id = qp.quote_id
	join uispet..animal_type at on qp.animal_type_id = at.id
	join uispet..product_gender pg on pg.id = qp.product_gender_id
	join uispet..breed b on b.id = qp.breed_id
	left join uispetmis..policy p on qp.id = p.quote_pet_id
	left join uispet..cover c on qp.cover_id = c.id
	left join uispet..title t on t.id = q.title_id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join uispet..clinic cl on vfv_clinicid = cl.id
	left join uispet..clinic_group cg on cg.id = cl.clinic_group_id
	LEFT JOIN uispet..sales_ip_address s on s.ip_address = q.ClientIpAddress
	left join uispet..discount d on d.code = q.promo_code

where 
--q.create_timestamp > '1 mar 2009' and 
q.quote_ref like @affinity+'%'
and 
(
	1 = case when @status = '' and quote_status_id <> 3 then 1 
				when @status = 'quote' then 1 --and quote_status_id in (1,2,3,4) then 1 
				when @status = 'policy' then 0
				when @status = 'referral' and quote_status_id in (3,5,6) then 1
				when @status = 'cancelled' then 0
				when @status = 'declined' and quote_status_id = 6 then 1 
				when @status = 'lapsed' then 0
				when @status = 'renewed' then 0
	end
)
and q.create_timestamp between @from and @to
and ISNULL(ISNULL(cancellation_instruction_date, renewaldate),'2030-12-31') >= @from


UNION
----plus the policies (quote_pet_id may be null if imported)
select case when affinitycode like '123%' then 'OTT'+policynumber else policynumber end [quote_ref]
	,case 
		when p.cancelleddate < @to then 'Cancelled'
		when floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0 
			THEN 'Renewal'+convert(varchar, floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12))
		when renewaldate < @to AND renewaldate < getdate() then 'Lapsed'
		when policyinceptiondate > @to AND q.update_timestamp < getdate() then 'Pending'
		else 'Policy' end [outcome]

	,ISNULL(cl.description,'') [practice]
	,ISNULL(cg.description,'') collate Latin1_General_CI_AS [vet group]
	,ISNULL(c.description,'') [product]

	,ISNULL(case when startdate > @to then 
		convert(varchar, uispetmis.dbo.get_premium_by_date(policyid, startdate)) 
		else convert(varchar, ps.premium) end,0)
		[premium]

	,LOWER(animalpetname) [pet name]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,ISNULL(convert(varchar, q.update_timestamp, 103),'')  collate Latin1_General_CI_AS [policy sold date]
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') [inception date]

	,case when startdate > @to 
		then convert(varchar, DATEADD(yy, -floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12), startdate), 103)
	else
		convert(varchar, startdate, 103) 
	end [policy start date]

	,case when p.cancelleddate < @to then convert(varchar, p.cancelleddate, 103) else '' end [cancellation date]
	,case when p.cancellation_instruction_date < @to then convert(varchar, p.cancellation_instruction_date, 103) else '' end
	,case when p.cancellation_instruction_date < @to then ISNULL(cr.description,'') else '' end [cancel reason]

	,case when cancelleddate is null and renewaldate < @to AND renewaldate < getdate() then ISNULL(convert(varchar, p.renewaldate, 103),'') else '' end [lapse effective date]
	,case when cancelleddate is null and renewaldate < @to AND renewaldate < getdate() then ISNULL(convert(varchar, p.renewaldate, 103),'') else '' end  [lapse instruction date]


	,ISNULL(ISNULL(at.description, AnimalTypeName),'Dog') [animal]
	,p.animalsexname [gender]
	,breeddescription [breed]
	,convert(int, p.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') chip
	,ISNULL(p.colourdescription,'') colour
	,convert(varchar, p.animaldateofbirth, 103) [pet dob]
	,'' vetvisit
	,'' accident
	,ISNULL(p.exclusion,'') exclusion
	,p.title
	,ISNULL(p.firstname,'') firstname
	,ISNULL(p.lastname,'') lastname
	,'' dob

	,case when LEN(p.Address1)> 4 then p.address1 else p.address1+' '+p.address2 end address1
	,case when LEN(p.Address1)> 4 then p.address2 else ' ' end address2
	,p.address3
	,p.address4
	,p.address5
	,p.postcode
	,ISNULL(p.telephone1,'') telephone
	,ISNULL(email_address,'') emailaddress

	,case when convert(float, ISNULL(prem.offer_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.offer_discount),'') end offer
	,case when convert(float, ISNULL(prem.multipet_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.multipet_discount),'') end multipet
	,case when ISNULL(q.ClientIpAddress,'') in (select ip_address from uispet..sales_ip_address) then 'N' else 'Y' end collate Latin1_General_CI_AS internet
	,case when d.staff = 1 then 'Y' else 'N' end [staff]
	,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end [contactable]
from uispetmis..policy p
	left join uispet..animal_type at on p.animal_type_id = at.id
	left join uispet..cover c on p.cover_id = c.id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join uispet..clinic cl on p.clinic_id = cl.id
	left join uispet..clinic_group cg on cg.id = cl.clinic_group_id
	left join uispet..quote_pet qp on p.quote_pet_id = qp.id
	left join uispet..quote q on q.id = qp.quote_id
	left JOIN uispetmis..LATEST_PREMIUM prem on p.policyid = prem.policy_id
	--left join uispetmis..payment_detail pd on p.policyid = pd.policy_id
	left join uispetmis.dbo.premium_snapshot(@to) ps on ps.policy_id = p.policyid
	left join uispet..discount d on d.code = q.promo_code

where p.affinitycode like @affinity+'%'
and p.affinitycode not like 'CC%' -- exclude caravan club
and 
(
	1 = case when @status = '' 
		and (
		--policies sold
		(ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND q.update_timestamp between @from and DATEADD(n,-1,@to))	--policyinceptiondate < DATEADD(n,-1,@to) AND renewaldate >= @from)
		--cancelled
		OR (cancellation_instruction_date between @from and DATEADD(n,-1,@to))
		--renewals
		OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND startdate between @from and DATEADD(n,-1,@to)) and policyinceptiondate < startdate --renewaldate between @from and DATEADD(n,-1,@to))
		--lapsed
		OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND renewaldate between @from and DATEADD(n,-1,@to)) 
		--and policyinceptiondate < startdate --renewaldate between @from and DATEADD(n,-1,@to))

		--OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0)
		--OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND policyinceptiondate > @to and q.update_timestamp <= @to)
		) 
		THEN 1
				when @status = 'quote' then 0
				when @status = 'policy' and policyinceptiondate < @to and cancelleddate IS NULL and renewaldate >= @from then 1
				when @status = 'referral' then 0
				when @status = 'cancelled' and cancellation_instruction_date between @from and DATEADD(n,-1,@to) then 1
				when @status = 'declined' then 0
				when @status = 'lapsed' and ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND renewaldate between @from and DATEADD(n,-1,@to) then 1
				when @status = 'renewed' AND ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND startdate between @from and DATEADD(n,-1,@to) and policyinceptiondate < startdate then 1
	end
)

order by 2

SET NOCOUNT OFF

END





















GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discountX_prc]'
GO
CREATE PROCEDURE [dbo].[discountX_prc]  (
	@id int
	,@expire_date smalldatetime = null
)

AS

SET NOCOUNT ON

	if @expire_date is null
		set @expire_date = GETDATE();

	update discount set end_date = @expire_date where id = @id


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_BFG1]'
GO
CREATE TABLE [dbo].[pricing_matrix_BFG1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[area_1] [decimal] (10, 2) NOT NULL,
[area_2] [decimal] (10, 2) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_BFG1] on [dbo].[pricing_matrix_BFG1]'
GO
ALTER TABLE [dbo].[pricing_matrix_BFG1] ADD CONSTRAINT [PK_pricing_matrix_BFG1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_BFG]'
GO
CREATE TABLE [dbo].[pricing_matrix_BFG]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[area_1] [decimal] (10, 2) NOT NULL,
[area_2] [decimal] (10, 2) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_BFG] on [dbo].[pricing_matrix_BFG]'
GO
ALTER TABLE [dbo].[pricing_matrix_BFG] ADD CONSTRAINT [PK_pricing_matrix_BFG] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quick_quote_calculate_BFG_U_prc]'
GO


CREATE PROCEDURE [dbo].[quick_quote_calculate_BFG_U_prc]
	@clinic_id int,
	@product_id int,
	@quote_pet_id int,
	@pet_count int,
	@promo_code nvarchar(10),
	@policy_start_date smalldatetime,
	@name varchar(32),
	@pet_DOB smalldatetime,
	@animal_type_id int,
	@cover_id int,
	@offer_discount decimal(5,2),
	@multipet_discount decimal(5,2),
	@promo_discount decimal(5,2),
	@ipt_factor float,
	@age_rating varchar(16),
	@breed_rating int,
	@sel_cover_only bit
AS
BEGIN
	--create a table variable with all the discounts for this product and the supplied promo code
	declare @promo table (cover_id int, discount decimal(5,2), staff bit)
	insert into @promo
	select cd.cover_id, discount, staff
	from cover_discount cd 
		join discount d on d.id = cd.discount_id
	where d.code = @promo_code
	AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))

	declare @area_id int;
	select @area_id = area_id from clinic where id=@clinic_id and product_id = @product_id
	
	
	/* INTRODUCTORY DISCOUNT */
	declare @coverIntroductoryDiscount table(discount decimal(10,2), cover_id int);
	--check if its not renewal
	if not exists(select * from uispetmis..POLICY p where p.quote_pet_id = @quote_pet_id)
	begin
		insert into @coverIntroductoryDiscount (discount, cover_id)
		select discount, cover_id
		from uispet..get_introductory_discount_by_product(GETDATE(), @product_id);
	end
		
	--OK, have all the factors, let's look up the value
	if @policy_start_date >= '2014-07-01'
	begin
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			,c.id cover_id,
			convert(decimal(10,2), 
				case @area_id when 1 then [area_1] when 2 then [area_2] end
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM pricing_matrix_bfg1
			JOIN cover c ON cover_id = c.id
			LEFT JOIN @promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE animal_type_id = @animal_type_id 
		AND	age	= @age_rating 
		AND	ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and c.product_id = @product_id
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	end
	else
	begin
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			,c.id cover_id,
			convert(decimal(10,2), 
				case @area_id when 1 then [area_1] when 2 then [area_2] end
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM pricing_matrix_bfg
			JOIN cover c ON cover_id = c.id
			LEFT JOIN @promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE animal_type_id = @animal_type_id 
		AND	age	= @age_rating 
		AND	ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and c.product_id = @product_id
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	end;
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discountU_prc]'
GO

CREATE PROCEDURE [dbo].[discountU_prc]  (
				@code nvarchar(10), --mandatory
				@description nvarchar(50) = null, 
				@staff bit = 0, 
				@affinity varchar(3), --mandatory
				@discount decimal(5,2) = null, 
				@funded_by int = null, 
				@start_date smalldatetime = null, 
				@end_date smalldatetime = null, 
				@persist_at_renewal bit = null)
AS
SET NOCOUNT ON

	if not exists (	
	select 1 from discount d 
		join cover_discount cd on d.id = cd.discount_id
		join cover c on cd.cover_id = c.id
		join product p on c.product_id = p.id		
	where code = @code
	and p.prod_prefix = @affinity
	and (CURRENT_TIMESTAMP between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))
	)
	begin
		--insert the main discount record
		insert into discount (code, description, staff, start_date, end_date) select @code, @description, @staff, @start_date, @end_date
		--now add a cover-specific record for all active covers in this product
		declare @new int
		select @new = @@IDENTITY
		insert into cover_discount (cover_id, discount_id, discount, funded_by, persist_at_renewal)
			select c.id, @new, @discount, @funded_by, @persist_at_renewal 
			from cover c join product p on c.product_id = p.id 
			where p.prod_prefix = @affinity
			and (CURRENT_TIMESTAMP between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))
		
	end
	else
	begin
		--edit the promo code configuration for all product's cover (including inactive covers)
		
		--finding the discount_id to update the details
		declare @discountId int;
		select top 1 
			@discountId= d.id
		from discount d 
			join cover_discount cd on d.id = cd.discount_id
			join cover c on cd.cover_id = c.id
			join product p on c.product_id = p.id		
		where d.code = @code
		and p.prod_prefix = @affinity;
		
		update discount
		set end_date = @end_date
		where id = @discountId;
		
		update cd
		set cd.persist_at_renewal = ISNULL(@persist_at_renewal, cd.persist_at_renewal)
		from discount d 
			join cover_discount cd on d.id = cd.discount_id
			join cover c on cd.cover_id = c.id
			join product p on c.product_id = p.id		
		where d.code = @code
		and p.prod_prefix = @affinity;
		
	end


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BlacklistedQuote]'
GO
CREATE TABLE [dbo].[BlacklistedQuote]
(
[QuoteId] [int] NOT NULL,
[UpdatedDateTime] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BlacklistedQuote] on [dbo].[BlacklistedQuote]'
GO
ALTER TABLE [dbo].[BlacklistedQuote] ADD CONSTRAINT [PK_BlacklistedQuote] PRIMARY KEY CLUSTERED  ([QuoteId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwWhitelistedQuote]'
GO




-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 View of whitelisted Quotes excluding those in backlist table
-- =============================================
CREATE VIEW [dbo].[vwWhitelistedQuote]
AS

	SELECT DISTINCT q.id, product_id, quote_ref, title_id, forename, surname, DOB, postcode, house_name_num, street, town, county, contact_num, email, 
	quote_status_id, q.cover_id, q.premium, q.create_timestamp, q.update_timestamp, product_excess_id, occupation_id, hear_about_us_id, other_policies, 
	motor_insurance_due, home_insurance_due, street2, policy_start_date, agree_contact, agree_terms, payment_detail_id, VFV_clinic, VFV_ClinicID, 
	ReferrerID, ClientIpAddress, NULL [schedule_doc], promo_code, email_sent, created_systuser_id, modified_systuser_id, converted_systuser_id, 
	min_cover_ordinal, opt_out_marketing, agg_remote_ref, affinity, agree_preexisting_excl, agree_waiting_excl, agree_illvac_excl, 
	agree_vicious_excl, enquiry_method_code, all_pet_names, confirm_excluded_breeds, q.annual_premium, agree_contact_phone, agree_contact_SMS, 
	agree_contact_email, agree_contact_letter, campaign_code, EmailAddressConfirmation, is_multi_pet, agree_contact_mmpportal

	FROM uispet..Quote q
	LEFT JOIN uispet..BlacklistedQuote bq ON q.id = bq.QuoteId
	WHERE bq.QuoteId IS NULL


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_decision_match_type]'
GO
CREATE TABLE [dbo].[iv_decision_match_type]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_iv_decision_match_type] on [dbo].[iv_decision_match_type]'
GO
ALTER TABLE [dbo].[iv_decision_match_type] ADD CONSTRAINT [PK_iv_decision_match_type] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_reportL_prc]'
GO




CREATE PROCEDURE [dbo].[iv_reportL_prc]
	@startDate AS DATE,
	@endDate AS DATE,
	@user_id INT = 0,
	@IncludePID BIT = 0
AS
BEGIN

DECLARE @StartTime DATETIME = GETDATE()

DECLARE @productionDate DATE =  '2015-03-09';--live
IF @startDate < @productionDate
	SET @startDate = @productionDate;

SELECT iv.[id]
      ,iv.[iv_profile_id]
      ,iv.[authentication_id]
      ,iv.[quote_id]
      ,iv.[band_text]
      ,iv.[score]
      ,iv.[forename]
      ,iv.[surname]
      ,iv.[date_of_birth]
      ,iv.[building_number]
      ,iv.[city]
      ,iv.[country]
      ,iv.[street]
      ,iv.[region]
      ,iv.[postcode]
      ,iv.[requested_date]
      ,iv.[match_level_api]
	  ,q.quote_ref 
INTO #tempRequestLog
FROM iv_request_log iv
--INNER JOIN uispet..quote q ON q.id = iv.quote_id
INNER JOIN uispet..vwWhitelistedQuote q ON q.id = iv.quote_id
WHERE iv.requested_date BETWEEN @startDate AND @endDate;

DECLARE @Rows INT = @@ROWCOUNT

	--IM-22 remove PID if @IncludePID false or user running report is MIS user
	IF (uispetmis.dbo.fnShowPID(@IncludePID, SUSER_NAME()) = 0)
	BEGIN
		UPDATE #tempRequestLog 
		SET 
		[forename] = '',
		[surname] = '',
		[date_of_birth]	= NULL,
		[building_number] = '',
		[city] = '',
		[country] = '',
		[street] = '',
		[region] = '',
		[postcode] = uispetmis.dbo.fnGetPostcodeArea(postcode)
	END
	
	SELECT * FROM #tempRequestLog 
	DROP TABLE #tempRequestLog
	

--final decision for quotes
SELECT ivRes.quote_id, q.quote_ref, ivres.band_pass, ivmt.description AS decision_type
FROM iv_decision_result ivRes
INNER JOIN iv_decision_match_type ivmt ON ivRes.iv_decision_match_type_id = ivmt.id
--INNER JOIN quote q ON ivRes.quote_id = q.id
INNER JOIN uispet..vwWhitelistedQuote q ON ivRes.quote_id = q.id
WHERE ivRes.create_timestamp BETWEEN @startDate AND @endDate
AND ivRes.id IN 
	(
	SELECT TOP 1 id
	FROM iv_decision_result iv
	WHERE iv.quote_id = ivRes.quote_id
	ORDER BY create_timestamp DESC
	)
	 
	SET @Rows = @Rows + @@ROWCOUNT

	DECLARE @ReportParameters VARCHAR(500) = '@startDate:' + ISNULL(CONVERT(VARCHAR,@startDate),'')
	+ ', @endDate:' + ISNULL(CONVERT(VARCHAR,@endDate),'')
    + ', @IncludePID:' + ISNULL(CONVERT(VARCHAR,@IncludePID),'')

	DECLARE @EndTime DATETIME = GETDATE()

	EXEC [uispetmis].[Audit].[InsReport] @ReportName='iv_reportL_prc', @ReportParameters=@ReportParameters, @UserId=@user_id, @Rows=@Rows, @StartTime = @StartTime, @EndTime = @EndTime


END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_DON1]'
GO
CREATE TABLE [dbo].[pricing_matrix_DON1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[premium] [decimal] (10, 2) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_DON1] on [dbo].[pricing_matrix_DON1]'
GO
ALTER TABLE [dbo].[pricing_matrix_DON1] ADD CONSTRAINT [PK_pricing_matrix_DON1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_DON]'
GO
CREATE TABLE [dbo].[pricing_matrix_DON]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[premium] [decimal] (10, 2) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_DON] on [dbo].[pricing_matrix_DON]'
GO
ALTER TABLE [dbo].[pricing_matrix_DON] ADD CONSTRAINT [PK_pricing_matrix_DON] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quick_quote_calculate_DON_U_prc]'
GO


CREATE PROCEDURE [dbo].[quick_quote_calculate_DON_U_prc] 
	@product_id int,
	@quote_pet_id int,
	@pet_count int,
	@promo_code nvarchar(10),
	@policy_start_date smalldatetime,
	@name varchar(32),
	@pet_DOB smalldatetime,
	@animal_type_id int,
	@cover_id int,
	@offer_discount decimal(5,2),
	@multipet_discount decimal(5,2),
	@promo_discount decimal(5,2),
	@ipt_factor float,
	@age_rating varchar(16),
	@breed_rating int,
	@min_cover_ordinal int,
	@sel_cover_only bit
AS
BEGIN
	--create a table variable with all the discounts for this product and the supplied promo code
	declare @promo table (cover_id int, discount decimal(5,2), staff bit)
	insert into @promo
	select cd.cover_id, discount, staff
	from cover_discount cd 
		join discount d on d.id = cd.discount_id
	where d.code = @promo_code
	AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))

	
	/* INTRODUCTORY DISCOUNT */
	declare @coverIntroductoryDiscount table(discount decimal(10,2), cover_id int);
	--check if its not renewal
	if not exists(select * from uispetmis..POLICY p where p.quote_pet_id = @quote_pet_id)
	begin
		insert into @coverIntroductoryDiscount (discount, cover_id)
		select discount, cover_id
		from uispet..get_introductory_discount_by_product(GETDATE(), @product_id);
	end
	
	--OK, have all the factors, let's look up the value
	if @policy_start_date >= '2014-12-08'
	begin
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			,c.id cover_id,
			convert(decimal(10,2), 
				premium
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM	pricing_matrix_don
			JOIN	cover c ON cover_id = c.id
			LEFT JOIN	@promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE	animal_type_id = @animal_type_id 
		AND		age	= @age_rating 
		AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and c.ordinal >= @min_cover_ordinal
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	END
	ELSE
	BEGIN
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			,c.id cover_id,
			convert(decimal(10,2), 
				premium
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM	pricing_matrix_don1
			JOIN	cover c ON cover_id = c.id
			LEFT JOIN	@promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE	animal_type_id = @animal_type_id 
		AND		age	= @age_rating 
		AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and c.ordinal >= @min_cover_ordinal
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	END;
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_blacklist_ip_address]'
GO
CREATE TABLE [dbo].[iv_blacklist_ip_address]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ip_address] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[affinity_code] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[quote_occurrences] [int] NULL,
[create_timestamp] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_quarantine_prc]'
GO
CREATE PROCEDURE [dbo].[quote_quarantine_prc] as 
begin

SELECT     ClientIpAddress, quote_ref, left(quote_ref,3) aff
into #spam_quotes
FROM quote
WHERE 
clientipaddress not in (select ip_address from sales_ip_address)
and create_timestamp > dateadd(d, -1, getdate())
and quote_status_id !=3 
and created_systuser_id IS NULL;

select   ClientIpAddress, left(quote_ref,3) aff, COUNT(1) AS DisgressionCount
into #tw
from #spam_quotes
GROUP BY ClientIpAddress, left(quote_ref,3)
having count(1) > 10

--deleting all records in the blacklist
DELETE FROM iv_blacklist_ip_address;

--inserting those ip address into our blacklist for identity validation
INSERT INTO iv_blacklist_ip_address (ip_address, affinity_code, quote_occurrences, create_timestamp)
SELECT ClientIpAddress, aff, DisgressionCount, GETDATE()
FROM #tw;

--changing the quote status to "Suspicious Activity"
UPDATE q
SET q.quote_status_id = 9
FROM quote q
INNER JOIN #spam_quotes sq ON sq.quote_ref = q.quote_ref
WHERE sq.quote_ref = q.quote_ref
AND q.ClientIpAddress IN (SELECT ClientIpAddress FROM #tw WHERE aff = left(q.quote_ref,3) );

drop table #tw
drop table #spam_quotes

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_profile_product]'
GO
CREATE TABLE [dbo].[iv_profile_product]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[iv_profile_id] [int] NOT NULL,
[search_history_days] [int] NULL,
[inactive_date] [date] NULL,
[acceptable_fail_result_days] [int] NOT NULL CONSTRAINT [DF__iv_profil__accep__3C2ACFCE] DEFAULT ((7))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_iv_profile_product] on [dbo].[iv_profile_product]'
GO
ALTER TABLE [dbo].[iv_profile_product] ADD CONSTRAINT [PK_iv_profile_product] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_profile]'
GO
CREATE TABLE [dbo].[iv_profile]
(
[id] [int] NOT NULL IDENTITY(5, 5),
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[profile_id_guid] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[enc_api_password] [varbinary] (256) NULL,
[api_company] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[api_url] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[auto_reset_password_days] [int] NOT NULL CONSTRAINT [DF__iv_profil__auto___60F24029] DEFAULT ((365)),
[last_password_reset_date] [datetime] NULL,
[api_username] [varchar] (256) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_iv_profile] on [dbo].[iv_profile]'
GO
ALTER TABLE [dbo].[iv_profile] ADD CONSTRAINT [PK_iv_profile] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_profile_productS_prc]'
GO

CREATE PROCEDURE [dbo].[iv_profile_productS_prc]  
(
	@product_id INT
)
AS
SET NOCOUNT ON

	OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
	DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;


	select top 1 
	ipp.product_id, 
	ip.profile_id_guid,
	ipp.search_history_days,
	ipp.acceptable_fail_result_days,
	ip.id as profile_id,
	ip.auto_reset_password_days,
	ip.last_password_reset_date,
	ip.api_url,
	ip.api_company,
	ip.api_username,
	CONVERT(varchar, DecryptByKey(ip.enc_api_password)) AS api_password
		from iv_profile_product ipp
		INNER join iv_profile ip on ip.id = ipp.iv_profile_id
	where ipp.product_id = @product_id
	and ipp.inactive_date is null;

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_matrix]'
GO
CREATE TABLE [dbo].[renewal_matrix]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[recurrence] [int] NULL,
[value] [int] NULL,
[claims] [int] NULL,
[loading] [decimal] (5, 2) NULL,
[underwriter] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[product_id] [int] NULL,
[effective_date] [date] NULL CONSTRAINT [DF__renewal_m__effec__5BED93EA] DEFAULT ('2014-01-01'),
[inactive_date] [date] NULL CONSTRAINT [DF__renewal_m__inact__5CE1B823] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_renewal_matrix] on [dbo].[renewal_matrix]'
GO
ALTER TABLE [dbo].[renewal_matrix] ADD CONSTRAINT [PK_renewal_matrix] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_cover]'
GO
CREATE TABLE [dbo].[renewal_cover]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[old_cover_id] [int] NULL,
[new_cover_id] [int] NULL,
[valid_from] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_renewal_cover] on [dbo].[renewal_cover]'
GO
ALTER TABLE [dbo].[renewal_cover] ADD CONSTRAINT [PK_renewal_cover] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_weighting_test]'
GO

create FUNCTION [dbo].[renewal_weighting_test](@policy_id int) 
RETURNS @weight TABLE (policy_id int, claims_count int, claims_value money, recurrence int, loading decimal(5,2), loading_standard decimal(5,2))
AS
BEGIN

--******************* TCF override *******************
-- if there is a pending renewal, don't recalculate the loading, use the loading off the renewal offer
--or if not started yet return nothings for upgrade before inception
--if exists (select 1 from uispetmis..PREMIUM where policy_id = @policy_id
--	and renewal in (1,2)
--	and effective_date > GETDATE())
--begin
--	insert into @weight
--	select TOP 1 policy_id, claims_count, claims_value, claims_recurrence recurrence, loading, loading_standard
--	from uispetmis..PREMIUM
--	where policy_id = @policy_id
--	and renewal in (1,2)
--	--and contacted = 1
--	and effective_date > GETDATE()
--	order by effective_date desc
--	return
--end
if exists (select 1 from uispetmis..POLICY where PolicyID = @policy_id and PolicyInceptionDate > GETDATE())
begin
	insert into @weight
	select @policy_id, 0, 0, 1, 1, 1 
return;
end
--******************* TCF override *******************

declare @claims_count int, @claims_value int;
declare @max_recurrence int;
declare @loading decimal(5,2);
declare @loading_standard decimal(5,2);
declare @prev_renewal_loading decimal(5,2);
declare @prev_renewal_claims_count int;
declare @underwriter varchar(3);
declare @product_id int;

declare @previous_calculation_date date --calcs done before renewal so need to search for claims that may have happened after previous year calculation
	, @last_renewal_premium_id int
	, @effective_date date
	, @renewal_date date;

select @loading_standard = 1, @loading = 1

select @last_renewal_premium_id = pre.id, 
@prev_renewal_loading = ISNULL(pre.loading,1), -- Obtain loading from table based on experience in current period
@prev_renewal_claims_count = ISNULL(pre.claims_count,0)
from uispetmis..policy p
join uispetmis..premium pre on pre.id = uispetmis.dbo.[get_premium_id_by_date](@policy_id, p.startdate) where p.policyid = @policy_id

select @previous_calculation_date = calculated_date 
	,@effective_date = effective_date
from uispetmis..PREMIUM where id = @last_renewal_premium_id;

declare @year_offset int; set @year_offset = 0;
if(DATEADD(DAY, 15, @effective_date) > GETDATE())
	set @year_offset = -1;

-- first find out the underwriter to we can apply the appropriate rules
select @underwriter = c.underwriter,
	@product_id = c.product_id,
	@renewal_date = p.renewaldate
	from uispetmis..policy p 
	left join renewal_cover rc on rc.old_cover_id = p.cover_id and rc.valid_from <= p.renewaldate --check for migration to new underwriter
	join cover c on ISNULL(rc.new_cover_id,p.cover_id) = c.id
	where p.policyid = @policy_id
	
select --sum(case when previousclaimid = 0 then 1 else 0 end) claims,
	@claims_count = count(distinct uispetmis.dbo.get_master_claim_id(c.claimid))
	--,sum(case when previousclaimid = 0 then total else 0 end) total_non_prev	
	--,sum(case when previousclaimid > 0 then total else 0 end) total_prev
	,@claims_value = sum(ISNULL(total,0))
	,@max_recurrence = max(ISNULL(case lcs.Recurrence when 'yes' then 3 when 'possibly' then 2 when 'no' then 1 else 1 end,1))
	from uispetmis..claim c
		join uispetmis..policy p on p.policyid = c.policyid
		join uispetmis..status s on s.statusid = c.statusid
		left join uispetmis..LOSS_CODE_SPECIFIC lcs on lcs.specificid = c.LossCodeSpecificID
	where c.policyid = @policy_id
--	and c.treatmentdatefrom >= ISNULL(@previous_calculation_date, p.startdate)
--	and c.treatmentdatefrom < p.renewaldate
	and c.treatmentdatefrom >= ISNULL(DATEADD(YEAR, @year_offset, @previous_calculation_date), DATEADD(YEAR, @year_offset, p.startdate))
	and c.treatmentdatefrom < DATEADD(YEAR, @year_offset, p.renewaldate)
	and s.Renewal = 1
	--and s.description = 'Ready to Process'

IF @underwriter IN('UIC','HAN','L&G')
BEGIN

	if(@underwriter in ('HAN','L&G')) select @underwriter = 'UIC' -- duplicate UIC loadings

	select top 1 @loading_standard = loading
		from renewal_matrix where recurrence = ISNULL(@max_recurrence,1)
		and claims = case when @claims_count > 3 then 3 else @claims_count end
		and value < ISNULL(@claims_value,1)
		and underwriter = @underwriter
		and (ISNULL(product_id,0) = @product_id or product_id IS NULL)
		and @renewal_date BETWEEN effective_date AND ISNULL(inactive_date,'2050-01-01')
		order by value desc
	
	declare @claims_last_two_years int
	set @claims_last_two_years = @claims_count + @prev_renewal_claims_count
		
	IF @loading_standard <= @prev_renewal_loading AND @claims_last_two_years > 0 
	BEGIN
		SELECT @loading = (@loading_standard + @prev_renewal_loading) / 2.0 --If loading below prior year use prior + current year divided by 2 (if two years claims free after loading aplied loading = 0%)
	END	
	ELSE
	BEGIN
		SELECT @loading = @loading_standard
	END
END
ELSE IF @underwriter = 'GLK'
BEGIN
	select top 1 @loading_standard = loading
		from renewal_matrix where recurrence = 0 -- GLK don't consider claims recurrence
		and claims = case when @claims_count > 6 then 6 else @claims_count end
		and value = 0  -- GLK do not consider claim value
		and underwriter = @underwriter
		and (ISNULL(product_id,0) = @product_id or product_id IS NULL)
		and @renewal_date BETWEEN effective_date AND ISNULL(inactive_date,'2050-01-01')
		order by value desc
		
	SELECT @loading = @loading_standard
END

insert into @weight
select @policy_id, @claims_count claims_count, ISNULL(@claims_value,0) claims_value, ISNULL(@max_recurrence,0) max_recurrence, @loading loading, @loading_standard loading_standard


--select lcs.recurrence, c.claimid, c.LossCodeSpecificID,s.description from claim c 
--join status s on s.statusid = c.statusid 
--left join LOSS_CODE_SPECIFIC lcs on lcs.specificid = c.LossCodeSpecificID
--where policyid = @policy_id --order by statusid;

return

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[affinity_activeL_prc]'
GO



CREATE PROCEDURE [dbo].[affinity_activeL_prc]

AS

SET NOCOUNT ON

	SELECT 	distinct p.prod_prefix id, p.prod_prefix description
	FROM product p 
		join cover c on c.product_id = p.id
	where CURRENT_TIMESTAMP between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01')
	order by description
	

SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_request_logI_prc]'
GO

CREATE PROCEDURE [dbo].[iv_request_logI_prc]  
(
	@profile_id int,
	@quote_id INT,
	@authentication_id varchar(256),
	@band_text varchar(256),
	@score int = null,
	@forename varchar(256),
	@surname varchar(256),
	@date_of_birth date,
	@building_number varchar(256),
	@city varchar(256),
	@country varchar(256),
	@street varchar(256),
	@region varchar(256),
	@postcode varchar(256),
	@requested_date datetime = NULL,
	@match_level_api VARCHAR(200) = ''
)
AS
SET NOCOUNT ON

if @requested_date is null
	set @requested_date = GETDATE()


INSERT INTO iv_request_log 
	(iv_profile_id, 
	quote_id, 
	authentication_id, 
	band_text, 
	score, 
	forename, 
	surname, 
	date_of_birth, 
	building_number, 
	city, 
	country, 
	street, 
	region, 
	postcode, 
	requested_date,
	match_level_api) 
VALUES(
 @profile_id,
 @quote_id,
 @authentication_id,
 @band_text,
 @score,
 @forename,
 @surname,
 @date_of_birth,
 @building_number,
 @city,
 @country,
 @street,
 @region,
 @postcode,
 @requested_date,
 @match_level_api
);

select SCOPE_IDENTITY();

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_request_logS_prc]'
GO

CREATE PROCEDURE [dbo].[iv_request_logS_prc]  
(
	@forename varchar(256),
	@surname varchar(256),
	@date_of_birth date,
	@building_number varchar(256),
	@city varchar(256),
	@country varchar(256),
	@street varchar(256),
	@region varchar(256),
	@postcode varchar(256),
	@days_to_search int,
	@profile_id int,
	@acceptableFailResultDays int = NULL,
	@quote_id INT = NULL
)
AS
SET NOCOUNT ON

	declare @requestDateMin as date;
	set @requestDateMin = DATEADD(dd , -@days_to_search, GETDATE());
	
	declare @acceptableFailDateMin as date;
	if @acceptableFailResultDays is not null
		set @acceptableFailDateMin = DATEADD(dd , -@acceptableFailResultDays, GETDATE());
	else
		set @acceptableFailDateMin = '1900-01-01';

	select top 1
		*
	from iv_request_log l
	where 
	l.forename = @forename
	and l.surname = @surname
	and l.date_of_birth = @date_of_birth
	and l.postcode = @postcode
	and @requestDateMin <= l.requested_date
	--and l.iv_profile_id = @profile_id --this history will be across all profiles!
	--if the result is Fail -> we need to look how many days should this result be considered. If it is more recent than the provided date, then consider it!
	and (
		1 = (case when l.band_text <> 'Pass' and @acceptableFailDateMin <= l.requested_date then 1 else 0 end)
		or
		1 = (case when l.band_text = 'Pass' then 1 else 0 end)
		)
	order by l.requested_date desc;


	select top 1 
		p.PolicyId as PolicyId
	from 
	uispetmis..POLICY p
	where p.FirstName = @forename
	and p.LastName = @surname
	and p.PostCode = @postcode
	and p.CancelledDate is null; --we need to discard cancelled policies. Lapsed ones are still good for us

	SELECT 
		CASE WHEN EXISTS (SELECT * FROM iv_blacklist_ip_address ip where ip.ip_address = q.ClientIpAddress AND ip.affinity_code = left(q.quote_ref,3)) THEN 1 ELSE 0 end AS is_suspicious_ip
	FROM dbo.quote q
	WHERE q.id = @quote_id;

	--real time validation for suspicious quotes
	--if the same ip address sent more then 5 request in the last day, then it may be a suspicous quote
	select  Count(1)  call_validation_service
	from quote q
	where q.id = @quote_id
	and 
	(
		(q.ClientIpAddress is null and 1=1)
		or 
		(
			q.ClientIpAddress is not null 
			and 
			q.ClientIpAddress not in
			(
				select q.ClientIpAddress--, left(q.quote_ref,3) as affinity, count(1) as ocurrences
				from iv_request_log ivrl
				inner join quote q on q.id = ivrl.quote_id
				where q.ClientIpAddress is not null 
				and q.ClientIpAddress not in( select * from sales_ip_address)
				and ivrl.requested_date > dateadd(dd, -1, getdate())
				group by q.ClientIpAddress, left(q.quote_ref,3)
				having count(1) >= 5
			)
		)
	)

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[GetVoucher]'
GO
CREATE PROCEDURE [Voucher].[GetVoucher] 
		@id INT 
		AS 
		SET NOCOUNT ON;
		SELECT [Id], [CampaignId], [Code], [Created], [Amount], [ValidFrom], [ExpireAfter]
		FROM Voucher.Voucher
		WHERE Id = @id

		SELECT [Id], [Affinity], [Description], [Created], [StartOn], [EndAfter]
		FROM Voucher.Campaign
		WHERE Id = (SELECT CampaignId FROM Voucher.Voucher WHERE Id = @id)

		SELECT [Guid], [VoucherId], [Created], [EntityType], [EntityIdentifier]
		FROM Voucher.Allocation
		WHERE VoucherId = @id

		SELECT [VoucherId], [Key], [Value]
		FROM Voucher.Property
		WHERE VoucherId = @id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[hear_about_us]'
GO
CREATE TABLE [dbo].[hear_about_us]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[product_id] [int] NULL,
[scope] [tinyint] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__hear_about_us__245D67DE] on [dbo].[hear_about_us]'
GO
ALTER TABLE [dbo].[hear_about_us] ADD CONSTRAINT [PK__hear_about_us__245D67DE] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[YDAYSDataFeedprc]'
GO


CREATE PROCEDURE [dbo].[YDAYSDataFeedprc] @rundate DATE = NULL, @fromdate DATE = NULL
AS

/*
T. Pullen 29.10.2015 This procedure generates data for Kwik Fit Bespoke feed of unconverted quotes.
T. Heath-Smith 03.03.2016 Completed all the missing development based on feedback from client
T. Heath-Smith 18.07.2016 further tweaks from client
S. Griffin     17.08.2016 replace . in TITLE and GENDER, 
						  replaced 01000 000 000 in F_PINS_TEL
						  Added in F_PINS_CAT_DOG and F_PINS_ADDPET(x)_CAT_DOG
T. Heath-Smith 05.02.2017 Fixed table variable to accomodate change in transactions report
*/

SET NOCOUNT ON
SET DATEFORMAT DMY

IF @rundate IS NULL
BEGIN
	DECLARE @now date
	SELECT @now = CONVERT(date, CURRENT_TIMESTAMP)
	SET @rundate = DATEADD(d, -1, @now);
END
ELSE 
BEGIN 
	SET @rundate = DATEADD(d, -1, @rundate);
END

IF @fromdate IS NULL
	SELECT	@fromdate = @rundate;

SELECT
q.id,
	 q.quote_ref						AS		'CUSTREFID'
	,q.email							AS		'EMAIL'
	,ISNULL(REPLACE(t.[description],'.',''),'')			AS		'TITLE'
	,q.forename							AS		'FIRSTNAME'
	,q.surname							AS		'LASTNAME'
	,ISNULL(CONVERT(varchar, q.DOB, 103), '')				AS		'DOB'
	,CASE WHEN REPLACE(t.[description],'.','') IN ('Miss', 'Mrs', 'Ms') THEN 'Female' WHEN REPLACE(t.[description],'.','') IN ('Dr', 'Prof') THEN 'Unknown' ELSE 'Male' END AS		'GENDER'
	,q.[house_name_num] + ' ' +  q.[street]	AS		'ADDRESS1'
	,q.street2							AS		'ADDRESS2'
	,q.[town]							AS		'ADDRESS3'
	,q.[county]							AS		'ADDRESS4'
	,q.postcode							AS		'POSTCODE'
	,REPLACE(q.contact_num,'01000 000 000','') AS 'F_PINS_TEL' 
	,ISNULL(q.promo_code,'')						AS		'F_PINS_PROMOCODE'
	,ISNULL(h.[description], '')					AS		'F_PINS_HOWHEAR'
	,ISNULL(ISNULL(ac.[description], c.[description]), 'Not selected')	AS		'F_PINS_LEVELOFCOVER'
	,qp.pet_name						AS		'F_PINS_PET_NAME'
	,CONVERT(VARCHAR, qp.pet_DOB, 103)	AS		'F_PINS_PET_DOB'
	,at.description						AS		'F_PINS_CAT_DOG'
	,pg.[gender_desc]					AS		'F_PINS_PET_GENDER'
	,CASE qp.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END				AS		'F_PINS_SPAYED_NEUTERED'
	,bc.[description]					AS		'F_PINS_BREED_CATEGORY'
	,b.[description]					AS		'F_PINS_BREED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_NAME'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_DOB'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_CAT_DOG'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_GENDER'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_SPAYED_NEUTERED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_BREED_CATEGORY'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_BREED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_NAME'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_DOB'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_CAT_DOG'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_GENDER'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_SPAYED_NEUTERED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_BREED_CATEGORY'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_BREED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_NAME'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_DOB'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_CAT_DOG'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_GENDER'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_SPAYED_NEUTERED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_BREED_CATEGORY'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_BREED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_NAME'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_DOB'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_CAT_DOG'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_GENDER'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_SPAYED_NEUTERED'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_BREED_CATEGORY'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_BREED'
	,CASE ISNULL(q.[agree_contact], 0)
			WHEN 1 THEN 'No' 
			ELSE 'Yes' END				AS		'F_PINS_MARKETING_OPTIN'
	,q.quote_ref						AS		'F_PINS_QUOTEREF'
	,CONVERT(varchar(50), '')				AS		'F_PINS_LEVELOFCOVER_ADDPET1'
	,CONVERT(varchar(50), '')				AS		'F_PINS_LEVELOFCOVER_ADDPET2'
	,CONVERT(varchar(50), '')				AS		'F_PINS_LEVELOFCOVER_ADDPET3'
	,CONVERT(varchar(50), '')				AS		'F_PINS_LEVELOFCOVER_ADDPET4'
	,ISNULL(q.premium,'')							AS		'F_PINS_TOTAL_MONTHLY'
	,ISNULL(q.premium * 12, '')						AS		'F_PINS_TOTAL_ANNUALLY'
	,ISNULL(qp.purchase_price, '')					AS		'F_PINS_PETPRICE'
	,ISNULL(qp.[colour],'')						AS		'F_PINS_PET_COLOUR'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_PRICE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET1_COLOUR'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_PRICE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET2_COLOUR'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_PRICE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET3_COLOUR'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_PRICE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_ADDPET4_COLOUR'
	,CASE WHEN NULLIF(q.motor_insurance_due,'0') IS NULL THEN '' ELSE LEFT(q.motor_insurance_due,3) END				AS		'F_PINS_MOTOR_RENEWAL'
	,CASE WHEN NULLIF(q.home_insurance_due,'0') IS NULL THEN '' ELSE LEFT(q.home_insurance_due,3) END				AS		'F_PINS_HOME_RENEWAL'
	,CONVERT(varchar(50), '')				AS		'F_PINS_POLICYREF'
	,CONVERT(VARCHAR, q.policy_start_date, 103)				AS		'F_PINS_STARTDATE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_CANCELDATE'
	,CONVERT(varchar(50), '')				AS		'F_PINS_RENEWALDATE'
	,(select count(1) from quote_pet where quote_id = q.id)	AS		'F_NUMBER_OF_PETS'
	,ISNULL( qp.premium, '')						AS		'F_PINS_PETPREMIUM'
	,CONVERT(varchar(50), '')				AS		'F_PINS_BOUGHTONLINE'
	,(select underwriter from cover where id = ISNULL(qp.cover_id, (SELECT TOP 1 id FROM cover WHERE product_id = 18 ORDER BY 1 DESC)))	AS		'F_PINS_UNDERWRITER'
	,CONVERT(varchar(50), '')		AS		'F_PINS_TOTAL_XS'
	,CONVERT(varchar(50), '')		AS		'F_PINS_PAYTYPE'
	,CONVERT(varchar(50), '')		AS		'F_PINS_INSTAL_DATE'
	,CONVERT(varchar(50), '')		AS		'F_PINS_INSTAL_VALUE'
	,CONVERT(varchar, q.create_timestamp, 103)	AS		'F_PINS_TRANSACTION_DATE'
	,CASE WHEN l.id IS NOT NULL then 'Outbound' WHEN q.created_systuser_id IS NOT NULL THEN 'Inbound' WHEN q.ReferrerID in ('AGGCTM','CONFIDOL','GOCOMP','AGGQZONE','MSTUDY') then 'Web - Aggregator' else 'Web - Direct' end AS 'F_PINS_SOURCE'
	,CASE q.ReferrerID WHEN 'AGGCTM' THEN 'Compare The Market' 
			WHEN 'CONFIDOL' THEN 'Confused'
			WHEN 'GOCOMP' THEN 'Go Compare'
			WHEN 'AGGQZONE' THEN 'Quote Zone'
			WHEN 'MSTUDY' THEN 'GoGetIt'
			ELSE 'Web - Direct' END	AS 'F_PINS_CAMPAIGN_CODE'
	,ISNULL(h.[description], '')		AS		'F_PINS_SOURCE_OF_ENQUIRY'
	,CONVERT(varchar(50), '')			AS		'F_PINS_POLICYTYPE'
	,CONVERT(varchar(50), 'Sale')		AS		'F_PINS_TRANSACTION'
	,CONVERT(varchar(50), 'Live Quote')	AS		'F_PINS_POLICY_STATUS'
	,CONVERT(varchar(50), 'Quote')		AS		'F_PINS_QUOTE_OR_POLICY'
	,CASE WHEN q.ReferrerID in ('AGGCTM','CONFIDOL','GOCOMP','AGGQZONE','MSTUDY') THEN 'Cover Details' ELSE 'Pet Details' END							AS		'F_PINS_FIRSTPAGE'
	,CASE when q.quote_status_id  = 1 and q.premium is null then 'Cover Details'
			when q.quote_status_id  = 1 and q.DOB is null then 'Your Details'
			when q.quote_status_id  = 1 then 'Your Quote'
			when q.quote_status_id  = 2 then 'Payment' end AS		'F_PINS_LASTPAGESEEN'
		,qp.id as last_qp_id
		,q.id quote_id
into #yday
FROM		Quote q
	LEFT JOIN	title t
		ON q.title_id = t.id
	LEFT JOIN hear_about_us h
		ON q.hear_about_us_id = h.id
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
	LEFT JOIN product_gender pg
		ON qp.product_gender_id = pg.id
	LEFT JOIN cover c
		ON q.cover_id = c.id
	LEFT JOIN cover ac
		ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b
		ON qp.breed_id = b.id
	LEFT JOIN breed_category bc
		ON b.breed_category_id = bc.id
	LEFT JOIN lead l 
		ON l.quote_id = q.id
	LEFT JOIN animal_type at 
		ON qp.animal_type_id = at.id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 --Kwik Fit Pet Insurance
and qp.id in 
(select MIN(qp.id) FROM			Quote q
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 
group by q.id
--having MIN(qp.id) != MAX(qp.id)
)
order by 1

-- now add any second pets --
update #yday
	set F_PINS_ADDPET1_NAME = qp.pet_name
		,F_PINS_ADDPET1_DOB = CONVERT(VARCHAR, qp.pet_DOB, 103)
		,F_PINS_ADDPET1_CAT_DOG = at.description
		,F_PINS_ADDPET1_GENDER = pg.[gender_desc]
		,F_PINS_ADDPET1_SPAYED_NEUTERED = CASE qp.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END
		,F_PINS_ADDPET1_BREED_CATEGORY = bc.[description]
		,F_PINS_ADDPET1_BREED = b.[description]
		,F_PINS_ADDPET1_PRICE = ISNULL(qp.purchase_price,'')
		,F_PINS_ADDPET1_COLOUR = qp.colour
		,F_PINS_LEVELOFCOVER_ADDPET1 = ISNULL(ISNULL(ac.[description], c.[description]), 'Not selected')
		,last_qp_id = qp.id
FROM #yday ths
	LEFT JOIN Quote q 
		ON q.id = ths.quote_id
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
	LEFT JOIN product_gender pg
		ON qp.product_gender_id = pg.id
	LEFT JOIN cover c
		ON q.cover_id = c.id
	LEFT JOIN cover ac
		ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b
		ON qp.breed_id = b.id
	LEFT JOIN breed_category bc
		ON b.breed_category_id = bc.id
	LEFT JOIN animal_type at 
		ON qp.animal_type_id = at.id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 --Kwik Fit Pet Insurance
and qp.id in (select top 1 qp.id from quote_pet qp where quote_id = ths.quote_id and id > ths.last_qp_id)

-- now add any third pets --
update #yday
	set F_PINS_ADDPET2_NAME = qp.pet_name
		,F_PINS_ADDPET2_DOB = CONVERT(VARCHAR, qp.pet_DOB, 103)
		,F_PINS_ADDPET2_CAT_DOG = at.description
		,F_PINS_ADDPET2_GENDER = pg.[gender_desc]
		,F_PINS_ADDPET2_SPAYED_NEUTERED = CASE qp.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END
		,F_PINS_ADDPET2_BREED_CATEGORY = bc.[description]
		,F_PINS_ADDPET2_BREED = b.[description]
		,F_PINS_ADDPET2_PRICE = ISNULL(qp.purchase_price,'')
		,F_PINS_ADDPET2_COLOUR = qp.colour
		,F_PINS_LEVELOFCOVER_ADDPET2 = ISNULL(ISNULL(ac.[description], c.[description]), 'Not selected')
		,last_qp_id = qp.id
FROM #yday ths
	LEFT JOIN Quote q 
		ON q.id = ths.quote_id
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
	LEFT JOIN product_gender pg
		ON qp.product_gender_id = pg.id
	LEFT JOIN cover c
		ON q.cover_id = c.id
	LEFT JOIN cover ac
		ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b
		ON qp.breed_id = b.id
	LEFT JOIN breed_category bc
		ON b.breed_category_id = bc.id
	LEFT JOIN animal_type at 
		ON qp.animal_type_id = at.id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 --Kwik Fit Pet Insurance
and qp.id in (select top 1 qp.id from quote_pet qp where quote_id = ths.quote_id and id > ths.last_qp_id)

-- now add any fourth pets --
update #yday
	set F_PINS_ADDPET3_NAME = qp.pet_name
		,F_PINS_ADDPET3_DOB = CONVERT(VARCHAR, qp.pet_DOB, 103)
		,F_PINS_ADDPET3_CAT_DOG = at.description
		,F_PINS_ADDPET3_GENDER = pg.[gender_desc]
		,F_PINS_ADDPET3_SPAYED_NEUTERED = CASE qp.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END
		,F_PINS_ADDPET3_BREED_CATEGORY = bc.[description]
		,F_PINS_ADDPET3_BREED = b.[description]
		,F_PINS_ADDPET3_PRICE = ISNULL(qp.purchase_price,'')
		,F_PINS_ADDPET3_COLOUR = qp.colour
		,F_PINS_LEVELOFCOVER_ADDPET3 = ISNULL(ISNULL(ac.[description], c.[description]), 'Not selected')
		,last_qp_id = qp.id
FROM #yday ths
	LEFT JOIN Quote q 
		ON q.id = ths.quote_id
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
	LEFT JOIN product_gender pg
		ON qp.product_gender_id = pg.id
	LEFT JOIN cover c
		ON q.cover_id = c.id
	LEFT JOIN cover ac
		ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b
		ON qp.breed_id = b.id
	LEFT JOIN breed_category bc
		ON b.breed_category_id = bc.id
	LEFT JOIN animal_type at 
		ON qp.animal_type_id = at.id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 --Kwik Fit Pet Insurance
and qp.id in (select top 1 qp.id from quote_pet qp where quote_id = ths.quote_id and id > ths.last_qp_id)

-- now add any fifth pets --
update #yday
	set F_PINS_ADDPET4_NAME = qp.pet_name
		,F_PINS_ADDPET4_DOB = CONVERT(VARCHAR, qp.pet_DOB, 103)
		,F_PINS_ADDPET4_CAT_DOG = at.description
		,F_PINS_ADDPET4_GENDER = pg.[gender_desc]
		,F_PINS_ADDPET4_SPAYED_NEUTERED = CASE qp.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END
		,F_PINS_ADDPET4_BREED_CATEGORY = bc.[description]
		,F_PINS_ADDPET4_BREED = b.[description]
		,F_PINS_ADDPET4_PRICE = ISNULL(qp.purchase_price,'')
		,F_PINS_ADDPET4_COLOUR = qp.colour
		,F_PINS_LEVELOFCOVER_ADDPET3 = ISNULL(ISNULL(ac.[description], c.[description]), 'Not selected')
		,last_qp_id = qp.id
FROM #yday ths
	LEFT JOIN Quote q 
		ON q.id = ths.quote_id
	LEFT JOIN quote_pet qp
		ON q.id = qp.quote_id
	LEFT JOIN product_gender pg
		ON qp.product_gender_id = pg.id
	LEFT JOIN cover c
		ON q.cover_id = c.id
	LEFT JOIN cover ac
		ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b
		ON qp.breed_id = b.id
	LEFT JOIN breed_category bc
		ON b.breed_category_id = bc.id
	LEFT JOIN animal_type at 
		ON qp.animal_type_id = at.id
WHERE q.quote_status_id IN (1,2)
AND CONVERT(date,q.create_timestamp) between @fromdate and @rundate
AND q.product_id = 18 --Kwik Fit Pet Insurance
and qp.id in (select top 1 qp.id from quote_pet qp where quote_id = ths.quote_id and id > ths.last_qp_id)
alter table #yday drop COLUMN last_qp_id;
alter table #yday drop COLUMN quote_id;


/************* NOW ADD THE TRANSACTIONS *******************************/
declare @temp_mi_transactions
TABLE (
	[Policy ID] [INT] NOT NULL,
	[Policy Reference] [VARCHAR](32) NULL,
	[Original Quote Reference] [VARCHAR](16) NULL,
	[Transaction Date] [DATE] NULL,
	[Outcome] [VARCHAR](12) NULL,
	[Product Code] [VARCHAR](4) NULL,
	[Product] [VARCHAR](256) NOT NULL,
	[Monthly Premium] [DECIMAL](10, 2) NOT NULL,
	[Annualised Gross Premium] [DECIMAL](13, 2) NULL,
	[Annualised IPT] [DECIMAL](10, 2) NULL,
	[Annualised Helplines] [DECIMAL](10, 2) NULL,
	[Annualised Net Premium] [DECIMAL](10, 2) NULL,
	[IPT Rate] [DECIMAL](5, 1) NULL,
	[Helplines] [DECIMAL](10, 2) NULL,
	[Pet Name] [VARCHAR](60) NULL,
	[Quote Date] [DATE] NULL,
	[Quote Time] [VARCHAR](5) NOT NULL,
	[Policy Sold Date] [DATE] NULL,
	[Policy Sold Time] [VARCHAR](5) NOT NULL,
	[Inception Date] [DATE] NULL,
	[Policy Start Date] [DATE] NULL,
	[Policy End Date] [DATE] NULL,
	[Renewal Date] [DATE] NULL,
	[Cancellation Date] [VARCHAR](30) NULL,
	[Cancellation Instruction Date] [VARCHAR](30) NULL,
	[Cancel Reason] [NVARCHAR](50) NOT NULL,
	[Species] [VARCHAR](64) NOT NULL,
	[Gender] [VARCHAR](6) NULL,
	[Breed] [VARCHAR](60) NULL,
	[Breed Group] [CHAR](1) NULL,
	[Purchase Price] [INT] NULL,
	[Microchipped] [VARCHAR](3) NOT NULL,
	[Microchip] [NVARCHAR](32) NULL,
	[Neutered] [VARCHAR](10) NOT NULL,
	[Colour] [VARCHAR](60) NOT NULL,
	[Pet DOB] [DATE] NULL,
	[Vet Visit] [VARCHAR](2000) NULL,
	[Accident Details] [VARCHAR](2000) NULL,
	[Exclusion] [NVARCHAR](1000) NOT NULL,
	[Title] [VARCHAR](50) NULL,
	[FirstName] [VARCHAR](35) NOT NULL,
	[LastName] [VARCHAR](35) NOT NULL,
	[Owner DOB] [DATE] NOT NULL,
	[Address1] [VARCHAR](86) NULL,
	[Address2] [VARCHAR](35) NULL,
	[Address3] [VARCHAR](35) NULL,
	[Address4] [VARCHAR](35) NULL,
	[Address5] [VARCHAR](35) NULL,
	[Postcode] [VARCHAR](10) NULL,
	[Telephone1] [VARCHAR](15) NOT NULL,
	[Telephone2] [VARCHAR](15) NOT NULL,
	[Telephone3] [VARCHAR](15) NOT NULL,
	[Email Address] [NVARCHAR](64) NOT NULL,
	[Offer Discount] [VARCHAR](30) NOT NULL,
	[Multipet Discount] [VARCHAR](30) NOT NULL,
	[Promotion Discount] [VARCHAR](30) NOT NULL,
	[Internet] [VARCHAR](1) NOT NULL,
	[Staff] [VARCHAR](1) NOT NULL,
	[Source] [NVARCHAR](50) NOT NULL,
	[Heard About Us] [VARCHAR](64) NOT NULL,
	[Promotion Code] [NVARCHAR](50) NOT NULL,
	[Contactable] [VARCHAR](1) NOT NULL,
	[Channel] [VARCHAR](100) NULL,
	[Aggregator Reference] [NVARCHAR](50) NOT NULL,
	[Monthly Instalments] [VARCHAR](3) NOT NULL,
	[Underwriter] [VARCHAR](3) NULL,
	[Original Product Selected on Aggregator] [VARCHAR](256) NOT NULL,
	[Administrator Commission Rate] [DECIMAL](5, 2) NULL,
	[Affinity Commission Rate] [DECIMAL](5, 2) NULL,
	[Introducer Commission Rate] [DECIMAL](5, 2) NULL,
	[Underwriter Commission Rate] [DECIMAL](5, 2) NULL,
	[Administrator Commission] [DECIMAL](10, 2) NULL,
	[Affinity Commission] [DECIMAL](10, 2) NULL,
	[Introducer Commission] [DECIMAL](10, 2) NULL,
	[Underwriter Commission] [DECIMAL](10, 2) NULL,
	[TPL Commission] [DECIMAL](10, 2) NULL)
	
INSERT INTO @temp_mi_transactions
EXEC uispetmis.dbo.report_mi_transactionsL_prc @start = @fromdate, @end = @rundate, @product_id = 18--, @underwriter = 'L&G'

--now insert into the yday file
INSERT INTO #yday
           ([id]
           ,[CUSTREFID]
           ,[EMAIL]
           ,[TITLE]
           ,[FIRSTNAME]
           ,[LASTNAME]
           ,[DOB]
           ,[GENDER]
           ,[ADDRESS1]
           ,[ADDRESS2]
           ,[ADDRESS3]
           ,[ADDRESS4]
           ,[POSTCODE]
           ,[F_PINS_TEL]
           ,[F_PINS_PROMOCODE]
           ,[F_PINS_HOWHEAR]
           ,[F_PINS_LEVELOFCOVER]
           ,[F_PINS_PET_NAME]
           ,[F_PINS_PET_DOB]
		   ,[F_PINS_CAT_DOG]
           ,[F_PINS_PET_GENDER]
           ,[F_PINS_SPAYED_NEUTERED]
           ,[F_PINS_BREED_CATEGORY]
           ,[F_PINS_BREED]
           ,[F_PINS_ADDPET1_NAME]
           ,[F_PINS_ADDPET1_DOB]
		   ,[F_PINS_ADDPET1_CAT_DOG]
           ,[F_PINS_ADDPET1_GENDER]
           ,[F_PINS_ADDPET1_SPAYED_NEUTERED]
           ,[F_PINS_ADDPET1_BREED_CATEGORY]
           ,[F_PINS_ADDPET1_BREED]
           ,[F_PINS_ADDPET2_NAME]
           ,[F_PINS_ADDPET2_DOB]
		   ,[F_PINS_ADDPET2_CAT_DOG]
           ,[F_PINS_ADDPET2_GENDER]
           ,[F_PINS_ADDPET2_SPAYED_NEUTERED]
           ,[F_PINS_ADDPET2_BREED_CATEGORY]
           ,[F_PINS_ADDPET2_BREED]
           ,[F_PINS_ADDPET3_NAME]
           ,[F_PINS_ADDPET3_DOB]
		   ,[F_PINS_ADDPET3_CAT_DOG]
           ,[F_PINS_ADDPET3_GENDER]
           ,[F_PINS_ADDPET3_SPAYED_NEUTERED]
           ,[F_PINS_ADDPET3_BREED_CATEGORY]
           ,[F_PINS_ADDPET3_BREED]
           ,[F_PINS_ADDPET4_NAME]
           ,[F_PINS_ADDPET4_DOB]
		   ,[F_PINS_ADDPET4_CAT_DOG]
           ,[F_PINS_ADDPET4_GENDER]
           ,[F_PINS_ADDPET4_SPAYED_NEUTERED]
           ,[F_PINS_ADDPET4_BREED_CATEGORY]
           ,[F_PINS_ADDPET4_BREED]
           ,[F_PINS_MARKETING_OPTIN]
           ,[F_PINS_QUOTEREF]
           ,[F_PINS_LEVELOFCOVER_ADDPET1]
           ,[F_PINS_LEVELOFCOVER_ADDPET2]
           ,[F_PINS_LEVELOFCOVER_ADDPET3]
           ,[F_PINS_LEVELOFCOVER_ADDPET4]
           ,[F_PINS_TOTAL_MONTHLY]
           ,[F_PINS_TOTAL_ANNUALLY]
           ,[F_PINS_PETPRICE]
           ,[F_PINS_PET_COLOUR]
           ,[F_PINS_ADDPET1_PRICE]
           ,[F_PINS_ADDPET1_COLOUR]
           ,[F_PINS_ADDPET2_PRICE]
           ,[F_PINS_ADDPET2_COLOUR]
           ,[F_PINS_ADDPET3_PRICE]
           ,[F_PINS_ADDPET3_COLOUR]
           ,[F_PINS_ADDPET4_PRICE]
           ,[F_PINS_ADDPET4_COLOUR]
           ,[F_PINS_MOTOR_RENEWAL]
           ,[F_PINS_HOME_RENEWAL]
           ,[F_PINS_POLICYREF]
           ,[F_PINS_STARTDATE]
           ,[F_PINS_CANCELDATE]
           ,[F_PINS_RENEWALDATE]
           ,[F_NUMBER_OF_PETS]
           ,[F_PINS_PETPREMIUM]
           ,[F_PINS_BOUGHTONLINE]
           ,[F_PINS_UNDERWRITER]
           ,[F_PINS_TOTAL_XS]
           ,[F_PINS_PAYTYPE]
           ,[F_PINS_INSTAL_DATE]
           ,[F_PINS_INSTAL_VALUE]
           ,[F_PINS_TRANSACTION_DATE]
           ,[F_PINS_SOURCE]
           ,[F_PINS_CAMPAIGN_CODE]
           ,[F_PINS_SOURCE_OF_ENQUIRY]
           ,[F_PINS_POLICYTYPE]
           ,[F_PINS_TRANSACTION]
           ,[F_PINS_POLICY_STATUS]
           ,[F_PINS_QUOTE_OR_POLICY]
           ,[F_PINS_FIRSTPAGE]
           ,[F_PINS_LASTPAGESEEN])
     SELECT 
	p.PolicyId
	,p.PolicyNumber					--AS		'CUSTREFID'
	,ISNULL(p.email_address, '')							--AS		'EMAIL'
	,REPLACE(p.Title,'.','')					--AS		'TITLE'
	,p.FirstName							--AS		'FIRSTNAME'
	,p.LastName							--AS		'LASTNAME'
	,ISNULL(CONVERT(VARCHAR, p.DOB, 103), '')				AS		'DOB'
	,CASE WHEN REPLACE(p.Title,'.','') IN ('Miss', 'Mrs', 'Ms') THEN 'Female' WHEN REPLACE(p.Title,'.','') IN ('Dr', 'Prof') THEN 'Unknown' ELSE 'Male' END		--AS		'GENDER'
	,p.Address1	--AS		'ADDRESS1'
	,p.Address2							--AS		'ADDRESS2'
	,p.Address3						--AS		'ADDRESS3'
	,p.Address4							--AS		'ADDRESS4'
	,p.PostCode							--AS		'POSTCODE'
	,REPLACE(p.Telephone1	,'01000 000 000','')					--AS		'F_PINS_TEL'
	,ISNULL(q.promo_code,'')						--AS		'F_PINS_PROMOCODE'
	,ISNULL(h.[description], '')		--AS		'F_PINS_HOWHEAR'
	,p.ProductName	--AS		'F_PINS_LEVELOFCOVER'
	,p.AnimalPetName						--AS		'F_PINS_PET_NAME'
	,CONVERT(VARCHAR, p.AnimalDateOfBirth, 103)	--AS		'F_PINS_PET_DOB'
	,p.[AnimalTypeName]				--AS		'F_PINS_CAT_DOG'

	,p.AnimalSexName					--AS		'F_PINS_PET_GENDER'
	,CASE p.neutered 
			WHEN 1 THEN 'Yes' 
			ELSE 'No' END				--AS		'F_PINS_SPAYED_NEUTERED'
	,bc.[description]					--AS		'F_PINS_BREED_CATEGORY'
	,b.[description]					--AS		'F_PINS_BREED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_NAME'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_DOB'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_CAT_DOG'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_GENDER'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_SPAYED_NEUTERED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_BREED_CATEGORY'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_BREED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_NAME'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_DOB'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_CAT_DOG'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_GENDER'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_SPAYED_NEUTERED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_BREED_CATEGORY'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_BREED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_NAME'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_DOB'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_CAT_DOG'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_GENDER'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_SPAYED_NEUTERED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_BREED_CATEGORY'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_BREED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_NAME'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_DOB'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_CAT_DOG'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_GENDER'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_SPAYED_NEUTERED'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_BREED_CATEGORY'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_BREED'
	,CASE ISNULL(q.[agree_contact], 0)
			WHEN 1 THEN 'No' 
			ELSE 'Yes' END				AS		'F_PINS_MARKETING_OPTIN'
	,q.quote_ref						--AS		'F_PINS_QUOTEREF'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_LEVELOFCOVER_ADDPET1'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_LEVELOFCOVER_ADDPET2'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_LEVELOFCOVER_ADDPET3'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_LEVELOFCOVER_ADDPET4'
	,q.premium							--AS		'F_PINS_TOTAL_MONTHLY'
	,tmp.[Annualised Gross Premium]					--AS		'F_PINS_TOTAL_ANNUALLY'
	,ISNULL(p.purchase_price,'')					--AS		'F_PINS_PETPRICE'
	,p.ColourDescription						--AS		'F_PINS_PET_COLOUR'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_PRICE'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET1_COLOUR'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_PRICE'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET2_COLOUR'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_PRICE'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET3_COLOUR'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_PRICE'
	,CONVERT(VARCHAR(50), '')				--AS		'F_PINS_ADDPET4_COLOUR'
	,CASE WHEN NULLIF(q.motor_insurance_due,'0') IS NULL THEN '' ELSE LEFT(q.motor_insurance_due,3) END				--AS		'F_PINS_MOTOR_RENEWAL'
	,CASE WHEN NULLIF(q.home_insurance_due,'0') IS NULL THEN '' ELSE LEFT(q.home_insurance_due,3) END				--AS		'F_PINS_HOME_RENEWAL'
	,p.PolicyNumber								--AS		'F_PINS_POLICYREF'
	,CONVERT(VARCHAR, p.StartDate, 103)				--AS		'F_PINS_STARTDATE'
	,CASE WHEN p.CancelledDate IS NULL THEN '' ELSE CONVERT(VARCHAR, p.CancelledDate, 103) END	--AS		'F_PINS_CANCELDATE'
	,CONVERT(VARCHAR, p.RenewalDate, 103)			--AS		'F_PINS_RENEWALDATE'
	,1	--AS		'F_NUMBER_OF_PETS'
	,qp.premium							--AS		'F_PINS_PETPREMIUM'
	,CASE WHEN q.created_systuser_id IS NULL THEN 'Yes' ELSE 'No' END				--AS		'F_PINS_BOUGHTONLINE'
	,ISNULL((SELECT underwriter FROM cover WHERE id = p.cover_id), '')	--AS		'F_PINS_UNDERWRITER'
	,0								--AS		'F_PINS_TOTAL_XS'
	,''								--AS		'F_PINS_PAYTYPE'
	,''								--AS		'F_PINS_INSTAL_DATE'
	,''								--AS		'F_PINS_INSTAL_VALUE'
	,CONVERT(VARCHAR, tmp.[Transaction Date], 103)	--AS		'F_PINS_TRANSACTION_DATE'
	,CASE WHEN l.id IS NOT NULL THEN 'Outbound' WHEN q.created_systuser_id IS NOT NULL THEN 'Inbound' WHEN q.ReferrerID IN ('AGGCTM','CONFIDOL','GOCOMP','AGGQZONE','MSTUDY') THEN 'Web - Aggregator' ELSE 'Web - Direct' END --AS 'F_PINS_SOURCE'
	,CASE q.ReferrerID WHEN 'AGGCTM' THEN 'Compare The Market' 
			WHEN 'CONFIDOL' THEN 'Confused'
			WHEN 'GOCOMP' THEN 'Go Compare'
			WHEN 'AGGQZONE' THEN 'Quote ZONE'
			WHEN 'MSTUDY' THEN 'GoGetIt'
			ELSE 'Web - Direct' END	--AS 'F_PINS_CAMPAIGN_CODE'
	,ISNULL(h.[description], '')--AS		'F_PINS_SOURCE_OF_ENQUIRY'
	,tmp.Outcome						--AS		'F_PINS_POLICYTYPE'
	,'TRANSACTION'							--AS		'F_PINS_TRANSACTION'
	,CASE WHEN tmp.Outcome IN ('Lapsed', 'Cancelled') THEN 'Cancelled Policy' ELSE 'Live Policy' END						--AS		'F_PINS_POLICY_STATUS'
	,'Policy'							--AS		'F_PINS_QUOTE_OR_POLICY'
	,'' --AS		'F_PINS_FIRSTPAGE'
	,'' --AS		'F_PINS_LASTPAGESEEN'
	FROM @temp_mi_transactions tmp
	JOIN uispetmis..policy p ON p.PolicyID = tmp.[Policy ID]
	JOIN quote_pet qp ON p.quote_pet_id = qp.id
	LEFT JOIN quote q ON qp.quote_id = q.id
	--LEFT JOIN title t ON p.Title = t.id
	LEFT JOIN hear_about_us h ON q.hear_about_us_id = h.id
	LEFT JOIN product_gender pg ON qp.product_gender_id = pg.id
	LEFT JOIN cover c ON p.cover_id = c.id
	LEFT JOIN cover ac ON qp.agg_cover_id = ac.id
	LEFT JOIN breed b ON p.breed_id = b.id
	LEFT JOIN breed_category bc ON b.breed_category_id = bc.id
	LEFT JOIN lead l ON l.quote_id = q.id

/***************************************************************/

/****************** UPDATE THE XS IN FORCE FOR THIS QUOTE/POLICY*/
UPDATE #yday
SET F_PINS_TOTAL_XS = CONVERT(INT, x.xs)
FROM uispet..cover c 
	JOIN uispetmis..AFFINITY a ON a.AffinityCode = c.affinity_code COLLATE Latin1_General_CI_AS
	JOIN uispetmis..affinity_document ad ON ad.affinity_code = a.AffinityCode
	JOIN (SELECT affinity_document_id, MAX(min_excess_amt) xs FROM uispetmis..affinity_document_excess GROUP BY affinity_document_id) x ON x.affinity_document_id = ad.id
WHERE c.product_id = 18
AND c.description = F_PINS_LEVELOFCOVER
AND CONVERT(DATE, F_PINS_TRANSACTION_DATE) BETWEEN c.cover_effective_date AND ISNULL(c.cover_inactive_date, '2070-01-01')

/******* PAYMENT DETAILS ********/
UPDATE #yday
SET F_PINS_PAYTYPE = CASE WHEN pd.payment_type_id = 1 THEN 'Pay IN FULL' ELSE 'Monthly' END
	,F_PINS_INSTAL_DATE = CASE WHEN pd.payment_type_id = 4 AND F_PINS_POLICYTYPE NOT IN ('Lapsed', 'Cancelled') THEN CONVERT(VARCHAR, DATEADD(DAY, pd.payday - 1, DATEADD(d, 1, EOMONTH(@rundate))), 103) ELSE '' END
	,F_PINS_INSTAL_VALUE = CASE WHEN pd.payment_type_id = 4 AND F_PINS_POLICYTYPE NOT IN ('Lapsed', 'Cancelled') THEN CONVERT(VARCHAR, uispetmis.dbo.get_premium_by_date(pd.policy_id, DATEADD(DAY, pd.payday - 1, DATEADD(d, 1, EOMONTH(@rundate))))) ELSE '' END
FROM #yday y 
	JOIN uispetmis.dbo.payment_detail pd ON pd.policy_id = y.id
	--JOIN uispetmis..payment_balance pb ON pb.policy_id = pd.policy_id
WHERE F_PINS_QUOTE_OR_POLICY = 'Policy'


--UPDATE FOR EMBEDDED COMMAS
UPDATE #yday
SET ADDRESS1 = REPLACE(ADDRESS1, ',', '')
	,ADDRESS2 = REPLACE(ADDRESS2, ',', '')
	,ADDRESS3 = REPLACE(ADDRESS3, ',', '')
	,ADDRESS4 = REPLACE(ADDRESS4, ',', '')
	,F_PINS_PET_COLOUR = REPLACE(F_PINS_PET_COLOUR, ',', '')

SELECT * FROM #yday;

--SELECT * FROM @temp_mi_transactions WHERE Outcome = 'Lapsed'

DROP TABLE #yday;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_genderL_prc]'
GO

CREATE PROCEDURE [dbo].[product_genderL_prc] (@product_id INT)
AS

SET NOCOUNT ON

	SELECT 	id, 
			gender_desc as description
	FROM	product_gender
	WHERE	product_id = @product_id
	ORDER BY id 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[GetVoucherByGuid]'
GO
CREATE PROCEDURE [Voucher].[GetVoucherByGuid] 
			@guid VARCHAR(36) 
			AS 
			SET NOCOUNT ON;
			SELECT v.* FROM Voucher.Voucher v
				INNER JOIN Voucher.Allocation a
				ON v.Id = a.VoucherId
				WHERE a.[Guid] = @guid
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[AllocateVoucher]'
GO


CREATE PROCEDURE [Voucher].[AllocateVoucher] 
		@campaignId INT,
		@amount DECIMAL,
		@issueDate DATETIME2,
		@entityType VARCHAR(255),
		@entityIdentifier INT,
		@guid VARCHAR(36)
		AS 
		SET NOCOUNT ON;
		INSERT INTO Voucher.Allocation
		(
			VoucherId,
			Created,
			EntityType,
			EntityIdentifier,
			[Guid]
		)
		OUTPUT INSERTED.VoucherId
		VALUES
		(   (SELECT TOP 1 v.[Id]
				FROM Voucher.Voucher v
				LEFT JOIN Voucher.Allocation a
				ON a.VoucherId = v.Id
				WHERE a.VoucherId IS NULL
				AND v.CampaignId = @campaignId
				AND v.Amount = @amount
				AND v.ValidFrom <= @issueDate
				AND v.ExpireAfter > DATEADD(DAY,90, GETDATE() )
				ORDER BY v.ValidFrom DESC, v.Id),   
			SYSDATETIME(), 
			@entityType,
			@entityIdentifier,
			@guid
		)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[VV_Clinic]'
GO
CREATE TABLE [dbo].[VV_Clinic]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[area_id] [int] NOT NULL,
[status] [tinyint] NULL CONSTRAINT [DF_VV_Clinic_status] DEFAULT (1)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__VV_Clinic__10AB74EC] on [dbo].[VV_Clinic]'
GO
ALTER TABLE [dbo].[VV_Clinic] ADD CONSTRAINT [PK__VV_Clinic__10AB74EC] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[VV_ClinicL_prc]'
GO


CREATE PROCEDURE [dbo].[VV_ClinicL_prc] (@type INT=NULL)
AS

SET NOCOUNT ON


	SELECT 	id, 
			description
	FROM	VV_Clinic
	WHERE	status = 1

	ORDER BY description 



SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_update_profile_passwordU_prc]'
GO


CREATE PROCEDURE [dbo].[iv_update_profile_passwordU_prc]  
(
	@profileId INT,
	@newPassword VARCHAR(256)
)	

AS
SET NOCOUNT ON

	OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
	DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;

	UPDATE dbo.iv_profile
	SET enc_api_password = EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @newPassword),
	last_password_reset_date = GETDATE()
	WHERE id = @profileId;

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_status]'
GO
CREATE TABLE [dbo].[outbound_status]
(
[id] [int] NOT NULL IDENTITY(31837, 1),
[description] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[completed] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_outbound_status] on [dbo].[outbound_status]'
GO
ALTER TABLE [dbo].[outbound_status] ADD CONSTRAINT [PK_outbound_status] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_statusL_prc]'
GO




CREATE PROCEDURE [dbo].[outbound_statusL_prc]
AS
SET NOCOUNT ON

select * from outbound_status
	

SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_createI_prc]'
GO



CREATE PROCEDURE [dbo].[policy_createI_prc] (@quote_id INT)
AS
SET NOCOUNT ON

	declare @exc_rsl nvarchar(255); select @exc_rsl = 'This policy does not provide cover for any Pre-existing Condition as described in the Policy Wording ‘Definitions’.  This will include, but is not limited to, the conditions you have informed us about';
	declare @exc_uic nvarchar(255); select @exc_uic = 'This policy does not provide cover for any Pre-existing Condition as described in Section 1 – Definitions';

	declare @acc_pl nvarchar(500); select @acc_pl = 'In respect of the Section relating to Public Liability - This policy does not cover Your liability in respect of any incident(s) that You have informed us of, or, any future incident of the same or similar nature or resulting from the behavioural traits and / or tendencies displayed in such incident(s).';
	declare @acc_tpl nvarchar(500); select @acc_tpl = 'In respect of the Section relating to Third Party Liability - This policy does not cover Your liability in respect of any incident(s) that You have informed us of, or, any future incident of the same or similar nature or resulting from the behavioural traits and / or tendencies displayed in such incident(s).';

	declare @newpolicies table (id int, pet_id int, premium decimal(10,2), email_address nvarchar(64) )
	declare @num_pets int
		,@pet_id int
		,@premium decimal(10,2)
		,@counter int
		,@email_address nvarchar(64)

	select @num_pets = count(1) FROM quote_pet where quote_id = @quote_id
	select @counter = 0

	-- ensure that the start date is not in the past
	declare @policy_start_date date, @today_date date;
	select @today_date = GETDATE();
	select @policy_start_date = case when q.policy_start_date < @today_date then @today_date else q.policy_start_date end
	from quote q
	where q.id=@quote_id;


	DECLARE CURSOR1 CURSOR FOR
	SELECT qp.id, qp.premium, q.email
		FROM quote_pet qp join quote q on q.id = qp.quote_id
		where quote_id = @quote_id
		--and q.quote_status_id in (1,2)

	OPEN CURSOR1 
	FETCH NEXT FROM  CURSOR1 INTO @pet_id, @premium, @email_address
	WHILE (@@FETCH_STATUS <> - 1) 
	BEGIN 
	IF (@@FETCH_STATUS <> - 2)

		select @counter = @counter + 1

		INSERT uispetmis..policy (
				AffinityCode,
				ClaimType,
				PrintDate,
				Printed,
				PolicyNumber,
				PuppyCoverNumber,
				PolicyInceptionDate,
				StartDate,
				RenewalDate,
				AnimalPetName,
				AnimalDateOfBirth,
				BreedDescription,
				ColourDescription,
				AnimalSexName,
				AnimalTypeName,
				ProductName,
				PaymentFrequencyName,  
				PolicyPremiumYearID,
				Title,
				FirstName,
				LastName,
				Address1,
				Address2,
				Address3,
				Address4,
				Address5,
				PostCode,
				Telephone1,
				AnimalDead,
				DateTimestamp,
				AnimalBabyName,
				PreExistingCondition,
				Exclusion,
				PreviousPolicyID,
				cdl_source_id,
				vol_excess,
				breed_excess,
				status_id,
				quote_pet_id,
				email_address,
				purchase_price,
				breed_id,
				animal_type_id,
				product_id,
				clinic_id,
				cover_id,
				exclusion_review,
				dob,
				microchipnumber,
				neutered)
		SELECT	c.affinity_code,		--AffinityCode
				NULL,					--ClaimType
				NULL,					--PrintDate
				NULL,					--Printed
	--			case when @num_pets >1 then q.quote_ref+'/'+convert(varchar,@counter)
	--				else q.quote_ref end,			--PolicyNumber


				left(q.quote_ref, 3) + convert(varchar, convert(int, right(q.quote_ref, len(q.quote_ref)-3)) + @counter - 1),	--PolicyNumber

				NULL,					--PuppyCoverNumber
				@policy_start_date,		-- PolicyInceptionDate
				@policy_start_date,	--StartDate
				--DATEADD(yy, 1, @policy_start_date), --RenewalDate ??
				uispetmis.[dbo].[CalculateRenewalDate](@policy_start_date, qp.cover_id),
				qp.pet_name,				--AnimalPetName
				qp.pet_DOB,  --AnimalDateOfBirth
				b.description,			--BreedDescription
				qp.colour,					--ColourDescription
				pg.gender_desc,			--AnimalSexName
				at.description,			--AnimalTypeName
				c.description,			--ProductName
				'CC',			--PaymentFrequencyName 
				0,						--PolicyPremiumYearID ???? 
				t.description,			--Title
				q.forename,				--FirstName
				q.surname,				--LastName
				q.house_name_num,		--Address1
				q.street,				--Address2
				q.street2,				--Address3
				q.town,					--Address4
				q.county,				--Address5
				q.postcode,				--PostCode
				q.contact_num,			--Telephone1
				'F',					--AnimalDead
				GETDATE(),				--DateTimestamp
				NULL,					--AnimalBabyName
				NULL, --qp.vetvisitdetails,		--PreExistingCondition
				

	case when qp.vetvisit = 1 then --Exclusion
					case when c.underwriter = 'RSL' then @exc_rsl else @exc_uic end
						+ case when qp.accident = 1 then 
							'.' + CHAR(13) + CHAR(10) + case when c.product_id in (2,19,13,22,14,11,6,3) then @acc_pl else @acc_tpl end 
						else '' end
				when qp.accident = 1 then 
						case when c.product_id in (2,19,13,22,14,11,6,3) then @acc_pl else @acc_tpl end else '' end,

				NULL,					--PreviousPolicyID
				NULL,					--cdl_source_id
				ISNULL(qp.vol_excess,0),						--vol_excess	
				0,						--breed_excess
				1,						--status
				@pet_id,				--quote_pet_id
				q.email,				--email_address
				qp.purchase_price,		--purchase_price
				qp.breed_id,			--breed_id
				qp.animal_type_id,		--animal_type_id
				q.product_id,			--product_id
				q.VFV_ClinicID,			--clinic_id
				qp.cover_id,			--cover_id
				qp.exclusion_review,	--review
				ISNULL(a.date_of_birth,q.DOB),
				microchipnumber,
				neutered
		FROM	quote q 
			JOIN quote_pet qp ON q.id = qp.quote_id
			JOIN breed b ON b.id = qp.breed_id
			JOIN product_gender pg ON pg.id = qp.product_gender_id
			JOIN cover c ON c.id = qp.cover_id
			JOIN animal_type at ON at.id = qp.animal_type_id
			LEFT JOIN title t ON t.id = q.title_id
			LEFT JOIN aggregator a on a.quote_id = q.id 
		WHERE	qp.id = @pet_id

		declare @new_policy_id int
		select @new_policy_id = @@identity

		INSERT @newpolicies(id, pet_id, premium, email_address) VALUES (@new_policy_id, @pet_id, @premium, @email_address)

		exec uispetmis.[CustomerPreferences].[ImportFromQuoteIntoPolicy] @quoteId=@quote_id, @policyId = @new_policy_id

		INSERT INTO uispetmis..PREMIUM
		(policy_id, 
		effective_date, 
		premium, 
		promo_discount, 
		offer_discount, 
		multipet_discount, 
		postcode, 
		cover_id, 
		promo_code, 
		IPT_absolute,
		commission_affinity_absolute,
		commission_administrator_absolute,
		commission_underwriter_absolute,
		credit_charge_absolute,
		helpline_charge_absolute,
		tpl_absolute,
		pricing_engine_id,
		vol_excess,
		promo_discount_absolute, 
		offer_discount_absolute, 
		multipet_discount_absolute
		)
		SELECT @new_policy_id, 
				@policy_start_date, 
				qp.premium, 
				qp.promo_discount, 
				qp.offer_discount, 
				qp.multipet_discount, 
				q.postcode, 
				qp.cover_id, 
				q.promo_code,
				qp.IPT_absolute,
				qp.commission_affinity_absolute,
				qp.commission_administrator_absolute,
				qp.commission_underwriter_absolute,
				qp.credit_charge_absolute,
				qp.helpline_charge_absolute,
				qp.tpl_absolute,
				qp.pricing_engine_id,
				qp.vol_excess,
				qp.promo_discount_absolute, 
				qp.offer_discount_absolute, 
				qp.multipet_discount_absolute
		FROM QUOTE q JOIN QUOTE_PET qp ON q.id = qp.quote_id
		WHERE qp.id = @pet_id

	FETCH NEXT FROM CURSOR1 INTO @pet_id, @premium, @email_address
	END 
	CLOSE CURSOR1 
	DEALLOCATE CURSOR1

	if @num_pets > 1
	begin
		declare @master_policy_id int
		select @master_policy_id = MIN(id) from @newpolicies
		insert into uispetmis..policy_related (master_policy_id, policy_id) 
		select @master_policy_id, id from @newpolicies
	end

	select * from @newpolicies


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[CreateVoucher]'
GO
CREATE PROCEDURE [Voucher].[CreateVoucher] 
		@campaignId INT,
		@code VARCHAR(255),
		@amount DECIMAL,
		@validFrom DATETIME2,
		@ExpireAfter DATETIME2
		AS 
		SET NOCOUNT ON;
		INSERT INTO Voucher.Voucher
		(
			CampaignId,
			Code,
			Created,
			Amount,
			ValidFrom,
			ExpireAfter
		)
		OUTPUT INSERTED.Id
		VALUES
		(
			@campaignId,
			@code,
			GETDATE(),
			@amount,
			@validFrom,
			@ExpireAfter
		)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_VFV1]'
GO
CREATE TABLE [dbo].[pricing_matrix_VFV1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[value_2] [money] NULL,
[value_3] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_VFV1] on [dbo].[pricing_matrix_VFV1]'
GO
ALTER TABLE [dbo].[pricing_matrix_VFV1] ADD CONSTRAINT [PK_pricing_matrix_VFV1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_VFV]'
GO
CREATE TABLE [dbo].[pricing_matrix_VFV]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[value_2] [money] NULL,
[value_3] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_VFV] on [dbo].[pricing_matrix_VFV]'
GO
ALTER TABLE [dbo].[pricing_matrix_VFV] ADD CONSTRAINT [PK_pricing_matrix_VFV] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quick_quote_calculate_VFV_U_prc]'
GO


CREATE PROCEDURE [dbo].[quick_quote_calculate_VFV_U_prc]
	@clinic_id int,
	@product_id int,
	@quote_pet_id int,
	@pet_count int,
	@promo_code nvarchar(10),
	@policy_start_date smalldatetime,
	@name varchar(32),
	@pet_DOB smalldatetime,
	@animal_type_id int,
	@cover_id int,
	@offer_discount decimal(5,2),
	@multipet_discount decimal(5,2),
	@promo_discount decimal(5,2),
	@ipt_factor float,
	@age_rating varchar(16),
	@breed_rating int,
	@sel_cover_only bit
AS
BEGIN

	declare @promo table (cover_id int, discount decimal(5,2), staff bit)
	insert into @promo
	select cd.cover_id, discount, staff
	from cover_discount cd 
		join discount d on d.id = cd.discount_id
	where d.code = @promo_code
	AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))

	declare @area_id int;
	select @area_id = area_id from clinic where id=@clinic_id and product_id = @product_id
			
	/* INTRODUCTORY DISCOUNT */
	declare @coverIntroductoryDiscount table(discount decimal(10,2), cover_id int);
	--check if its not renewal
	if not exists(select * from uispetmis..POLICY p where p.quote_pet_id = @quote_pet_id)
	begin
		insert into @coverIntroductoryDiscount (discount, cover_id)
		select discount, cover_id
		from uispet..get_introductory_discount_by_product(GETDATE(), @product_id);
	end
					
	if @policy_start_date < '2013-06-12'
	begin	
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			--,case when @policy_start_date < '2010-10-20' then c.id+100 else c.id+200 end cover_id,
			,c.id cover_id,
			convert(decimal(10,2), 
				case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM	pricing_matrix_vfv
			--JOIN	cover c ON cover_id = case when @policy_start_date < '2010-10-20' then c.id+100 else c.id+200 end
			JOIN	cover c ON cover_id = c.id
			LEFT JOIN	@promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE	animal_type_id = @animal_type_id 
		AND		age	= @age_rating 
		AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	end
	else
	begin
		SELECT	@name pet
			,@quote_pet_id pet_id
			,@animal_type_id animal_type_id
			--,case when @policy_start_date < '2010-10-20' then c.id+100 else c.id+200 end cover_id,
			,c.id cover_id,
			convert(decimal(10,2), 
				case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
				* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
				* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
				* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
				* @ipt_factor)
				as premium
			,case when @cover_id = c.id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM	pricing_matrix_vfv1
			--JOIN	cover c ON cover_id = case when @policy_start_date < '2010-10-20' then c.id+100 else c.id+200 end
			JOIN	cover c ON cover_id = c.id
			LEFT JOIN	@promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE	animal_type_id = @animal_type_id 
		AND		age	= @age_rating 
		AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		and (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
		and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
	end;
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Voucher].[CreateVoucherProperty]'
GO
CREATE PROCEDURE [Voucher].[CreateVoucherProperty]
		@voucherId INT,
		@key VARCHAR(255),
		@value VARCHAR(255)
		AS 
		SET NOCOUNT ON;
		INSERT INTO Voucher.Property
		(
		    VoucherId,
		    [Key],
		    [Value]
		)
		VALUES
		(   @voucherId, 
		    @key,
			@value
		)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Debenhams].[GetPoliciesForVouchers]'
GO
CREATE PROCEDURE [Debenhams].[GetPoliciesForVouchers]
		AS
			SET NOCOUNT ON;
			SELECT p.PolicyID, DATEADD(DAY,vc.DaysAfterInception,p.PolicyInceptionDate) as PolicyInceptionDate, p.email_address, p.AffinityCode, q.ReferrerID, vc.Value
				FROM uispetmis..POLICY p
			JOIN uispet..quote_pet qp
				ON qp.id = p.quote_pet_id
			JOIN uispet..quote q
				ON q.id = qp.quote_id
			JOIN uispet.Voucher.Campaign c
				ON c.[Affinity] = q.[affinity]
			JOIN uispet.Debenhams.VoucherConfig vc
				ON vc.CampaignId = c.Id
				AND ISNULL(vc.ReferrerId,'') = ISNULL(q.ReferrerID,'')
			LEFT JOIN [uispet].[Voucher].[Allocation] a
				ON a.EntityType = 'Policy'
				AND a.EntityIdentifier = p.PolicyID
			WHERE a.VoucherId IS NULL
			AND p.PolicyInceptionDate BETWEEN c.StartOn AND c.EndAfter
			AND p.PolicyInceptionDate <= DATEADD(DAY, -vc.DaysAfterInception, GETDATE())
			AND p.CancelledDate IS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_email_detailS_prc]'
GO
CREATE PROCEDURE [dbo].[quote_email_detailS_prc]  (@id INT)
		
AS
SET NOCOUNT ON


		SELECT	q.id,p.id as product_id,
		quote_ref,
		t.description + ' ' + forename + ' ' + dbo.PROPERCASE(surname) as holder_name,

		t.description as title,
		ISNULL(forename,'') as forename,
		ISNULL(surname,'') as surname,
		ISNULL(q.postcode,'') as postcode,
		ISNULL(q.house_name_num,'') as house_name_num,
		ISNULL(q.street,'') as street ,
		ISNULL(q.street2,'') as street2 ,
		ISNULL(q.town,'') as town,
		ISNULL(q.county,'') as county,
		ISNULL(contact_num,'') as contact_num,
		ISNULL(q.email,'') as email,
		c.description as cover,
		p.description as product,
		CONVERT(VARCHAR(11), policy_start_date,113) as policy_start_date,
		CONVERT(VARCHAR(11), q.update_timestamp,113) as quote_date,


		--end of punter stuff, now tiddles

		qp.pet_name,
		pg.gender_desc as gender,
		b.description as breed,
		qp.cover_id,
		isnull(qp.colour, 'N/A') as colour,
		CONVERT(VARCHAR(11), pet_DOB,113) as pet_DOB,
		at.description as pet_type,
		qp.premium  * 12 as annual_premium,
		qp.premium as monthly_premium,
		p.description as affinity_name,
		p.email as prod_email,
		p.tel as prod_tel,
		p.web_address as web_address,
		isnull(p.footer_text,'') as footer_text,
		p.affinity_code,
		p.admin_email,
		--isnull(ct.description,'') as card_type,
		--isnull(pd.card_number,'') as card_number,
		'' as card_type,
		'' as card_number,

		REPLACE(q.house_name_num + ' ' + q.street + ', ' + q.street2 + ', ' + q.town + ', ' + q.county + ', ' + q.postcode, ', ,',',') as full_address

		FROM quote q
			JOIN quote_pet qp On qp.quote_id = q.id
			LEFT JOIN title t ON t.id = q.title_id
			JOIN cover c ON c.id = qp.cover_id
			JOIN product p ON p.id = q.product_id
			JOIN breed b ON b.id = qp.breed_id
			JOIN product_gender pg ON qp.product_gender_id = pg.id
			JOIN animal_type at ON qp.animal_type_id = at.id
			LEFT JOIN payment_detail pd ON pd.id = q.payment_detail_id
			left join card_type ct ON ct.id = pd.card_type_id
		WHERE	qp.id =@id


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[create_quote_ref]'
GO

CREATE FUNCTION [dbo].[create_quote_ref] (@quote_id INT, @pet_name VARCHAR(32))  
RETURNS VARCHAR(8000) AS  
	BEGIN 
		DECLARE @retval varchar(16)

		--Don't know what foramay this will take so just use surname and id for the mo. 

		SET @retval = LEFT(@pet_name, 3) + CONVERT(VARCHAR, (9*@quote_id) + 80000000) -- DO NOT ALLOW THIS TO GET INTO PRODUCTION, THIS MUST BE 30000000

	 RETURN  UPPER(@retval)     		

	
	END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quoteU_prc]'
GO





CREATE PROCEDURE [dbo].[quoteU_prc]  (
		@action			CHAR(1),
		@id				INT = NULL,
		@quote_ref		VARCHAR(16) = NULL ,
		@product_id		INT = NULL,
		@customer_id	INT	= NULL,
		@title_id		INT = NULL,
		@forename		VARCHAR(32) = NULL,
		@surname		VARCHAR(32) = NULL,
		@DOB			DATE = NULL,
		@postcode		VARCHAR(16) = NULL,
		@house_name_num VARCHAR(50) = NULL,
		@street			VARCHAR(32)	= NULL,
		@street2		VARCHAR(32)	= NULL,
		@town			VARCHAR(32)	= NULL,
		@county			VARCHAR(32) = NULL,
		@contact_num	VARCHAR(32)	 = NULL,
		@email			VARCHAR(64)	= 	NULL,
		@quote_status_id		INT	= 	NULL ,
		@cover_id			INT		= NULL,
		@product_excess_id	 INT	= NULL,
		@occupation_id		INT = NULL,
		@hear_about_us_id	INT = NULL,
		@other_policies		VARCHAR(1024) = NULL,
		@motor_insurance_due	VARCHAR(4) = NULL,
		@home_insurance_due	VARCHAR(4)= NULL,
		@policy_start_date	VARCHAR(16) = NULL,
		@agree_contact		BIT = NULL,
		@agree_terms		BIT = NULL,
		@clinic_id	INT = NULL,
		@premium	MONEY = NULL,
		@client_ip NVARCHAR(20) = NULL,
		@referrer_id NVARCHAR(50) = NULL,
		@promo_code NVARCHAR(10) = NULL,
		@created_systuser_id INT = NULL,
		@modified_systuser_id INT = NULL,
		@charity_id INT = NULL,
		@min_cover_ordinal INT = NULL,
		@opt_out_marketing VARCHAR(1) = 'N',
		@remote_ref NVARCHAR(50) = NULL,
		@agree_illvac_excl BIT = NULL,
		@agree_preexisting_excl BIT = NULL,
		@agree_vicious_excl BIT = NULL,
		@agree_waiting_excl BIT = NULL,
		@enquiry_method_code CHAR(1) = NULL,
		@agree_phone BIT = NULL,
		@agree_letter BIT = NULL,
		@agree_SMS BIT = NULL,
		@agree_email BIT = NULL
) 
AS
SET NOCOUNT ON

	SET DATEFORMAT DMY

	DECLARE @new_id			INT
	DECLARE	@new_quote_ref	VARCHAR(16) 

	IF @action = 'I'
		BEGIN
		
		--EOD-14119 removal of setting referrer_id
		-- Before we begin, let's get the referrer code shall we?
		/*IF (@referrer_id is NULL or @referrer_id = '') and @product_id not in (31)
		BEGIN
			-- Select the first quote done in the last 30 days from a combination of the ...
			select top 1 @referrer_id = source from
				-- ...quote table...
				(select top 1 DATEDIFF(d,quote.create_timestamp,GETDATE()) as age, quote_ref, forename, surname, referrerID as source from quote 
				where product_id = @product_id
				and forename = @forename
				and surname = @surname
				and replace(postcode,' ','') like replace(@postcode,' ','')			
				and ReferrerID is not null
				and DATEDIFF(d,quote.create_timestamp,GETDATE()) <= 30

				UNION
				-- ...and the aggregator table
				select top 1 DATEDIFF(d,aggregator.create_timestamp,GETDATE()) as age, quote_ref, forename, surname, source from aggregator 
				where affinity=(select affinity_code from product where id = @product_id)
				and forename = @forename
				and surname = @surname
				and replace(postcode,' ','') like replace(@postcode,' ','')				
				and source is not null
				and DATEDIFF(d,aggregator.create_timestamp,GETDATE()) <= 30) as a

			order by age 
		
		END	*/	
		
		INSERT	quote (
				quote_ref,
				product_id,
				title_id,
				forename,
				surname,
				DOB,
				postcode,
				house_name_num,
				street,
				town,
				county,
				contact_num,
				email,
				quote_status_id,
				cover_id,
				product_excess_id,
				occupation_id,
				hear_about_us_id,
				other_policies,
				motor_insurance_due,
				home_insurance_due,
				policy_start_date,
				agree_terms,
				VFV_clinicID,					
				premium,
				clientipaddress,
				referrerid,
				promo_code,
				created_systuser_id,
				min_cover_ordinal,
				street2,
				opt_out_marketing,
				agg_remote_ref,
				agree_illvac_excl,
				agree_preexisting_excl,
				agree_vicious_excl,
				agree_waiting_excl,
				enquiry_method_code) 
		SELECT	NULL,
				@product_id,
				@title_id,
				@forename,
				@surname,
				@DOB,
				@postcode,
				@house_name_num,
				@street,
				@town,
				@county,
				@contact_num,
				@email,
				1,
				@cover_id,
				@product_excess_id,
				@occupation_id,
				@hear_about_us_id,
				@other_policies,
				@motor_insurance_due,
				@home_insurance_due,
				@policy_start_date,
				@agree_terms,
				@clinic_id,
				@premium,
				@client_ip,
				@referrer_id,
				@promo_code,
				@created_systuser_id,
				@min_cover_ordinal,
				@street2,
				@opt_out_marketing,
				@remote_ref,
				@agree_illvac_excl,
				@agree_preexisting_excl,
				@agree_vicious_excl,
				@agree_waiting_excl,
				@enquiry_method_code

		DECLARE @prod_prefix	varchar(3)
		select @prod_prefix = prod_prefix from product where id = @product_id
	
		SELECT @new_id = SCOPE_IDENTITY()	--IDENT_CURRENT('quote')
		SELECT	@new_quote_ref = dbo.create_quote_ref(@new_id, @prod_prefix)
		UPDATE	quote
		SET		quote_ref = @new_quote_ref 
		WHERE	id = @new_id
		
		SELECT  @new_quote_ref  AS quote_ref,
				@new_id AS new_id,
				@prod_prefix AS prod_prefix, 
				ISNULL(@referrer_id,'') AS referrer
	END 
	ELSE IF @action = 'U'
	BEGIN
		IF (@id IS NULL)
			BEGIN
				RAISERROR ('Incomplete Details', 10,1)
				RETURN
			END 
		ELSE
			BEGIN
				UPDATE	quote
				SET		quote_ref		= ISNULL(@quote_ref,quote_ref), 
						title_id		= ISNULL(@title_id,title_id),
						forename		= ISNULL(@forename, forename),
						surname			= ISNULL(@surname, surname),
						DOB				= ISNULL(@DOB,DOB),
						postcode		= ISNULL(@postcode,postcode),
						house_name_num	= ISNULL(@house_name_num,house_name_num),
						street			= ISNULL(@street,street), 
						street2			= ISNULL(@street2,street2), 
						town			= ISNULL(@town,town),
						county			= ISNULL(@county,county), 
						contact_num		= ISNULL(@contact_num,contact_num),
						email			= ISNULL(@email, email),
						quote_status_id = ISNULL(@quote_status_id, quote_status_id),		
						cover_id		= ISNULL(@cover_id,cover_id), 
						product_excess_id	= ISNULL(@product_excess_id,product_excess_id),
						update_timestamp	= CURRENT_TIMESTAMP,
						occupation_id		= ISNULL(@occupation_id, occupation_id),
						hear_about_us_id	= ISNULL(@hear_about_us_id, hear_about_us_id),
						other_policies		= ISNULL(@other_policies, other_policies),
						motor_insurance_due	= ISNULL(@motor_insurance_due, motor_insurance_due),
						home_insurance_due	= ISNULL(@home_insurance_due, home_insurance_due),
						policy_start_date	= ISNULL(@policy_start_date, policy_start_date),
						agree_contact_email = ISNULL(@agree_email, agree_contact_email),
						agree_contact_SMS =  ISNULL(@agree_SMS, agree_contact_SMS),
						agree_contact_letter = ISNULL(@agree_letter, agree_contact_letter),
						agree_contact_phone= ISNULL(@agree_phone, agree_contact_phone),

						agree_terms			= ISNULL(@agree_terms, agree_terms),
						VFV_clinicID		= isnull(@clinic_id,VFV_clinicID),
						premium			= isnull(@premium,premium),
						promo_code	= CASE WHEN isnull(@promo_code,'') = '' THEN promo_code ELSE @promo_code END,
						modified_systuser_id = ISNULL(modified_systuser_id, @modified_systuser_id),	--only update if not previously populated
						converted_systuser_id = ISNULL(@modified_systuser_id, modified_systuser_id) --always update most recent user in case converted to policy
						,product_id = isnull(@product_id, product_id)
						,min_cover_ordinal = ISNULL(@min_cover_ordinal, min_cover_ordinal)
						,agree_illvac_excl= ISNULL(@agree_illvac_excl, agree_illvac_excl)
						,agree_preexisting_excl= ISNULL(@agree_preexisting_excl, agree_preexisting_excl)
						,agree_vicious_excl= ISNULL(@agree_vicious_excl, agree_vicious_excl)
						,agree_waiting_excl= ISNULL(@agree_waiting_excl, agree_waiting_excl)
						,enquiry_method_code = ISNULL(@enquiry_method_code, enquiry_method_code)
				WHERE	id = @id 

			/* CHARITY DONATION? */
			IF(@charity_id IS NOT NULL)
			BEGIN
				DELETE quote_pet_charity
				FROM quote_pet_charity qpc
					JOIN quote_pet qp ON qpc.quote_pet_id = qp.id
					JOIN quote q ON qp.quote_id = q.id
				WHERE q.id = @id

				INSERT quote_pet_charity (quote_pet_id, charity_id, donation)
				SELECT id, @charity_id, CASE WHEN (12 * premium) >= 300 THEN 10 ELSE 5 END 
				FROM quote_pet
				WHERE quote_id = @id
			END
		END 
	END 

	--now set the affinity according to the cover products that were offered
	UPDATE quote SET affinity = qx.ac3
	FROM quote q JOIN 
	(SELECT DISTINCT q.id, q.quote_ref, LEFT(c.affinity_code, 3) ac3 FROM quote q
		JOIN cover c ON q.product_id = c.product_id AND
		policy_start_date BETWEEN c.cover_effective_date AND ISNULL(c.cover_inactive_date,'2050-01-01')
	) qx ON qx.id = q.id
	WHERE q.id = ISNULL(@new_id, @id)	--insert or updated

	SET @id = ISNULL(@new_id, @id)

	IF (@agree_email IS NOT NULL)
	BEGIN
		EXEC uispetmis.Contact.InsertMarketingPreference @Source='Quote', @Identifier=@id, @Method='Email', @Purpose='Marketing', @Enabled=@agree_email, @Ordinal=1, @Domain='', @Username='', @Channel='Website'
	END

	IF (@agree_phone IS NOT NULL)
	BEGIN
		EXEC uispetmis.Contact.InsertMarketingPreference @Source='Quote', @Identifier=@id, @Method='Telephone', @Purpose='Marketing', @Enabled=@agree_phone, @Ordinal=1, @Domain='', @Username='', @Channel='Website'
	END

		IF (@agree_letter IS NOT NULL)
	BEGIN
		EXEC uispetmis.Contact.InsertMarketingPreference @Source='Quote', @Identifier=@id, @Method='Post', @Purpose='Marketing', @Enabled=@agree_letter, @Ordinal=1, @Domain='', @Username='', @Channel='Website'
	END

		IF (@agree_SMS IS NOT NULL)
	BEGIN
		EXEC uispetmis.Contact.InsertMarketingPreference @Source='Quote', @Identifier=@id, @Method='SMS', @Purpose='Marketing', @Enabled=@agree_SMS, @Ordinal=1, @Domain='', @Username='', @Channel='Website'
	END
SET NOCOUNT OFF






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_audit]'
GO
CREATE TABLE [dbo].[outbound_audit]
(
[id] [int] NOT NULL IDENTITY(29188, 1),
[outbound_id] [int] NOT NULL,
[outbound_status_id] [int] NULL,
[user_id] [int] NOT NULL,
[notes] [ntext] COLLATE Latin1_General_CI_AS NOT NULL,
[datetimestamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_outbound_audit] on [dbo].[outbound_audit]'
GO
ALTER TABLE [dbo].[outbound_audit] ADD CONSTRAINT [PK_outbound_audit] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_auditL_prc]'
GO



CREATE PROCEDURE [dbo].[outbound_auditL_prc] (@id int)
AS
SET NOCOUNT ON

BEGIN
		select os.description status, s.initial who, notes, datetimestamp [when]
		from outbound_audit oa
			join outbound_status os on oa.outbound_status_id = os.id
			join uispetmis..systuser s on s.id = oa.user_id
		where outbound_id = @id
		order by datetimestamp desc

END

set rowcount 0

SET NOCOUNT OFF









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_pet_introductory_discount]'
GO
CREATE TABLE [dbo].[quote_pet_introductory_discount]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[quote_pet_id] [int] NOT NULL,
[introductory_discount_id] [int] NOT NULL,
[discount] [decimal] (5, 2) NOT NULL,
[funded_by] [int] NULL,
[applied_date] [smalldatetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_quote_pet_introductory_discount] on [dbo].[quote_pet_introductory_discount]'
GO
ALTER TABLE [dbo].[quote_pet_introductory_discount] ADD CONSTRAINT [PK_quote_pet_introductory_discount] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_pet_introductory_discount_quote_pet_id_2074B] on [dbo].[quote_pet_introductory_discount]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_pet_introductory_discount_quote_pet_id_2074B] ON [dbo].[quote_pet_introductory_discount] ([quote_pet_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_quote_pet_introductory_discount_quote_pet_id_AEED2] on [dbo].[quote_pet_introductory_discount]'
GO
CREATE NONCLUSTERED INDEX [IDX_quote_pet_introductory_discount_quote_pet_id_AEED2] ON [dbo].[quote_pet_introductory_discount] ([quote_pet_id]) INCLUDE ([funded_by])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[update_quote_pet_introductory_discount_prc]'
GO

CREATE PROCEDURE [dbo].[update_quote_pet_introductory_discount_prc]
(	
	@quote_pet_id int,
	@applicable_date date = null
)
AS
BEGIN
		
		if @applicable_date is null
			set @applicable_date = GETDATE();
		
		if exists(select * from quote_pet_introductory_discount where quote_pet_id = @quote_pet_id)
		begin
			--delete these records if already exists
			delete from quote_pet_introductory_discount where quote_pet_id = @quote_pet_id;
		end
		
		--update the new values
		insert into quote_pet_introductory_discount(quote_pet_id, introductory_discount_id, discount, funded_by, applied_date)
		select 
		   qp.id as quote_pet_id,
		   cid.id as introductory_discount_id,
		   cid.discount,
		   cid.funded_by,
		   @applicable_date 
		from cover_introductory_discount cid
		inner join cover c on c.id = cid.cover_id
		inner join quote_pet qp on qp.id = @quote_pet_id
		where c.id = qp.cover_id
		and @applicable_date between cid.effective_date and isnull(cid.inactive_date,'2050-01-01')
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_list_SteveL_prc]'
GO



create PROCEDURE [dbo].[quote_list_SteveL_prc] @status INT = 0, @affinity VARCHAR(10), @surname NVARCHAR(255), @petname NVARCHAR(155), @postcode VARCHAR(10), @quoteref VARCHAR(20) = NULL, @user_id INT = 0, @systuser_id INT,
		@from_date VARCHAR(10), @to_date VARCHAR(10), @email VARCHAR(64)
AS
SET NOCOUNT ON

-- Parse FROM and TO dates
declare @from_date_parsed smalldatetime;
set @from_date_parsed = convert(smalldatetime, @from_date, 103);

declare @to_date_parsed smalldatetime
set @to_date_parsed = convert(smalldatetime, @to_date, 103);
set @to_date_parsed = DATEADD(d,1,@to_date_parsed)

DECLARE @qid int, @pet varchar(64)


--added support for policynumbers out of multi pet quotes (quote ref is incremented)
if(@quoteref != '')
begin

	if exists (select 1 from uispetmis..POLICY where policynumber = @quoteref)
	begin

		declare @single_quote_id int	
		select @single_quote_id = quote_id 
		from uispetmis..POLICY p
			join quote_pet qp on p.quote_pet_id = qp.id
		where policynumber = @quoteref

		select q.id,
			quote_status_id,
			dbo.PROPERCASE(q.surname) as owner,
			q.quote_ref,
			ISNULL(q.all_pet_names,'') pet_name,
			UPPER(q.postcode) postcode,
			premium,
			qs.description as quote_status,
			q.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL THEN 'C' 
					else su.Initial end		
				+ ISNULL('/'+su1.Initial,'')
				+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
			source
		,q.ReferrerID as referrer
		,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end ok_to_contact
		,case ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_phone
		,case ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_SMS
		,case ISNULL(agree_contact_email,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_email
		,case ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_letter
		,q.email
		from quote q
			JOIN quote_status qs ON q.quote_status_id = qs.id
			LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
			LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
			LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
			LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
			join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(q.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
		where q.id = @single_quote_id
	
		return;
	END 
END

IF(@status=0)
BEGIN
	select top 1000 q.id,
		quote_status_id,
		dbo.PROPERCASE(q.surname) as owner,
		q.quote_ref,
		ISNULL(q.all_pet_names,'') pet_name,
		UPPER(q.postcode) postcode,
		premium,
		qs.description as quote_status,
		q.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL THEN 'C' 
					else su.Initial end		
			+ ISNULL('/'+su1.Initial,'')
			+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
		source
		,q.ReferrerID as referrer
	,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end ok_to_contact
	,case ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_phone
		,case ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_SMS
		,case ISNULL(agree_contact_email,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_email
		,case ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_letter
	,q.email
	from quote q
		JOIN quote_status qs ON q.quote_status_id = qs.id
		LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
		LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
		LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
		LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
		join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(q.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
	WHERE
	ISNULL(qs.id,0) <> 9 --Suspicious Activity
	AND q.create_timestamp >= @from_date_parsed AND q.create_timestamp < @to_date_parsed
	and q.quote_ref like @affinity+'%'
	and q.quote_ref like '%'+@quoteref+'%'
	and q.surname LIKE @surname+'%'
	and isnull(q.all_pet_names,'') LIKE '%'+@petname+'%'
	and REPLACE(q.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
	and ((@user_id <> 0 AND @user_id IN (created_systuser_id, modified_systuser_id, converted_systuser_id)) or (@user_id=0))
	--and left(quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
	and q.email like '%'+@email+'%'
	
	--and q.surname <> 'Ci Test'
	and (q.ClientIpAddress not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 1) or q.ClientIpAddress is null)
	and q.surname not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 4)

	order by q.create_timestamp DESC
	--OPTION (OPTIMIZE FOR (@petName UNKNOWN, @postCode UNKNOWN, @surname UNKNOWN, @affinity UNKNOWN, @quoteref UNKNOWN, @email UNKNOWN, @user_id UNKNOWN))
	OPTION (RECOMPILE)

END
ELSE
BEGIN
	SELECT TOP 1000 q.id,
		quote_status_id,
		dbo.PROPERCASE(q.surname) AS owner,
		q.quote_ref,
		ISNULL(q.all_pet_names,'') pet_name,
		q.postcode,
		premium,
		qs.description AS quote_status,
		q.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL THEN 'C' 
					ELSE su.Initial END		
			+ ISNULL('/'+su1.Initial,'')
			+ ISNULL('/'+su2.Initial,'')
		source,
		q.ReferrerID AS referrer
	,CASE ISNULL(agree_contact,0) WHEN 1 THEN 'N' ELSE 'Y' END ok_to_contact
	,CASE ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) WHEN 1 THEN 'N' ELSE 'Y' END agree_contact_phone
		,CASE ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) WHEN 1 THEN 'N' ELSE 'Y' END agree_contact_SMS
		,CASE ISNULL(agree_contact_email,ISNULL(agree_contact,0)) WHEN 1 THEN 'N' ELSE 'Y' END agree_contact_email
		,CASE ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) WHEN 1 THEN 'N' ELSE 'Y' END agree_contact_letter
	,q.email
	FROM quote q
	JOIN quote_status qs ON q.quote_status_id = qs.id
	LEFT JOIN sales_ip_address s ON s.ip_address = q.ClientIpAddress
	LEFT JOIN uispetmis..systuser su ON su.id = q.created_systuser_id
	LEFT JOIN uispetmis..systuser su1 ON su1.id = q.modified_systuser_id
	LEFT JOIN uispetmis..systuser su2 ON su2.id = q.converted_systuser_id
	JOIN uispetmis..systuser_affinity sa ON sa.systuser_id = @systuser_id AND LEFT(sa.affinity_code,3) = LEFT(q.quote_ref,3) COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE
    ISNULL(qs.id,0) <> 9 --Suspicious Activity
	AND q.create_timestamp >= @from_date_parsed AND q.create_timestamp < @to_date_parsed
	AND quote_status_id = @status
	AND q.quote_ref LIKE @affinity+'%'
	AND q.quote_ref LIKE '%'+@quoteref+'%'
	AND q.surname LIKE @surname+'%'
	AND ISNULL(q.all_pet_names,'') LIKE '%'+@petname+'%'
	AND REPLACE(q.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
	AND ((@user_id <> 0 AND @user_id IN (created_systuser_id, modified_systuser_id, converted_systuser_id)) OR (@user_id=0))
	--and left(quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
	AND q.email LIKE '%'+@email+'%'
	
	--and q.surname <> 'Ci Test'
	AND (q.ClientIpAddress NOT IN (SELECT Value FROM QuoteListExclusion WHERE QuoteListExclusionTypeId = 1) OR q.ClientIpAddress IS NULL)
	AND q.surname NOT IN (SELECT Value FROM QuoteListExclusion WHERE QuoteListExclusionTypeId = 4)

	ORDER BY q.create_timestamp DESC
	--OPTION (OPTIMIZE FOR (@petName UNKNOWN, @postCode UNKNOWN, @surname UNKNOWN, @affinity UNKNOWN, @quoteref UNKNOWN, @email UNKNOWN, @user_id UNKNOWN))
	OPTION (RECOMPILE)

END

SET ROWCOUNT 0


SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[proper_case_string]'
GO

CREATE FUNCTION [dbo].[proper_case_string] 
(
	@string VARCHAR(128)
)  
RETURNS VARCHAR(128) AS  
	BEGIN 
		DECLARE @retval		VARCHAR(128)
		DECLARE @count		INT 
		DECLARE @count2		INT

		--Make everything lowercase to begin with.
		SET @string = LOWER(@string)
		
		SET @count = LEN(@string)
		SET @count2 = 1 

		SET @retval = UPPER(LEFT(@string, 1))
		
		WHILE @count > @count2			
			BEGIN
				
				IF SUBSTRING(@string,@count2 , 1) = ' ' 
					SET @retval = @retval + UPPER(SUBSTRING(@string,@count2 + 1  , 1))
				ELSE
					SET @retval = @retval + SUBSTRING(@string,@count2 + 1  , 1)
				SET @count2 = @count2  + 1  
			END 

		RETURN @retval
		
	END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound]'
GO
CREATE TABLE [dbo].[outbound]
(
[id] [int] NOT NULL IDENTITY(265892, 1),
[product_id] [int] NULL,
[title] [nvarchar] (64) COLLATE Latin1_General_CI_AS NULL,
[forename] [nvarchar] (64) COLLATE Latin1_General_CI_AS NULL,
[surname] [nvarchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address1] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address3] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[address4] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[postcode] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email_address] [nvarchar] (256) COLLATE Latin1_General_CI_AS NULL,
[telephone] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[client_type] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[client_status] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[create_timestamp] [datetime] NULL,
[update_timestamp] [datetime] NULL,
[quote_id] [int] NULL,
[outbound_status_id] [int] NULL,
[last_user_id] [int] NULL,
[attempts] [int] NULL CONSTRAINT [DF_outbound_attempts2] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [pk_outbound] on [dbo].[outbound]'
GO
ALTER TABLE [dbo].[outbound] ADD CONSTRAINT [pk_outbound] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_listL_prc]'
GO




CREATE PROCEDURE [dbo].[outbound_listL_prc] 
@status_id int = 0
,@surname nvarchar(255)
,@postcode varchar(10)
,@user_id int
AS
SET NOCOUNT ON

BEGIN
		select o.*
		,dbo.proper_case_string(o.Title + ' ' + o.forename + ' ' + o.surname) customer
		,o.postcode
		,os.description outbound_status
		,s.Initial last_user
		,coalesce(NULLIF(address4,''), NULLIF(address3,''), address2) as town
	from outbound o
		JOIN outbound_status os ON o.outbound_status_id = os.id
		LEFT JOIN uispetmis..systuser s on o.last_user_id = s.id
	where 
	((@status_id > 0 and outbound_status_id = @status_id) OR (@status_id = 0 and outbound_status_id < 31841))	--Unable To Contact
	and o.surname like @surname+'%'
	and REPLACE(o.postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
	and ((@user_id <> 0 AND @user_id = o.last_user_id) or (@user_id=0))
	order by o.surname
END

set rowcount 0

SET NOCOUNT OFF










GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit_vfvL_prc]'
GO
CREATE PROCEDURE [dbo].[cover_benefit_vfvL_prc] (@quote_id int)
AS

SET NOCOUNT ON

declare @policy_start_date date
select @policy_start_date = policy_start_date from quote where id = @quote_id

select 0 ordinal,'' Col1,'Gold' Col2,'Silver' Col3,'Bronze' Col4
union
select 1, 'Preventative Care Package', 'Included', 'Not Included', 'Not Included'
union
select 2, 'Veterinary Fees<sup>1</sup><br /><sup>per condition per policy period</sup>', '£5,000', '£5,000', '£2,000'
union
select 3, 'Complementary Medicine<sup>1</sup>', '£750', '£750', '£500'
union
select 4, 'Special Diet<sup>1</sup>', '£100', '£100', '£100'
union
select 5, 'Death of Pet from Accident<br />or Illness<sup>2</sup><br />', '£1,000', '£1,000', '£750'
union
select 6, 'Public Liability<sup>3</sup>', '£2,000,000', '£2,000,000', '£2,000,000'
union
select 7, 'Emergency Boarding Kennel<br />& Cattery Fees', '£750', '£750', '£750'
union
select 8, 'Holiday Cancellation', '£1,000', '£1,000', '£1,000'
union
select 9, 'Theft or Straying<br />Advertising and Reward', '£1,000<br />£250', '£1,000<br />£250', '£750<br />£100'
union
select 10, 'Accidental Damage', '£250', '£250', '£50'
union
select 11, 'Transportation Costs', '£200', '£200', '£100'
union
select 12, 'Personal Accident', '£200', '£200', '£50'
union
select 13, 'Dog Walker', '£100', '£100', '£100'
union
select 14, 'Overseas Travel - Emergency<br />Vet Treatment Abroad', '£2,000', '£2,000', '£2,000'
union
select 15, 'Overseas Travel – Quarantine<br />Loss of Documents', '£1,500<br />£250', '£1,500<br />£250', '£750<br />£150'
where @policy_start_date >= '2009-12-01'



SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discount_fund]'
GO
CREATE TABLE [dbo].[discount_fund]
(
[id] [int] NOT NULL,
[description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_discount_fund] on [dbo].[discount_fund]'
GO
ALTER TABLE [dbo].[discount_fund] ADD CONSTRAINT [PK_discount_fund] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_introductory_discountL_prc]'
GO
CREATE PROCEDURE [dbo].[cover_introductory_discountL_prc]  (
	@cover_id int
)

AS

SET NOCOUNT ON

	select 
		cid.* ,
		df.description as funded_by_description,
		case when ISNULL(cid.inactive_date,'2050-01-01') >= CONVERT(date, GETDATE()) then 'Active' else 'Inactive' end as discount_status_description,
		case when ISNULL(cid.inactive_date,'2050-01-01') >= CONVERT(date, GETDATE()) then 1 else 0 end as discount_status
	from cover_introductory_discount cid
	inner join discount_fund df on df.id = cid.funded_by
	where cid.cover_id = @cover_id
	order by discount_status,  cid.effective_date;


	select 
		isnull(SUM(cid.discount),0) as total_cover_discount
	from cover_introductory_discount cid
	inner join discount_fund df on df.id = cid.funded_by
	where cid.cover_id = @cover_id
	and ISNULL(cid.inactive_date,'2050-01-01') >= CONVERT(date, GETDATE());

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outbound_quoteU_prc]'
GO


CREATE PROCEDURE [dbo].[outbound_quoteU_prc] @id int, @client_ip nvarchar(20), @user_id int
AS

SET NOCOUNT ON

declare @product_id int
	,@title_id int
	,@forename varchar(32)
	,@surname varchar(32)
	,@address1 varchar(50)
	,@address2 varchar(32)
	,@address3 varchar(32)
	,@address4 varchar(32)
	,@postcode varchar(16)
	,@telephone varchar(32)
	,@email_address varchar(64)
	,@policy_start_date smalldatetime	--default policy start date to today

--get the matching title id from the title table
select @title_id = t.id
from outbound o 
	LEFT JOIN title t on t.description = o.title
where o.id = @id

if(@title_id IS NULL)
select @title_id = ISNULL(t.id,1)
from outbound o 
	LEFT JOIN title t on t.description = o.title + '.'
where o.id = @id


select @product_id = product_id 
	,@forename = forename
	,@surname = surname
	,@address1 = address1
	,@address2 = address2
	,@address3 = address3
	,@address4 = address4
	,@postcode = postcode
	,@telephone = telephone
	,@email_address = email_address
	,@policy_start_date = convert(varchar, getdate(),106)	
from outbound
where id = @id

declare @quote_record table (new_quote_ref varchar(16), new_id int, prod_prefix varchar(3),referrer varchar(50))

insert into @quote_record
exec [dbo].[quoteU_prc]
		@action='I'
		,@product_id = @product_id
		,@title_id = @title_id
		,@forename = @forename
		,@surname = @surname
		,@postcode = @postcode
		,@house_name_num = @address1
		,@street = @address2
		,@street2 = @address3
		,@town = @address4
		,@contact_num = @telephone
		,@email = @email_address
		,@policy_start_date = @policy_start_date
		,@promo_code = ''
		,@client_ip = @client_ip
		,@created_systuser_id = @user_id
		,@referrer_id = 'OUTBOUND'

select new_id quote_id from @quote_record

SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_introductory_discountU_prc]'
GO
create PROCEDURE [dbo].[cover_introductory_discountU_prc]  (
	 @id int = null
	,@inactive_date smalldatetime = null
	,@effective_date smalldatetime = null
	,@funded_by_id int = null
	,@discount decimal(10,2) = null
	,@cover_id int = null
)

AS

SET NOCOUNT ON


if @id is not null
begin
	--update	
	if @inactive_date is null
		set @inactive_date = GETDATE();

	update cover_introductory_discount set inactive_date = @inactive_date where id = @id
end
else
begin
	--insert
	if @cover_id is not null
	or @effective_date is not  null
	or @discount is not  null
	or @funded_by_id is not  null
	begin
		--validation okay
		insert into cover_introductory_discount(cover_id,discount, effective_date, inactive_date, funded_by)
		values (@cover_id, @discount, @effective_date, @inactive_date, @funded_by_id);
	end
end

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_quoteidU_prc]'
GO

-- =============================================
-- Author:		Steve Powell
-- Create date: 16/12/2011
-- Description:	Prcedure to update the aggregator quote with their ref no
-- =============================================
CREATE PROCEDURE [dbo].[aggregator_quoteidU_prc]
	-- Add the parameters for the stored procedure here
	@agg_quote_id int,
	@quote_ref varchar(16),
	@agg_remote nvarchar(50)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--validate the request by both id and quote ref
	IF NOT EXISTS(SELECT 1 FROM aggregator where id = @agg_quote_id and quote_ref = @quote_ref)
	RETURN

    UPDATE [uispet].[dbo].[aggregator]
    SET 
		remote_reference = @agg_remote
	WHERE
		id = @agg_quote_id
		
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetSummaryByReferenceAndSource]'
GO

CREATE PROCEDURE [Pricing].[GetSummaryByReferenceAndSource]
	@quoteReference VARCHAR(30),
	@sourceId INT
AS
BEGIN

	SET NOCOUNT ON;

	-- Website
	IF(@sourceId = 1)
	BEGIN

	;WITH webPrices
	AS
	(
		SELECT
		RANK() OVER(PARTITION BY b.SequenceID ORDER BY b.Created DESC) AS Row#,
		assets.AssetID AS AssetID,
		b.CoverId AS CoverID,
		b.Premium AS Premium,
		b.Created AS Created,
		b.Breakdown AS Breakdown,
		b.Id AS BreakdownId
		FROM [uispet].[dbo].[quote_pet] qp
		INNER JOIN uispet..quote q ON q.id = qp.quote_id
		RIGHT JOIN uispet.Pricing.Breakdown b ON b.Identifier = qp.id AND b.SourceId = 1
		INNER JOIN uispet..Assets assets ON assets.Type = 'Quote' AND assets.Identifier = qp.id
		WHERE q.quote_ref = @quoteReference
		AND b.Created >= qp.update_timestamp
	)


    SELECT
	@quoteReference AS Quote,
	wp.AssetID AS Asset,
	wp.CoverId AS Cover,
	wp.Premium AS Premium,
	wp.Created AS Created,
	wp.Breakdown AS Breakdown,
	wp.BreakdownId AS BreakdownId
	FROM webPrices wp
	WHERE wp.Row# = 1;

	END
	
	-- Aggregator
	IF(@sourceId = 4)
	BEGIN

	SELECT
	a.quote_ref AS Quote,
	assets.AssetID AS Asset,
	b.CoverId AS Cover,
	b.Premium AS Premium,
	b.Created AS Created,
	b.Breakdown AS Breakdown,
	b.Id AS BreakdownId
	FROM [uispet].[dbo].[aggregator_pet] ap
	INNER JOIN uispet..aggregator a ON a.id = ap.aggregator_id
	RIGHT JOIN uispet.Pricing.Breakdown b ON b.Identifier = ap.id AND b.SourceId = 4
	INNER JOIN uispet..Assets assets ON assets.Type = 'Aggregator' AND assets.Identifier = ap.id
	WHERE a.quote_ref = @quoteReference
	AND b.Created >= ap.update_timestamp

	END

	-- Enquiry
	IF(@sourceId = 5)
	BEGIN

	SELECT 
	@quoteReference AS Quote,
	b.AssetID AS Asset,
	b.CoverId AS Cover,
	b.Premium AS Premium,
	b.Created AS Created,
	b.Breakdown AS Breakdown,
	b.Id AS BreakdownId
	FROM uispet.Pricing.Breakdown b
	WHERE b.SourceId = 5
	AND b.SequenceID = @quoteReference;

	END

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[coverL_prc]'
GO
CREATE PROCEDURE [dbo].[coverL_prc]  (
	@onlyActive int = 1
)

AS

SET NOCOUNT ON

	select 
		c.*, 
		p.id as	product_id,
		p.description as product_description,
		c.affinity_code +  ' - ' + p.description + ' - '+c.description as cover_full_description
	from cover c
	inner join product p on p.id = c.product_id
	where (@onlyActive = 1 and GETDATE() < ISNULL(c.cover_inactive_date,'2050-01-01'))
		or (@onlyActive = 0)
	order by cover_full_description asc



SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outboundS_prc]'
GO



CREATE PROCEDURE [dbo].[outboundS_prc] (@id int)
AS
SET NOCOUNT ON

BEGIN
		select o.*
			,q.quote_ref
			,os.description outbound_status
			,dbo.proper_case_string(o.Title + ' ' + o.forename + ' ' + o.surname) customer

		from outbound o 
			left join quote q on o.quote_id = q.id
			join outbound_status os on o.outbound_status_id = os.id
		where o.id = @id
END

set rowcount 0

SET NOCOUNT OFF









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_status]'
GO
CREATE TABLE [dbo].[lead_status]
(
[id] [int] NOT NULL IDENTITY(100, 1),
[description] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_lead_status] on [dbo].[lead_status]'
GO
ALTER TABLE [dbo].[lead_status] ADD CONSTRAINT [PK_lead_status] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_listL_prc]'
GO











CREATE PROCEDURE [dbo].[lead_listL_prc] 
@status_id int = 100
,@days int
,@clinic_id int
,@affinity varchar(10)
,@surname nvarchar(255)
,@petname nvarchar(155)
,@postcode varchar(10)
,@user_id int
,@auto tinyint = 0
WITH RECOMPILE AS

begin
--SET NOCOUNT ON

declare @date date
select @date = dateadd(d, @days, getdate())

select top 1000 CASE WHEN @auto  = 0 and lead_status_id = 100 AND DATEDIFF(day, getdate(), contact_date) < 0 THEN 'Overdue!'
			WHEN @auto  = 0 and lead_status_id = 100 AND DATEDIFF(day, getdate(), contact_date) = 0 THEN 'Today!'
			ELSE 'View!' END actiontext
	,q.*
	,q.surname as owner
	,ISNULL(lp.pet_name,'') pet_name
	,q.postcode
	,qs.description lead_status
	,'' last_user
	,p.prod_prefix affinity
	,c.description clinic
	,ISNULL(q.source,'') source
	,agg.quote_ref
	,COALESCE(q.contact_time, lt.time_description) as lead_time_description
from lead q
	left JOIN lead_pet lp on lp.lead_id = q.id
	JOIN lead_status qs ON q.lead_status_id = qs.id
	JOIN product p on q.product_id = p.id
	LEFT JOIN clinic c on q.clinic_id = c.id
	LEFT JOIN aggregator agg on agg.lead_id = q.id
	left join lead_time lt on q.lead_time_id = lt.id
where lead_status_id = @status_id
and CONVERT(date, contact_date) <= @date
and p.prod_prefix like @affinity+'%'
and q.surname like @surname+'%'
and REPLACE(q.postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
and (((@clinic_id > 0 OR @clinic_id < 0) AND q.clinic_id = @clinic_id) OR (@clinic_id=0))
and ((@auto = 0 and ISNULL(q.source,'')!= 'gocomp') or (@auto = 1 and ISNULL(q.source,'') = 'gocomp'))
order by q.contact_date desc, q.contact_time desc

set rowcount 0

SET NOCOUNT OFF

END











GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_detail]'
GO
CREATE TABLE [dbo].[aggregator_detail]
(
[id] [int] NOT NULL,
[description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[referrer_code] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[sales_channel] [int] NOT NULL CONSTRAINT [DF__aggregato__sales__69678A99] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator_details] on [dbo].[aggregator_detail]'
GO
ALTER TABLE [dbo].[aggregator_detail] ADD CONSTRAINT [PK_aggregator_details] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_aggregator_details] on [dbo].[aggregator_detail]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_aggregator_details] ON [dbo].[aggregator_detail] ([referrer_code])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DeriveSalesSource]'
GO
CREATE FUNCTION [dbo].[DeriveSalesSource]
(
	@quoteId INT
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @result VARCHAR(15);
	
	IF @quoteId IS NULL
		RETURN 'Imported'; --TODO: Replace with CDL mappings when doing the CDL work
	ELSE
	BEGIN
		SELECT @result =
			CASE
				-- Quotes with no intervention (online) and an Agg ReferrerId
				WHEN (ISNULL(q.created_systuser_id, 0) = 0 AND ISNULL(q.modified_systuser_id,0) = 0 AND ISNULL(q.converted_systuser_id,0) = 0 
					AND ISNULL(q.referrerid,'') IN (SELECT DISTINCT referrer_code FROM uispet..aggregator_detail)) 
					THEN 'Aggregator'

				WHEN (ISNULL(q.created_systuser_id, 0) = 0 AND ISNULL(q.modified_systuser_id,0) = 0 AND ISNULL(q.converted_systuser_id,0) = 0 
					AND ((ISNULL(q.referrerid,'') = 'CVS-Iframe') OR ISNULL(q.referrerid,'') = 'Vet')) 
					THEN 'Referral'

				-- Quotes with no intervention (online) and No Agg ReferrerId
				WHEN (ISNULL(q.created_systuser_id, 0) = 0 AND ISNULL(q.modified_systuser_id,0) = 0 AND ISNULL(q.converted_systuser_id,0) = 0 
					AND ISNULL(q.referrerid,'') NOT IN (SELECT DISTINCT referrer_code FROM uispet..aggregator_detail)) 
					THEN 'Website'
				
				ELSE 'Telephone'
			END
		FROM quote q
		WHERE q.id = @quoteId;
	END;
		
	RETURN @result;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_breed_group_ZEN]'
GO



CREATE FUNCTION [dbo].[get_breed_group_ZEN](
	@CoverId INT,
	@UPPBreedId INT
) 
RETURNS INT
AS 
BEGIN
	DECLARE @BreedGroupId AS INT;
	DECLARE @ZENBreedId INT

	--Get Zen Breed id
	SELECT @ZENBreedId = zenith_breed_id
	FROM uispet..zenith_breed_mapping 
	WHERE upp_breed_id = @UPPBreedId
	AND Cover_Id = @CoverId

	--Get Zen Breed Group
	SELECT TOP 1 @BreedGroupId = RenewalBreedGroupId
	FROM ZenithPricingEngine..BreedGroup bg 
	WHERE BreedId = @ZENBreedId

	RETURN @BreedGroupId;
END 


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[agg_quote_listL_prc]'
GO



CREATE PROCEDURE [dbo].[agg_quote_listL_prc] @affinity varchar(10), @surname nvarchar(255), @petname nvarchar(155), @postcode varchar(10), @quoteref varchar(20) = NULL, @systuser_id int,
	@from_date varchar(10), @to_date varchar(10), @email varchar(64), @archive bit = 0, @source varchar(64) = null
AS
SET NOCOUNT ON

-- Parse FROM and TO dates
declare @from_date_parsed smalldatetime;
set @from_date_parsed = convert(smalldatetime, @from_date, 103);

declare @to_date_parsed smalldatetime
set @to_date_parsed = convert(smalldatetime, @to_date, 103);
set @to_date_parsed = DATEADD(d,1,@to_date_parsed)

IF @archive = 1
BEGIN
		select top 500 a.id,		
			dbo.PROPERCASE(a.surname) as owner,
			a.quote_ref,
			isnull(a.all_pet_names,'') pet_name,
			UPPER(a.postcode) postcode,
			null as premium,
			'Aggregator Quote' as quote_status,
			99 as quote_status_id,
			a.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL then
			case WHEN s.ip_address IS NULL then 'C' else 'S' END 
			else su.Initial end		
			+ ISNULL('/'+su1.Initial,'')
			+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
			source
			,a.source as referrer,
			'N' as ok_to_contact,
			q.quote_ref as live_quote_ref,	
			ISNULL(a.quote_id,0) as live_quote_id,
			a.email,
			q.agree_contact_phone,
			q.agree_contact_sms,
			q.agree_contact_email,
			q.agree_contact_letter
		from aggregator_archive a		
			LEFT JOIN quote q on a.quote_id = q.id
			LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
			LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
			LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
			LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
			join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(a.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
			--left join aggregator_pet ap on a.id = ap.aggregator_id
		where
			a.create_timestamp >= @from_date_parsed AND a.create_timestamp < @to_date_parsed
		and a.quote_ref like @affinity+'%'
		and a.quote_ref like '%'+@quoteref+'%'
		and a.surname LIKE @surname+'%'
		and isnull(a.all_pet_names,'') LIKE '%'+@petname+'%'
		and REPLACE(a.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
		--and left(a.quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
		and a.email like '%'+@email+'%'
		and a.source like 
			case 
			when @source IS null then '%'
			else @source
			end
		order by a.create_timestamp desc
END
ELSE
BEGIN
		select top 500 a.id,		
			dbo.PROPERCASE(a.surname) as owner,
			a.quote_ref,
			isnull(a.all_pet_names,'') pet_name,
			UPPER(a.postcode) postcode,
			null as premium,
			'Aggregator Quote' as quote_status,
			99 as quote_status_id,
			a.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL then
			case WHEN s.ip_address IS NULL then 'C' else 'S' END 
			else su.Initial end		
			+ ISNULL('/'+su1.Initial,'')
			+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
			source
			,a.source as referrer,
			'N' as ok_to_contact,
			q.quote_ref as live_quote_ref,	
			ISNULL(a.quote_id,0) as live_quote_id,
			a.email,
			q.agree_contact_phone,
			q.agree_contact_sms,
			q.agree_contact_email,
			q.agree_contact_letter
		from aggregator (NOLOCK) a		
			LEFT JOIN quote q on a.quote_id = q.id
			LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
			LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
			LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
			LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
			join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(a.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
			--left join aggregator_pet ap on a.id = ap.aggregator_id
		where
			a.create_timestamp >= @from_date_parsed AND a.create_timestamp < @to_date_parsed
		and a.quote_ref like @affinity+'%'
		and a.quote_ref like '%'+@quoteref+'%'
		and a.surname LIKE @surname+'%'
		and isnull(a.all_pet_names,'') LIKE '%'+@petname+'%'
		and REPLACE(a.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
		--and left(a.quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
		and a.email like '%'+@email+'%'
		and a.source like 
			case 
			when @source IS null then '%'
			else @source
			end
		order by a.create_timestamp desc

END

set rowcount 0

SET NOCOUNT OFF









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_address_format]'
GO
CREATE FUNCTION [dbo].[quote_address_format] (@quote_id INT)  
RETURNS VARCHAR(2000) AS  
	BEGIN 
		DECLARE @retval varchar(2000)

		DECLARE @house_name_num VARCHAR(50)
		DECLARE @street VARCHAR(32)
		DECLARE @town  VARCHAR(32)
		DECLARE @county VARCHAR(32)

		SELECT  @house_name_num = house_name_num,
				@street = street,
				@town = town,
				@county = county 
		FROM	quote
		WHERE	id = @quote_id 
	 
		IF (@house_name_num IS NOT NULL) AND (@street IS NOT NULL) AND (@town IS NOT NULL) AND (@county IS NOT NULL)
			BEGIN
				SELECT @retval =  @house_name_num + CHAR(10) 
								+ @street+ CHAR(10) 
								+ @town + CHAR(10)
								+ @county 
				
			END 
		ELSE IF (@house_name_num IS NULL) AND (@street IS NOT NULL) AND (@town IS NOT NULL) AND (@county IS NOT NULL)
			BEGIN
				SELECT @retval = @street+ CHAR(10) 
								+ @town + CHAR(10)
								+ @county 
				
			END 
		ELSE IF (@house_name_num IS NOT NULL) AND (@street IS NULL) AND (@town IS NOT NULL) AND (@county IS NOT NULL)
			BEGIN
				SELECT @retval = @house_name_num  + CHAR(10) 
								+ @town + CHAR(10)
								+ @county 
				
			END 


		RETURN  @retval

	
	END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[outboundU_prc]'
GO



CREATE PROCEDURE [dbo].[outboundU_prc] (@id int, @user_id int, @outbound_status_id int, @notes ntext, @quote_id int = NULL)
AS
SET NOCOUNT ON

BEGIN

	insert into outbound_audit(outbound_id, user_id, outbound_status_id, notes, datetimestamp)
	select @id, @user_id, @outbound_status_id, @notes, CURRENT_TIMESTAMP

	declare @num_attempts int
	select @num_attempts = count(1) 
	from outbound_audit oa 
		join outbound_status os on os.id = oa.outbound_status_id
	where oa.outbound_id = @id
	and os.completed = 1

	update outbound 
	set outbound_status_id = @outbound_status_id 
		,last_user_id = @user_id
		,attempts = @num_attempts
		,update_timestamp = CURRENT_TIMESTAMP
		,quote_id = ISNULL(@quote_id, quote_id)
	where id = @id

END

set rowcount 0

SET NOCOUNT OFF









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_initialsS_prc]'
GO



CREATE PROCEDURE [dbo].[quote_initialsS_prc] @id int
as

set nocount on

	select created_systuser_id, modified_systuser_id, converted_systuser_id, quote_status_id
	from quote
	where id = @id
		

set nocount off
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_initialsU_prc]'
GO



CREATE PROCEDURE [dbo].[quote_initialsU_prc] @id int, @created_systuser_id int, @modified_systuser_id int, @converted_systuser_id int
as

set nocount on

	update quote 
	set created_systuser_id = @created_systuser_id
		,modified_systuser_id = @modified_systuser_id
		,converted_systuser_id = @converted_systuser_id
	where id = @id
		

set nocount off
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[fnRemoveNonNumericCharacters]'
GO

CREATE Function [dbo].[fnRemoveNonNumericCharacters](@strText VARCHAR(1000)) 
RETURNS VARCHAR(1000) 
AS 
BEGIN 
    WHILE PATINDEX('%[^0-9]%', @strText) > 0 
    BEGIN 
        SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '') 
    END 
    RETURN @strText 
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit_HTML_prc]'
GO


CREATE PROCEDURE [dbo].[cover_benefit_HTML_prc] (@pet_id INT)
AS
SET NOCOUNT ON


declare @product_id INT
declare @cover_id INT

select @product_id = product_id
	,@cover_id=qp.cover_id 
from quote_pet qp join cover c on c.id = qp.cover_id
where qp.id = @pet_id

select '<tr><td align="right" width="40%">' + s.description + ':</td><td width="1%">&nbsp;</td><td><strong>' + cb.description + '</strong></td></tr>' as Row 
	
from cover_benefit cb join section s on cb.section_id = s.id
where cb.cover_id = @cover_id
and (section_num is not null OR @product_id = 3)
and ordinal > 0
order by convert(int, dbo.fnRemoveNonNumericCharacters(section_num)), s.id
	

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quoteS_prc_test]'
GO


CREATE PROCEDURE [dbo].[quoteS_prc_test]  (@id INT)
		
AS
SET NOCOUNT ON


		SELECT	q.id, 
				q.quote_ref,
				q.title_id,
				ISNULL(q.forename,'') as forename,
				ISNULL(q.surname,'') as surname,
				DATEPART(d, q.DOB) as DOB_dd,
				DATEPART(m, q.DOB) as DOB_mm,
				DATEPART(yyyy, q.DOB) as DOB_yyyy,
				ISNULL(q.postcode,'') as postcode,
				ISNULL(q.house_name_num,'') as house_name_num,
				ISNULL(q.street,'') as street ,
				ISNULL(q.street2,'') as street2 ,
				ISNULL(q.town,'') as town,
				ISNULL(q.county,'') as county,
				ISNULL(q.contact_num,'') as contact_num,
				ISNULL(q.email,'') as email,
				q.quote_status_id,
				q.cover_id,
				q.product_excess_id,
				q.occupation_id,
				q.hear_about_us_id,
				ISNULL(q.other_policies,'') as other_policies,
				q.motor_insurance_due,
				q.home_insurance_due,
				CONVERT(VARCHAR(11), q.policy_start_date,113) as policy_start_date,
				ISNULL(q.agree_contact, 0) as agree_contact,
				ISNULL(q.VFV_clinic,'') as VFV_clinic,
				ISNULL(q.VFV_clinicID,'') as VFV_clinicID,
				ISNULL(q.VFV_clinicID,'') as clinic_id,
				ISNULL(c.clinic_group_id,'') as clinic_group_id,
				q.agree_terms,
				q.promo_code,
				q.referrerID,
				q.agree_illvac_excl,
				q.agree_preexisting_excl,
				q.agree_vicious_excl,
				q.agree_waiting_excl,
				ISNULL(t.description,'') as title,
				p.description productdescription
		FROM	quote q
			left join clinic c on c.id = q.vfv_clinicID
			LEFT JOIN title t on q.title_id = t.id
			LEFT JOIN product p on p.id=q.product_id
		WHERE	q.id = @id

		select qp.id, ISNULL(qp.premium,0) premium, ISNULL(qpc.charity_id,0) charity_id
		from quote_pet qp
			LEFT JOIN quote_pet_charity qpc on qpc.quote_pet_id = qp.id
		where quote_id = @id

SET NOCOUNT OFF





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_emailS_prc]'
GO
CREATE PROCEDURE [dbo].[quote_emailS_prc] (@id INT, @email_type INT = 1)
AS

SET NOCOUNT ON

	DECLARE @email_subject VARCHAR(128)
	DECLARE @email_body	VARCHAR(8000)
	DECLARE @email_to	VARCHAR(64)
	DECLARE @email_blind_cc	VARCHAR(64)
	DECLARE @email_from VARCHAR(64)
	DECLARE	@email_header VARCHAR(8000)
	DECLARE	@pet_details VARCHAR(8000)
	DECLARE	@policy_details VARCHAR(8000)
	DECLARE	@cust_details	VARCHAR(8000)
	DECLARE	@price_details	VARCHAR(256)
	DECLARE	@price		MONEY
	DECLARE	@cover_required	VARCHAR(256)
	DECLARE @DD INT
	DECLARE @cover_id INT


	IF @email_type = 1 --Successful Application. 
		BEGIN

			select @DD = isnull(payment_type_id ,-1)
			FROM payment_detail pd, quote q
			where q.payment_detail_id = pd.id
			and q.id = @id

			If @DD is NULL
			BEGIN
				set @DD = -1
			END

			SELECT 	DD=@DD, cover_id = c.id,
					email_subject = 'Pet Insurance From ' + ISNULL(p.description,'') + '. Quote Ref: ' + ISNULL(q.quote_ref, 'Not Supplied'),
					email_to	= q.email,
					email_from = p.email,
					email_blind_cc = p.admin_email,
					price		= ISNULL(qp.premium, 0),
					cover_required = ISNULL(c.description, 'Not Supplied'),
					petid = qp.id,
					policy_id = pol.policyid
			FROM	quote q
				JOIN quote_pet qp on qp.quote_id = q.id
				JOIN UISPETMIS..policy pol on qp.id = pol.quote_pet_id
				LEFT JOIN	title t ON	t.id = q.title_id
				JOIN	product p ON	p.id = q.product_id
				JOIN	cover c ON	c.id = qp.cover_id
			WHERE	q.id = @id


					
		END 
	ELSE IF @email_type = 2 --Failed application
		BEGIN
			SELECT	@email_subject = 'Failed Application - Quote Ref: ' + ISNULL(q.quote_ref, 'Not Supplied'),
					@email_header = 'Please find details of failed application below.' + CHAR(10) + CHAR(13),
					@email_to	=  p.admin_email,
					@email_from = p.email,
					@email_blind_cc = '',
					@price		= ISNULL(q.premium, 0),
					@cover_required = ISNULL(c.description, 'Not Supplied')						
			FROM	quote q
			LEFT JOIN	title t 
				ON	t.id = q.title_id
			JOIN	product p 
				ON	p.id = q.product_id
			LEFT JOIN	cover c 
				ON	c.id = q.cover_id
			WHERE	q.id = @id		
		END 
	ELSE IF @email_type = 3  --Need admin to ring the punter
		BEGIN
			declare @incident varchar(4000)
				select @incident = 'Vet Visit Details -'  + convert(varchar(2000),q.vetvisitdetails)
						+ CHAR(10) +
						'Incident Details '  + convert(varchar(2000),q.accidentdetails)
				FROM	quote_pet q
				WHERE	q.quote_id =@id
		
			SELECT	@email_subject = 'Please contact customer - Quote Ref: ' + ISNULL(q.quote_ref,'Not Supplied'),
					@email_header = 'Please find details of application below.' + CHAR(10) + CHAR(13) + @incident + CHAR(10) + CHAR(13) ,
					@email_to	=  p.admin_email,
					@email_from = p.email,
					@email_blind_cc = 'craig.simpson@iridescent.co.uk;Russell.Hughes@ultimateservices.co.uk',
					@price		= ISNULL(q.premium, 0),
					@cover_required = ISNULL(c.description, 'Not Supplied')						
			FROM	quote q
			LEFT JOIN	title t 
				ON	t.id = q.title_id
			JOIN	product p 
				ON	p.id = q.product_id
			LEFT JOIN	cover c 
				ON	c.id = q.cover_id
			WHERE	q.id = @id		


		END 
	
		SELECT	@pet_details = 'Pet Name: ' + ISNULL(q.pet_name,'Not Supplied') + CHAR(10) 
							+ 'Pet Type: ' + ISNULL(at.description,'Not Supplied')  + CHAR(10) 
							+ 'Gender: ' + ISNULL(pg.gender_desc,'Not Supplied')  + CHAR(10) 
							+ 'DOB: '	+ ISNULL(convert(varchar(11),q.pet_DOB,113),'Not Supplied') + CHAR(10)
							+ 'Breed: ' + ISNULL(b.description,'Not Supplied')  + CHAR(10) 
							+ 'Pet Type: ' + ISNULL(at.description,'Not Supplied')  + CHAR(10) 
		FROM	quote_pet q
		LEFT JOIN	product_gender pg
			ON	pg.id = q.product_gender_id
		LEFT JOIN	animal_type at 
			ON	at.id = q.animal_type_id
		LEFT JOIN	breed b 
			ON	b.id = q.breed_id
		WHERE	q.quote_id = @id

		SELECT @cust_details = 'Salutation: ' + ISNULL(t.description,'Not Supplied') + CHAR(10)
							+ 'Forename: ' + ISNULL(q.forename,'Not Supplied')  + CHAR(10)
							+ 'Surname: ' + ISNULL(q.surname,'Not Supplied')  + CHAR(10)
							+ 'Address2: ' + ISNULL(q.street,'Not Supplied')  + CHAR(10)
							+ 'Town: ' + ISNULL(q.town,'Not Supplied')  + CHAR(10)
							+ 'County: ' + ISNULL(q.county,'Not Supplied') + CHAR(10)
							+ 'Postcode: ' + ISNULL(q.postcode,'Not Supplied')  + CHAR(10)
							+ 'Tel: ' + ISNULL(q.contact_num,'Not Supplied')  + CHAR(10)
							+ 'Email: ' + ISNULL(q.email,'Not Supplied') 
		FROM	quote q 
		LEFT JOIN	title t 
			ON	t.id = q.title_id
		LEFT JOIN	hear_about_us h
			ON	h.id = q.hear_about_us_id 
		WHERE	q.id = @id
/*		
		SELECT	@pet_details = @pet_details + c.description + ': ' + CASE WHEN q.product_underwriting_check_id  IS NULL THEN 'No' ELSE 'Yes' END + CHAR(10) 
		FROM	product_underwriting_check  c 
		LEFT JOIN product_underwriting_check_quote q 
			ON	q.product_underwriting_check_id  = c.id
			AND q.quote_id = @id
*/
		IF @price > 0
			BEGIN
				SET @price_details = '£' + CONVERT(VARCHAR(8), (@price)) + ' per month. (£' + CONVERT(VARCHAR(8),@price * 12) + ' per year)'
			END
		ELSE
			BEGIN
				SET @price_details = 'Not Supplied'
			END 

		SELECT @email_body = @email_header + '--------------------------------------------------------' + CHAR(10) + CHAR(13) 
					+ 'Cover Summary: '+ CHAR(10) + CHAR(13)
					+ 'Cover required: ' + @cover_required +   CHAR(10) + CHAR(13)
					+ 'Your details' + CHAR(10) + CHAR(13) + @cust_details + CHAR(10) + CHAR(13) 
					+ 'Pet Details' +  CHAR(10) + CHAR(13)+  @pet_details+ '--------------------------------------------------------' 	+ CHAR(10) + CHAR(13) 
					+ 'Total Cost '+  CHAR(10) +  CHAR(13) + @price_details + CHAR(10) + + '--------------------------------------------------------' 	
					


		SELECT	@email_subject as email_subject,
				@email_body as email_body,
				@email_to as email_to,
				@email_from as email_from,
				@email_blind_cc as email_blind_cc,
				@DD as DD,
				@cover_id as cover_id

SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_expireU_prc]'
GO

CREATE PROCEDURE [dbo].[quote_expireU_prc] 
AS

SET NOCOUNT ON

	
	DECLARE @expiration_days	INT
	DECLARE @product_id			INT
	DECLARE	@products			TABLE (id INT IDENTITY(1,1), product_id INT, quote_expiration_days INT)
	DECLARE	@count				INT
	
	INSERT	@products (product_id,  quote_expiration_days)
	SELECT	id,  quote_expiration_days
	FROM	product 

	SET @count = @@ROWCOUNT 

	WHILE @count > 0 
		BEGIN
			SELECT	@product_id = id,
					@expiration_days = quote_expiration_days
			FROM	@products 
			WHERE	id = @count 

			UPDATE	quote
			SET		quote_status_id= 4
			WHERE	product_id = @product_id 
			AND		DATEADD(dd, @expiration_days, create_timestamp) < CURRENT_TIMESTAMP  

			SET @count = @count - 1 
		END 
	




SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_detail_createI_prc]'
GO



CREATE PROCEDURE [dbo].[payment_detail_createI_prc] (@quote_pet_id INT, @policy_id INT)				
AS
SET NOCOUNT ON

--in case of DD specify the collection date
--declare @payday int
--select @payday = case when DATEPART(d, startdate) = 1 then 15  
--	when DATEPART(d, startdate) > 14 then 15 else 1 end
--from uispetmis..policy
--where policyid = @policy_id

/******************* PAYDAY CALCULATION *************************/
--Default payday is 14 days after sold, but not before inception.
--anything over 28th flips to 1st
declare @payday int
declare @first_pay_date datetime
select @first_pay_date = DATEADD(DAY, 14, GETDATE())

select @payday = DATEPART(DAY, case when @first_pay_date < StartDate then StartDate else @first_pay_date end)
from uispetmis..policy
where policyid = @policy_id

if @payday > 28 
	select @payday = 1
/******************* END PAYDAY CALCULATION *********************/

declare @card_number varchar(16)
	,@card_name varchar(64)
	,@valid_from varchar(64)
	,@valid_to varchar(64)
	,@account_number varchar(32)
	,@account_name varchar(64)
	,@sort_code varchar(6)
	,@issue_number varchar(4)
	,@payment_type_id int
	,@card_type_id int

OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;

select @card_number = CONVERT(varchar, DecryptByKey(enc_card_number))
	,@card_name = CONVERT(varchar, DecryptByKey(enc_card_name))
	,@valid_from = CONVERT(varchar, DecryptByKey(enc_valid_from))
	,@valid_to = CONVERT(varchar, DecryptByKey(enc_valid_to))
	,@account_number = CONVERT(varchar, DecryptByKey(enc_account_number))
	,@account_name = CONVERT(varchar, DecryptByKey(enc_account_name))
	,@sort_code = CONVERT(varchar, DecryptByKey(enc_sort_code))
	,@issue_number = CONVERT(varchar, DecryptByKey(enc_issue_number))
	,@card_type_id = card_type_id
	,@payment_type_id = payment_type_id
from payment_detail pd
	join quote q on pd.id = q.payment_detail_id
	join quote_pet qp on q.id = qp.quote_id
where qp.id = @quote_pet_id

exec uispetmis..payment_detailU_prc
	@policy_id = @policy_id
	,@card_number = @card_number
	,@card_name = @card_name
	,@valid_from = @valid_from
	,@valid_to = @valid_to
	,@account_number = @account_number
	,@account_name = @account_name
	,@sort_code = @sort_code
	,@issue_number = @issue_number
	,@card_type = @card_type_id
	,@payday = @payday
	,@payment_type_id = @payment_type_id

--insert uispetmis..payment_detail
--	(id,payment_type_id,card_type_id
--		,enc_card_number,enc_card_name,enc_valid_from,enc_valid_to
--		,enc_account_number,enc_account_name,enc_sort_code,enc_issue_number
--		,ClientIPAddress,post_result,create_date,
--		update_date,amount,policy_id,payday)
--
--	select pd.id,
--		payment_type_id,
--		card_type_id,
--		enc_card_number,
--		enc_card_name,
--		enc_valid_from,
--		enc_valid_to,
--		enc_account_number,
--		enc_account_name,
--		enc_sort_code,
--		enc_issue_number,
--		pd.ClientIPAddress,
--		post_result,
--		create_date,
--		update_date,
--		qp.premium,
--		@policy_id,
--		@payday
--from payment_detail pd
--	join quote q on pd.id = q.payment_detail_id
--	join quote_pet qp on q.id = qp.quote_id
--where qp.id = @quote_pet_id

--declare @payment_type_id int
--declare @amount decimal(10,2)

--assume all good
--now quickly if it's a DD payment then exec the schedule proc

--select @payment_type_id = payment_type_id
--	,@amount = qp.premium
--from payment_detail pd
--	join quote q on pd.id = q.payment_detail_id
--	join quote_pet qp on q.id = qp.quote_id
--where qp.id = @quote_pet_id

--IF (@payment_type_id = 3)
--BEGIN
--	select @amount = @amount * 12
--	exec UISPetMIS..payment_schedule_prc @policy_id ,@amount, 1
--END
--ELSE IF (@payment_type_id = 4)
--BEGIN
--	exec UISPetMIS..payment_schedule_prc @policy_id ,@amount, 12
--END

SET NOCOUNT OFF







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_audit]'
GO
CREATE TABLE [dbo].[lead_audit]
(
[id] [int] NOT NULL IDENTITY(20000, 1),
[lead_id] [int] NOT NULL,
[user_id] [int] NOT NULL,
[description] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NOT NULL,
[datetimestamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [pk_lead_audit] on [dbo].[lead_audit]'
GO
ALTER TABLE [dbo].[lead_audit] ADD CONSTRAINT [pk_lead_audit] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_lead_id] on [dbo].[lead_audit]'
GO
CREATE NONCLUSTERED INDEX [idx_lead_id] ON [dbo].[lead_audit] ([lead_id]) WITH (ALLOW_PAGE_LOCKS=OFF)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_leadU_prc]'
GO


CREATE PROCEDURE [dbo].[aggregator_leadU_prc] (@quote_ref varchar(16), @ip_address varchar(20), @tel_no varchar(32) = NULL, @email varchar(64) = NULL)
AS
BEGIN

	SET NOCOUNT ON;

	declare @aggregator_id int
		,@lead_id int
	
	SELECT @aggregator_id = id
		,@lead_id = lead_id
	FROM aggregator 
	WHERE quote_ref = @quote_ref

	IF(@aggregator_id IS NULL)
		RETURN
	ELSE IF(@lead_id IS NOT NULL)
		SELECT @lead_id new_id
	ELSE
	BEGIN

		declare @product_id int
			,@title_id int

		SELECT @product_id = p.id
		FROM product p
			JOIN aggregator a on a.affinity = p.prod_prefix
		WHERE a.quote_ref = @quote_ref

		SELECT @title_id = ISNULL(t.id,1)
		FROM aggregator a 
			LEFT JOIN title t on a.title = t.description
		WHERE a.quote_ref = @quote_ref

			INSERT INTO [dbo].[lead]
           ([product_id]
           ,[title_id]
           ,[forename]
           ,[surname]
           ,[house]
           ,[postcode]
           ,[telephone]
           ,[email_address]
           ,[lead_status_id]
           ,[contact_date]
           ,[contact_time]
           ,[ip_address]
           ,[lead_reason_id]
			,[clinic_vet_name]
			,[source])
     SELECT
           @product_id
           ,@title_id
           ,forename
           ,surname
           ,'TBA'
           ,postcode
           ,ISNULL(@tel_no,contact_num_day)
           ,ISNULL(@email,email)
           ,100
           ,getdate()
           ,'pm'
           ,@ip_address
           ,15
			,''
			,[source]
		FROM aggregator
		WHERE id = @aggregator_id

		declare @new_id int
		SELECT @new_id = @@IDENTITY 

	insert into lead_audit (lead_id, [user_id], description, datetimestamp)
	values (@new_id, 0, 'Lead Created', CURRENT_TIMESTAMP)

		
		UPDATE aggregator
		SET lead_id = @new_id
		WHERE id = @aggregator_id

		--and the pet details
		INSERT INTO lead_pet (
			lead_id
			,pet_name
			,animal_type_id
			,product_gender_id
			,breed_id
			,purchase_price
			,colour
			,pet_DOB)
		SELECT @new_id
			,pet_name
			,case animal_type when 'C' then 2 when 'D' then 1 end
			,case sex when 'M' then 1 when 'F' then 2 end
			,breed_id
			,purchase_price
			,colour
			,pet_DOB
		FROM aggregator_pet
		WHERE aggregator_id = @aggregator_id

		SELECT @new_id new_id

	END
	
	

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cookie_dump]'
GO
CREATE TABLE [dbo].[cookie_dump]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[aid] [int] NULL,
[source] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ip_address] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[datetimestamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cookie_dump] on [dbo].[cookie_dump]'
GO
ALTER TABLE [dbo].[cookie_dump] ADD CONSTRAINT [PK_cookie_dump] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cookie_dumpU_prc]'
GO
CREATE PROCEDURE [dbo].[cookie_dumpU_prc]
(@aid int, @source varchar(50), @ip_address varchar(20))
as

begin

insert into cookie_dump (aid, source, ip_address, datetimestamp)
select @aid, @source, @ip_address, CURRENT_TIMESTAMP

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_petU_prc]'
GO

CREATE PROCEDURE [dbo].[aggregator_petU_prc]  (
	@aggregator_id int
	,@pet_name nvarchar(64)
	,@animal_type char(1)
	,@sex char(1)
	,@breed_id int
	,@purchase_price money = NULL
	,@colour nvarchar(50) = NULL
	,@pet_dob smalldatetime
	,@microchip varchar(32) = NULL
	,@vetvisit bit = NULL
	,@not_working bit = NULL
	,@sick bit = NULL
	,@owned bit = NULL
	,@kept_at_home bit = NULL
	,@not_breeding bit = NULL
	,@not_racing bit = NULL
	,@not_hunting bit = NULL
	,@neutered bit = NULL
)
AS
SET NOCOUNT ON
SET DATEFORMAT DMY

--CHECK IF THE BREED FROM THE AGGREGATOR HAS BEEN MAPPED TO A DIFFERENT ONE LOCALLY
SELECT @breed_id = ISNULL(map_to_breed_id, @breed_id) FROM breed where id = @breed_id;

INSERT INTO [aggregator_pet]
           ([aggregator_id]
           ,[pet_name]
           ,[animal_type]
           ,[sex]
           ,[breed_id]
           ,[purchase_price]
           ,[colour]
           ,[pet_dob]
		   ,[microchip]
		   ,[vetvisit]
           ,[not_working]
           ,[sick]
           ,[owned]
           ,[kept_at_home]
           ,[not_breeding]
           ,[not_racing]
           ,[not_hunting]
           ,[neutered])
     VALUES
           (@aggregator_id
           ,@pet_name
           ,@animal_type
           ,@sex
           ,@breed_id
           ,@purchase_price
           ,@colour
           ,@pet_dob
		   ,@microchip
		   ,@vetvisit
           ,@not_working
           ,@sick
           ,@owned
           ,@kept_at_home
           ,@not_breeding
           ,@not_racing
           ,@not_hunting
           ,@neutered)


select @@IDENTITY new_id


update aggregator
set all_pet_names = ISNULL(apn.pet_names,'')
from aggregator a
	OUTER APPLY	(select STUFF((SELECT ',' + pet_name FROM aggregator_pet WHERE aggregator_id = a.id order by pet_name FOR XML PATH ('')),1,1,'')) apn(pet_names)
where a.id = @aggregator_id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_detailS_prc]'
GO


CREATE PROCEDURE [dbo].[aggregator_detailS_prc]
(
	@referrer_code varchar(20)
)
AS

SET NOCOUNT ON

	select *
	from aggregator_detail
	where referrer_code = @referrer_code;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_petU_prc]'
GO


CREATE PROCEDURE [dbo].[quote_petU_prc]  (
		@action			CHAR(1),
		@id				INT = NULL,
		@quote_id		INT = NULL,
		@pet_name		VARCHAR(64)	= NULL,
		@animal_type_id	INT	= NULL,
		@breed_id		INT	= NULL,
		@pet_DOB	smalldatetime = NULL,
		@product_gender_id	 INT= NULL,
		@purchase_price	MONEY = NULL,
		@chip_number		varchar(32)= NULL,
		@vet_visit		varchar(500)= NULL,
		@colour		varchar(32)= NULL,
		@premium	money = NULL,
		@cover_id	int = NULL,
		@agg_cover_id	int = NULL,
		@exclusion varchar(255) = NULL,		
		@promo_discount decimal(5,2) = NULL,  
		@offer_discount decimal(5,2) = NULL,  
		@multipet_discount decimal(5,2) = NULL,
		@exclusion_review bit = NULL,
		@neutered bit = NULL,
		@pricing_engine_id INT = null
)		

AS

SET NOCOUNT ON

	SET DATEFORMAT DMY

	DECLARE @new_id			INT
	DECLARE	@new_quote_ref	VARCHAR(16) 

	IF @action = 'X'
	BEGIN
		SELECT @quote_id = quote_id FROM quote_pet WHERE id=@id
		DELETE quote_pet WHERE id = @id
	END
	ELSE IF @action = 'I'
	BEGIN
		INSERT	quote_pet (
				quote_id,
				pet_name,
				animal_type_id,
				breed_id,
				pet_DOB,
				product_gender_id,
				purchase_price,
				colour,
				premium,
				exclusion,
				cover_id,
				agg_cover_id,
				microchip,
				microchipnumber,
				neutered,
				pricing_engine_id) 
		SELECT	@quote_id,
				@pet_name,
				@animal_type_id,
				@breed_id,
				@pet_DOB,
				@product_gender_id,
				@purchase_price,
				@colour,
				@premium,
				@exclusion,
				@cover_id,
				@agg_cover_id,
				CASE when NULLIF(@chip_number,'') IS NULL THEN 0 ELSE 1 END,
				@chip_number,
				@neutered,
				ISNULL(@pricing_engine_id,1)--1 is UIC


		SELECT @new_id = @@IDENTITY
					
		SELECT  @new_id as new_id,
				@pet_name as pet_name
				
		update quote
		set all_pet_names = ISNULL(qpn.pet_names, '')
		from quote q
			OUTER APPLY	(select STUFF((SELECT ',' + pet_name FROM quote_pet WHERE quote_id = q.id order by pet_name FOR XML PATH ('')),1,1,'')) qpn(pet_names)
		where q.id = @quote_id
				
		RETURN

	END 
	ELSE IF @action = 'U'
	BEGIN
		IF (@id IS NULL)
			BEGIN
				RAISERROR ('Incomplete Details', 10,1)
				RETURN
			END 
		ELSE
			BEGIN
				UPDATE	quote_pet
				SET		pet_name		= ISNULL(@pet_name, pet_name),
						animal_type_id	= ISNULL(@animal_type_id,animal_type_id),
						breed_id		= ISNULL(@breed_id, breed_id),
						pet_DOB			= ISNULL(@pet_DOB, pet_DOB),
						product_gender_id = ISNULL(@product_gender_id, product_gender_id),
						purchase_price	= ISNULL(@purchase_price,purchase_price),
						update_timestamp	= CURRENT_TIMESTAMP,
						microchipnumber	= isnull(@chip_number,microchipnumber),
						vet_visit   = isnull(@vet_visit,vet_visit),
						colour			= isnull(@colour,colour),
						premium			= isnull(@premium,premium),
						cover_id		= isnull(@cover_id,cover_id),
						exclusion		= ISNULL(@exclusion, exclusion),
						promo_discount	= ISNULL(@promo_discount, promo_discount),
						offer_discount	= ISNULL(@offer_discount, offer_discount),
						multipet_discount	= ISNULL(@multipet_discount, multipet_discount),
						exclusion_review	= ISNULL(@exclusion_review, exclusion_review),
						neutered	= ISNULL(@neutered, neutered),
						pricing_engine_id = ISNULL(@pricing_engine_id, pricing_engine_id)
				WHERE	id = @id;

				/*INTRODUCTORY DISCOUNT*/
				--save the applied discount
				--get the applied discount by cover in the quote created date (this information need to match with the applied discount)
				--delete all records in the quote_pet_introductory discount and update again
				if @cover_id is not null
				begin
					exec uispet..update_quote_pet_introductory_discount_prc @quote_pet_id=@id;
				end
			END 
		SELECT @quote_id = quote_id FROM quote_pet WHERE id=@id
	END 

	declare @total money
	SELECT @total = sum(isnull(premium, 0))
	from quote_pet where quote_id = @quote_id

	UPDATE quote set premium = @total
	where id = @quote_id
	
	
	update quote
	set all_pet_names = ISNULL(qpn.pet_names,'')
	from quote q
		OUTER APPLY	(select STUFF((SELECT ',' + pet_name FROM quote_pet WHERE quote_id = q.id order by pet_name FOR XML PATH ('')),1,1,'')) qpn(pet_names)
	where q.id = @quote_id


SET NOCOUNT OFF
















GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_pet_checkU_prc]'
GO

CREATE PROCEDURE [dbo].[quote_pet_checkU_prc]  (
		@action	char(1),
		@id	int=NULL,
		@microchip bit=0,
		@microchipnumber nvarchar(20)=NULL,
		@vetvisit bit=NULL,
		@vetvisitreason text=NULL,
		@accident bit=NULL,
		@accidentreason text=NULL,
		@not_working bit=NULL,
		@sick bit=NULL,
		@sickreason text=NULL,
		@kfconfirm bit=NULL,
		@owned bit=NULL,
		@kept_at_home bit=NULL, 
		@not_breeding bit=NULL, 
		@not_racing bit=NULL,
		@not_hunting bit=NULL,
		@purchase_price	MONEY = NULL,
		@colour		varchar(32)= NULL,
		@neutered bit=NULL
  ) 
AS

SET NOCOUNT ON

	IF @action = 'U'
	BEGIN
		UPDATE	quote_pet
		SET	microchip=@microchip
			,microchipnumber=@microchipnumber
			,vetvisit=@vetvisit
			,vetvisitdetails=@vetvisitreason
			,accident=@accident
			,accidentdetails=@accidentreason
			,not_working=@not_working
			,sick=@sick
			,sickdetails=@sickreason
			,kfconfirm=@kfconfirm
			,owned = @owned
			,kept_at_home = @kept_at_home
			,not_breeding = @not_breeding
			,not_racing = @not_racing
			,not_hunting = @not_hunting
			,purchase_price = @purchase_price
			,colour = @colour
			,neutered = @neutered
		WHERE id = @id
	END 

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[leadU_prc]'
GO




CREATE PROCEDURE [dbo].[leadU_prc]
           (@id INT = NULL
			,@product_id INT = NULL
			,@title_id INT = NULL
            ,@forename NVARCHAR(32) = NULL
            ,@surname NVARCHAR(32) = NULL
            ,@house NVARCHAR(50) = NULL
            ,@postcode VARCHAR(16) = NULL
            ,@telephone VARCHAR(32) = NULL
            ,@email_address NVARCHAR(128) = NULL
            ,@lead_status_id INT = NULL
            ,@contact_date DATE = NULL
            ,@contact_time VARCHAR(10) = NULL
            ,@clinic_id INT = NULL
            ,@clinic_vet_name NVARCHAR(100) = NULL
            ,@ip_address VARCHAR(20) = NULL
            ,@lead_reason_id INT = NULL
            ,@lead_failure_reason_id INT = NULL
			,@quote_id INT = NULL
			,@user_id INT = NULL
			,@broker_referrer VARCHAR(50) = NULL
			,@mobile_telephone VARCHAR(32) = NULL
			,@work_telephone VARCHAR(32) = NULL
			,@lead_time_id TINYINT = NULL)
AS

SET NOCOUNT ON

-- prevent foreign key error while creating the quote
IF @clinic_id = 0
	SET @clinic_id = NULL;

IF(@id IS NULL)
BEGIN
	INSERT INTO [dbo].[lead]
           ([product_id]
           ,[title_id]
           ,[forename]
           ,[surname]
           ,[house]
           ,[postcode]
           ,[telephone]
           ,[email_address]
           ,[lead_status_id]
           ,[contact_date]
           ,[contact_time]
           ,[clinic_id]
           ,[clinic_vet_name]
           ,[ip_address]
           ,[lead_reason_id]
           ,[broker_referrer]
           ,[mobile_telephone]
           ,[work_telephone]
           ,[lead_time_id])
     VALUES
           (@product_id
           ,@title_id
           ,@forename
           ,@surname
           ,@house
           ,@postcode
           ,@telephone
           ,@email_address
           ,100
           ,@contact_date
           ,@contact_time
           ,@clinic_id
           ,@clinic_vet_name
           ,@ip_address
           ,@lead_reason_id
           ,@broker_referrer
           ,@mobile_telephone
           ,@work_telephone
           ,@lead_time_id)

	declare @new_id int
	select @new_id = @@IDENTITY
	
	insert into lead_audit (lead_id, [user_id], description, datetimestamp)
	values (@new_id, 0, 'Lead Created', CURRENT_TIMESTAMP)

	select @new_id
END
ELSE
BEGIN

	if(@user_id>-1)
	begin
		declare @old_status int
		select @old_status = lead_status_id from lead where id=@id

		if(@old_status <> @lead_status_id)
		begin
			
			declare @description nvarchar(255)
			select @description = description from lead_status where id=@lead_status_id

			insert into lead_audit (lead_id, [user_id], description, datetimestamp)
			values (@id, @user_id, 'Lead status changed to ['+@description+']', CURRENT_TIMESTAMP)

		end
	end

	UPDATE [dbo].[lead]
	SET product_id = ISNULL(@product_id, product_id)
		,title_id = ISNULL(@title_id, title_id)
        ,forename = ISNULL(@forename, forename)
		,surname = ISNULL(@surname, surname)
		,house = ISNULL(@house, house)
		,postcode = ISNULL(@postcode, postcode)
		,telephone = ISNULL(@telephone, telephone)
		,mobile_telephone = ISNULL(@mobile_telephone, mobile_telephone)
		,work_telephone = ISNULL(@work_telephone, work_telephone)
		,email_address = ISNULL(@email_address, email_address)
		,lead_status_id = ISNULL(@lead_status_id, lead_status_id)
		,contact_date = ISNULL(@contact_date, contact_date)
		,contact_time = ISNULL(@contact_time, contact_time)
		,clinic_id = ISNULL(@clinic_id, clinic_id)
		,clinic_vet_name = ISNULL(@clinic_vet_name, clinic_vet_name)
		,ip_address = ISNULL(@ip_address, ip_address)
		,lead_reason_id = ISNULL(@lead_reason_id, lead_reason_id)
		,update_timestamp = getdate()
		,quote_id = ISNULL(@quote_id, quote_id)
		,lead_failure_reason_id = ISNULL(@lead_failure_reason_id, lead_failure_reason_id)
		,[broker_referrer] = ISNULL(@broker_referrer,broker_referrer)
		,[lead_time_id] = ISNULL(@lead_time_id,lead_time_id)
	WHERE id=@id

	select @id

END

SET NOCOUNT OFF






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_quoteU_prc]'
GO



CREATE PROCEDURE [dbo].[aggregator_quoteU_prc] @id int, @quote_ref varchar(16) = NULL, @cover_id int = NULL, @k9 int = NULL
AS

SET NOCOUNT ON


--if conversion triggered by K9 quote ref retrieved to bypass validation
IF @k9 = 1 (SELECT @quote_ref = quote_ref FROM aggregator WHERE id = @id)

--validate the request by both id and quote ref
IF NOT EXISTS(SELECT 1 FROM aggregator where id = @id and quote_ref = @quote_ref)
	RETURN

declare @min_cover_ordinal int
select @min_cover_ordinal = 1

--Sam asked us to show all covers 08/07/2015
--if(left(@quote_ref,3) = 'CPU' or left(@quote_ref,3) = 'PUR')
--begin
--	--get the min_cover_ordinal in case there is a restriction
--	--introduced for GRN
--	-- rolled out to PUR and CPU 21/12/2011
--	if @cover_id IS NOT NULL
--		select @min_cover_ordinal = ordinal from cover where id = @cover_id

--	--this is an override for GRN
--	--they want to show at least 3 products even if they are cheaper than the selected one
--	--so implement this using a minimum @min_cover_ordinal of 3
--	if(@min_cover_ordinal > 3)
--		select @min_cover_ordinal = 3
--	--end
--end

--already converted using this link - just return the quote details
IF EXISTS(SELECT 1 FROM aggregator where id = @id and quote_ref = @quote_ref and quote_id IS NOT NULL)
BEGIN
	--update the min cover limit in case another link was previously used
	if @cover_id IS NOT NULL
	BEGIN
		update quote 
		set min_cover_ordinal = @min_cover_ordinal
			,cover_id = @cover_id 
		from quote q 
			join aggregator a on a.quote_id = q.id 
		where a.id = @id

		update quote_pet 
		set cover_id = @cover_id 
		from quote_pet qp
			join aggregator a on a.quote_id = qp.quote_id 
		where a.id = @id
	END

	select q.id quote_id
		,q.quote_ref 
		,a.source
		,c.affinity_code affinity_code
	from quote q 
		join aggregator a on a.quote_id = q.id 
		left join cover c on c.id = @cover_id
	where a.id = @id
	RETURN
END

declare @product_id int
	,@title_id int
	,@forename varchar(32)
	,@surname varchar(32)
	,@postcode varchar(16)
	,@telephone varchar(32)
	,@email_address varchar(64)
	,@clinic_id int
	,@referrer_id nvarchar(50)
	,@policy_start_date smalldatetime	--default policy start date to today
	,@date_of_birth smalldatetime
	,@house varchar(50)
	,@address1 varchar(32)
	,@address2 varchar(32)
	,@address3 varchar(32)
	,@address4 varchar(32)
	,@opt_out_marketing varchar(1)
	,@remote_ref nvarchar(50)
	,@promo_code varchar(10)
	,@agree_contact bit
	
--get the matching title id from the title table	
select @title_id = t.id	
from aggregator a 
LEFT JOIN title t on t.description = a.title
where a.id = @id

IF(@title_id IS NULL)
BEGIN
	select @title_id = ISNULL(t.id,1)
	from aggregator a 
		LEFT JOIN title t on t.description = a.title+'.'
	where a.id = @id
END

select @product_id = p.id 
	,@title_id= @title_id
	,@forename = forename
	,@surname = surname
	,@postcode = a.postcode
	,@house = NULL
	,@telephone = a.contact_num_day
	,@email_address = a.email
	,@clinic_id = NULL
	,@policy_start_date = policy_start_date	--convert(varchar, getdate(),106)
	,@referrer_id = source
	,@date_of_birth = date_of_birth
	,@house = house
	,@address1 = a.address1
	,@address2 = a.address2
	,@address3 = a.address3
	,@address4 = a.address4
	,@opt_out_marketing = a.opt_out_marketing
	,@remote_ref = a.remote_reference
	,@promo_code = a.promo_code
	,@agree_contact = CASE p.id WHEN 31 THEN 0 ELSE 1 END	
	--default to no contact - will appear as lead if ok to contact - logic reversed on bit field!!		-
	--(apart from L&G)
from aggregator a join product p on a.affinity = p.prod_prefix
where a.id = @id

declare @quote_record table (new_quote_ref varchar(16), new_id int, prod_prefix varchar(3), referrer varchar(50))

--check the start date is not in the past
IF @policy_start_date < CURRENT_TIMESTAMP
	select @policy_start_date = DATEADD(DAY, 0, DATEDIFF(DAY, 0, GETDATE()))

insert into @quote_record
exec [dbo].[quoteU_prc]
		@action='I'
		,@product_id = @product_id
		,@title_id = @title_id
		,@forename = @forename
		,@surname = @surname
		,@postcode = @postcode
		,@contact_num = @telephone
		,@email = @email_address
		,@clinic_id	= @clinic_id
		,@policy_start_date = @policy_start_date 
		,@promo_code = @promo_code
		,@referrer_id =@referrer_id
		,@min_cover_ordinal = @min_cover_ordinal
		,@agree_contact = @agree_contact
		,@dob = @date_of_birth
		,@house_name_num = @house
		,@street = @address1
		,@street2 = @address2
		,@town = @address3
		,@county = @address4
		,@opt_out_marketing = @opt_out_marketing
		,@remote_ref=@remote_ref

declare @quote_id int
	,@pet_name	VARCHAR(64)
	,@animal_type_id INT
	,@breed_id INT
	,@pet_DOB smalldatetime
	,@product_gender_id INT
	,@purchase_price MONEY
	,@colour varchar(32)
	,@microchip varchar(32)
	,@vetvisit bit
	,@not_working bit
	,@sick bit
	,@owned bit
	,@kept_at_home bit
	,@not_breeding bit
	,@not_racing bit
	,@not_hunting bit
	,@neutered BIT
	,@pricing_engine_id int;

select @quote_id = new_id from @quote_record

--update either the aggregator or the lead record to link it to the new quote
declare @lead_id int
select @lead_id = lead_id from aggregator where id = @id
if(@lead_id IS NOT NULL)
begin
	declare @discard table (id int)
	insert into @discard exec leadU_prc @id=@lead_id,@quote_id=@quote_id,@user_id=0,@lead_status_id=102
end
else
	update aggregator set quote_id = @quote_id where id = @id


select qr.new_id quote_id
	,qr.new_quote_ref quote_ref 
	,@referrer_id source
	,c.affinity_code affinity_code
from @quote_record qr
	join quote q on qr.new_id = q.id
	left join cover c on c.id = @cover_id




--loop for pets
DECLARE CURSOR1 CURSOR FOR
SELECT pet_name
	,case animal_type when 'D' then 1 when 'C' then 2 end
	,breed_id
	,pet_dob
	,case sex when 'M' then 1 when 'F' then 2 end
	,purchase_price
	,colour
	,microchip
	,vetvisit
	,not_working
	,sick
	,owned
	,kept_at_home
	,not_breeding
	,not_racing
	,not_hunting
	,neutered
	,pricing_engine_id
FROM aggregator_pet
WHERE aggregator_id = @id

OPEN CURSOR1 
FETCH NEXT FROM CURSOR1 INTO @pet_name,@animal_type_id,@breed_id,@pet_DOB,@product_gender_id,@purchase_price,@colour,@microchip,@vetvisit,@not_working,@sick,@owned,@kept_at_home,@not_breeding,@not_racing,@not_hunting,@neutered,@pricing_engine_id
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
IF (@@FETCH_STATUS <> - 2)

	exec [dbo].[quote_petU_prc]
		@action='I'
		,@quote_id = @quote_id
		,@pet_name = @pet_name
		,@animal_type_id = @animal_type_id
		,@breed_id = @breed_id
		,@pet_DOB = @pet_DOB
		,@product_gender_id = @product_gender_id
		--,@purchase_price	= @purchase_price
		--,@colour = @colour
		,@cover_id = @cover_id
		,@agg_cover_id = @cover_id
		,@chip_number = @microchip
		,@pricing_engine_id = @pricing_engine_id
		
	DECLARE @new_pet_id as int
	SELECT @new_pet_id = @@IDENTITY
		
	DECLARE @has_microchip bit;
	select @has_microchip = case when nullif(@microchip,'') is null then 0 else 1 end;
		
	exec [dbo].[quote_pet_checkU_prc]
		 @action			= 'U'	
		,@id				= @new_pet_id
		,@vetvisit = @vetvisit
		,@not_working = @not_working
		,@sick = @sick
		,@owned = @owned
		,@kept_at_home = @kept_at_home
		,@not_breeding = @not_breeding
		,@not_racing = @not_racing
		,@not_hunting = @not_hunting
		,@purchase_price	= @purchase_price
		,@colour = @colour
		,@neutered = @neutered
		,@microchip = @has_microchip
		,@microchipnumber = @microchip;
		

	/*
	-- Update Quote Pet record for aggregators who have already asked some of the pet check questions
	
	IF @referrer_id = 'GOCOMP'				-- Go Compare
	BEGIN
		exec [dbo].[quote_pet_checkU_prc]
		 @action			= 'U'
		,@id				= @new_pet_id	-- AGG QUESTION SET:
		,@not_working		= 1				-- My pet is not used for security, guarding or any other business purpose				
		,@owned				= 1				-- Are you the owner and keeper of this pet?
		,@not_breeding		= 1				-- My pet is not used for track racing, coursing or for breeding
		,@not_racing		= 1				-- My pet is not used for track racing, coursing or for breeding
		,@not_hunting		= 1				-- My pet is not used for track racing, coursing or for breeding
	END
	ELSE IF  @referrer_id = 'AGGQZONE'		-- QuoteZone
	BEGIN
		exec [dbo].[quote_pet_checkU_prc]
		 @action			= 'U'
		,@id				= @new_pet_id	-- AGG QUESTION SET:
		,@not_working		= 1				-- Is your pet used in a professional capacity as any of the following: guarding, secuity services or racing?			
		,@not_racing		= 1				-- Is your pet used in a professional capacity as any of the following: guarding, secuity services or racing?
	END*/

FETCH NEXT FROM CURSOR1 INTO @pet_name,@animal_type_id,@breed_id,@pet_DOB,@product_gender_id,@purchase_price,@colour,@microchip,@vetvisit,@not_working,@sick,@owned,@kept_at_home,@not_breeding,@not_racing,@not_hunting,@neutered,@pricing_engine_id
END 
CLOSE CURSOR1 
DEALLOCATE CURSOR1

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_request]'
GO
CREATE TABLE [dbo].[aggregator_request]
(
[id] [int] NOT NULL IDENTITY(6114, 1),
[request] [text] COLLATE Latin1_General_CI_AS NULL,
[response] [text] COLLATE Latin1_General_CI_AS NULL,
[datetimestamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator_request] on [dbo].[aggregator_request]'
GO
ALTER TABLE [dbo].[aggregator_request] ADD CONSTRAINT [PK_aggregator_request] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_requestU_prc]'
GO


CREATE PROCEDURE [dbo].[aggregator_requestU_prc]  (
	@id int = NULL
	,@request text = NULL
	,@response text = NULL	
)
AS

SET NOCOUNT ON

IF(@request IS NOT NULL)
BEGIN
	INSERT INTO [aggregator_request] ([request], [datetimestamp]) VALUES (@request, CURRENT_TIMESTAMP)
	select @@IDENTITY new_id
END
ELSE
BEGIN
	UPDATE [aggregator_request] set [response] = @response where id = @id
END

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[employment_status]'
GO
CREATE TABLE [dbo].[employment_status]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [nvarchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_employment_status] on [dbo].[employment_status]'
GO
ALTER TABLE [dbo].[employment_status] ADD CONSTRAINT [PK_employment_status] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[occupationL_prc]'
GO

CREATE PROCEDURE [dbo].[occupationL_prc]
AS

SET NOCOUNT ON

	SELECT 	id, 
			description
	FROM	employment_status
	ORDER BY description 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[page_tracker]'
GO
CREATE TABLE [dbo].[page_tracker]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[page_name] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[session_id] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[time_stamp] [smalldatetime] NOT NULL CONSTRAINT [DF__page_trac__time___22FF2F51] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__page_tracker__220B0B18] on [dbo].[page_tracker]'
GO
ALTER TABLE [dbo].[page_tracker] ADD CONSTRAINT [PK__page_tracker__220B0B18] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[page_trackerI_prc]'
GO

CREATE PROCEDURE [dbo].[page_trackerI_prc] 
				(	@page_name	VARCHAR(64),
					@session_id VARCHAR(256)
				)
AS		

SET NOCOUNT ON


	INSERT page_tracker (page_name, session_id)
	VALUES(@page_name, @session_id)




SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[create_aggregator_ref]'
GO



CREATE FUNCTION [dbo].[create_aggregator_ref] (@aggregator_id int, @affinity varchar(3))  
RETURNS varchar(16) AS  
	BEGIN 

--	declare @next int
--	SELECT TOP 1 @next = a1.suffix_id + 1
--	FROM aggregator a1
--		left join aggregator a2 on a1.suffix_id + 1 = a2.suffix_id
--	where a2.quote_ref IS NULL
--	and a1.suffix_id is not null
--	order by a1.suffix_id

		RETURN  UPPER(@affinity + CONVERT(varchar, 10000000 + @aggregator_id))	--(9*@aggregator_id) + 10000001))
	END








GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregatorU_prc]'
GO






CREATE PROCEDURE [dbo].[aggregatorU_prc]  (
	@source varchar(64)
	,@affinity varchar(3)
	,@title varchar(32)
	,@forename varchar(32)
	,@surname varchar(32)
	,@postcode varchar(16)
	,@contact_num_day varchar(32)
	,@contact_num_eve varchar(32)
	,@email varchar(64)
	,@policy_start_date date
	,@client_ip_address varchar(20)
	,@promo_code varchar(10)
	,@aggregator_request_id int
	,@date_of_birth date = NULL
	,@house nvarchar(50) = NULL
	,@address1 varchar(32) = NULL
	,@address2 varchar(32) = NULL
	,@address3 varchar(32) = NULL
	,@address4 varchar(32) = NULL
	,@remote_reference varchar(50) = NULL
	,@opt_out_marketing varchar(1) = 'N'
)
AS
SET NOCOUNT ON
SET DATEFORMAT DMY

--IF @affinity = 'PUR' 
--BEGIN
--	SELECT @promo_code = 'SPRING13'
--END

IF @affinity = 'CPU' 
BEGIN
	SELECT @promo_code = 'FEB14PROMO'
END

IF (@affinity = 'GRU' AND @source = 'GOCOMP')
BEGIN
	SELECT @promo_code = 'KFISGOCOMP'
END

IF @affinity = 'LAG' AND CONVERT(date, CURRENT_TIMESTAMP) BETWEEN '2015-05-01' AND '2015-05-31'
BEGIN
	SELECT @promo_code = 'LAG15MAY12'
END

IF @affinity = 'LAG' AND CONVERT(date, CURRENT_TIMESTAMP) BETWEEN '2015-06-01' AND '2015-08-31'
BEGIN
	SELECT @promo_code = 'LAG15AGG12'
END

IF @affinity = 'LAG' AND CONVERT(date, CURRENT_TIMESTAMP) BETWEEN '2015-09-01' AND '2016-12-31'
BEGIN
	SELECT @promo_code = 'LAG15AGG13'
END

INSERT INTO [aggregator]
           ([source]
           ,[affinity]
		   ,[title]
		   ,[forename]
		   ,[surname]
           ,[postcode]
           ,[contact_num_day]
           ,[contact_num_eve]
           ,[email]
           ,[status_id]
           ,[policy_start_date]
           ,[client_ip_address]
           ,[promo_code]

           ,[date_of_birth]
           ,[house]
           ,[address1]
           ,[address2]
           ,[address3]
           ,[address4]
           ,[remote_reference]
           ,[opt_out_marketing])
     VALUES
           (@source
           ,@affinity
           ,@title
           ,@forename
           ,@surname
           ,@postcode
           ,@contact_num_day
           ,@contact_num_eve
           ,@email
           ,1
           ,@policy_start_date
           ,@client_ip_address
           ,@promo_code
           ,@date_of_birth
           ,@house
           ,@address1
           ,@address2
           ,@address3
           ,@address4
           ,@remote_reference
           ,@opt_out_marketing)

	DECLARE @new_id int
		,@new_quote_ref varchar(16)

	SELECT @new_id = @@IDENTITY	--IDENT_CURRENT('aggregator') PROBLEM WITH CONCURRENCY!!

	SELECT	@new_quote_ref = dbo.[create_aggregator_ref](@new_id, @affinity)

	UPDATE	aggregator
	SET		quote_ref = @new_quote_ref 
		,suffix_id = convert(int, right(@new_quote_ref , 8))
	WHERE	id = @new_id

	SELECT @new_quote_ref quote_ref, @new_id new_id





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[hear_about_usL_prc]'
GO


CREATE PROCEDURE [dbo].[hear_about_usL_prc] @product_id int = 0, @scope int = 0
AS

SET NOCOUNT ON

--default set labelled as product_id 0.
IF NOT EXISTS (SELECT 1 from hear_about_us where product_id = @product_id)
BEGIN
	SELECT 	id, 
			description
	FROM	hear_about_us
	WHERE product_id = 0
	AND scope = 0
	AND id <> 14
UNION
	SELECT 	id, 
			description
	FROM	hear_about_us
	WHERE id = 14
	AND @product_id not in (3,6,11,13,14,22)	--NOT TVIS
UNION
	SELECT 	id, 
			description
	FROM	hear_about_us
	WHERE id = 23
	AND @product_id in (3,6,11,13,14,22)	--TVIS

END
ELSE
BEGIN
	IF @scope = 0
	BEGIN
		SELECT 	id, 
				description
		FROM	hear_about_us
		WHERE product_id = @product_id
		AND scope = 0
	END
	ELSE
	BEGIN
		SELECT 	id, 
				description
		FROM	hear_about_us
		WHERE product_id = 0
		AND scope = @scope
			UNION
		SELECT 	id, 
				description
		FROM	hear_about_us
		WHERE product_id = @product_id
		AND scope = 0
		
		order by description
	END

END
SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_dd_service_organisation]'
GO

CREATE FUNCTION [dbo].[get_dd_service_organisation]
(
	@product_id int,
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select 
		sprod.product_id,
		sprod.valid_from_date,
		serv.service_number,
		serv.service_bank_description,
		sorg.description as organisation_name,
		sorg.url_logo,
		sorg.footer,
		serv.waiting_days
	from dd_service_product sprod
	inner join dd_service serv on sprod.dd_service_id = serv.id
	inner join dd_service_organisation sorg on serv.dd_service_organisation_id = sorg.id
	where sprod.id = 
					(select top 1 id 
					from dd_service_product ddsp 
					where ddsp.product_id = @product_id
						and ddsp.valid_from_date <= @date
					order by ddsp.valid_from_date desc)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_statusL_prc]'
GO

CREATE PROCEDURE [dbo].[quote_statusL_prc] 
AS

SET NOCOUNT ON

	select * from quote_status 
	WHERE id <> 9 --Suspicious Activity
	order by id	

SET NOCOUNT OFF







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SetQuoteEmailSent]'
GO
CREATE PROCEDURE [dbo].[SetQuoteEmailSent]
	@quoteId int
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE uispet..quote SET email_sent = 1 WHERE id = @quoteId;

	DELETE FROM QuoteEmailQueue WHERE QuoteId = @quoteId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[card_typeL_prc]'
GO

CREATE PROCEDURE [dbo].[card_typeL_prc] 
AS

SET NOCOUNT ON

	SELECT 	id, 
			value,
			description
	FROM	card_type
	ORDER BY id 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_premium_overrideU_prc]'
GO


CREATE procedure [dbo].[renewal_premium_overrideU_prc]
(@policy_reference varchar(50), 
@premium money = NULL, 
@cover_id int = NULL,
 @systuser_id int = NULL, 
 @cover_discount_id int = NULL,
 @pricingEngineId INT = null)
as
begin

	SELECT @pricingEngineId = ISNULL(@pricingEngineId, 1);

	--get the policy_id for the provided reference
	declare @policy_id int
		,@renewal_date date
		,@old_cover_id int
	select @policy_id = policyid 
		,@renewal_date = renewaldate
		,@old_cover_id = cover_id
	from uispetmis..policy
	where policynumber = @policy_reference

	if(@cover_id IS NULL)
	begin
		if exists (select 1 from renewal_cover where old_cover_id = @old_cover_id)
		select @cover_id = new_cover_id from renewal_cover where old_cover_id = @old_cover_id
	end

	
	if @policy_id IS NOT NULL
	begin
	
		

		delete renewal_premium_override 
		where policy_id = @policy_id
		and renewal_year = year(@renewal_date)
	
		insert into renewal_premium_override
		(policy_id, renewal_year, premium, cover_id, systuser_id, datetimestamp, cover_discount_id, pricing_engine_id)
		select @policy_id, year(@renewal_date), @premium, @cover_id, @systuser_id, CURRENT_TIMESTAMP, @cover_discount_id, @pricingEngineId
	end
	
	select *, @renewal_date renewal_date 
	from renewal_premium_override
	where id = SCOPE_IDENTITY() 

end

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sagepay_transactionS_prc]'
GO


CREATE PROCEDURE [dbo].[sagepay_transactionS_prc]
(
	@VPSTxId varchar(256)
)
AS

SET NOCOUNT ON

select * 
from sagepay_transaction st
where st.VPSTxId = @VPSTxId;

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[replaceSup]'
GO

CREATE Function [dbo].[replaceSup](
	@strText VARCHAR(1000),
	@cover_id int,
	@flagStripText bit
) 
RETURNS VARCHAR(1000) 
AS 
BEGIN 
	select @strText =
		CASE WHEN @cover_id in (311,312,313,314,315,  391,392,393,394,395) THEN
				REPLACE(
					REPLACE(
						REPLACE(@strText,'<sup>1</sup>','*'),
						'<sup>2</sup>','#'),
					'<sup>3</sup>','***')
			ELSE
				REPLACE(
					REPLACE(
						REPLACE(@strText,'<sup>1</sup>','*'),
						'<sup>2</sup>','**'),
					'<sup>3</sup>','')
			END;

	if @flagStripText = 1
		select @strText = replace(@strText, '<br /><sup>per condition per policy period</sup>','');
		
	return @strText;
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_schedule_section_config_value]'
GO

CREATE FUNCTION [dbo].[get_schedule_section_config_value]
(
    @affinityCode VARCHAR(10),
	@sectionId INT,
	@typeId int
)
RETURNS VARCHAR(max)
AS
BEGIN

    RETURN (SELECT TOP 1 sc.Value FROM uispetmis..schedule_configuration sc WHERE sc.AffinityCode = 
	 @affinityCode AND sc.SectionId = @sectionId)	

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit_scheduleL_prc]'
GO





CREATE PROCEDURE [dbo].[cover_benefit_scheduleL_prc] (@affinityCode varchar(10), @report_type varchar(30), @sectionParentId INT)
AS

SET NOCOUNT ON

declare @product_id INT
declare @cover_id INT
DECLARE @sectionId int

-- REPORT TYPES
-- get_only_parents
-- get_children_by_parent
-- get_parent_by_id
-- get_other_sections

-- select top 1 @cover_id = cover_id
-- from uispetmis..premium
-- where policy_id = @id
-- order by effective_date desc ,id desc

-- select @product_id = product_id from cover where id = @cover_id

select @cover_id = c.id, @product_id = c.product_id
FROM uispetmis..AFFINITY a
INNER JOIN uispet..cover c on a.AffinityCode=c.affinity_code collate Latin1_General_CI_AS
where a.AffinityCode = @affinityCode

IF @report_type = 'get_only_parents'
BEGIN
	-- return only those rows that has subcovers. Return only parents cover. The children will be loaded in other
	select convert(varchar, s.id) section,
		--CASE WHEN dbo.get_schedule_section_config_value(@affinityCode,s.id,1) IS not NULL THEN
		--dbo.get_schedule_section_config_value(@affinityCode,s.id,1)
		--ELSE
		dbo.replaceSup(s.description, cb.cover_id, 1) AS  cover
		,case when charindex('<br />',cb.description) > 0 then left(cb.description, charindex('<br />',cb.description)-1)
			else cb.description end benefit 
		,excess
		,max_ben
		,section_num
		,convert(int, dbo.fnRemoveNonNumericCharacters(section_num)) section_num_val
		,s.ordinal
	from cover_benefit cb 
		join section s on cb.section_id = s.id and s.product_id = @product_id
	where cb.cover_id = @cover_id
	and (section_num is not null)
	and s.id in (select section_id from section where section_id is not null)
	order by section_num_val, ordinal
END
ELSE IF @report_type = 'get_children_by_parent'
BEGIN
	select convert(varchar, s.id) section,
		--CASE WHEN dbo.get_schedule_section_config_value(@affinityCode,s.id,1) IS not NULL THEN
		--dbo.get_schedule_section_config_value(@affinityCode,s.id,1)
		--ELSE
		dbo.replaceSup(s.description, cb.cover_id, 1)  AS  cover
		,
		CASE WHEN charindex('<br />',cb.description) > 0 then left(cb.description, charindex('<br />',cb.description)-1)
			else cb.description end benefit 
		,excess
		,max_ben
		,section_num
		,convert(int, dbo.fnRemoveNonNumericCharacters(section_num)) section_num_val
		,s.ordinal
	from cover_benefit cb 
		join section s on cb.section_id = s.id and s.product_id = @product_id
	where cb.cover_id = @cover_id
	and (section_num is not null)
	and s.section_id = @sectionParentId
	order by section_num_val, ordinal
END
ELSE IF @report_type = 'get_parent_by_id'
BEGIN
	select convert(varchar, s.id) section,
		--CASE WHEN dbo.get_schedule_section_config_value(@affinityCode,s.id,1) IS not NULL THEN
		--dbo.get_schedule_section_config_value(@affinityCode,s.id,1)
		--ELSE
		dbo.replaceSup(s.description, cb.cover_id, 1) AS  cover
		,case when charindex('<br />',cb.description) > 0 then left(cb.description, charindex('<br />',cb.description)-1)
			else cb.description end benefit 
		,excess
		,max_ben
		,section_num
		,convert(int, dbo.fnRemoveNonNumericCharacters(section_num)) section_num_val
		,s.ordinal
	from cover_benefit cb 
		join section s on cb.section_id = s.id and s.product_id = @product_id
	where cb.cover_id = @cover_id
	and (section_num is not null)
	and s.id = @sectionParentId
	order by section_num_val, ordinal
END
ELSE IF @report_type = 'get_other_sections'
BEGIN
	select convert(varchar, s.id) section,
		CASE WHEN dbo.get_schedule_section_config_value(@affinityCode,s.id,1) IS not NULL THEN
		dbo.get_schedule_section_config_value(@affinityCode,s.id,1)
		ELSE
		dbo.replaceSup(s.description, cb.cover_id, 1) END AS  cover
		,case when charindex('<br />',cb.description) > 0 then left(cb.description, charindex('<br />',cb.description)-1)
			else cb.description end benefit 
		,excess
		,max_ben
		,section_num
		,convert(int, dbo.fnRemoveNonNumericCharacters(section_num)) section_num_val
		,s.ordinal
	from cover_benefit cb 
		join section s on cb.section_id = s.id and s.product_id = @product_id
	where cb.cover_id = @cover_id
	and (section_num is not null)
	-- dont return sub-sections, they will be returned in subcover condition
	and s.section_id is null 
	and s.id not in (select section_id from section where section_id is not null)
	order by section_num_val, ordinal
END

SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Quotes].[QuoteLinks]'
GO




CREATE VIEW [Quotes].[QuoteLinks]
AS
SELECT 'AAN' Code, '(AAN) AA' Name, 'https://staging-aapet.ultimateins.co.uk/QAAN/index.aspx?uid={1}' Url, 'Staging' Environment, 1 Sort, 'NewQuote' [Type]
UNION
SELECT 'AAZ' Code, '(AAZ) AA' Name, 'https://staging-aapet.ultimateins.co.uk/QAAN/pet-insurance-retrieve.aspx' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'ADM' Code, '(ADM) Admiral' Name, 'https://staging-admiral.ultimateins.co.uk/QuoteAndBuy/Step1' Url, 'Staging' Environment, 2 Sort, 'NewQuote' [Type]
UNION
SELECT 'ASD' Code, '(ASD) ASDA' Name, 'https://asda-dev.uispet.co.uk/?refKey={0}' Url, 'Staging' Environment, 12 Sort, 'NewQuote' [Type]
UNION
SELECT 'ASD' Code, '(ASD) ASDA' Name, 'https://asda-dev.uispet.co.uk/QuoteAndBuy/AggregatorReferral?enc={0}' Url, 'Staging' Environment, 12 Sort, 'AggregatorClickThrough' [Type]
UNION
SELECT 'ASD' Code, '(ASD) ASDA' Name, 'https://asda-dev.uispet.co.uk/QuoteAndBuy/CoverOptions?refKey={0}' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'ASD' Code, 'Asda' Name, 'https://asda-dev.uispet.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 12 Sort, 'RecallQuote' [Type]
UNION
SELECT 'AVZ' Code, '(AVZ) Aviva' Name, 'https://staging-aviva.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 1 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'CPQ' Code, '(CPQ) Computer Quote' Name, 'https://staging-computerquote.ultimateins.co.uk/QuoteAndBuy/Step1' Url, 'Staging' Environment, 3 Sort, 'NewQuote' [Type]
UNION
SELECT 'DEB' Code, '(DEB) Debenhams' Name, 'https://staging-debenhams.uispet.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'DFT' Code, 'Default' Name, 'https://Staging-default.uispet.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 12 Sort, 'RecallQuote' [Type]
UNION
SELECT 'GFR' Code, '(GFR) Geoffrey' Name, 'https://staging-geoffrey.ultimateins.co.uk/QuoteAndBuy/YouAndYourPetDetail?refKey={0}' Url, 'Staging' Environment, 13 Sort, 'NewQuote' [Type]
UNION
SELECT 'GFR' Code, '(GFR) Geoffrey' Name, 'https://staging-geoffrey.ultimateins.co.uk/home/retrievequote' Url, 'Staging' Environment, 13 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'KWF' Code, '(KWF) Kwikfit' Name, 'https://kwikfit-dev.uispet.co.uk/QuoteAndBuy/Step0?refKey={0}' Url, 'Staging' Environment, 4 Sort, 'NewQuote' [Type]
UNION
SELECT 'LAG' Code, '(LAG) Legal & General' Name, 'https://landg-dev.uispet.co.uk/QuoteAndBuy/Step0?refKey={0}' Url, 'Staging' Environment, 5 Sort, 'NewQuote' [Type]
UNION
SELECT 'LIV' Code, '(LIV) LV=' Name, 'https://staging-lv.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'MPC' Code, '(MPC) MiPetCover' Name, 'https://staging-mipetcover.ultimateins.co.uk/QuoteAndBuy/CoverOptions?refKey={0}' Url, 'Staging' Environment, 12 Sort, 'NewQuote' [Type]
UNION
SELECT 'MPC' Code, '(MPC) MiPetCover' Name, 'https://staging-mipetcover.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PAW' Code, '(PAW) Paws & Claws' Name, 'https://staging-paws.uispet.co.uk/QuoteAndBuy/Step1?refKey={0}' Url, 'Staging' Environment, 6 Sort, 'NewQuote' [Type]
UNION
SELECT 'PAW' Code, '(PAW) Paws & Claws' Name, 'https://staging-paws.uispet.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 6 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PDS' Code, '(PDS) PDSA' Name, 'https://staging-pdsa.ultimateins.co.uk/?refKey={0}' Url, 'Staging' Environment, 11 Sort, 'NewQuote' [Type]
UNION
SELECT 'PDS' Code, '(PDS) PDSA' Name, 'https://staging-pdsa.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 11 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PNP' Code, '(PNP) Pets in a Pickle' Name, 'https://staging-petsinapickle.ultimateins.co.uk/home/retrievequote' Url, 'Staging' Environment, 12 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PTW' Code, '(PTW) Pet Wise' Name, 'https://staging-petwise.ultimateins.co.uk/QuoteAndBuy/YouAndYourPetDetail?refKey={0}' Url, 'Staging' Environment, 9 Sort, 'NewQuote' [Type]
UNION
SELECT 'SLF' Code, '(SLF) Sun Life' Name, 'https://sunlife-dev.uispet.co.uk/QuoteAndBuy/YouAndYourPetDetail?refKey={0}' Url, 'Staging' Environment, 10 Sort, 'NewQuote' [Type]
UNION
SELECT 'UIS' Code, '(UIS) Ultimate Insurance' Name, 'https://staging-uis.uispet.co.uk/QUIS/index.aspx?uid={1}' Url, 'Staging' Environment, 8 Sort, 'NewQuote' [Type]
UNION
SELECT 'PET' Code, '(PET) Ageas Pet' Name, 'https://staging-pet-ageas.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 13 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PET' Code, '(PET) Ageas Pet' Name, 'https://staging-pet-ageas.ultimateins.co.uk/QuoteAndBuy/YouAndYourPetDetail?refKey={0}' Url, 'Staging' Environment, 13 Sort, 'NewQuote' [Type]
UNION
SELECT 'PUP' Code, '(PUR) Purely Pets' Name, 'https://staging-purelypets.ultimateins.co.uk/QuoteAndBuy/YouAndYourPetDetail?refKey={0}' Url, 'Staging' Environment, 7 Sort, 'NewQuote' [Type]
UNION
SELECT 'PUP' Code, '(PUR) Purely Pets' Name, 'https://staging-purelypets.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 7 Sort, 'RetrieveQuote' [Type]
UNION
SELECT 'PUR' Code, '(PUR) Purely Pets' Name, 'https://staging-purelypets.ultimateins.co.uk/Home/RetrieveQuote' Url, 'Staging' Environment, 7 Sort, 'RetrieveQuote' [Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Quotes].[GetQuoteLink]'
GO

create PROCEDURE [Quotes].[GetQuoteLink]
(
	@quoteId INT,
	@linkType VARCHAR(30)
)
AS
	SELECT ql.*, q.quote_ref QuoteReference, q.id QuoteId
	FROM uispet.Quotes.QuoteLinks ql
	INNER JOIN uispet.dbo.quote q ON LEFT(q.quote_ref,3) = ql.Code
	WHERE q.id = @quoteId
	AND ql.Type = @linkType
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic_services_by_group]'
GO
CREATE PROCEDURE [dbo].[clinic_services_by_group] (@clinic_id int)
as 
begin

	select service_profile 
	from clinic_group cg
		join clinic c on c.clinic_group_id = cg.id
	where c.id = @clinic_id

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sagepay_transactionU]'
GO


CREATE PROCEDURE [dbo].[sagepay_transactionU]
(
	@quote_id int = null,
	@policy_id int = null,
	@VendorTxId varchar(256),
	@VPSTxId varchar(256),
	@SecurityKey varchar(256)
)
AS

SET NOCOUNT ON

insert into sagepay_transaction (quote_id, policy_id, VendorTxId, VPSTxId, SecurityKey, ApplicationTypeId, create_timestamp)
values (@quote_id, @policy_id, @VendorTxId, @VPSTxId, @SecurityKey, 1, GETDATE());


SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_create_detailsI_prc]'
GO

CREATE PROCEDURE [dbo].[policy_create_detailsI_prc]
	@policy_start_date DATE
	,@cover_id INT
	,@pet_name VARCHAR(60)
	,@pet_dob SMALLDATETIME
	,@breed_id INT
	,@colour VARCHAR(60)
	,@product_gender_id INT
	,@animal_type_id INT
	,@title_id INT
	,@customer_first_name VARCHAR(35)
	,@customer_last_name VARCHAR(35)
	,@address1 VARCHAR(35)
	,@address2 VARCHAR(35)
	,@address3 VARCHAR(35)
	,@address4 VARCHAR(35)
	,@address5 VARCHAR(35)
	,@postcode VARCHAR(10)
	,@telephone_number VARCHAR(15)
	,@vetvisit BIT
	,@accident BIT
	,@voluntary_excess MONEY
	,@pet_id INT
	,@email VARCHAR(64)
	,@purchase_price MONEY
	,@product_id INT
	,@clinic_id INT
	,@exclusion_review BIT
	,@dob DATETIME
	,@microchipnumber VARCHAR(32)
	,@neutered BIT
	,@premium DECIMAL(10,2)
	,@promo_discount DECIMAL(5,2)
	,@offer_discount DECIMAL(5,2)
	,@multipet_discount DECIMAL(5,2)
	,@promo_code VARCHAR(50)
	,@ipt_absolute MONEY
	,@commission_affinity_absolute MONEY
	,@commission_administrator_absolute MONEY
	,@commission_underwriter_absolute MONEY
	,@credit_charge_absolute MONEY
	,@helpline_charge_absolute MONEY
	,@tpl_absolute MONEY
	,@pricing_engine_id BIGINT
	,@promo_discount_absolute MONEY
	,@offer_discount_absolute MONEY
	,@multipet_discount_absolute MONEY
	,@policy_number VARCHAR(32)
	,@master_policy_id INT = NULL
	,@allow_inception_in_past BIT = 0
AS
BEGIN

declare @exc_rsl nvarchar(255); select @exc_rsl = 'This policy does not provide cover for any Pre-existing Condition as described in the Policy Wording ‘Definitions’.  This will include, but is not limited to, the conditions you have informed us about';
	declare @exc_uic nvarchar(255); select @exc_uic = 'This policy does not provide cover for any Pre-existing Condition as described in Section 1 – Definitions';

	declare @acc_pl nvarchar(500); select @acc_pl = 'In respect of the Section relating to Public Liability - This policy does not cover Your liability in respect of any incident(s) that You have informed us of, or, any future incident of the same or similar nature or resulting from the behavioural traits and / or tendencies displayed in such incident(s).';
	declare @acc_tpl nvarchar(500); select @acc_tpl = 'In respect of the Section relating to Third Party Liability - This policy does not cover Your liability in respect of any incident(s) that You have informed us of, or, any future incident of the same or similar nature or resulting from the behavioural traits and / or tendencies displayed in such incident(s).';

	-- ensure that the start date is not in the past
	IF @allow_inception_in_past = 0
	BEGIN
		declare @today_date date = GETDATE();
		select @policy_start_date = case when @policy_start_date < @today_date then @today_date else @policy_start_date END
    END

	INSERT uispetmis.dbo.policy (
			AffinityCode,
			ClaimType,
			PrintDate,
			Printed,
			PolicyNumber,
			PuppyCoverNumber,
			PolicyInceptionDate,
			StartDate,
			RenewalDate,
			AnimalPetName,
			AnimalDateOfBirth,
			BreedDescription,
			ColourDescription,
			AnimalSexName,
			AnimalTypeName,
			ProductName,
			PaymentFrequencyName,  
			PolicyPremiumYearID,
			Title,
			FirstName,
			LastName,
			Address1,
			Address2,
			Address3,
			Address4,
			Address5,
			PostCode,
			Telephone1,
			AnimalDead,
			DateTimestamp,
			AnimalBabyName,
			PreExistingCondition,
			Exclusion,
			PreviousPolicyID,
			cdl_source_id,
			vol_excess,
			breed_excess,
			status_id,
			quote_pet_id,
			email_address,
			purchase_price,
			breed_id,
			animal_type_id,
			product_id,
			clinic_id,
			cover_id,
			exclusion_review,
			dob,
			microchipnumber,
			neutered)
	SELECT	c.affinity_code,				--AffinityCode
			NULL,							--ClaimType
			NULL,							--PrintDate
			NULL,							--Printed
			@policy_number,					--PolicyNumber
			NULL,							--PuppyCoverNumber
			@policy_start_date,				--PolicyInceptionDate
			@policy_start_date,				--StartDate
			uispetmis.[dbo].[CalculateRenewalDate](@policy_start_date, @cover_id), -- RenewalDate
			@pet_name,						--AnimalPetName
			@pet_dob,						--AnimalDateOfBirth
			b.description,					--BreedDescription
			@colour,						--ColourDescription
			pg.gender_desc,					--AnimalSexName
			at.description,					--AnimalTypeName
			c.description,					--ProductName
			'CC',							--PaymentFrequencyName 
			0,								--PolicyPremiumYearID ???? 
			t.description,					--Title
			@customer_first_name,			--FirstName
			@customer_last_name,			--LastName
			@address1,						--Address1
			@address2,						--Address2
			@address3,						--Address3
			@address4,						--Address4
			@address5,						--Address5
			@postcode,						--PostCode
			@telephone_number,				--Telephone1
			'F',							--AnimalDead
			GETDATE(),						--DateTimestamp
			NULL,							--AnimalBabyName
			NULL,							--PreExistingCondition
			case when @vetvisit = 1 then	--Exclusion
				case when c.underwriter = 'RSL' then @exc_rsl else @exc_uic end
					+ case when @accident = 1 then 
						'.' + CHAR(13) + CHAR(10) + case when c.product_id in (2,19,13,22,14,11,6,3) then @acc_pl else @acc_tpl end 
					else '' end
			when @accident = 1 then 
					case when c.product_id in (2,19,13,22,14,11,6,3) then @acc_pl else @acc_tpl end else '' end,
			NULL,							--PreviousPolicyID
			NULL,							--cdl_source_id
			ISNULL(@voluntary_excess, 0),	--vol_excess	
			0,								--breed_excess
			1,								--status
			@pet_id,						--quote_pet_id
			@email,							--email_address
			@purchase_price,				--purchase_price
			@breed_id,						--breed_id
			@animal_type_id,				--animal_type_id
			@product_id,					--product_id
			@clinic_id,						--clinic_id
			@cover_id,						--cover_id
			@exclusion_review,				--review
			@dob,							--dob
			@microchipnumber,				--microchipnumber
			@neutered						--neutered
	FROM cover c
		JOIN breed b ON b.id = @breed_id
		JOIN product_gender pg ON pg.id = @product_gender_id
		JOIN animal_type at ON at.id = @animal_type_id
		LEFT JOIN title t ON t.id = @title_id
	WHERE c.id = @cover_id
	    
	DECLARE @new_policy_id INT = @@identity

	INSERT INTO uispetmis..PREMIUM (
		policy_id, 
		effective_date, 
		premium, 
		promo_discount, 
		offer_discount, 
		multipet_discount, 
		postcode, 
		cover_id, 
		promo_code, 
		IPT_absolute,
		commission_affinity_absolute,
		commission_administrator_absolute,
		commission_underwriter_absolute,
		credit_charge_absolute,
		helpline_charge_absolute,
		tpl_absolute,
		pricing_engine_id,
		vol_excess,
		promo_discount_absolute, 
		offer_discount_absolute, 
		multipet_discount_absolute
	)
	VALUES (
		@new_policy_id, 
		@policy_start_date, 
		@premium, 
		@promo_discount, 
		@offer_discount, 
		@multipet_discount, 
		@postcode, 
		@cover_id, 
		@promo_code,
		@ipt_absolute,
		@commission_affinity_absolute,
		@commission_administrator_absolute,
		@commission_underwriter_absolute,
		@credit_charge_absolute,
		@helpline_charge_absolute,
		@tpl_absolute,
		@pricing_engine_id,
		@voluntary_excess,
		@promo_discount_absolute, 
		@offer_discount_absolute, 
		@multipet_discount_absolute
	)
	

	IF (@master_policy_id IS NOT NULL)
	BEGIN
		INSERT INTO uispetmis..policy_related (master_policy_id, policy_id) 
		VALUES (@master_policy_id, @new_policy_id)

		-- Add @master_policy_id, @master_policy_id row if it doesn't exist
		IF (NOT EXISTS
				(SELECT * FROM uispetmis..policy_related 
				WHERE policy_id = @master_policy_id 
					AND master_policy_id = @master_policy_id))
		BEGIN
			
			INSERT INTO uispetmis..policy_related (master_policy_id, policy_id) 
			VALUES (@master_policy_id, @master_policy_id)

		END
	END

	SELECT [PolicyID]
		  ,[AffinityCode]
		  ,[ClaimType]
		  ,[PrintDate]
		  ,[Printed]
		  ,[PolicyNumber]
		  ,[PuppyCoverNumber]
		  ,[PolicyInceptionDate]
		  ,[StartDate]
		  ,[RenewalDate]
		  ,[AnimalPetName]
		  ,[AnimalDateOfBirth]
		  ,[BreedDescription]
		  ,[ColourDescription]
		  ,[AnimalSexName]
		  ,[AnimalTypeName]
		  ,[ProductName]
		  ,[PaymentFrequencyName]
		  ,[PolicyPremiumYearID]
		  ,[Title]
		  ,[FirstName]
		  ,[LastName]
		  ,[Address1]
		  ,[Address2]
		  ,[Address3]
		  ,[Address4]
		  ,[Address5]
		  ,[PostCode]
		  ,[Telephone1]
		  ,[AnimalDead]
		  ,[DateTimestamp]
		  ,[AnimalBabyName]
		  ,[PreExistingCondition]
		  ,[Exclusion]
		  ,[PreviousPolicyID]
		  ,[cdl_source_id] [CdlSourceId]
		  ,[vol_excess] [VolExcess]
		  ,[breed_excess] [BreedExcess]
		  ,[status_id] [StatusId]
		  ,[quote_pet_id] [QuotePetId]
		  ,[CancelledDate]
		  ,[cancellation_reason_id] [CancellationReasonId]
		  ,[email_address] [EmailAddress]
		  ,[purchase_price] [PurchasePrice]
		  ,[breed_id] [BreedId]
		  ,[animal_type_id] [AnimalTypeId]
		  ,[product_id] [ProductId]
		  ,[clinic_id] [ClinicId]
		  ,[cover_id] [CoverId]
		  ,[cancellation_instruction_date] [CancellationInstructionDate]
		  ,[last_payment_review] [LastPaymentReview]
		  ,[exclusion_review] [ExclusionReview]
		  ,[coinsurance]
		  ,[telephone2]
		  ,[telephone3]
		  ,[dob]
		  ,[microchipnumber]
		  ,[email_sent] [EmailSent]
		  ,[send_documents] [SendDocuments]
		  ,[neutered]
		  ,[ImportedDetails]
		  ,[CustomerInArrears]
	FROM uispetmis..policy
	WHERE policyId = @new_policy_id

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Correspondence].[GetQuote]'
GO

CREATE PROCEDURE [Correspondence].[GetQuote]
(
	@quoteId INT
)
AS


SELECT 
	--Quote Info
	q.id AS Id,
	q.quote_ref AS Reference,
	q.affinity AS AffinityCode,
	ISNULL(q.policy_start_date, GETDATE()) AS CoverStartDate,
	DATEADD(dd, p.quote_expiration_days,  ISNULL(q.policy_start_date, GETDATE()) ) AS ExpirationDate,
	qp.vol_excess AS VoluntaryExcess,

	-- Customer	
	ISNULL(t.description,'') AS Title,
	q.forename AS FirstName,
	q.surname AS LastName,
	ISNULL(q.email, '') AS EmailAddress,
	--if there is any pet where owned is 0, then its not owned and kept
	CAST((SELECT CASE WHEN COUNT(1) > 0 THEN 0 ELSE 1 END AS OwnerAndKeeperOfPet FROM uispet..quote_pet WHERE quote_id = @quoteId AND ISNULL(owned,0) = 0) AS BIT) AS OwnerAndKeeperOfPet,
	ISNULL(q.contact_num, '') AS TelephoneNumber,
	q.DOB AS CustomerDateOfBirth,

	--Address
	ISNULL(q.house_name_num,'') AS Address1,
	ISNULL(q.street,'') AS Address2,
	ISNULL(q.street2,'') AS Address3,
	ISNULL(q.town,'') AS City,
	ISNULL(q.county,'') AS County,
	ISNULL(q.PostCode,'') AS Postcode


FROM uispet..quote q
LEFT JOIN uispet.dbo.title t ON q.title_id = t.id --left join because title_id is nullable
INNER JOIN uispet..product p ON p.id = q.product_id
LEFT JOIN uispet..quote_pet qp ON q.id = qp.quote_id 
WHERE q.id = @quoteId;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_weighting]'
GO

CREATE FUNCTION [dbo].[renewal_weighting](@policy_id int) 
RETURNS @weight TABLE (policy_id int, claims_count int, claims_value money, recurrence int, loading decimal(5,2), loading_standard decimal(5,2))
AS
BEGIN

--******************* TCF override *******************
-- if there is a pending renewal, don't recalculate the loading, use the loading off the renewal offer
--or if not started yet return nothings for upgrade before inception
if exists (select 1 from uispetmis..PREMIUM where policy_id = @policy_id
	and renewal in (1,2)
	and effective_date > GETDATE())
begin
	insert into @weight
	select TOP 1 policy_id, claims_count, claims_value, claims_recurrence recurrence, loading, loading_standard
	from uispetmis..PREMIUM
	where policy_id = @policy_id
	and renewal in (1,2)
	--and contacted = 1
	and effective_date > GETDATE()
	order by effective_date desc
	return
end
if exists (select 1 from uispetmis..POLICY where PolicyID = @policy_id and PolicyInceptionDate > GETDATE())
begin
	insert into @weight
	select @policy_id, 0, 0, 1, 1, 1 
return;
end
--******************* TCF override *******************

declare @claims_count int, @claims_value int;
declare @max_recurrence int;
declare @loading decimal(5,2);
declare @loading_standard decimal(5,2);
declare @prev_renewal_loading decimal(5,2);
declare @prev_renewal_claims_count int;
declare @underwriter varchar(3);
declare @product_id int;

declare @previous_calculation_date date --calcs done before renewal so need to search for claims that may have happened after previous year calculation
	, @last_renewal_premium_id int
	, @effective_date date
	, @renewal_date date;

select @loading_standard = 1, @loading = 1

select @last_renewal_premium_id = pre.id, 
@prev_renewal_loading = ISNULL(pre.loading,1), -- Obtain loading from table based on experience in current period
@prev_renewal_claims_count = ISNULL(pre.claims_count,0)
from uispetmis..policy p
join uispetmis..premium pre on pre.id = uispetmis.dbo.[get_premium_id_by_date](@policy_id, p.startdate) where p.policyid = @policy_id

select @previous_calculation_date = calculated_date 
	,@effective_date = effective_date
from uispetmis..PREMIUM where id = @last_renewal_premium_id;

declare @year_offset int; set @year_offset = 0;
if(DATEADD(DAY, 15, @effective_date) > GETDATE())
	set @year_offset = -1;

-- first find out the underwriter to we can apply the appropriate rules
select @underwriter = c.underwriter,
	@product_id = c.product_id,
	@renewal_date = p.renewaldate
	from uispetmis..policy p 
	left join renewal_cover rc on rc.old_cover_id = p.cover_id and rc.valid_from <= p.renewaldate --check for migration to new underwriter
	join cover c on ISNULL(rc.new_cover_id,p.cover_id) = c.id
	where p.policyid = @policy_id
	
select --sum(case when previousclaimid = 0 then 1 else 0 end) claims,
	@claims_count = count(distinct uispetmis.dbo.get_master_claim_id(c.claimid))
	--,sum(case when previousclaimid = 0 then total else 0 end) total_non_prev	
	--,sum(case when previousclaimid > 0 then total else 0 end) total_prev
	,@claims_value = sum(ISNULL(total,0))
	,@max_recurrence = max(ISNULL(case lcs.Recurrence when 'yes' then 3 when 'possibly' then 2 when 'no' then 1 else 1 end,1))
	from uispetmis..claim c
		join uispetmis..policy p on p.policyid = c.policyid
		join uispetmis..status s on s.statusid = c.statusid
		left join uispetmis..LOSS_CODE_SPECIFIC lcs on lcs.specificid = c.LossCodeSpecificID
	where c.policyid = @policy_id
--	and c.treatmentdatefrom >= ISNULL(@previous_calculation_date, p.startdate)
--	and c.treatmentdatefrom < p.renewaldate
	and c.treatmentdatefrom >= ISNULL(DATEADD(YEAR, @year_offset, @previous_calculation_date), DATEADD(YEAR, @year_offset, p.startdate))
	and c.treatmentdatefrom < DATEADD(YEAR, @year_offset, p.renewaldate)
	and s.Renewal = 1
	--and s.description = 'Ready to Process'

IF @underwriter IN('UIC','HAN','L&G')
BEGIN

	if(@underwriter in ('HAN','L&G')) select @underwriter = 'UIC' -- duplicate UIC loadings

	select top 1 @loading_standard = loading
		from renewal_matrix where recurrence = ISNULL(@max_recurrence,1)
		and claims = case when @claims_count > 3 then 3 else @claims_count end
		and value < ISNULL(@claims_value,1)
		and underwriter = @underwriter
		and (ISNULL(product_id,0) = @product_id or product_id IS NULL)
		and @renewal_date BETWEEN effective_date AND ISNULL(inactive_date,'2050-01-01')
		order by value desc
	
	declare @claims_last_two_years int
	set @claims_last_two_years = @claims_count + @prev_renewal_claims_count
		
	IF @loading_standard <= @prev_renewal_loading AND @claims_last_two_years > 0 
	BEGIN
		SELECT @loading = (@loading_standard + @prev_renewal_loading) / 2.0 --If loading below prior year use prior + current year divided by 2 (if two years claims free after loading aplied loading = 0%)
	END	
	ELSE
	BEGIN
		SELECT @loading = @loading_standard
	END
END
ELSE IF @underwriter = 'GLK'
BEGIN
	select top 1 @loading_standard = loading
		from renewal_matrix where recurrence = 0 -- GLK don't consider claims recurrence
		and claims = case when @claims_count > 6 then 6 else @claims_count end
		and value = 0  -- GLK do not consider claim value
		and underwriter = @underwriter
		and (ISNULL(product_id,0) = @product_id or product_id IS NULL)
		and @renewal_date BETWEEN effective_date AND ISNULL(inactive_date,'2050-01-01')
		order by value desc
		
	SELECT @loading = @loading_standard
END

insert into @weight
select @policy_id, @claims_count claims_count, ISNULL(@claims_value,0) claims_value, ISNULL(@max_recurrence,0) max_recurrence, @loading loading, @loading_standard loading_standard


--select lcs.recurrence, c.claimid, c.LossCodeSpecificID,s.description from claim c 
--join status s on s.statusid = c.statusid 
--left join LOSS_CODE_SPECIFIC lcs on lcs.specificid = c.LossCodeSpecificID
--where policyid = @policy_id --order by statusid;

return

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sagepay_transactionU_prc]'
GO


CREATE PROCEDURE [dbo].[sagepay_transactionU_prc]
(
	@quote_id int = null,
	@policy_id int = null,
	@VendorTxId varchar(256),
	@VPSTxId varchar(256),
	@SecurityKey varchar(256)
)
AS

SET NOCOUNT ON


insert into sagepay_transaction (quote_id, policy_id, VendorTxId, VPSTxId, SecurityKey, ApplicationTypeId, create_timestamp)
values (@quote_id, @policy_id, @VendorTxId, @VPSTxId, @SecurityKey, 1, GETDATE());


SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_detailsS_prc]'
GO

CREATE PROCEDURE [dbo].[quote_detailsS_prc]  (@id INT)
		
AS
SET NOCOUNT ON


		SELECT	q.id,
				pd.amount premium,
				--q.premium,
				quote_ref,
				t.description+' '+ISNULL(forename,'')+' '+ISNULL(surname,'') as owner,
				DATEPART(yyyy, DOB) as YOB,
				ISNULL(postcode,'') as postcode,
				ISNULL(house_name_num,'') as house_name_num,
				ISNULL(street,'') as street ,
				ISNULL(street2,'') as street2 ,
				ISNULL(town,'') as town,
				ISNULL(county,'') as county,
				ISNULL(contact_num,'') as contact_num,
				ISNULL(email,'') as email,
				qs.description quote_status,
				case q.quote_status_id when 5 then 1 else 0 end information_needed,
				cover_id,
				CONVERT(VARCHAR(11), policy_start_date,113) as policy_start_date,
				ISNULL(VFV_clinic,'') as VFV_clinic,
				ISNULL(VFV_clinicID,'') as VFV_clinicID,
				ISNULL(pt.description,'') payment_method,
				ISNULL(referrerID, '') referrer,
				ISNULL(promo_code, '') promo_code
		FROM	quote q
		LEFT JOIN title t on q.title_id = t.id
		JOIN quote_status qs on q.quote_status_id = qs.id
		LEFT JOIN payment_detail pd on pd.id = q.payment_detail_id
		LEFT JOIN payment_type pt on pt.id = pd.payment_type_id
		WHERE	q.id = @id

		SELECT	b.description breed, 
				qp.*,
				ISNULL(qp.vetvisit, 0) vetvisit_conv,
				ISNULL(qp.accident, 0) accident_conv,
				ISNULL(qp.sick, 0) sickdetails_conv,
				at.description animal,
				pg.gender_desc gender,
				c.description cover,
				qp.exclusion,
				case q.quote_status_id when 5 then 1 else 0 end information_needed,
				qp.neutered neutered
		FROM	quote_pet qp
		join quote q on qp.quote_id = q.id
		join breed b on b.id = qp.breed_id
		join animal_type at on at.id = qp.animal_type_id
		join product_gender pg on pg.id = qp.product_gender_id
		left join cover c on c.id = qp.cover_id
		WHERE	quote_id = @id

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_details_from_quote]'
GO

CREATE PROCEDURE [dbo].[policy_details_from_quote]
	@quoteId INT
AS
BEGIN

	SELECT q.policy_start_date [PolicyStartDate],
			qp.premium,
			q.email,
			qp.pet_name [PetName],
			qp.pet_DOB [PetDob],
			qp.breed_id [BreedId],
			qp.colour,
			qp.product_gender_id [ProductGenderId],
			qp.animal_type_id [AnimalTypeId],
			c.id [CoverId],
			q.title_id [TitleId],
			q.forename,
			q.surname,
			q.house_name_num [HouseNameNum],		
			q.street,
			q.street2,
			q.town,
			q.county,
			q.postcode,
			q.contact_num [ContactNumber],
			qp.vetvisit,
			qp.accident,
			qp.vol_excess [VolExcess],
			qp.purchase_price [PurchasePrice],
			qp.id [PetId],
			q.product_id [ProductId],
			q.VFV_ClinicID [ClinicId],
			qp.exclusion_review [ExclusionReview],
			ISNULL(a.date_of_birth, q.DOB) [Dob],
			qp.microchipnumber,
			qp.neutered,
			qp.promo_discount [PromoDiscount], 
			qp.offer_discount [OfferDiscount], 
			qp.multipet_discount [MultipetDiscount],
			q.promo_code [PromoCode],
			qp.IPT_absolute [IptAbsolute],
			qp.commission_affinity_absolute [CommissionAffinityAbsolute],
			qp.commission_administrator_absolute [CommissionAdministratorAbsolute],
			qp.commission_underwriter_absolute [CommissionUnderwriterAbsolute],
			qp.credit_charge_absolute [CreditChargeAbsolute],
			qp.helpline_charge_absolute [HelplineChargeAbsolute],
			qp.tpl_absolute [TplAbsolute],
			qp.pricing_engine_id [PricingEngineId],
			qp.promo_discount_absolute [PromoDiscountAbsolute], 
			qp.offer_discount_absolute [OfferDiscountAbsolute],
			qp.multipet_discount_absolute [MultipetDiscountAbsolute],
			p.affinity_code [AffinityCode],
			q.update_timestamp [SoldDate],
			q.agree_contact [AgreeContact],
			q.agree_contact_phone [AgreeContactPhone],
			q.agree_contact_SMS [AgreeContactSMS],
			q.agree_contact_email [AgreeContactEmail],
			q.agree_contact_letter [AgreeContactLetter],
			q.ReferrerID,
			q.enquiry_method_code [EnquiryMethodCode],
			q.ClientIpAddress,
			mu.username [ModifiedUsername],
			cu.username [ConvertedUsername],
			q.quote_status_id [QuoteStatusId],
			q.agg_remote_ref [AggRemoteRef],
			qp.quote_id [QuoteId], 
			qp.agg_cover_id [AggCoverId], 
			qp.vetvisitdetails [VetVisitDetails],
			qp.accidentdetails [AccidentDetails],
			q.quote_ref [QuoteReference],
			q.create_timestamp [QuoteCreated],
			qu.username [QuoteCreatedUsername],
			q.hear_about_us_id [HearAbboutUsId],
			dbo.DeriveSalesSource(@quoteId) [Channel]
	FROM quote q
	JOIN quote_pet qp ON q.id = qp.quote_id
	JOIN cover c ON c.id = ISNULL(qp.cover_id, q.cover_id)
	JOIN product p ON p.id = c.product_id
	LEFT JOIN aggregator a ON a.quote_id = q.id
	LEFT JOIN uispetmis..systuser mu ON mu.id = q.modified_systuser_id
	LEFT JOIN uispetmis..systuser cu ON cu.id = q.converted_systuser_id
	LEFT JOIN uispetmis..systuser qu ON qu.id = q.created_systuser_id
	WHERE q.id = @quoteId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetDocumentsByQuote]'
GO

CREATE FUNCTION [dbo].[GetDocumentsByQuote](@quoteId int)
RETURNS @documents TABLE
	(
		DocumentTypeId varchar(20), 
		DocumentName varchar(50)  null,
		DocumentId int null,
		DocumentAuditId int  null
	)
AS 
BEGIN

	declare @cover_id int;
	declare @effective_date date;


	--as these documents have a configuration 'per cover'
	--we are going to get it from the first cover of the product

	declare @productId int;
	SELECT @effective_date = ISNULL(policy_start_date, GETDATE()), @productId = q.product_id
	FROM uispet..quote q
	WHERE q.id = @quoteId
	
	--first active cover for that product
	SELECT TOP 1 
		@cover_id = c.id
	FROM cover c
	WHERE @effective_date BETWEEN isnull(c.cover_effective_date,'1970-01-01') AND ISNULL(c.cover_inactive_date,'2050-01-01')
	AND  c.product_id=@productId


	-- TOBA
	declare @toba_id int, @toba_updated_timestamp datetime, @toba_hist_id int;		
	SELECT	top 1
			@toba_id = case when at.toba is null then null else at.id end,
			@toba_updated_timestamp = at.updated_timestamp
			--@cover_inactive_date = ISNULL(c.cover_inactive_date,'2050-01-01')
	FROM uispet..cover c LEFT JOIN uispetmis..affinity_toba at
							ON LEFT(c.affinity_code,3) = at.affinity_code COLLATE Latin1_General_CI_AS
							AND at.valid_from_date = 
								(SELECT MAX(at2.valid_from_date) FROM uispetmis..affinity_toba at2
									WHERE at.affinity_code = at2.affinity_code and at2.valid_from_date <= @effective_date)
	WHERE c.id=@cover_id;

	SELECT top 1 @toba_hist_id = ada.id
	FROM uispetmis..affinity_document_audit ada
	WHERE ada.affinity_toba_id=@toba_id
		AND ada.updated_datetimestamp <= @effective_date
	ORDER BY ada.updated_datetimestamp desc

	insert into @documents(DocumentTypeId,DocumentId,DocumentAuditId)
	values('toba',@toba_id,@toba_hist_id);

	-- PW & FK
	declare @pw_id int, @kf_id int, @updated_timestamp datetime, @pw_hist_id int, @kf_hist_id int;
	SELECT	top 1
			@pw_id = case when ad.document is null then null else ad.id end,
			@kf_id = case when ad.key_facts is null then null else ad.id end,
			@updated_timestamp = ad.uploaded_date
			--@cover_inactive_date = ISNULL(c.cover_inactive_date,'2050-01-01')
	FROM uispet..cover c LEFT JOIN uispetmis..affinity_document ad 
							ON  c.affinity_code = ad.affinity_code COLLATE Latin1_General_CI_AS  
							AND ad.valid_from = 
								(SELECT MAX(ad2.valid_from) FROM uispetmis..affinity_document ad2 
									WHERE ad.affinity_code = ad2.affinity_code AND ad2.valid_from <= @effective_date)
	WHERE c.id=@cover_id;


	SELECT top 1 @pw_hist_id = ada.id
	FROM uispetmis..affinity_document_audit ada
	WHERE ada.affinity_document_id=@pw_id
		AND ada.description = 'Policy Wording'
		AND ada.updated_datetimestamp <= @effective_date
	ORDER BY ada.updated_datetimestamp desc

	insert into @documents(DocumentTypeId,DocumentId,DocumentAuditId)
	values('pw',@pw_id,@pw_hist_id);

	SELECT top 1 @kf_hist_id = ada.id
	FROM uispetmis..affinity_document_audit ada
	WHERE ada.affinity_document_id=@kf_id
		AND ada.description = 'Key Facts'
		AND ada.updated_datetimestamp <= @effective_date
	ORDER BY ada.updated_datetimestamp desc

	insert into @documents(DocumentTypeId,DocumentId,DocumentAuditId)
	values('kf',@kf_id,@kf_hist_id);


	-- II
	--insert into @documents(DocumentTypeId,DocumentId,DocumentAuditId)
	--select ii.document_type_id, ii.document_id, ii.document_audit_id from dbo.get_important_information_udf(@policyId,NULL,NULL) ii;


	-- Update descriptions
	update @documents
	set DocumentName = (select top 1 
						description 
						FROM uispetmis..document_type 
						WHERE id COLLATE SQL_Latin1_General_CP1_CS_AS = docs.DocumentTypeId COLLATE SQL_Latin1_General_CP1_CS_AS 
						) 
	from @documents docs;





	-- Select output table
	DELETE FROM @documents
	where DocumentId is null --or document_type_id='dm';

	return
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_schedule]'
GO
CREATE TABLE [dbo].[policy_schedule]
(
[id] [int] NOT NULL,
[schedule_doc] [image] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_policy_schedule] on [dbo].[policy_schedule]'
GO
ALTER TABLE [dbo].[policy_schedule] ADD CONSTRAINT [PK_policy_schedule] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[schedule_blobU_prc]'
GO
CREATE PROCEDURE [dbo].[schedule_blobU_prc] (@id	int, @blob image)
AS

set nocount on

	if exists (select 1 from policy_schedule where id = @id)
	BEGIN
		update policy_schedule
		set	schedule_doc = @blob
		where id = @id
	END
	ELSE
	BEGIN
		insert into policy_schedule (id, schedule_doc) VALUES (@id, @blob)
	END

set nocount off


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ProductDetailsFromCoverId]'
GO

CREATE PROCEDURE [dbo].[ProductDetailsFromCoverId]
	@coverId INT
AS
BEGIN

	SELECT C.product_id, P.prod_prefix FROM dbo.cover C
	JOIN product P ON P.id = C.product_id
	WHERE C.id = @coverId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_retrieveS_prc]'
GO



CREATE PROCEDURE [dbo].[quote_retrieveS_prc]  (@ref VARCHAR(16) , @postcode VARCHAR(16))
AS

SET NOCOUNT ON
	
	declare @quote_id int;

	SELECT @quote_id = q.id
	FROM	quote  q JOIN product p ON q.product_id = p.id
	WHERE	REPLACE(quote_ref, ' ', '') = REPLACE(@ref, ' ', '')
		AND REPLACE(q.postcode,' ', '') = REPLACE(@postcode, ' ', '');

	-- if quote_ref and postcode not found on QUOTE table, search AGGREGATOR table
	IF @quote_id is null
	BEGIN
		declare @agg_id int
		declare @agg_quote_ref varchar(16)
	
		SELECT	@agg_id = a.id,
				@agg_quote_ref = a.quote_ref,
				@quote_id = a.quote_id
		FROM	aggregator  a
		WHERE	REPLACE(a.quote_ref, ' ', '') = REPLACE(@ref, ' ', '')
			AND REPLACE(a.postcode,' ', '') = REPLACE(@postcode, ' ', '')

		-- if aggregator quote doesn't have quote_id, then covert aggregator quote to standard quote and use the new quote id
		if @agg_id is not null and @quote_id is null
		begin
			SELECT
				 null			id
				,null			prod_prefix
				,null			quote_ref
				,null			quote_status_id
				,1				new_agg_quote
				,@agg_id		agg_id
				,@agg_quote_ref	agg_quote_ref;
			
			return;
		end;
	end;
	
	SELECT
		q.id
		,p.prod_prefix
		,quote_ref
		,quote_status_id
		,0		new_agg_quote
		,null	agg_id
		,null	agg_quote_ref
	FROM quote  q JOIN product p ON q.product_id = p.id
	WHERE q.id=@quote_id;
	
	
	
--now for fun with the aggregators
--CPU business rule about their minimum visible product
/*
select a.id aid
	,a.quote_ref qref
	--,case when affinity = 'CPU' and source = 'GOCOMP' then 221 else NULL end cover_id
	,NULL cover_id
from lead l
	join aggregator a on a.lead_id = l.id
where l.quote_id is null
and REPLACE(a.quote_ref, ' ', '') = REPLACE(@ref, ' ', '')
AND	REPLACE(a.postcode,' ', '') = REPLACE(@postcode, ' ', '')
*/

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Correspondence].[GetDocumentLinksByQuote]'
GO

CREATE PROCEDURE [Correspondence].[GetDocumentLinksByQuote] 
(
	@quoteId AS INT
)
AS

DECLARE @baseDocumentViewerURL VARCHAR(200) = 'http://documentation.ultimatepetpartners.co.uk/';--this changes based on the environment


--loading the documents returned fromt the function into this temp/memory table
declare @documents TABLE
	(
		DocumentTypeId varchar(20), 
		DocumentName varchar(50)  null,
		DocumentId int null
	);
INSERT INTO @documents
        ( DocumentTypeId ,
          DocumentName ,
          DocumentId
        )
SELECT d.DocumentTypeId, d.DocumentName, d.DocumentId FROM [GetDocumentsByQuote](@quoteId) d

--we have now to convert it to columns 
DECLARE @documentLinks TABLE
(
	QuoteId INT,
	TobaURL VARCHAR(300),
	PolicyWordingURL VARCHAR(300),
	KeyFactsURL VARCHAR(300),
	ClaimFormURL VARCHAR(300)
);
--insert the record that will returned, and then just update the links..
INSERT INTO @documentLinks
        ( QuoteId ,
          TobaURL ,
          PolicyWordingURL ,
          KeyFactsURL ,
          ClaimFormURL
        )
VALUES  ( @quoteId , -- PolicyId - int
          '' , -- TobaURL - varchar(300)
          '' , -- PolicyWordingURL - varchar(300)
          '' , -- KeyFactsURL - varchar(300)
          ''  -- ClaimFormURL - varchar(300)
        )

DECLARE @link VARCHAR(300) = NULL;
--the we are selecting are related to the uispetmis..document_type table

--TOBA
SELECT @link = @baseDocumentViewerURL+'?doctype=toba&docid='+CAST(d.DocumentId AS varchar(20)) FROM @documents d WHERE d.DocumentTypeId = 'toba'
UPDATE @documentLinks
SET TobaURL = @link WHERE 1=1

--Key Facts
SELECT @link = @baseDocumentViewerURL+'?doctype=kf&docid='+CAST(d.DocumentId AS varchar(20)) FROM @documents d WHERE d.DocumentTypeId = 'kf'
UPDATE @documentLinks
SET KeyFactsURL = @link WHERE 1=1

--Policy Wording
SELECT @link = @baseDocumentViewerURL+'?doctype=pw&docid='+CAST(d.DocumentId AS varchar(20)) FROM @documents d WHERE d.DocumentTypeId = 'pw'
UPDATE @documentLinks
SET PolicyWordingURL = @link WHERE 1=1

--Claim Form
--this one is not returned by the function - it is simpler because it uses just the product id to load the document
DECLARE @productId int;
SELECT @productId = q.product_id
FROM uispet..quote q WHERE q.id = @quoteId;

SELECT @link = @baseDocumentViewerURL+'?doctype=cf&prodid='+CAST(@productId AS varchar(20))
UPDATE @documentLinks
SET ClaimFormURL = @link WHERE 1=1


SELECT * FROM @documentLinks
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_breed_score]'
GO
CREATE TABLE [dbo].[product_breed_score]
(
[product_id] [int] NOT NULL,
[breed_id] [int] NOT NULL,
[rating] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_breed_score] on [dbo].[product_breed_score]'
GO
ALTER TABLE [dbo].[product_breed_score] ADD CONSTRAINT [PK_product_breed_score] PRIMARY KEY CLUSTERED  ([product_id], [breed_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_breed_group]'
GO
CREATE TABLE [dbo].[product_breed_group]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NULL,
[breed_id] [int] NULL,
[rating] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL,
[breed] [nvarchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_breed_group] on [dbo].[product_breed_group]'
GO
ALTER TABLE [dbo].[product_breed_group] ADD CONSTRAINT [PK_product_breed_group] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breedL_prc]'
GO

CREATE PROCEDURE [dbo].[breedL_prc] (@animal_type_id INT, @product_id int = 0, @breed_category_id int = 0, @sales_type VARCHAR(50) = 'NB')
AS

SET NOCOUNT ON

	if(@animal_type_id in (1,2))
	BEGIN
		IF (SELECT COUNT(*) FROM uispet..product_pricing_engine WHERE product_id = @product_id AND new_business_effective_date <= GETDATE() AND pricing_engine_id = 2) > 0
		BEGIN
			SELECT DISTINCT b.id id, b.description description FROM uispet..breed b
				INNER JOIN uispet..zenith_breed_mapping zbm ON zbm.upp_breed_id = b.id
				WHERE zbm.cover_id IN (SELECT c.id FROM uispet..cover c WHERE c.product_id = @product_id AND c.cover_effective_date <= GETDATE() AND (c.cover_inactive_date > GETDATE() OR c.cover_inactive_date IS null))
				--AND b.breed_category_id IN (@breed_category_id, 0)
				AND (b.breed_category_id = @breed_category_id or @breed_category_id = 0)
				AND b.animal_type_id = @animal_type_id
				-- if it is new business we have to filter out the breeds that are renewal only. Renewal only breed should be loaded always when its not new business
				AND zbm.sales_type = @sales_type
				--AND b.map_to_breed_id IS NULL
				ORDER BY b.description
		END
		ELSE
		BEGIN
			IF @product_id IN (1,5,9,10,12,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32)
			BEGIN
			SELECT breed_id id
				,LOWER(breed)AS description
			FROM product_breed_group pbg
			join breed b on b.id = pbg.breed_id 
			where product_id = CASE WHEN @product_id in (26,31) THEN 1 ELSE 32 END
			and rating in ('S','M','L','H','G','P','N')
			and (b.breed_category_id = @breed_category_id or @breed_category_id = 0)
			and animal_type_id = @animal_type_id 
			order by breed
		END
			ELSE
			BEGIN
			--IF @product_id = 22
			--	select @product_id = 11
			--ELSE 
			IF @product_id not in (3,6,11,13,14,22,103) --2
				SELECT @product_id = 1
			IF @product_id = 14
				SELECT @product_id = 13				

			IF(@animal_type_id = 1)
			BEGIN
				SELECT 	min(b.id) id, 
						LOWER(b.description) description
				FROM	breed b join product_breed_score pbs
							ON b.id = pbs.breed_id
				WHERE	b.animal_type_id = 1
				AND pbs.product_id = @product_id
				AND pbs.rating BETWEEN 1 AND 4
				and (b.breed_category_id = @breed_category_id or @breed_category_id = 0)
				and animal_type_id = @animal_type_id 
				GROUP BY  description
				ORDER BY description 
			END
			ELSE
			BEGIN
				SELECT 	min(id) id, 
						LOWER(description)AS description
				FROM	breed
				WHERE	animal_type_id = @animal_type_id 
				and (breed_category_id = @breed_category_id or @breed_category_id = 0)
				GROUP BY  description
				ORDER BY description 
			END

		END
		END

	END
	ELSE
	BEGIN
		SELECT 	min(id) id, 
				LOWER(description)AS description
		FROM	breed
		WHERE	animal_type_id = @animal_type_id 
		and (breed_category_id = @breed_category_id or @breed_category_id = 0)
		GROUP BY  description
		ORDER BY description 
	END
SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QueueQuoteEmail]'
GO
CREATE PROCEDURE [dbo].[QueueQuoteEmail]
	@quoteId INT,
	@userId int
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (SELECT COUNT(1) FROM QuoteEmailQueue WHERE QuoteId = @quoteId) = 0  
	BEGIN
		INSERT INTO QuoteEmailQueue (QuoteId, Created) VALUES (@quoteId, GETDATE())
	end

	EXEC uispetmis..diaryU_prc 	
		@reminder_days=0,
		@description='Quote Email was requested to be sent',
		@user_id=@userId,
		@quote_id=@quoteId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[animal_typeL_prc]'
GO

CREATE PROCEDURE [dbo].[animal_typeL_prc]
AS

SET NOCOUNT ON

	SELECT 	id, 
			description
	FROM	animal_type
	ORDER BY description 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[titleL_prc]'
GO

CREATE PROCEDURE [dbo].[titleL_prc]
AS

SET NOCOUNT ON

	SELECT 	id, 
			description
	FROM	title
	ORDER BY id 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit_specificL_prc]'
GO

-- =============================================
-- Author:		Steve Powell
-- Create date: 31/05/2012
-- Description:	Returns cover benefits for specified cover id
-- =============================================
CREATE PROCEDURE [dbo].[cover_benefit_specificL_prc]
	@cover_id as int = null,
	@policy_id as int = null,
	@filter_type as varchar(30) = null,
	@filter_section_id as int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @cover_id is null and @policy_id is not null
	begin
		select top 1 @cover_id = cover_id
		from uispetmis..premium
		where policy_id = @policy_id
		order by effective_date desc, id desc;
	end


		declare @tempCoverBenefitTable as table 
		(section varchar(300),
		 benefit varchar(300), 
		 section_id int, 
		 parent_section_id int, 
		 ordinal int, 
		 parent_ordinal int);
		 
		insert into @tempCoverBenefitTable
		select dbo.replaceSup(isnull(s.alternative_description, s.description), @cover_id, 0) as section,
			CASE WHEN cb.id IN (6160, 6177, 6228, 6245) THEN 'Lifetime' + NCHAR(0x00B9)
			WHEN cb.id IN (6125,6142,6159,6176,6193,6210,6227,6244) THEN ISNULL(cb.description, '')
			WHEN product_id = 31 and len(isnull(cb.alternative_description,cb.description)) = 1 then replace(isnull(cb.alternative_description,cb.description),'Y','Yes')
				 else isnull(cb.alternative_description,cb.description) end as benefit,
		s.id as section_id, 
		isnull(s.section_id,0) as parent_section_id, 
		s.ordinal
		,case when s.section_id IS null then s.ordinal else 
		(select sec.ordinal from section sec where sec.id = s.section_id)
		 end as parent_ordinal
		from cover_benefit cb
			inner join section s on cb.section_id = s.id
		where cb.cover_id = @cover_id and (len(isnull(cb.alternative_description,cb.description)) > 1 or isnull(cb.alternative_description,cb.description) = 'Y')
		order by parent_ordinal, s.ordinal;
		
		if @filter_type is null or @filter_section_id is null
		begin
			select * from @tempCoverBenefitTable;
		end
		else if @filter_type = 'get_by_section_id'
			begin
				select * from @tempCoverBenefitTable c
				where c.section_id = @filter_section_id;
			end 
			else if @filter_type = 'get_by_parent_section_id'
			begin
				select * from @tempCoverBenefitTable c
				where c.parent_section_id = @filter_section_id;
			end
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_detailS_prc]'
GO



CREATE PROCEDURE [dbo].[payment_detailS_prc]  (@quote_id INT)
AS

SET NOCOUNT ON


			DECLARE @id UNIQUEIDENTIFIER 
			SELECT  @id = payment_detail_id 
			FROM	quote 
			WHERE	id = @quote_id 

OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;

			SELECT	CONVERT(varchar(256),pd.id) as id,
					payment_type_id,
					card_type_id,
					CONVERT(varchar, DecryptByKey(enc_card_number)) card_number,
					CONVERT(varchar, DecryptByKey(enc_card_name)) card_name,
					LEFT(CONVERT(varchar, DecryptByKey(enc_valid_from)),2) valid_from_month,
					RIGHT(CONVERT(varchar, DecryptByKey(enc_valid_from)),2) valid_from_year,
					LEFT(CONVERT(varchar, DecryptByKey(enc_valid_to)),2) valid_to_month,
					RIGHT(CONVERT(varchar, DecryptByKey(enc_valid_to)),2) valid_to_year,
					CONVERT(varchar, DecryptByKey(enc_account_number)) account_number,
					CONVERT(varchar, DecryptByKey(enc_account_name)) account_name,
					sort_code1 = LEFT(CONVERT(varchar, DecryptByKey(enc_sort_code)),2),
					sort_code2 = SUBSTRING(CONVERT(varchar, DecryptByKey(enc_sort_code)),3,2),
					sort_code3 = RIGHT(CONVERT(varchar, DecryptByKey(enc_sort_code)),2),
					CONVERT(varchar, DecryptByKey(enc_issue_number)) issue_number,
					ClientIPAddress,
					post_result	,
					CONVERT(VARCHAR(11), create_date) as create_date,
					CONVERT(VARCHAR(11), update_date) as update_date,
					ct.value card_type
			FROM	payment_detail pd 
				left JOIN card_type ct on ct.id = pd.card_type_id
			WHERE	pd.id = @id


			

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Core].[GetAffinitiesByUsername]'
GO
CREATE PROCEDURE [Core].[GetAffinitiesByUsername]
	@userName varchar(50) = null
AS
BEGIN

--DECLARE @userName varchar(50) = null
	
	SET NOCOUNT ON;

	IF (@userName IS NULL)
		BEGIN
			SELECT DISTINCT 
				LEFT(af.[AffinityCode], 3)  AS 'Code'
				,af.[Description] AS 'Name'
				,af.active [Enabled]
			FROM [uispet].dbo.AFFINITY as af
			WHERE af.[Description] IS NOT NULL
				AND ((af.active IS NOT NULL) AND (af.active = 1))
			ORDER BY LEFT(af.[AffinityCode], 3) ASC;
		END
	ELSE
		BEGIN
			SELECT DISTINCT [Code], [Name], [Enabled] FROM (
				SELECT LEFT(sua.[affinity_code], 3) AS 'Code'
					,af.[Description] AS 'Name'
					,af.active [Enabled]
				FROM [uispet].[dbo].[systuser_affinity] AS sua
					INNER JOIN [uispet].[dbo].[systuser] AS su ON su.id = sua.systuser_id
					INNER JOIN [uispet].[dbo].[AFFINITY] AS af ON LEFT(sua.[affinity_code], 3) = LEFT(af.AffinityCode, 3)
				WHERE ((af.active IS NOT NULL) AND (af.active = 1))
					AND su.username = @userName

				UNION

				SELECT LEFT(sga.[affinity_code], 3)  AS 'Code'
					,af.[Description] AS 'Name'
					,af.active [Enabled]
				FROM [uispet].[dbo].[systuser] as su 
					INNER JOIN [uispet].[dbo].systuser_group as sug ON su.id = sug.user_id
					INNER JOIN [uispet].[dbo].[systgroup_affinity] as sga ON sug.group_id = sga.systgroup_id
					INNER JOIN [uispet].[dbo].[AFFINITY] as af ON LEFT(sga.[affinity_code], 3) = LEFT(af.AffinityCode, 3)
				WHERE af.[Description] IS NOT NULL
				AND ((af.active IS NOT NULL) AND (af.active = 1))
					AND su.username = @userName
				) as af
			ORDER BY [Code] ASC;
		END    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discount_valid_prc]'
GO
CREATE PROCEDURE [dbo].[discount_valid_prc] @code nvarchar(10), @product_id int
AS
BEGIN

	SELECT distinct d.code 
	FROM cover c 
		join cover_discount cd on c.id = cd.cover_id
		join discount d on cd.discount_id = d.id
	WHERE d.code = @code
	AND c.product_id = @product_id
	AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_petS_prc]'
GO



--quoteS_prc 1
CREATE PROCEDURE [dbo].[quote_petS_prc]  (@id INT)
		
AS

SET NOCOUNT ON


		SELECT	qp.*, b.breed_category_id, b.[description] breed
		FROM	quote_pet qp
		LEFT JOIN breed b on qp.breed_id = b.id 
		WHERE	qp.id = @id


SET NOCOUNT OFF





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discountL_prc]'
GO

CREATE PROCEDURE [dbo].[discountL_prc]  (@status int = 1, @affinity varchar(3) = '')

AS

SET NOCOUNT ON

	select distinct d.*, p.prod_prefix affinity, cd.discount, df.[description] [funded_by]
			,isnull(cd.persist_at_renewal,0) as persist_at_renewal
			,case when isnull(d.staff,0) = 1 then 0 else 1 end as edit_promocode_persistence
	from discount d 
		join cover_discount cd on d.id = cd.discount_id
		join discount_fund df on cd.funded_by = df.id 
		join cover c on c.id = cd.cover_id
		join product p on p.id = c.product_id
	where ((@status = 1 AND ISNULL(end_date, DATEADD(d, 1, getdate())) > getdate())
		or (@status = 0 AND ISNULL(end_date, DATEADD(d, 1, getdate())) < getdate()))
	and p.prod_prefix like @affinity + '%'
	order by d.code

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_sales_source]'
GO


CREATE FUNCTION [dbo].[get_sales_source]
(
	@quote_id INT
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @result VARCHAR(100);
	
	IF @quote_id IS NULL
		RETURN 'Imported';
	ELSE
	BEGIN
		SELECT @result =
			CASE
				WHEN (ISNULL(q.created_systuser_id, 0) = 0 AND ISNULL(q.modified_systuser_id,0) = 0 AND ISNULL(q.converted_systuser_id,0) = 0 
					AND ((ISNULL(q.referrerid,'') = 'CVS-Iframe') OR ISNULL(q.referrerid,'') = 'Vet')) THEN 'Vet' --MIP-36 CVS

				-- Quotes with no intervention (online)
				WHEN (ISNULL(q.created_systuser_id, 0) = 0 AND ISNULL(q.modified_systuser_id,0) = 0 AND ISNULL(q.converted_systuser_id,0) = 0) THEN 'Web'
				
				-- Call centre created quotes
				WHEN ISNULL(q.ClientIpAddress,'') IN (SELECT ip_address FROM uispet..sales_ip_address) THEN
						 -- Lead scenario (outbound)
					CASE WHEN ISNULL(q.enquiry_method_code,'') = 'O' THEN 'Outbound'
						 -- Quote scenario (inbound)
						 ELSE 'Inbound' END
						 
				-- Non call centre created quotes with call centre intervention
					 --Customer calls to progress online quote / agg quote (inbound)
				WHEN ISNULL(q.enquiry_method_code,'') = 'I' THEN 'Inbound'
					 --Call centre calls customer to progress  online quote / agg quote (outbound)
				ELSE 'Outbound'
			END
		FROM quote q
		WHERE q.id = @quote_id;
	END;
		
	RETURN ISNULL(@result,'Other');
END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_sales_source_deb]'
GO

-- =============================================
-- Author:		Yvonne Hawley
-- Create date: April 2017
-- Description:	EOD-5212 Get sales source of Internet/Telephony/Web for 
--				new debenhams monthly feed to replace CDL feed
-- =============================================
CREATE FUNCTION [dbo].[get_sales_source_deb]
(
	@QuoteId INT
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @result VARCHAR(100) = 'Telephony'

	--call original function to get Imported, Web, Outbound, Inbound, Other
	--Also check CDLPolicyImportWrk for imported policies 
	--override to Internet/Telephony
	--override to Aggregator comes later
	SELECT @result = CASE uispet.dbo.get_sales_source(@QuoteId) 
		WHEN 'Imported' THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(import.channel, 'Imported'), 'Inbound', 'Telephony'), 'Outbound', 'Telephony'), 'Other', 'Telephony'), 'Call Centre', 'Telephony'), 'Web', 'Internet') 
		ELSE                 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(uispet.dbo.get_sales_source(@QuoteId), 'Inbound', 'Telephony'), 'Outbound', 'Telephony'), 'Other', 'Telephony'), 'Call Centre', 'Telephony'), 'Web', 'Internet') END
	FROM UISPet.dbo.quote q
	LEFT JOIN UISPet.dbo.quote_pet qp ON qp.quote_id = q.id
	LEFT JOIN UISPetMIS.dbo.Policy p ON p.quote_pet_id = qp.id
	LEFT JOIN (SELECT policyref, channel FROM UISPETMIS..CDLPolicyImportWrk) import ON p.PuppyCoverNumber = import.PolicyRef
	WHERE q.Id = @QuoteId

	--Check against valid Aggregator values
	DECLARE @ReferrerID nvarchar(100)
	SELECT @ReferrerID = ReferrerID 
	FROM UISPET..quote q
	JOIN UISPET.dbo.aggregator_detail a ON a.referrer_code = q.ReferrerID
	WHERE q.id = @QuoteId

	--override to Aggregator if there is a code specified
	IF (ISNULL(@ReferrerID, '') <> '')
	BEGIN
		SELECT @result = 'Aggregator'
	END
	
	RETURN @result

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_list_referralL_prc]'
GO


CREATE PROCEDURE [dbo].[quote_list_referralL_prc] 
AS

SET NOCOUNT ON

declare @petstrings table (qid int, petstring nvarchar(255))

DECLARE CURSOR1 CURSOR FOR
select quote_id, pet_name
from quote_pet qp
	join quote q on q.id = qp.quote_id
where q.quote_status_id = 5
order by quote_id, q.id

DECLARE @qid int, @pet varchar(64)
declare @lastqid int
select @lastqid = 0
declare @petstring nvarchar(255)

OPEN CURSOR1 
FETCH NEXT FROM  CURSOR1 INTO  @qid,@pet
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
IF (@@FETCH_STATUS <> - 2)
	
	if (@lastqid<>@qid)
	begin
		if(@lastqid>0)
			insert @petstrings values (@lastqid, @petstring)
		select @petstring = @pet
		select @lastqid = @qid
	end
	else
	begin
		select @petstring = @petstring+','+@pet
	end

	FETCH NEXT FROM CURSOR1 INTO @qid,@pet
END 
insert @petstrings values (@lastqid, @petstring)
CLOSE CURSOR1 
DEALLOCATE CURSOR1

	select q.id,
		quote_status_id,
		ISNULL(forename+' '+surname, '-') as owner,
		--ISNULL(t.description+' '+forename+' '+surname, '-') as owner,
		quote_ref,
		ISNULL(ps.petstring,'') pet_name,
		postcode,
		premium,
		qs.description as quote_status,
		q.create_timestamp,
		CASE WHEN s.ip_address IS NULL then 'C' else 'S' END source,
		policy_start_date
	from quote q
	JOIN quote_status qs ON q.quote_status_id = qs.id
	LEFT JOIN @petstrings ps on q.id = ps.qid
	LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
	--LEFT JOIN payment_detail pd on pd.id = q.payment_detail_id

	where quote_status_id = 5
	order by q.create_timestamp desc

SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_quoteS_prc]'
GO




CREATE PROCEDURE [dbo].[aggregator_quoteS_prc]  (@quote_ref VARCHAR(16))
		
AS
SET NOCOUNT ON


		/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT TOP 1000 [id]
		  ,[source]
		  ,[affinity]
		  ,[title]
		  ,[forename]
		  ,[surname]
		  ,[quote_ref]
		  ,[postcode]
		  ,[contact_num_day]
		  ,[contact_num_eve]
		  ,[email]
		  ,[status_id]
		  ,[create_timestamp]
		  ,[update_timestamp]
		  ,[policy_start_date]
		  ,[client_ip_address]
		  ,[promo_code]
		  ,[quote_id]
		  ,[lead_id]
		  ,[date_of_birth]
		  ,[house]
		  ,[address1]
		  ,[address2]
		  ,[address3]
		  ,[address4]
		  ,[remote_reference]
		  ,[suffix_id]
		  ,[opt_out_marketing]
		  ,[all_pet_names]
	  FROM [uispet].[dbo].[aggregator] WHERE quote_ref = @quote_ref

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iv_decision_resultI_prc]'
GO

create PROCEDURE [dbo].[iv_decision_resultI_prc]  
(
	@quote_id int,
	@iv_decision_match_type_id int,
	@policy_id int = null,
	@iv_request_log_id int = null,
	@band_pass varchar(50)
)
AS
SET NOCOUNT ON

	if @iv_request_log_id = 0
	set @iv_request_log_id = null;
	
	if @policy_id = 0
	set @policy_id = null;

	insert into iv_decision_result(quote_id, iv_decision_match_type_id, policy_id, iv_request_log_id, band_pass)
	values (@quote_id,@iv_decision_match_type_id,@policy_id,@iv_request_log_id,@band_pass);

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[testimonial]'
GO
CREATE TABLE [dbo].[testimonial]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[date] [date] NOT NULL,
[product_prefix] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[testimonial] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[testimonial_footer] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ordinal] [int] NULL,
[active] [bit] NULL,
[user_last_change_id] [int] NULL,
[update_timestamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_testimonial] on [dbo].[testimonial]'
GO
ALTER TABLE [dbo].[testimonial] ADD CONSTRAINT [PK_testimonial] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[testimonialL_prc]'
GO


CREATE PROCEDURE [dbo].[testimonialL_prc] (@product_prefix varchar(3) = null, @limit int = NULL, @only_active bit = 1, @product_id int = null)
AS
BEGIN
SET NOCOUNT ON;

	if @product_id is not null
		select @product_prefix = p.prod_prefix from product p where p.id = @product_id;
	
	declare @firstOrdinal int;
	declare @lastOrdinal int;
	
	select @firstOrdinal = MIN(t.ordinal),
	@lastOrdinal = MAX(t.ordinal)
	from testimonial t
	where t.product_prefix=@product_prefix;
	
	
	if @limit is null
	begin
		select *,
		case 
			when t.ordinal = @firstOrdinal
				then 'First' 
			when t.ordinal = @lastOrdinal
				then 'Last'
			else convert(varchar(5),t.ordinal) end as ordinal_description
		from testimonial t
		where t.product_prefix=@product_prefix
		and (
			(@only_active = 1 and t.active = 1)
			or
			@only_active = 0
			)
		order by ordinal;
	end
	else
	begin
		select top(@limit) *,
		case 
			when t.ordinal = @firstOrdinal
				then 'First' 
			when t.ordinal = @lastOrdinal
				then 'Last'
			else convert(varchar(5),t.ordinal) end as ordinal_description
		from testimonial t
		where t.product_prefix=@product_prefix
		and (
			(@only_active = 1 and t.active = 1)
			or
			@only_active = 0
			)
		order by ordinal;
	end;

SET NOCOUNT OFF;
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[underwriting_checkL_prc]'
GO


CREATE PROCEDURE [dbo].[underwriting_checkL_prc](@quote_id INT)
AS

SET NOCOUNT ON

SELECT quote_id, id, animal_type_id, owned, kept_at_home, not_breeding, not_working, not_racing, not_hunting, cover_id
FROM quote_pet
WHERE quote_id = @quote_id
ORDER BY id

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_billing_addressS_prc]'
GO


CREATE PROCEDURE [dbo].[payment_billing_addressS_prc]  (@quote_id INT)
AS

SET NOCOUNT ON


	SELECT	dbo.quote_address_format(id) as address,
			postcode 
	FROM	quote
	WHERE	id = @quote_id


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[schedule_blobS_prc]'
GO
CREATE PROCEDURE [dbo].[schedule_blobS_prc] @id int
AS
BEGIN
	SELECT schedule_doc from policy_schedule where id = @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_product]'
GO
CREATE TABLE [dbo].[renewal_product]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[old_product_id] [int] NULL,
[new_product_id] [int] NULL,
[old_product_prefix] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[new_product_prefix] [char] (3) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_renewal_product] on [dbo].[renewal_product]'
GO
ALTER TABLE [dbo].[renewal_product] ADD CONSTRAINT [PK_renewal_product] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix]'
GO
CREATE TABLE [dbo].[pricing_matrix]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group_code] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[effective_date] [date] NOT NULL,
[inactive_date] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix] on [dbo].[pricing_matrix]'
GO
ALTER TABLE [dbo].[pricing_matrix] ADD CONSTRAINT [PK_pricing_matrix] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_factor]'
GO
CREATE TABLE [dbo].[pricing_factor]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NULL,
[cover_id] [int] NULL,
[animal_type_id] [int] NULL,
[factor_effective_date] [date] NOT NULL,
[factor_inactive_date] [date] NULL,
[description] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[metric] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[factor] [decimal] (5, 4) NOT NULL CONSTRAINT [DF_pricing_factor_factor] DEFAULT ((1)),
[applies_to] [varchar] (2) COLLATE Latin1_General_CI_AS NULL,
[apply_on_override] [bit] NOT NULL CONSTRAINT [DF_pricing_factor_apply_on_override] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_factor] on [dbo].[pricing_factor]'
GO
ALTER TABLE [dbo].[pricing_factor] ADD CONSTRAINT [PK_pricing_factor] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_pricing_factor]'
GO

CREATE FUNCTION [dbo].[get_pricing_factor](
	@start_date date,
	@applies_to varchar(2),			-- null: All, 'NB': NB only, 'R': Renewal only
	@product_id int,
	@cover_id int = null,
	@animal_type_id int = null,
	@sex char(1) = null,
	@age varchar(2),
	@area_rating int,
	@breed_group char(1),
	@neutered bit = null,
	@premium_effective_date date
) 
RETURNS decimal(5,4)
AS 
BEGIN
	declare @result as decimal(5,4);
	select @result = 1;

	select @result = @result * pf.factor
	from pricing_factor pf
	where pf.product_id = @product_id
		and (pf.cover_id is null or (pf.cover_id is not null and pf.cover_id = @cover_id))
		and (pf.animal_type_id is null or (pf.animal_type_id is not null and pf.animal_type_id = @animal_type_id))
		and @start_date between pf.factor_effective_date and ISNULL(pf.factor_inactive_date, @start_date)
		and ((isnull(@applies_to,'') in ('', 'NB', 'M') and isnull(pf.applies_to,'') in('', 'NB', 'M')) or
			 (isnull(@applies_to,'') in ('', 'R')  and isnull(pf.applies_to,'') in('', 'R')))
		and ( (	 @premium_effective_date = pf.factor_effective_date
				and ((pf.description = 'sex' and pf.metric = @sex) or
				     (pf.description = 'neutered' and pf.metric = convert(varchar(10), @neutered)) or
				     (pf.apply_on_override = 1 and 
						((pf.description = 'inflation' and pf.metric = 'value') or
						(pf.description = 'age' and pf.metric = @age) or
						(pf.description = 'area_rating' and pf.metric = convert(varchar(10), @area_rating)) or
						(pf.description = 'breed_group_code' and pf.metric = @breed_group)
				     ))))

				or ( @premium_effective_date < pf.factor_effective_date
					and ((pf.description = 'sex' and pf.metric = @sex) or
						 (pf.description = 'neutered' and pf.metric = convert(varchar(10), @neutered)) or
						 (pf.description = 'inflation' and pf.metric = 'value') or
						 (pf.description = 'age' and pf.metric = @age) or
						 (pf.description = 'area_rating' and pf.metric = convert(varchar(10), @area_rating)) or
						 (pf.description = 'breed_group_code' and pf.metric = @breed_group)
						 )))
		and not (isnull(@applies_to,'') in('', 'M') and pf.factor = 0)
 
	return isnull(@result, 1);

END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_breed_group_mapping]'
GO
CREATE TABLE [dbo].[product_breed_group_mapping]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[animal_type_id] [int] NOT NULL,
[effective_date] [date] NOT NULL,
[lookup_product_id] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_breed_group_lookup] on [dbo].[product_breed_group_mapping]'
GO
ALTER TABLE [dbo].[product_breed_group_mapping] ADD CONSTRAINT [PK_product_breed_group_lookup] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_breed_rating]'
GO


CREATE FUNCTION [dbo].[get_breed_rating](
	@product_id int,
	@animal_type_id int,
	@breed_id int,
	@date date
) 
RETURNS int
AS 
BEGIN
	declare @breed_rating as int;
	declare @lookup_product_id as int;

	select top 1 @lookup_product_id = pbgm.lookup_product_id
	from product_breed_group_mapping pbgm
	where pbgm.product_id = @product_id
		and pbgm.animal_type_id = @animal_type_id
		and pbgm.effective_date <= @date
	order by effective_date desc;
	
	select @breed_rating = rating
	from product_breed_score
	where breed_id = @breed_id and product_id = @lookup_product_id;

	return @breed_rating;
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_breed_group]'
GO


CREATE FUNCTION [dbo].[get_breed_group](
	@product_id int,
	@animal_type_id int,
	@breed_id int,
	@date date
) 
RETURNS char(1)
AS 
BEGIN
	declare @breed_group as char(1);
	declare @lookup_product_id as int;

	select top 1 @lookup_product_id = pbgm.lookup_product_id
	from product_breed_group_mapping pbgm
	where pbgm.product_id = @product_id
		and pbgm.animal_type_id = @animal_type_id
		and pbgm.effective_date <= @date
	order by effective_date desc;
	
	select @breed_group = rating
	from product_breed_group
	where breed_id = @breed_id and product_id = @lookup_product_id;

	return @breed_group;
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_area_rating_mapping]'
GO
CREATE TABLE [dbo].[product_area_rating_mapping]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[effective_date] [date] NOT NULL,
[lookup_product_id] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_product_area_rating_mapping] on [dbo].[product_area_rating_mapping]'
GO
ALTER TABLE [dbo].[product_area_rating_mapping] ADD CONSTRAINT [PK_product_area_rating_mapping] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_area_rating]'
GO


CREATE FUNCTION [dbo].[get_area_rating](
	@product_id int,
	@postcode varchar(12),
	@date date
) 
RETURNS int
AS 
BEGIN
	IF @postcode LIKE 'BFPO%'
		return NULL;
	
	/*******NO QUOTE VSR POSTCODE RATED POLICIES**********/
	/*************FOR CHANNEL ISLANDS*********************/
	IF LEFT(@postcode, 2) IN ('GY','JE','IM') and @product_id in (11)
		return NULL;


	declare @area_rating as int;
	declare @lookup_product_id as int;

	select top 1 @lookup_product_id = parm.lookup_product_id
	from product_area_rating_mapping parm
	where parm.product_id = @product_id
		and parm.effective_date <= @date
	order by parm.effective_date desc;
	
	
	SELECT @area_rating = rating
	FROM	postcode p
	JOIN	product_postcode_score pps
		ON	pps.postcode_id = p.id
	WHERE	p.description = LEFT(@postcode, 2) 
	AND pps.product_id = @lookup_product_id;

	IF (@area_rating IS NULL)
		SELECT	@area_rating = rating
		FROM	postcode p
		JOIN	product_postcode_score pps
			ON	pps.postcode_id = p.id
		WHERE	p.description = LEFT(@postcode, 1) 
		AND pps.product_id = @lookup_product_id;
		
	return @area_rating;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quick_quote_calculateU_prc]'
GO
CREATE PROCEDURE [dbo].[quick_quote_calculateU_prc]  (@quote_pet_id int,@pet_count int, @promo_code nvarchar(10) = NULL, @is_aggregator tinyint = 0, @sel_cover_only bit = 0)
AS

/*
//@min_cover_ordinal describes the minimal cover level to show - this is new for green 
//for aggregator quotes so we don't show lower levels of cover than the one pre-selected
*/

SET NOCOUNT ON
declare @postcode varchar(12),
		@animal_type_id int,
		@breed_id int,
		@product_id int,
		@pet_DOB smalldatetime,
		@area_rating int,
		@breed_rating int,
		@age_rating	varchar(16),
		@sex char(1),
		@clinic_id int,
		@name varchar(32),
		@cover_id int,
		@policy_start_date smalldatetime,
		@min_cover_ordinal int,
		@breed_group char(1),
		@neutered bit,
		@source varchar(64)

declare @offer_discount decimal(5,2), @multipet_discount decimal(5,2), @promo_discount decimal(5,2), @quote_renewable bit = 0;

if(@is_aggregator=0)
BEGIN
	SELECT	@product_id		= product_id
			,@animal_type_id	= animal_type_id
			,@breed_id		= breed_id
			,@postcode		= postcode
			,@clinic_id	= VFV_clinicID
			,@name			= pet_name
			,@cover_id		= qp.cover_id
			--,@pet_age_at_policy_start_date = convert(float, DATEDIFF(mm, pet_DOB, policy_start_date))/12
			,@age_rating = case 
				when DATEADD(yy,8,pet_DOB) < policy_start_date and not @product_id in (29,30) then 'O8' 
				when DATEADD(yy,6,pet_DOB) < policy_start_date and @product_id in (29,30) then 'O6' 
				when DATEADD(yy,6,pet_DOB) >= policy_start_date and @product_id in (29,30) then 'U6' 				
				else 'U8' end
			,@policy_start_date = policy_start_date
			,@min_cover_ordinal = ISNULL(q.min_cover_ordinal,1)
			,@pet_DOB = qp.pet_DOB
			,@sex = CASE WHEN (qp.product_gender_id = 1) THEN 'M' ELSE 'F' END
			,@neutered = qp.neutered
	FROM	quote q join quote_pet qp on q.id = qp.quote_id
	WHERE	qp.id = @quote_pet_id
END
ELSE IF (@is_aggregator > 1)
BEGIN
	-- MTAs needs to stay with the current underwriter until renewal.  
	-- If the current cover is still active UW change has not occured and policy start date (for purposes of rate calc) will be today's date
	-- If the current cover has expired 
	
	DECLARE @curr_date DATE = GETDATE()
	
	-- for testing we can emulate a future date here....
	-- SELECT @curr_date = '2013-10-13'
	
	SELECT @quote_renewable = 1; -- to enable MTA quotes for discontinued products
	
	SELECT 
		@policy_start_date =
			CASE
				WHEN c.cover_inactive_date IS NULL THEN @curr_date														-- STILL ACTIVE
				WHEN c.cover_inactive_date > @curr_date THEN @curr_date													-- STILL ACTIVE
				WHEN c.underwriter <> ISNULL(nc.underwriter,c.underwriter) THEN DATEADD(day,-1,c.cover_inactive_date)	-- CHANGE IN UW (ROLL BACK DATE)
				ELSE @curr_date																							-- SAME UW
			END,
		@cover_id = case when @sel_cover_only = 1 then p.cover_id else null end
	FROM uispetmis..policy p
		JOIN cover c on c.id = p.cover_id 		
		LEFT JOIN renewal_cover rc on rc.old_cover_id = p.cover_id
		LEFT JOIN cover nc on rc.new_cover_id = nc.id
	WHERE p.policyid = @quote_pet_id

	--this is an upgrade or downgrade.
	--the quote_pet_id is the policy id - sorry!
	SELECT	top 1 @product_id = ISNULL(rp.new_product_id, p.product_id)
			,@animal_type_id = p.animal_type_id
			,@breed_id = p.breed_id
			,@postcode = p.postcode
			,@name = AnimalPetName
			--,@age_rating = case when DATEADD(yy,8,AnimalDateOfBirth) < StartDate then 'O8' else 'U8' end
			,@age_rating = case 
				when DATEADD(yy,8,AnimalDateOfBirth) < startdate and not product_id in (29,30) then 'O8' 
				when DATEADD(yy,6,AnimalDateOfBirth) < startdate and product_id in (29,30) then 'O6' 
				when DATEADD(yy,6,AnimalDateOfBirth) >= startdate and product_id in (29,30) then 'U6' 				
				else 'U8' end
			,@min_cover_ordinal = 1
			,@offer_discount = prm.offer_discount
			,@multipet_discount = prm.multipet_discount
			,@promo_discount = prm.promo_discount
			,@clinic_id	= clinic_id
			--,@policy_start_date = CURRENT_TIMESTAMP
			,@pet_DOB = p.AnimalDateOfBirth
			,@sex = CASE WHEN (p.animalsexname = 'Male') THEN 'M' ELSE 'F' END
			,@neutered = p.neutered
	FROM uispetmis..policy p 
		join uispetmis..PREMIUM_PLUS_IMPORTED prm on p.policyid = prm.policy_id
		left join uispet..renewal_product rp on rp.old_product_id = p.product_id
	WHERE p.policyid = @quote_pet_id
		and prm.renewal < 3
	order by prm.effective_date desc
	
	
	
END
ELSE
BEGIN

-- rating based on the pet age for LAG - BEGIN
	declare @aggregatorId int = 0;
	declare @productId int = 0;
	declare @animal_type char = '';
	declare @animalDob date = null;

	select @aggregatorId = qp.aggregator_id
	FROM	aggregator_pet qp 
	WHERE	qp.id = @quote_pet_id ;

	select top 1 
			@productId = p.id,
			@animal_type = animal_type ,
			@animalDob = qp.pet_DOB,
			@policy_start_date = q.policy_start_date
	FROM	aggregator q join aggregator_pet qp on q.id = qp.aggregator_id
				join product p on p.prod_prefix = q.affinity
	WHERE	qp.aggregator_id = @aggregatorId
	order by qp.id asc;

	DECLARE @petage INT = DATEDIFF(HOUR, @animalDob,@policy_start_date)/8766;

	if (@productId = 31)
	begin

		select @promo_code = 'RATING' + @animal_type + convert(varchar, @petage);

		update aggregator set promo_code = @promo_code
		where id = @aggregatorId;

	end
-- rating based on the pet age for LAG - END

	SELECT	@product_id	= p.id
			,@animal_type_id = case animal_type when 'D' then 1 else '2' end
			,@breed_id = breed_id
			,@postcode = q.postcode
			,@policy_start_date = q.policy_start_date
			,@name = pet_name
			--,@pet_age_at_policy_start_date = convert(float, DATEDIFF(mm, pet_DOB, policy_start_date))/12
			,@age_rating = case 
				when DATEADD(yy,8,qp.pet_DOB) < q.policy_start_date and not p.id in (30) then 'O8' 
				when DATEADD(yy,6,qp.pet_DOB) < q.policy_start_date and p.id in (30) then 'O6' 
				when DATEADD(yy,6,qp.pet_DOB) >= q.policy_start_date and p.id in (30) then 'U6' 				
				else 'U8' end
			,@min_cover_ordinal = 1 
			,@pet_DOB = qp.pet_DOB
			,@promo_code = q.promo_code
			,@sex = qp.sex
			,@neutered = qp.neutered
			,@source = q.[source]
	FROM	aggregator q join aggregator_pet qp on q.id = qp.aggregator_id
				join product p on p.prod_prefix = q.affinity
	WHERE	qp.id = @quote_pet_id
	
	--stop MSM getting LAG quotes until further notice
	--if(@source = 'MONEYSUP' and @product_id = 31)
		--return;		
END

-- CS: 21.06.2013 now this is done inside each quick_quote_calculate_XYZ_U_prc sub procedure
--create a table variable with all the discounts for this product and the supplied promo code
/*declare @promo table (cover_id int, discount decimal(5,2), staff bit)
insert into @promo
select cd.cover_id, discount, staff
from cover_discount cd 
	join discount d on d.id = cd.discount_id
where d.code = @promo_code
AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))*/


	--//////////////////////////////
	--/////////// BREED ////////////
	--//////////////////////////////
	IF @product_id in (3,13,14,1,9,11,6,22)
		SELECT @breed_rating = dbo.get_breed_rating(@product_id, @animal_type_id, @breed_id, @policy_start_date);
	ELSE
		SELECT @breed_group = dbo.get_breed_group(@product_id, @animal_type_id, @breed_id, @policy_start_date);


	--//////////////////////////////
	--/////////// AREA  ////////////
	--//////////////////////////////
	select @area_rating = dbo.get_area_rating(@product_id, @postcode, @policy_start_date);
	
	/*********POSTCODES NOT LIABLE TO IPT ****************/
	/*****************************************************/
	declare @ipt_factor float;
	if(LEFT(@postcode, 2) IN ('GY','JE','IM') and @policy_start_date >= '2012-04-01')
		select @ipt_factor = 1/1.06;
	else
		select @ipt_factor = 1;	
	/*****************************************************/
	/*****************************************************/

	--------------------------------------------------
	-- TVIS brands
	--------------------------------------------------
	IF @product_id = 3 --village vet
	BEGIN
		exec quick_quote_calculate_VFV_U_prc	@clinic_id = @clinic_id,
												@product_id = @product_id,
												@quote_pet_id = @quote_pet_id,
												@pet_count = @pet_count,
												@promo_code = @promo_code,
												@policy_start_date = @policy_start_date,
												@name = @name,
												@pet_DOB = @pet_DOB,
												@animal_type_id = @animal_type_id,
												@cover_id = @cover_id,
												@offer_discount = @offer_discount,
												@multipet_discount = @multipet_discount,
												@promo_discount = @promo_discount,
												@ipt_factor = @ipt_factor,
												@age_rating = @age_rating,
												@breed_rating = @breed_rating,
												@sel_cover_only = @sel_cover_only;
	END
	
	--ELSE 
	--IF @product_id = 6	--pet doctors 
	--BEGIN
	--	exec quick_quote_calculate_PDP_U_prc	@clinic_id = @clinic_id,
	--											@product_id = @product_id,
	--											@quote_pet_id = @quote_pet_id,
	--											@pet_count = @pet_count,
	--											@promo_code = @promo_code,
	--											@policy_start_date = @policy_start_date,
	--											@name = @name,
	--											@pet_DOB = @pet_DOB,
	--											@animal_type_id = @animal_type_id,
	--											@cover_id = @cover_id,
	--											@ipt_factor = @ipt_factor,
	--											@age_rating = @age_rating,
	--											@breed_rating = @breed_rating,
	--											@sel_cover_only = @sel_cover_only,
	--											@quote_renewable = @quote_renewable; 
	--END
	
	--ELSE 
	IF @product_id = 11	--vetsure 
	BEGIN
		exec quick_quote_calculate_VSR_U_prc	@clinic_id = @clinic_id,
												@product_id = @product_id,
												@quote_pet_id = @quote_pet_id,
												@pet_count = @pet_count,
												@promo_code = @promo_code,
												@policy_start_date = @policy_start_date,
												@name = @name,
												@pet_DOB = @pet_DOB,
												@animal_type_id = @animal_type_id,
												@cover_id = @cover_id,
												@offer_discount = @offer_discount,
												@multipet_discount = @multipet_discount,
												@promo_discount = @promo_discount,
												@ipt_factor = @ipt_factor,
												@age_rating = @age_rating,
												@breed_rating = @breed_rating,
												@area_rating = @area_rating,
												@min_cover_ordinal = @min_cover_ordinal,
												@sel_cover_only = @sel_cover_only;
	END
	
	ELSE IF @product_id = 22	--donaldson
	BEGIN
		exec quick_quote_calculate_DON_U_prc	@product_id = @product_id,
												@quote_pet_id = @quote_pet_id,
												@pet_count = @pet_count,
												@promo_code = @promo_code,
												@policy_start_date = @policy_start_date,
												@name = @name,
												@pet_DOB = @pet_DOB,
												@animal_type_id = @animal_type_id,
												@cover_id = @cover_id,
												@offer_discount = @offer_discount,
												@multipet_discount = @multipet_discount,
												@promo_discount = @promo_discount,
												@ipt_factor = @ipt_factor,
												@age_rating = @age_rating,
												@breed_rating = @breed_rating,
												@min_cover_ordinal = @min_cover_ordinal,
												@sel_cover_only = @sel_cover_only;
	END
	
	ELSE IF @product_id in (13,14)	--bfg vsa
	BEGIN		
		exec quick_quote_calculate_BFG_U_prc 	@clinic_id = @clinic_id,
												@product_id = @product_id,
												@quote_pet_id = @quote_pet_id,
												@pet_count = @pet_count,
												@promo_code = @promo_code,
												@policy_start_date = @policy_start_date,
												@name = @name,
												@pet_DOB = @pet_DOB,
												@animal_type_id = @animal_type_id,
												@cover_id = @cover_id,
												@offer_discount = @offer_discount,
												@multipet_discount = @multipet_discount,
												@promo_discount = @promo_discount,
												@ipt_factor = @ipt_factor,
												@age_rating = @age_rating,
												@breed_rating = @breed_rating,
												@sel_cover_only = @sel_cover_only;
	END

	--------------------------------------------------
	-- Inactive/Old brands
	--------------------------------------------------
	--ELSE IF @product_id IN (28)	--KFI
	--BEGIN		
	--	exec quick_quote_calculate_KFI_U_prc	@product_id = @product_id,
	--											@quote_pet_id = @quote_pet_id,
	--											@pet_count = @pet_count,
	--											@promo_code = @promo_code,
	--											@policy_start_date = @policy_start_date,
	--											@name = @name,
	--											@pet_DOB = @pet_DOB,
	--											@animal_type_id = @animal_type_id,
	--											@cover_id = @cover_id,
	--											@offer_discount = @offer_discount,
	--											@multipet_discount = @multipet_discount,
	--											@promo_discount = @promo_discount,
	--											@ipt_factor = @ipt_factor,
	--											@age_rating = @age_rating,
	--											@breed_group = @breed_group,
	--											@area_rating = @area_rating,
	--											@min_cover_ordinal = @min_cover_ordinal,
	--											@sel_cover_only = @sel_cover_only;
	--END
	
	--------------------------------------------------
	-- Other brands
	--------------------------------------------------
	ELSE
	BEGIN
		-- Table variable for the quotes
		declare @quotes table (
			pet varchar(32),
			pet_id int,
			animal_type_id int,
			cover_id int,
			premium decimal(10,2),
			selected bit,
			promo_discount decimal(5,2),
			offer_discount decimal(5,2),
			multipet_discount decimal(5,2),
			description varchar(256),
			product_id int,
			coinsurance float,
			excess float
		);

		-- Choose the correct value for @applies_to flag
		declare @applies_to varchar(2);
		if @is_aggregator <= 1
			select @applies_to = 'NB';
		else
			select @applies_to = 'M';
	
		-- Create a table variable with all the discounts for this product and the supplied promo code
		declare @promo table (cover_id int, discount decimal(5,2), staff bit)
		insert into @promo
		select cd.cover_id, discount, staff
		from cover_discount cd 
			join discount d on d.id = cd.discount_id
		where d.code = @promo_code
			AND getdate() BETWEEN ISNULL(d.start_date, DATEADD(d, -1, getdate())) AND ISNULL(d.end_date, DATEADD(d, 1, getdate()))

		/* INTRODUCTORY DISCOUNT */
		declare @coverIntroductoryDiscount table(discount decimal(10,2), cover_id int);
		--check if its not renewal
		if not exists(select * from uispetmis..POLICY p where p.quote_pet_id = @quote_pet_id)
		begin
			insert into @coverIntroductoryDiscount (discount, cover_id)
			select discount, cover_id
			from uispet..get_introductory_discount_by_product(GETDATE(), @product_id);
		end
							
		-- Calculate the quotes
		insert into @quotes(pet,pet_id,animal_type_id,cover_id,premium,selected,promo_discount,offer_discount,multipet_discount,description,product_id,coinsurance,excess)
		SELECT
			@name pet
			,@quote_pet_id as pet_id
			,@animal_type_id as animal_type_id
			,c.id as cover_id
			,convert(decimal(10,2),
				value 
					* case p.staff when 1 then 1 else ((100-ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)))/100) end --offer
					* case p.staff when 1 then 1 else case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end end --multipet
					* ISNULL((100-ISNULL(@promo_discount, p.discount)),100) /100 --promotional rate
					* @ipt_factor
					* dbo.get_pricing_factor(@policy_start_date, @applies_to, c.product_id, c.id, @animal_type_id, @sex, @age_rating, @area_rating, @breed_group, @neutered, u.effective_date)
					)
			as premium
			,case when u.cover_id = @cover_id then 1 else 0 end selected
			,ISNULL(ISNULL(@promo_discount, p.discount),0) as promo_discount
			,case p.staff when 1 then 0 else ISNULL(ISNULL(@offer_discount, ISNULL(introDiscount.discount,0)), 0) end as offer_discount
			,case p.staff when 1 then 0 else case @pet_count when 1 then 0 else ISNULL(ISNULL(@multipet_discount, c.multipet_discount), 0) end end as multipet_discount
			,c.[description]
			,@product_id product_id
			,gcae.coinsurance coinsurance
			,gcae.excess excess
		FROM cover c 
			JOIN pricing_matrix u ON u.cover_id = c.id and u.id = ( select top 1 id
																	from pricing_matrix pm
																	where pm.cover_id = u.cover_id
																		and pm.animal_type_id = @animal_type_id
																		and pm.age = @age_rating
																		and ISNULL(pm.breed_group_code,'') 	= ISNULL(@breed_group,'')
																		and	area_rating	= @area_rating
																		and pm.effective_date <= @policy_start_date
																	order by pm.effective_date desc, id desc)
			LEFT JOIN	@promo p on p.cover_id = c.id
			CROSS APPLY get_coinsurance_and_excess(c.affinity_code, @pet_DOB, @policy_start_date) gcae
			left join @coverIntroductoryDiscount introDiscount on c.id = introDiscount.cover_id
		WHERE (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
			AND	c.product_id = @product_id
			and c.ordinal >= @min_cover_ordinal
			AND ((c.animal_type_id = @animal_type_id AND EXISTS(SELECT * FROM dbo.cover WHERE product_id = @product_id AND @policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01') and animal_type_id IS NOT NULL)) OR c.animal_type_id IS NULL)
			and (@sel_cover_only = 0 or (@sel_cover_only = 1 and c.id=@cover_id))
		order by ordinal
		
		-- If any price value is equal to 0, then return empty result set
		if exists(select 1 from @quotes where isnull(premium,0) = 0)
			delete from @quotes;
			
		-- Select the quotes
		select * from @quotes;
	END 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_calculateU_prc]'
GO


CREATE PROCEDURE [dbo].[quote_calculateU_prc]  (@quote_id INT) 
AS

SET NOCOUNT ON

DECLARE @pet_count int
SELECT @pet_count = COUNT(id) FROM quote_pet WHERE quote_id = @quote_id
declare @promo_code nvarchar(10)
select @promo_code = promo_code from quote where id = @quote_id

DECLARE CURSOR1 CURSOR FOR
SELECT id FROM quote_pet WHERE quote_id = @quote_id

DECLARE @id int 

OPEN CURSOR1 
FETCH NEXT FROM  CURSOR1 INTO  @id
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
	IF (@@FETCH_STATUS <> - 2)

	exec quick_quote_calculateU_prc @id, @pet_count, @promo_code

	FETCH NEXT FROM CURSOR1 INTO @id
END 
CLOSE CURSOR1 
DEALLOCATE CURSOR1

SET NOCOUNT OFF


PRINT 'completed build of quote_calculateU_prc'


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_calculate_allU_prc]'
GO


CREATE PROCEDURE [dbo].[quote_calculate_allU_prc] (@quote_id int)
AS

SET NOCOUNT ON
	declare @result table(
		pet varchar(50),
		pet_id int,
		animal_type_id int,
		cover_id int,
		premium	float,
		selected bit,
		promo_discount float,
		offer_discount float,
		multipet_discount float,
		description varchar(1000),
		product_id int,
		coinsurance decimal(5,2),
		excess float
	);

	insert into @result
	exec quote_calculateU_prc @quote_id

	select r.*, c.ordinal
	from @result r join cover c on r.cover_id=c.id
	order by c.ordinal, r.cover_id, pet_id;
	
	
SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[housekeeping_prc]'
GO

CREATE PROCEDURE [dbo].[housekeeping_prc]
AS
BEGIN

declare @id int

--begin tran
--leads that have been converted but nobody clicked on the button
WHILE EXISTS (SELECT 1 FROM lead where lead_status_id <> 103 and quote_id in (select id from quote where quote_status_id=3))
BEGIN
	select @id = MIN(id) from lead where lead_status_id <> 103 and quote_id in (select id from quote where quote_status_id=3)
	exec leadU_prc @id=@id, @user_id=0, @lead_status_id=103
END

--leads that have lapsed and need to be auto-failed
WHILE EXISTS (SELECT 1 from lead l join quote q on l.quote_id = q.id and q.quote_status_id = 4 and l.lead_status_id = 102)
BEGIN
	select @id = MIN(l.id) from lead l join quote q on l.quote_id = q.id and q.quote_status_id = 4 and l.lead_status_id = 102
	update lead set lead_failure_reason_id = 202 where id = @id
	exec leadU_prc @id=@id, @user_id=0, @lead_status_id=104
END

--auto leads that are over 7 days older to be archived
update lead 
set lead_status_id = 106
where ISNULL(source,'') = 'gocomp' and lead_status_id = 100 and contact_date <= (getdate()-7)

--expired quotes
update quote 
set quote_status_id = 4 
where DATEDIFF(d, create_timestamp, getdate()) > 30
and quote_status_id in (1,2)
--rollback tran


/**************** DASHBOARD UPDATES ****************/

/***UPDATE THE BOOK ON NEW PREMIUM RECORDS ****/
update uispetmis..PREMIUM
set book = year(uispetmis.dbo.start_date_for_premium_id(id))	--YEAR(startdate)
FROM uispetmis..PREMIUM pr 
	join uispetmis..POLICY p on p.policyid = pr.policy_id
WHERE ISNULL(pr.book,0) != year(uispetmis.dbo.start_date_for_premium_id(id))--IS NULL
--AND renewal in (0,2)

--update uispetmis..premium
--set book = year(p.startdate)
--from uispetmis..premium pr 
--	join uispetmis..policy p on p.policyid = pr.policy_id
--where year(effective_date) != book
--and year(p.startdate) != book

update uispetmis..charge 
set premium_id = uispetmis.dbo.get_premium_id_by_date(policy_id, datetimestamp)
where premium_id is null

--************************ SETTING PREMIUM_ID ON PAYMENTS FOR FINANCIAL REPORTING ****************************
-- wipe out premium ids linking to non existent premium records from payment table
update
uispetmis..payment set premium_id = NULL
from uispetmis..payment where premium_id not in (select id from uispetmis..premium)
-- update payments with null premium ids link to premium record

--DTP-108 criteria. For declined renewals, we need to call [get_premium_id_by_date_IncludingDeclinedRenewals]
--function which will include declined renewal premiums
UPDATE uispetmis..payment
SET premium_id = uispetmis.dbo.[get_premium_id_by_date_IncludingDeclinedRenewals](pay.policy_id, collection_date)
FROM uispetmis..payment pay
JOIN uispetmis..policy pol on pay.policy_id = pol.PolicyID
JOIN  
	(SELECT MAX(id) 'MaxId', policy_id, renewal
	FROM uispetmis..PREMIUM 
	GROUP BY policy_id, renewal) AS MaxPrem ON pol.PolicyID = MaxPrem.policy_id
WHERE premium_id is null
AND amount < 0
AND pay.collection_date >= pol.RenewalDate
AND pay.collection_date > '2016-01-01'
AND MaxPrem.Renewal = 3 --declined renewal

--For remaining null premiums and non-declined renewals, get_premium_id_by_date
--function will exlude declined renewal premiums. 
UPDATE uispetmis..payment
SET premium_id = uispetmis.dbo.get_premium_id_by_date(policy_id, collection_date)
WHERE premium_id is null
--************************ FIX FOR ANNUAL PAYERS PAYING IN ADVANCE OF RENEWAL ****************************

--update uispetmis..payment 
--set premium_id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,6,collection_date))
--from uispetmis..payment pay
--	join uispetmis..payment_detail pd on pay.policy_id = pd.policy_id	
--	join uispetmis..PREMIUM prm on prm.id = pay.premium_id
--where payment_type_id = 1
--and DATEADD(MONTH, 6, effective_date) < datetimestamp
--and pay.amount > 0
--and payment_type in ('C','Q')
--and uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,6,collection_date)) != pay.premium_id

update uispetmis..payment 
set premium_id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
from uispetmis..payment pay
	join uispetmis..payment_detail pd on pay.policy_id = pd.policy_id	
	join uispetmis..PREMIUM prm1 on prm1.id = pay.premium_id
	join uispet..cover c1 on prm1.cover_id = c1.id
	join uispetmis..PREMIUM prm2 on prm2.id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
	join uispet..cover c2 on prm2.cover_id = c2.id
where payment_type_id = 1
and pay.amount > 0
and payment_type in ('C','Q')
and uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date)) != pay.premium_id
and (c1.underwriter != c2.underwriter or prm1.start_date != prm2.start_date)	--added check for payment report period dates 2014-09-02
and pay.collection_date >= DATEADD(DAY, -3, CONVERT(date, GETDATE())) --ADDED SO NOT TO OVERWRITE MANUAL CORRECTIONS

--ALSO CHECK FOR MISC CREDITS (WHERE A SINGLE PAYMENT FOR MULTIPLE POLICIES IS RE-ALLOCATED)
update uispetmis..payment 
set premium_id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
from uispetmis..payment pay
	join uispetmis..payment_detail pd on pay.policy_id = pd.policy_id	
	join uispetmis..PREMIUM prm1 on prm1.id = pay.premium_id
	join uispet..cover c1 on prm1.cover_id = c1.id
	join uispetmis..PREMIUM prm2 on prm2.id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
	join uispet..cover c2 on prm2.cover_id = c2.id
where payment_type_id = 1
and payment_type in ('M')
and uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date)) != pay.premium_id
--and c1.underwriter != c2.underwriter /**** removed underwriter check - needed for all payments in case of cancellation ***/
and exists (select 1 from uispetmis..PAYMENT where payment_type = 'M' and collection_date = pay.collection_date
and amount = -pay.amount)
and pay.collection_date >= DATEADD(DAY, -3, CONVERT(date, GETDATE())) --ADDED SO NOT TO OVERWRITE MANUAL CORRECTIONS

--ALSO CHECK ADVANCE CARD PAYMENTS MADE BY NEW DD PAYERS WHO HAVE SWITCHED FROM ANNUAL (FEW)

update uispetmis..payment 
set premium_id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1, collection_date))
from uispetmis..payment pay
	join uispetmis..payment_detail pd on pay.policy_id = pd.policy_id	
	join uispetmis..PREMIUM prm1 on prm1.id = pay.premium_id
	join uispet..cover c1 on prm1.cover_id = c1.id
	join uispetmis..PREMIUM prm2 on prm2.id = uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
	join uispet..cover c2 on prm2.cover_id = c2.id
where payment_type_id = 4
and pay.amount = uispetmis.dbo.get_premium_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date))
and payment_type in ('C','Q')
and uispetmis.dbo.get_premium_id_by_date(pay.policy_id, DATEADD(MONTH,1,collection_date)) != pay.premium_id
and c1.underwriter != c2.underwriter

--************************ END FIX FOR ANNUAL PAYERS PAYING IN ADVANCE OF RENEWAL ****************************

--************************ MAKE SURE RETURNED DDs OR BACS REFUNDS HAVE SAME UNDERWRITER AS ORIGINAL ****************************
update p2 set premium_id = p1.premium_id
	--SELECT p2.payment_id, p1.payment_id, p2.* 
	FROM uispetmis..payment p1
		JOIN uispetmis..payment p2 on p1.policy_id = p2.policy_id
	join uispetmis..PREMIUM prm1 on prm1.id = p1.premium_id
	join uispet..cover c1 on prm1.cover_id = c1.id
	join uispetmis..PREMIUM prm2 on prm2.id = p2.premium_id
	join uispet..cover c2 on prm2.cover_id = c2.id
	where p1.payment_type in ('D')

			and p1.amount = -p2.amount 
			and p2.datetimestamp between p1.datetimestamp and DATEADD(day, 7, p1.datetimestamp)
			and p1.amount > 0
			and p1.premium_id != p2.premium_id
		and c1.underwriter != c2.underwriter
--************************ END OF MAKE SURE RETURNED DDs OR BACS REFUNDS HAVE SAME UNDERWRITER AS ORIGINAL ****************************



-- Correct premium ID for any claims where treatment dates have been set / amended
update uispetmis..claim set premium_id = uispetmis.dbo.get_premium_id_by_date(policyid,[TreatmentDateFrom])
where 
	uispetmis.dbo.get_premium_id_by_date(policyid,[TreatmentDateFrom]) is not null 
	and ISNULL(premium_id,0)  <> uispetmis.dbo.get_premium_id_by_date(policyid,[TreatmentDateFrom])
	and chequedate >= DATEADD(month,-2,GetDate())

-- Correct policy affinity code for any claims where treatment dates have been set / amended
update uispetmis..claim set policy_affinity_code = cov.affinity_code 
from
 uispetmis..claim c
join uispetmis..premium pre on c.premium_id = pre.id 
join uispet..cover cov on pre.cover_id = cov.id 
where c.policy_affinity_code is null or (c.policy_affinity_code <> cov.affinity_code COLLATE Latin1_General_CI_AS)

--and now updates to the breed group on premium table

--select c.product_id, count(1) from uispetmis..premium p
--	join uispet..cover c on p.cover_id = c.id
--	join uispetmis..POLICY pol on pol.policyid = p.policy_id
--where breed_group is null
--and animal_type_id = 1
--group by c.product_id order by 1

--------------------------------------------------
-- Breed lookup
--------------------------------------------------
update uispetmis..premium 
set breed_group = case dbo.get_breed_rating(c.product_id, p.animal_type_id, p.breed_id, prm.effective_date)
when 1 then 'H'
when 2 then 'L'
when 3 then 'M'
when 4 then 'S'
when 5 then 'X'
end
from uispetmis..premium prm
	join uispet..cover c on prm.cover_id = c.id
	join uispetmis..POLICY p on p.policyid = prm.policy_id
where c.product_id in (3,13,14,11,6,22)
and prm.breed_group IS NULL

update uispetmis..premium 
set breed_group = dbo.get_breed_group(c.product_id, p.animal_type_id, p.breed_id, prm.effective_date)
from uispetmis..premium prm
	join uispet..cover c on prm.cover_id = c.id
	join uispetmis..POLICY p on p.policyid = prm.policy_id
where c.product_id not in (3,13,14,11,6,22)
and prm.breed_group IS NULL

--and now hoover up any stragglers
update uispetmis..premium 
set breed_group = case when b.breed_category_id = 11 then 'P'
					   when b.breed_category_id = 12 then 'N'
					   else 'U' end
from uispetmis..premium prm
	join uispetmis..POLICY p on p.policyid = prm.policy_id
	join uispet..breed b on b.id = p.breed_id --and b.breed_category_id in (11, 12)
where	prm.breed_group IS NULL;

--------------------------------------------------
-- End	Breed lookup
--------------------------------------------------

--select p.breed_id, p.policynumber, b.description, prm.* from uispetmis..premium prm 
--	join uispetmis..POLICY p on p.policyid = prm.policy_id
--	join uispet..cover c on c.id = prm.cover_id
--	join uispet..breed b on b.id = p.breed_id
--where 
----c.product_id = 22 and 
--p.animal_type_id = 1 
----and breed_group = 's'
--and breed_group is null
----order by premium

exec uispetmis..premium_start_date_prc	--sets the start date in premium records (needed if mta)

--intermittent issue with converted quotes not getting the correct status applied - under investigation 2011-09-26
update quote 
set quote_status_id = 3 
from quote
where quote_status_id !=3 
and quote_ref collate SQL_Latin1_General_CP1_CI_AS in (select policynumber from uispetmis..policy)


--this is a fix for 466 policies that are a whole month in arrears
--at renewal their payday is being altered
--they have been informed by mail

--begin tran
--select pf.*, pd.payday 

update uispetmis..payday_fix
set done = 1
from uispetmis..payday_fix pf
	join uispetmis..payment_detail pd on pf.policy_id = pd.policy_id
	join uispetmis..policy p on p.policyid = pf.policy_id
where old_payday = pd.payday	--not changed
and cancelleddate is null	--not cancelled
and pf.renewal_date = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))

update uispetmis..payment_detail
set payday = pf.new_payday
from uispetmis..payday_fix pf
	join uispetmis..payment_detail pd on pf.policy_id = pd.policy_id
	join uispetmis..policy p on p.policyid = pf.policy_id
where old_payday = pd.payday	--not changed
and cancelleddate is null	--not cancelled
and pf.renewal_date = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))


--rollback tran

-- Delete email tracking entries that don't have entry in email table
delete from uispetmis..email_activity
where timestamp < DATEADD(MINUTE, -10, GETDATE())
	and not exists(select 1 from uispetmis..email where tracking_id=email_tracking_id);

	/* POPULATE THE AREA FOR NEWLY ADDED PREMIUM RECORDS */
	update uispetmis..premium set area_id = c.area_id
	from uispetmis..premium prm 
		join uispetmis..policy p on p.policyid = prm.policy_id
		join uispet..clinic c on p.clinic_id = c.id
	where prm.area_id is null

	update uispetmis..premium 
	set area_id = case when c.underwriter = 'UIC' then 5 - rating  else rating end
	from uispetmis..premium prm 
		join uispet..cover c on c.id = prm.cover_id
		join uispet..postcode p on p.description = LEFT(prm.postcode, 2) collate Latin1_General_CI_AS
		JOIN uispet..product_postcode_score pps on pps.postcode_id = p.id AND pps.product_id = case when c.underwriter = 'UIC' then 12 else 1 end
	where area_id is null

	update uispetmis..premium 
	set area_id = case when c.underwriter = 'UIC' then 5 - rating  else rating end
	from uispetmis..premium prm 
		join uispet..cover c on c.id = prm.cover_id
		join uispet..postcode p on p.description = LEFT(prm.postcode, 1) collate Latin1_General_CI_AS
		JOIN uispet..product_postcode_score pps on pps.postcode_id = p.id AND pps.product_id = case when c.underwriter = 'UIC' then 12 else 1 end
	where area_id is null

exec uispetmis..admin_fee_allocate_prc

exec uispetmis..[imported_policy_auditU_prc]

END






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_by_referenceS_prc]'
GO

CREATE PROCEDURE [dbo].[quote_by_referenceS_prc]
	@quote_ref varchar(16)
AS
BEGIN
	SELECT id from quote where quote_ref = @quote_ref
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[schedule_emailS_prc]'
GO
CREATE PROCEDURE [dbo].[schedule_emailS_prc] (@quote_pet_id int)
AS

SET NOCOUNT ON

--	DECLARE @email_subject varchar(128)
--	DECLARE @email_body	varchar(8000)
--	DECLARE @email_to	varchar(64)
--	DECLARE @email_blind_cc	varchar(64)
--	DECLARE @email_from varchar(64)
--	DECLARE	@email_header varchar(8000)
--	DECLARE	@pet_details varchar(8000)
--	DECLARE	@policy_details varchar(8000)
--	DECLARE	@cust_details	varchar(8000)
--	DECLARE	@price_details	varchar(256)
--	DECLARE	@price money
--	DECLARE	@cover_required	varchar(256)
	DECLARE @DD int
--	DECLARE @cover_id int

	BEGIN

		select @DD = ISNULL(payment_type_id ,-1)
		FROM payment_detail pd
			JOIN quote q on q.payment_detail_id = pd.id
			JOIN quote_pet qp on qp.quote_id = q.id
		WHERE qp.id = @quote_pet_id

		IF @DD is NULL
		BEGIN
			set @DD = -1
		END

		SELECT 	DD=@DD,
				cover_id = c.id,

				email_subject = case when p.id = 19 then 'Admiral Pet Insurance. Policy Reference: ' + ISNULL(q.quote_ref, 'Not Supplied')
					else 'Pet Insurance From ' + ISNULL(p.description,'') + '. Policy Reference: ' + ISNULL(q.quote_ref, 'Not Supplied') end,

				--email_subject = 'Pet Insurance From ' + ISNULL(p.description,'') + '. Policy Reference: ' + ISNULL(q.quote_ref, 'Not Supplied'),


				email_to	= pol.email_address,
				email_from = p.email,
				email_blind_cc = p.admin_email,
				price		= ISNULL(qp.premium, 0),
				cover_required = ISNULL(c.description, 'Not Supplied'),
				petid = qp.id,
				policy_id = pol.policyid
		FROM	quote q
			JOIN quote_pet qp on qp.quote_id = q.id
			JOIN UISPETMIS..policy pol on qp.id = pol.quote_pet_id
			LEFT JOIN title t ON t.id = q.title_id
			JOIN product p ON p.id = q.product_id
			JOIN cover c ON	c.id = qp.cover_id
		WHERE qp.id = @quote_pet_id			
	END 
					
--	SELECT	@email_subject as email_subject,
--		@email_body as email_body,
--		@email_to as email_to,
--		@email_from as email_from,
--		@email_blind_cc as email_blind_cc,
--		@DD as DD,
--		@cover_id as cover_id

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_payment_detailS_prc]'
GO

CREATE PROCEDURE [dbo].[quote_payment_detailS_prc]  
(       
	@quote_id			int
)

AS
SET NOCOUNT ON

OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;

SELECT 	
		qpd.id,
		qpd.quote_id,
		CONVERT(varchar, DecryptByKey(qpd.enc_account_number)) account_number,
		CONVERT(varchar, DecryptByKey(qpd.enc_account_name)) account_name,
		CONVERT(varchar, DecryptByKey(qpd.enc_sort_code)) sort_code
FROM quote_payment_detail qpd
WHERE qpd.quote_id = @quote_id;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_payment_detailU_prc]'
GO


CREATE PROCEDURE [dbo].[quote_payment_detailU_prc]  
(       @quote_id			int,
		@account_number		varchar(32)  = NULL,
		@account_name		varchar(64)  = NULL,
		@sort_code			varchar(6) =  NULL
)

AS
SET NOCOUNT ON

OPEN SYMMETRIC KEY PaymentDetailsSymmetricKey
DECRYPTION BY CERTIFICATE PaymentDetailsCertificate;


DECLARE @quotePaymentDetailId INT;
SELECT @quotePaymentDetailId = qpd.id
FROM dbo.quote_payment_detail qpd
WHERE qpd.quote_id = @quote_id;

IF @quotePaymentDetailId IS NOT NULL
BEGIN
	
	--update
	UPDATE quote_payment_detail
	SET enc_account_number = ISNULL(EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @account_number), enc_account_number)
		,enc_account_name = ISNULL(EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @account_name), enc_account_name)
		,enc_sort_code = ISNULL(EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @sort_code), enc_sort_code)
		,timestamp = GETDATE()
	WHERE id = @quotePaymentDetailId;

END
ELSE
begin
--insert
	
	INSERT INTO quote_payment_detail
	        ( quote_id ,
	          enc_account_number ,
	          enc_account_name ,
	          enc_sort_code ,
	          timestamp
	        )
	VALUES  ( @quote_id , -- quote_id - int
	          EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @account_number) , -- enc_account_number - varbinary(256)
	          EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @account_name) , -- enc_account_name - varbinary(256)
	          EncryptByKey(Key_GUID('PaymentDetailsSymmetricKey'), @sort_code) , -- enc_sort_code - varbinary(256)
	          GETDATE()  -- timestamp - datetime
	        )
	
end

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[replaceCaveats]'
GO

create function [dbo].[replaceCaveats](
	@strText VARCHAR(1000),
	@product_id int
) 
RETURNS VARCHAR(1000) 
AS 
BEGIN 
	select @strText =
		CASE WHEN @product_id in (31) THEN
				REPLACE(
					REPLACE(@strText,'***',''),
					'#','')
			ELSE
				@strText
			END;
		
	return @strText;
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefitL_prc]'
GO



CREATE PROCEDURE [dbo].[cover_benefitL_prc] (
	@product_id INT = NULL,
	@quote_id int = NULL,
	@min_cover_ordinal int = NULL,
	@replaceSups bit = 0,
	@filter_section_id int = NULL,
	@filter_parent_section_id int = NULL,
	@include_help bit = 0,
	@context char(1) = 'W'		-- W -> Q&B Website, E -> email, L -> Letter
)
AS

SET NOCOUNT ON

	declare @policy_start datetime
	
	IF @quote_id IS NOT NULL
	BEGIN
		SELECT 
			@min_cover_ordinal = 
				CASE WHEN  @min_cover_ordinal IS NOT NULL THEN @min_cover_ordinal
				ELSE q.min_cover_ordinal END,
			@policy_start = ISNULL(q.policy_start_date,CURRENT_TIMESTAMP),
			@product_id = ISNULL(@product_id, q.product_id)
		FROM 
			quote q
		WHERE q.id = @quote_id
		
		if @min_cover_ordinal is null
			SELECT @min_cover_ordinal = min(c.ordinal)
			FROM cover c
			WHERE c.product_id=@product_id and
				 (@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'));

		--check that 11 month reminders are using latest products
		IF(DATEADD(DAY, 60, @policy_start) < CURRENT_TIMESTAMP)
			select @policy_start = CURRENT_TIMESTAMP

	END
	ELSE
	BEGIN
		SELECT @min_cover_ordinal = ISNULL(@min_cover_ordinal,1), @policy_start = CURRENT_TIMESTAMP
	END		

	if(@product_id=11 AND @quote_id IS NOT NULL)
	begin
		declare @service_profile int
		select @service_profile = ISNULL(cg.service_profile,0)
		from quote q 
			join clinic c on q.vfv_clinicid = c.id
			join clinic_group cg on c.clinic_group_id = cg.id
		where q.id = @quote_id

		if(@service_profile=1)
			SELECT @min_cover_ordinal = 1
		else
			SELECT @min_cover_ordinal = 2
	end
		
	--Need to get table structure dynamically
	DECLARE @cols	VARCHAR(2000)
	DECLARE	@row1	VARCHAR(1000)
	DECLARE	@last_row	VARCHAR(1000)
	DECLARE	@table_def VARCHAR(8000) 
	DECLARE @col_count int
	DECLARE @count2 int
	DECLARE @count3 int
	DECLARE	@section_count INT
	DECLARE @values varchar(8000)
	DECLARE @sub_section_id varchar(10)
	DECLARE @section_id varchar(10)
	declare @whereClause varchar(200) = ''
	
	if @filter_section_id IS NOT NULL
	begin
		SET @whereClause = 'where section_id = '+ STR(@filter_section_id)
	end
	
	if @filter_parent_section_id IS NOT NULL
	begin
		if LEN(@whereClause) > 0
		begin
			SET @whereClause = @whereClause+ ' and parent_section_id = '+STR(@filter_parent_section_id)
		end
		else
		begin
			SET @whereClause = 'where parent_section_id = '+STR(@filter_parent_section_id)
		end
	end

	--Find out how many "covers" we're dealing with
	SELECT	@col_count = COUNT(*) 
	FROM	cover c
	WHERE	c.product_id = @product_id
	AND		(@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	
	
and ordinal >= @min_cover_ordinal

--select @col_count

	SET @count2 = 1

	--Use that to create the correct amount of cols 
	--While we're using the loop, also grabbing the first line of data.(Which is the cover descriptions) 
	SET	@cols = 'Col1 VARCHAR(256), '
	SET	@row1 = 'INSERT @my_table SELECT 0, '''', '	
	SET	@last_row = 'INSERT @my_table SELECT 50000, ''cover_id'', '	
	if @include_help = 1
	begin
		SET @cols = @cols + 'Help1 VARCHAR(4000), '
		SET	@row1 = @row1 + ''''', '	
		SET	@last_row = @last_row + ''''', '	
	end

	WHILE @count2 <= @col_count  
		BEGIN
			---Get the first row which is the cover description
			SELECT	@row1 = @row1 + '''' + description + ''', ' + (case when @include_help=1 then ''''', ' else '' end)
			FROM	cover c
			WHERE	c.product_id = @product_id
			AND		c.ordinal = @count2 + @min_cover_ordinal - 1
			AND		(@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))

			--Get the ids for the last row
			SELECT	@last_row = @last_row + '''' + CONVERT(VARCHAR(8),id) + ''', ' + (case when @include_help=2 then ''''', ' else '' end)
			FROM	cover c
			WHERE	c.product_id = @product_id
			AND		c.ordinal = @count2 + @min_cover_ordinal - 1
			AND     (@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))

			--Create col names for the table
			SELECT	@cols  = @cols + 'Col' + CONVERT(VARCHAR(4), @count2 + 1) + ' VARCHAR(256), ' + (case when @include_help=1 then 'Help' + CONVERT(VARCHAR(4), @count2 + 1) + ' VARCHAR(4000), ' else '' end)

			SET  @count2 = @count2 + 1 
		END 
	
	--SET @cols = LEFT(@cols, LEN(@cols) -1)
	SET @cols = @cols + ' section_id int ,parent_section_id int '
	--SET @row1 = LEFT(@row1, LEN(@row1) -1) + CHAR(10)
	set @row1 = @row1 + '0,0 '
	SET @last_row  = LEFT(@last_row, LEN(@last_row) -1) + CHAR(10)
	SET @table_def = 'DECLARE @my_table TABLE (ordinal INT, ' + @cols  + ')' + CHAR(10)

--select @cols,@row1,@last_row,@table_def
--return
	--Now we have to grab the benefit data. 2 loops - 1 for each section and one for each cover benefit
	declare @temp as table(id int identity(1,1), ordinal int);
	insert into @temp(ordinal)
	SELECT distinct ordinal FROM section WHERE product_id = @product_id
		and id in (
			select distinct section_id
			from cover_benefit
			where cover_id in(
				select id
				from cover c
				where @policy_start between c.cover_effective_date and isnull(c.cover_inactive_date, '2050-01-01')))
				order by ordinal;
	--select * from @temp
	--select * from @temp t join section s on t.section_id=s.id
	select @section_count = COUNT(*) from @temp;

	SET @count2 = 1 
	SET @count3 = 1 
	SET @values = ''

	declare @current_ordinal int;
	
	WHILE @section_count >= @count3 	
		BEGIN
				select @current_ordinal = ordinal from @temp where id=@count3;
				
				SELECT @values = @values + 'INSERT @my_table SELECT ' + CONVERT(VARCHAR(4), s.ordinal) + ', '  + '''' + 
				case when @context = 'W' then dbo.replaceCaveats(case when @replaceSups = 1 then dbo.replaceSup(isnull(s.alternative_description,s.description), c.id, 0)
																	  else isnull(s.alternative_description,s.description) end, s.product_id)
					 else
						case when @replaceSups = 1 then dbo.replaceSup(isnull(s.alternative_description,s.description), c.id, 0)
						else isnull(s.alternative_description,s.description) end
				end
					+ case 
						when c.product_id not in (1,5,6,8,9,10,11,12,13,14,17,18,20,21,22,23,24,25,26,27,28,29,30,31,32,35) and  cb.Excess is not null and replace(cb.Excess,'-','') <> '' and cb.Excess <> 'Nil'
						then 
							case when s.id = 316 then '<br /><span class="excess">Excess per claim</span>' 
							else '<br /><span class="excess">Excess</span>' end else '' end 
					+ '''' + ', ' + (case when @include_help=1 then '''' + ISNULL(s.help_content,'') + ''', ' else '' end)
					FROM	section s
						JOIN	cover c 
							ON	c.product_id = s.product_id
							AND	c.ordinal = @count2 + @min_cover_ordinal - 1
						JOIN	cover_benefit cb
							ON	cb.cover_id = c.id
							AND	cb.section_id = s.id
						WHERE	s.product_id = @product_id
						AND		s.ordinal = @current_ordinal
						AND     (@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))

--select @values
				WHILE @col_count >= @count2 
					BEGIN 
						
						SELECT  
							@sub_section_id = ISNULL(s.section_id,'0'),
							@section_id = ISNULL(s.id, '0')
							,@values = @values + '''' + 
								case when @product_id = 31 and isnull(cb.alternative_description, cb.[description]) = 'Y' then (case when @context = 'W' then '<span class="included"></span>' else 'Yes' end)
									when @product_id = 31 and ISNULL(cb.alternative_description, cb.[description]) = '-' then (case when @context = 'W' then '<span class="notincluded"></span>' else 'No' end)
									when ISNULL(cb.alternative_description, cb.[description]) = '-' then '<span class="notincluded">-</span>'
									else ISNULL(cb.alternative_description, cb.[description]) end +
								case when c.product_id not in (1,5,6,8,9,10,11,12,13,14,17,18,20,21,22,23,24,25,26,27,28,29,30,31,32,35) and cb.Excess is not null and replace(cb.Excess,'-','') <> '' and cb.Excess <> 'Nil'
									then '<br /><span class="excess">' + rtrim(replace(replace(replace(ISNULL(cb.Excess,''), ' per condition', ''), ' per claim',''), '*', '')) + '</span>' 
									else '' end
								+ '''' + ', ' + (case when @include_help=1 then '''' + ISNULL(cb.help_content,'') + ''', ' else '' end)
						FROM	section s
						JOIN	cover c 
							ON	c.product_id = s.product_id
							AND	c.ordinal = @count2 + @min_cover_ordinal - 1
						JOIN	cover_benefit cb
							ON	cb.cover_id = c.id
							AND	cb.section_id = s.id
						WHERE	s.product_id = @product_id
						AND		s.ordinal = @current_ordinal
						AND     (@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))

						SET @count2 = @count2  + 1 

					END 
				--SET	@values = LEFT(@values, LEN(@values)-1)
				set @values = @values + @section_id+ ','+@sub_section_id
				SET @values = @values + CHAR(10) 

				SET	@count2 = 1 

				SET @count3 = @count3 + 1 
				
			
			END 	

	SELECT @values = @values + CHAR(10)

--select @table_def + @row1 +  @values +  @last_row + 'SELECT * FROM @my_table ORDER BY ordinal'
--return
	exec (@table_def + @row1 +  @values +  --@last_row + 
	'select 
	t.*,
	case when t.parent_section_id <> 0 
	then (select ordinal from @my_table where section_id = t.parent_section_id) 
	else t.ordinal
	end as ordinal_parent
	from
	(select * from @my_table
	'+@whereClause+')
	t ORDER BY ordinal_parent, parent_section_id, ordinal')
	--'SELECT * FROM @my_table ORDER BY ordinal, section_id')



SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_benefit_plus_premL_prc]'
GO

CREATE PROCEDURE [dbo].[cover_benefit_plus_premL_prc] (@quote_id int = NULL, @min_cover_ordinal int = NULL)
AS

SET NOCOUNT ON

DECLARE @pet_count int, 
		@product_id int,		
		@promo_code nvarchar(10),
		@policy_start datetime,
		@premium_rows VARCHAR(max),
		@premium_vals VARCHAR(max)

SELECT @pet_count = COUNT(id) FROM quote_pet WHERE quote_id = @quote_id

SELECT @product_id = product_id, @policy_start = ISNULL(policy_start_date,CURRENT_TIMESTAMP), @promo_code = promo_code from quote where id = @quote_id

-- USED TO STORE ALL QUOTES FOR ALL PETS
CREATE TABLE #tmpquotes 
(
   pet varchar(64),
   pet_id int,
   cover_id int,
   premium money,
   selected bit,
   promo_discount decimal(5,2),
   offer_discount decimal(5,2),
   multipet_discount decimal(5,2),
   description varchar(256), 
   product_id int,
   coinsurance int,
   excess money   
)

-- USED TO STORE BENEFIT TABLE 
CREATE TABLE #tmpben (ordinal INT, Col1 varchar(256))

-- USED TO STORE PET IDs OF EACH PET
CREATE TABLE #tmppets (pet_id INT, complete bit)

DECLARE CURSOR1 CURSOR FOR
SELECT id FROM quote_pet WHERE quote_id = @quote_id

DECLARE @id int 

-- ITERATE THRU PETS IN QUOTE, CALL QQCALC AND BUILD QUOTES IN TO ONE TABLE
OPEN CURSOR1 
FETCH NEXT FROM  CURSOR1 INTO  @id
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
	IF (@@FETCH_STATUS <> - 2)

	insert into #tmpquotes
	exec quick_quote_calculateU_prc @id, @pet_count, @promo_code

	FETCH NEXT FROM CURSOR1 INTO @id
END 
CLOSE CURSOR1 
DEALLOCATE CURSOR1

-- FIND OUT HOW MANY COLS THE BENEFIT TABLE WILL HAVE
DECLARE @col_count int

SELECT	@col_count = COUNT(*) 
FROM	cover c
WHERE	c.product_id = @product_id
AND		(@policy_start between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01'))	

-- BUILD ALTER STATEMENT TO ADD REQUIRED COLUMNS TO TEMP BENEFIT TABLE
declare @table_def varchar(max) = 'ALTER TABLE #tmpben ADD '
DECLARE @counter int = 2

WHILE @counter <= @col_count +1
BEGIN
	SET @table_def = @table_def + 'Col' + CONVERT(varchar,@counter) + ' varchar(256)'		
	
	IF @counter <= @col_count 
	BEGIN
		SET @table_def = @table_def + ', '
	END
	
	SET @counter = @counter + 1
END

-- RUN ALTER STATEMENT, TEMP BEN TABLE NOW HAS THE CORRECT NUMBER OF COLS
exec (@table_def)

-- INSERT RESULTS FROM COVER_BENEFITL_PRC IN TO TEMP BEN TABLE
insert into #tmpben
exec cover_benefitL_prc @product_id = @product_id, @quote_id =  @quote_id  

-- GET A LIST OF UNIQUE PET IDS FROM QUOTES TABLE 
insert into #tmppets (pet_id, complete)
select distinct pet_id, 0 from #tmpquotes

SET @table_def = ''

-- FOR EACH PET....
WHILE (SELECT COUNT(1) FROM #tmppets WHERE complete = 0) > 0
BEGIN
	DECLARE @currpet INT
	DECLARE @premium MONEY
	DECLARE @currpetname VARCHAR(64)
	SELECT TOP 1 @currpet = pet_id FROM #tmppets WHERE complete = 0 ORDER BY pet_id
	SELECT TOP 1 @currpetname = pet, @premium = premium FROM #tmpquotes WHERE pet_id = @currpet
	
	SET @counter = 2	
	
	-- BUILD INSERT STATEMENT FOR COVER BENEFIT TABLE TO ADD PREMIUM ROW FOR CURRENT PET
	SET @premium_rows = 'INSERT INTO #tmpben (ordinal, Col1'
	SET @premium_vals = ' VALUES('+CONVERT(VARCHAR,@currpet)+',''<b>Premium for ' + @currpetname + '</b>'''
	
	WHILE @counter <= @col_count +1
	BEGIN	
		SELECT @premium_rows = @premium_rows + ',Col' + CONVERT(varchar,@counter)
		SELECT @premium_vals = @premium_vals + ',''<b>£' + CONVERT(varchar,@premium) + '</b>'''
				
		SET @counter = @counter + 1
	END
	
	SET @premium_rows = @premium_rows + ') '
	SET @premium_vals = @premium_vals + ');'
	
	SET @table_def = @table_def + @premium_rows + @premium_vals
	
	UPDATE #tmppets SET complete = 1 WHERE pet_id = @currpet
	

END

-- RUN INSERT STATEMENTS
exec (@table_def)

-- SELECT BEN TABLE + PREMS
select * from #tmpben order by ordinal

drop table #tmpquotes, #tmpben,#tmppets

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreakdownsBySequenceId]'
GO

CREATE PROCEDURE [dbo].[BreakdownsBySequenceId]
	@sequenceId VARCHAR(36)
AS
BEGIN

	SELECT Id, Breakdown, Uploaded FROM Pricing.Breakdown C
	WHERE SequenceId = @sequenceId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ProductIdFromCode]'
GO

CREATE PROCEDURE [dbo].[ProductIdFromCode]
	@productCode VARCHAR(6)
AS
BEGIN

	SELECT id FROM Product
	WHERE affinity_code = @productCode

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DocumentIdsFromCoverId]'
GO

CREATE PROCEDURE [dbo].[DocumentIdsFromCoverId]
	@coverId INT,
	@effectiveDate DATETIME
AS
BEGIN

DECLARE @documents TABLE
(
	DocumentTypeId varchar(20), 
	DocumentId int null
);

INSERT INTO @documents
	SELECT TOP 1 'toba', at.id FROM uispetmis..affinity_toba AT
	JOIN cover C ON LEFT(C.affinity_code,3) = AT.affinity_code collate Latin1_General_CI_AS
	WHERE C.id = @coverId
	AND AT.valid_from_date <= @effectiveDate
	ORDER BY AT.valid_from_date DESC

-- Policy Wording and Key Facts
DECLARE @documentId INT;

SELECT TOP 1 @documentId = AD.id FROM uispetmis..affinity_document AD
JOIN cover C ON C.affinity_code = AD.affinity_code collate Latin1_General_CI_AS
WHERE C.id = @coverId
AND AD.valid_from <= @effectiveDate
ORDER BY AD.valid_from DESC

INSERT INTO @documents
VALUES
('pw', @documentId),
('kf', @documentId)

SELECT * FROM @documents

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_underwriting_check_quoteU_prc]'
GO


CREATE PROCEDURE [dbo].[product_underwriting_check_quoteU_prc]  (
		@action							CHAR(1),
		@page_id						INT = NULL,
		@product_underwriting_check_id	INT = NULL,
		@quote_id						INT
) 

AS

SET NOCOUNT ON


	IF @action = 'D'
		BEGIN
			DELETE	product_underwriting_check_quote 
			FROM	product_underwriting_check_quote pq
			JOIN	product_underwriting_check p
				ON	p.id = pq.product_underwriting_check_id
				AND	p.page_id = @page_id
			WHERE	quote_id	= @quote_id

			RETURN
		END 
	
	IF @action = 'I'
		BEGIN
			INSERT	product_underwriting_check_quote (product_underwriting_check_id, quote_id) 
			VALUES (@product_underwriting_check_id, @quote_id) 

			RETURN
		END 



SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_sourceL_prc]'
GO


CREATE PROCEDURE [dbo].[aggregator_sourceL_prc]
AS
BEGIN

	select distinct a.source 
	from aggregator a 
	order by source
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_underwriting_check_quoteL_prc]'
GO


CREATE PROCEDURE [dbo].[product_underwriting_check_quoteL_prc]  (
		@page_id						INT,
		@quote_id						INT
) 

AS

SET NOCOUNT ON


		SELECT	product_underwriting_check_id as id
		FROM	product_underwriting_check_quote pq
		JOIN	product_underwriting_check p 
			ON	pq.product_underwriting_check_id  = p.id
			AND	p.page_id = @page_id
		WHERE	pq.quote_id = @quote_id



SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_pet_archive]'
GO
CREATE TABLE [dbo].[aggregator_pet_archive]
(
[id] [int] NOT NULL IDENTITY(17822, 1),
[aggregator_id] [int] NOT NULL,
[pet_name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[animal_type] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[sex] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[breed_id] [int] NULL,
[purchase_price] [money] NULL,
[colour] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[pet_DOB] [date] NULL,
[create_timestamp] [smalldatetime] NOT NULL,
[update_timestamp] [smalldatetime] NOT NULL,
[microchip] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[original_breed_id] [int] NULL,
[vetvisit] [bit] NULL,
[not_working] [bit] NULL,
[sick] [bit] NULL,
[owned] [bit] NULL,
[kept_at_home] [bit] NULL,
[not_breeding] [bit] NULL,
[not_racing] [bit] NULL,
[not_hunting] [bit] NULL,
[neutered] [bit] NULL,
[pricing_engine_id] [bigint] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_aggregator_pet_archive] on [dbo].[aggregator_pet_archive]'
GO
ALTER TABLE [dbo].[aggregator_pet_archive] ADD CONSTRAINT [PK_aggregator_pet_archive] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx-aggregator-id] on [dbo].[aggregator_pet_archive]'
GO
CREATE NONCLUSTERED INDEX [idx-aggregator-id] ON [dbo].[aggregator_pet_archive] ([aggregator_id]) INCLUDE ([pet_name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[aggregator_archive_prc]'
GO

CREATE PROCEDURE [dbo].[aggregator_archive_prc]
AS

-- =============================================
-- Author:		Steve Powell
-- ALTER date: 05/09/2012
-- Description:	Archive agg quotes over 3 months old
-- Modified 9.12.2015 Tom Pullen - rewritten for simpler error handling and for new FKs
-- =============================================



BEGIN

	DECLARE @InsertCount int
	DECLARE @DeleteCount int
	DECLARE @InsertCount2 int
	DECLARE @DeleteCount2 int

SET NOCOUNT ON

BEGIN TRANSACTION

BEGIN TRY

SET IDENTITY_INSERT aggregator_archive ON

	-- Get all aggregator records older than 3 months and stick them in the archive table 
INSERT INTO aggregator_archive ([id],[source],[affinity],[title],[forename],[surname],[quote_ref],[postcode],[contact_num_day],[contact_num_eve],
		[email],[status_id],[create_timestamp],[update_timestamp],[policy_start_date],[client_ip_address],[promo_code],[quote_id],[lead_id],[date_of_birth],
		[house],[address1],[address2],[address3],[address4],[remote_reference],[suffix_id] ,[opt_out_marketing], [all_pet_names])
SELECT [id],[source],[affinity],[title],[forename],[surname],[quote_ref],[postcode],[contact_num_day],[contact_num_eve],
		[email],[status_id],[create_timestamp],[update_timestamp],[policy_start_date],[client_ip_address],[promo_code],[quote_id],[lead_id],[date_of_birth],
		[house],[address1],[address2],[address3],[address4],[remote_reference],[suffix_id] ,[opt_out_marketing], [all_pet_names]
FROM aggregator WHERE create_timestamp < DATEADD(mm,-3,GETDATE())
SET @InsertCount2 = @@ROWCOUNT

SET IDENTITY_INSERT aggregator_archive OFF

SET IDENTITY_INSERT aggregator_pet_archive ON

INSERT INTO aggregator_pet_archive ([id],[aggregator_id],[pet_name],[animal_type],[sex],[breed_id],[purchase_price],[colour],
					[pet_DOB],[create_timestamp],[update_timestamp],[microchip], [original_breed_id],
					[vetvisit],[not_working],[sick],[owned],[kept_at_home],[not_breeding],[not_racing],[not_hunting],[neutered], [pricing_engine_id])
					SELECT ap.* from aggregator_archive aa JOIN aggregator_pet ap ON ap.aggregator_id = aa.id

SET @InsertCount = @@ROWCOUNT

SET IDENTITY_INSERT aggregator_pet_archive OFF


DELETE aggregator_pet FROM aggregator_pet AP JOIN aggregator_pet_archive APA ON AP.id = APA.id
SET @DeleteCount = @@ROWCOUNT

IF @InsertCount <> @DeleteCount
RAISERROR('Aggregator pet totals did not match.  Transactions rolled back.', 16, 1)

DELETE aggregator FROM aggregator A INNER JOIN aggregator_archive AA ON A.id = AA.id
SET @DeleteCount2 = @@ROWCOUNT      

IF @InsertCount2 <> @DeleteCount2
RAISERROR('Aggregator totals did not match.  Transactions rolled back.', 16, 1)





COMMIT TRANSACTION

END TRY

BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ERRORNUMBER,
        ERROR_MESSAGE() AS ERRORMESSAGE;
        
       ROLLBACK TRANSACTION
END CATCH

SELECT @InsertCount as [Aggregator Records Added to Archive],@DeleteCount as [Live Aggregator Records Deleted], @InsertCount2 as [Aggregator Pet Records Added to Archive], @DeleteCount2 as [Live Aggregator Pet Records Deleted]						


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic_by_groupL_prc]'
GO
CREATE PROCEDURE [dbo].[clinic_by_groupL_prc] (@clinic_group_id int = NULL, @clinic_id int = NULL)
AS
BEGIN
	SET NOCOUNT ON;

	IF(@clinic_group_id IS NOT NULL)
	BEGIN
		select id, description
		from clinic
		where clinic_group_id = @clinic_group_id
			and status=1
		order by description
	END
	
	IF(@clinic_id IS NOT NULL)
	BEGIN
		select c.id, c.description
		from clinic c
			join clinic_group cg on c.clinic_group_id = cg.id
			join clinic c1 on c1.clinic_group_id = cg.id
		where c1.id = @clinic_id
			and c.status=1
		order by c.description
	END

	SET NOCOUNT OFF;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_animal_typeS_prc]'
GO

CREATE PROCEDURE [dbo].[quote_animal_typeS_prc] (@quote_id INT)
AS

SET NOCOUNT ON

	SELECT 	animal_type_id
	FROM	quote_pet 
	WHERE	quote_id = @quote_id

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_cover_checkS_prc]'
GO


CREATE PROCEDURE [dbo].[quote_cover_checkS_prc] (@quote_id INT)
AS

SET NOCOUNT ON

	declare @response varchar(500)
	declare	@action INT


	if exists (select 1 from quote_pet 
				where quote_id = @quote_id
				and (vetvisit=1 or accident=1))
	BEGIN
			SELECT 	response = '<p>Thank you for providing the additional information relating to your pet which will be reviewed by one of our vet nurses as soon as possible. If we cannot immediately confirm your cover we may need to call you using the telephone number you provided earlier in order to discuss this directly with you.</p><p>Once cover is in place we will email your policy documents to you.  Please monitor your spam / junk email folder just in case the email is filtered here.</p><p>If you think we may have difficulty reaching you on the number you provided please call our customer services and quote the reference number shown.</p>'
					,action_id = 2
	END

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic_groupL_prc]'
GO

CREATE PROCEDURE [dbo].[clinic_groupL_prc] (@product_id int)
AS
BEGIN
	SET NOCOUNT ON;

	select distinct cg.id, cg.description
	from clinic_group cg
		join clinic c on c.clinic_group_id = cg.id
	where c.product_id = @product_id
	and active = 1
	order by cg.description
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_listL_prc]'
GO


CREATE PROCEDURE [dbo].[quote_listL_prc] @status int = 0, @affinity varchar(10), @surname nvarchar(255), @petname nvarchar(155), @postcode varchar(10), @quoteref varchar(20) = NULL, @user_id int = 0, @systuser_id int,
		@from_date varchar(10), @to_date varchar(10), @email varchar(64)
AS
SET NOCOUNT ON

-- Parse FROM and TO dates
declare @from_date_parsed smalldatetime;
set @from_date_parsed = convert(smalldatetime, @from_date, 103);

declare @to_date_parsed smalldatetime
set @to_date_parsed = convert(smalldatetime, @to_date, 103);
set @to_date_parsed = DATEADD(d,1,@to_date_parsed)

DECLARE @qid int, @pet varchar(64)


--added support for policynumbers out of multi pet quotes (quote ref is incremented)
if(@quoteref != '')
begin

	if exists (select 1 from uispetmis..POLICY where policynumber = @quoteref)
	begin

		declare @single_quote_id int	
		select @single_quote_id = quote_id 
		from uispetmis..POLICY (nolock) p
			join quote_pet  (nolock) qp on p.quote_pet_id = qp.id
		where policynumber = @quoteref

		select q.id,
			quote_status_id,
			dbo.PROPERCASE(q.surname) as owner,
			q.quote_ref,
			ISNULL(q.all_pet_names,'') pet_name,
			UPPER(q.postcode) postcode,
			premium,
			qs.description as quote_status,
			q.create_timestamp update_timestamp,
			CASE WHEN q.created_systuser_id IS NULL then
				case WHEN s.ip_address IS NULL then 'C' else 'S' END 
					else su.Initial end		
				+ ISNULL('/'+su1.Initial,'')
				+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
			source
		,q.ReferrerID as referrer
		,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end ok_to_contact
		,case ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_phone
		,case ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_SMS
		,case ISNULL(agree_contact_email,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_email
		,case ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_letter
		,q.email
		from quote (nolock) q
			JOIN quote_status qs ON q.quote_status_id = qs.id
			LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
			LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
			LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
			LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
			join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(q.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
		where q.id = @single_quote_id
	
		return;
	end 
end

if(@status=0)
BEGIN
	select top 1000 q.id,
		quote_status_id,
		dbo.PROPERCASE(q.surname) as owner,
		q.quote_ref,
		ISNULL(q.all_pet_names,'') pet_name,
		UPPER(q.postcode) postcode,
		premium,
		qs.description as quote_status,
		q.create_timestamp update_timestamp,
		CASE WHEN q.created_systuser_id IS NULL then
			case WHEN s.ip_address IS NULL then 'C' else 'S' END 
				else su.Initial end		
			+ ISNULL('/'+su1.Initial,'')
			+ case quote_status_id when 3 then ISNULL('/'+su2.Initial,'') else '' end
		source
		,q.ReferrerID as referrer
	,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end ok_to_contact
	,case ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_phone
		,case ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_SMS
		,case ISNULL(agree_contact_email,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_email
		,case ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_letter
	,q.email
	from quote (nolock) q
		JOIN quote_status qs ON q.quote_status_id = qs.id
		LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
		LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
		LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
		LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
		join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(q.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
	WHERE
	ISNULL(qs.id,0) <> 9 --Suspicious Activity
	AND q.create_timestamp >= @from_date_parsed AND q.create_timestamp < @to_date_parsed
	and q.quote_ref like @affinity+'%'
	and q.quote_ref like '%'+@quoteref+'%'
	and q.surname LIKE @surname+'%'
	and isnull(q.all_pet_names,'') LIKE '%'+@petname+'%'
	and REPLACE(q.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
	and ((@user_id <> 0 AND @user_id IN (created_systuser_id, modified_systuser_id, converted_systuser_id)) or (@user_id=0))
	--and left(quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
	and q.email like '%'+@email+'%'
	
	and (q.ClientIpAddress not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 1) or q.ClientIpAddress is null)
	and q.surname not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 4)
	
	--when loading it by quote reference, we have to ignore if it is a policy conversion quote
	AND ((@quoteref = '' AND q.id NOT IN (SELECT QuoteId FROM uispetmis.Renewals.PolicyConversion)) OR @quoteref != '')

	order by q.create_timestamp desc
END
ELSE
BEGIN
	select top 1000 q.id,
		quote_status_id,
		dbo.PROPERCASE(q.surname) as owner,
		q.quote_ref,
		ISNULL(q.all_pet_names,'') pet_name,
		q.postcode,
		premium,
		qs.description as quote_status,
		q.create_timestamp update_timestamp,
		CASE WHEN q.created_systuser_id IS NULL then
			case WHEN s.ip_address IS NULL then 'C' else 'S' END 
				else su.Initial end		
			+ ISNULL('/'+su1.Initial,'')
			+ ISNULL('/'+su2.Initial,'')
		source,
		q.ReferrerID as referrer
	,case ISNULL(agree_contact,0) when 1 then 'N' else 'Y' end ok_to_contact
	,case ISNULL(agree_contact_phone,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_phone
		,case ISNULL(agree_contact_SMS,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_SMS
		,case ISNULL(agree_contact_email,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_email
		,case ISNULL(agree_contact_letter,ISNULL(agree_contact,0)) when 1 then 'N' else 'Y' end agree_contact_letter
	,q.email
	from quote (nolock) q
	JOIN quote_status qs ON q.quote_status_id = qs.id
	LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
	LEFT JOIN uispetmis..systuser su on su.id = q.created_systuser_id
	LEFT JOIN uispetmis..systuser su1 on su1.id = q.modified_systuser_id
	LEFT JOIN uispetmis..systuser su2 on su2.id = q.converted_systuser_id
	join uispetmis..systuser_affinity sa on sa.systuser_id = @systuser_id and left(sa.affinity_code,3) = left(q.quote_ref,3) collate SQL_Latin1_General_CP1_CI_AS
	WHERE
    ISNULL(qs.id,0) <> 9 --Suspicious Activity
	AND q.create_timestamp >= @from_date_parsed AND q.create_timestamp < @to_date_parsed
	and quote_status_id = @status
	and q.quote_ref like @affinity+'%'
	and q.quote_ref like '%'+@quoteref+'%'
	and q.surname LIKE @surname+'%'
	and isnull(q.all_pet_names,'') LIKE '%'+@petname+'%'
	and REPLACE(q.Postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
	and ((@user_id <> 0 AND @user_id IN (created_systuser_id, modified_systuser_id, converted_systuser_id)) or (@user_id=0))
	--and left(quote_ref,3) in (select left(affinity_code,3) collate SQL_Latin1_General_CP1_CI_AS from uispetmis..systuser_affinity where systuser_id = @systuser_id) 
	and q.email like '%'+@email+'%'
	
	and (q.ClientIpAddress not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 1) or q.ClientIpAddress is null)
	and q.surname not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 4)

	
	--when loading it by quote reference, we have to ignore if it is a policy conversion quote
	AND ((@quoteref = '' AND q.id NOT IN (SELECT QuoteId FROM uispetmis.Renewals.PolicyConversion)) OR @quoteref != '')


	order by q.create_timestamp desc
END

set rowcount 0


SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[error_tracker]'
GO
CREATE TABLE [dbo].[error_tracker]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[error_page] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[error_type] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[error_message] [varchar] (8000) COLLATE Latin1_General_CI_AS NOT NULL,
[stack_trace] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[time_stamp] [datetime] NOT NULL CONSTRAINT [DF__error_tra__time___4830B400] DEFAULT (getdate()),
[product_id] [int] NULL,
[notification_sent] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__error_tracker__473C8FC7] on [dbo].[error_tracker]'
GO
ALTER TABLE [dbo].[error_tracker] ADD CONSTRAINT [PK__error_tracker__473C8FC7] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[error_trackerI_prc]'
GO


CREATE PROCEDURE [dbo].[error_trackerI_prc] 
				(	@error_page		VARCHAR(500)= NULL,
					@error_type		VARCHAR(128)= NULL,
					@error_message	VARCHAR(8000)= NULL,
					@stack_trace	VARCHAR(8000)= NULL	,
					@product_id		INT = NULL	

				)
AS		

SET NOCOUNT ON


	INSERT error_tracker (error_page, error_type, error_message, stack_trace, product_id)
	VALUES(@error_page, @error_type, @error_message, @stack_trace, @product_id)





SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TEMPlead_listL_prc]'
GO










CREATE PROCEDURE [dbo].[TEMPlead_listL_prc] 
@status_id int = 100
,@days int
,@clinic_id int
,@affinity varchar(10)
,@surname nvarchar(255)
,@petname nvarchar(155)
,@postcode varchar(10)
,@user_id int
,@auto tinyint = 0
WITH RECOMPILE AS

begin
--SET NOCOUNT ON

declare @date smalldatetime
select @date = dateadd(d, @days, getdate())

--declare #audit table (aid int, lead_id int)
--INSERT INTO @audit (aid, lead_id)
select 
	MAX(la.id) aid, la.lead_id
	into #audit
	from lead q JOIN 
		lead_audit la 
	ON q.id = la.lead_id
	where lead_status_id = @status_id
	group by la.lead_id


CREATE NONCLUSTERED INDEX [idx_lead_id] ON #audit (	[lead_id] ASC)

--if(@auto = 1)
	--set rowcount 500

	select top 1000 CASE WHEN @auto  = 0 and lead_status_id = 100 AND DATEDIFF(day, getdate(), contact_date) < 0 THEN 'Overdue!'
				WHEN @auto  = 0 and lead_status_id = 100 AND DATEDIFF(day, getdate(), contact_date) = 0 THEN 'Today!'
				ELSE 'View!' END actiontext
		,q.*
		,q.surname as owner
		,ISNULL(lp.pet_name,'') pet_name
		--,ISNULL(ps.petstring,'') pet_name
		,q.postcode
		,qs.description lead_status
		,'' last_user	--s.Initial last_user
		,p.prod_prefix affinity
		,c.description clinic
		,ISNULL(q.source,'') source
		,agg.quote_ref
	from lead q
		left JOIN lead_pet lp on lp.lead_id = q.id
		JOIN lead_status qs ON q.lead_status_id = qs.id
		--LEFT JOIN @petstrings ps on q.id = ps.qid
		LEFT JOIN #audit a on a.lead_id = q.id
		LEFT JOIN lead_audit la on la.id = a.aid
		LEFT JOIN uispetmis..systuser s on la.user_id = s.id 
		JOIN product p on q.product_id = p.id
		LEFT JOIN clinic c on q.clinic_id = c.id
		LEFT JOIN aggregator agg on agg.lead_id = q.id
		

	where lead_status_id = @status_id
	and contact_date <= @date
	and p.prod_prefix like @affinity+'%'
	and q.surname like @surname+'%'
	--and ps.petstring like '%'+@petname+'%'
	and REPLACE(q.postcode,' ','') LIKE REPLACE(@postcode,' ','')+'%'
--	and ((@user_id <> 0 AND @user_id = la.user_id) or (@user_id=0))
	and (((@clinic_id > 0 OR @clinic_id < 0) AND q.clinic_id = @clinic_id) OR (@clinic_id=0))

	and ((@auto = 0 and ISNULL(q.source,'')!= 'gocomp') or (@auto = 1 and ISNULL(q.source,'') = 'gocomp' and q.contact_date > (getdate()-7)))

	--not (agg.source = 'gocomp')	-- and q.create_timestamp < '2011-06-23')
	order by q.contact_date desc, q.contact_time desc


set rowcount 0

SET NOCOUNT OFF

END









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[productS_prc]'
GO
CREATE PROCEDURE [dbo].[productS_prc] (@id INT)
AS		

SET NOCOUNT ON


	SELECT 	description,
			email,
			address1,
			address2,
			town,
			county,
			postcode,
			tel,
			web_address,
			admin_email,
			quote_expiration_days,
			prod_prefix
	FROM	product 
	WHERE	id = @id




SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_cover_excess]'
GO
CREATE VIEW [dbo].[vw_cover_excess]
AS
SELECT     TOP (100) PERCENT x.product_id, x.cover_id, cb.excess, CONVERT(int, REPLACE(REPLACE(CASE WHEN charindex('/', cb.excess) 
                      > 0 THEN LEFT(cb.excess, charindex('/', cb.excess) - 1) WHEN charindex(' ', cb.excess) > 0 THEN LEFT(cb.excess, charindex(' ', cb.excess) - 1) 
                      ELSE cb.excess END, '*', ''), '£', '')) AS nexcess
FROM         (SELECT     product_id, cover_id, COUNT(1) AS policies
                       FROM          uispetmis.dbo.POLICY
                       WHERE      (cover_id IN
                                                  (SELECT     uispetmis.dbo.POLICY.cover_id
                                                    FROM          dbo.cover
                                                    WHERE      (underwriter = 'rsl')))
                       GROUP BY product_id, cover_id) AS x INNER JOIN
                      dbo.section AS s ON s.product_id = x.product_id INNER JOIN
                      dbo.cover_benefit AS cb ON cb.section_id = s.id AND cb.cover_id = x.cover_id
WHERE     (s.description LIKE 'veterinary fees%') AND (CHARINDEX(' ', cb.excess) > - 1)
ORDER BY x.product_id, x.cover_id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PDP1]'
GO
CREATE TABLE [dbo].[pricing_matrix_PDP1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[area_1] [money] NOT NULL,
[area_2] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_PDP1] on [dbo].[pricing_matrix_PDP1]'
GO
ALTER TABLE [dbo].[pricing_matrix_PDP1] ADD CONSTRAINT [PK_pricing_matrix_PDP1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PDP]'
GO
CREATE TABLE [dbo].[pricing_matrix_PDP]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[area_1] [money] NOT NULL,
[area_2] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_pricing_matrix_PDP] on [dbo].[pricing_matrix_PDP]'
GO
ALTER TABLE [dbo].[pricing_matrix_PDP] ADD CONSTRAINT [PK_pricing_matrix_PDP] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[premium_cap]'
GO

CREATE FUNCTION [dbo].[premium_cap](@uncapped_premium money, @weighting decimal(5,2), @policy_id int, @suppress_cap bit = 0, @renewal_date date)
RETURNS money
AS
BEGIN

	if(@suppress_cap = 1)
		return @uncapped_premium

	declare @premium money;
	select @premium = @uncapped_premium;
	
	--increase limited to 100% where there is no weighting applied	
	if(@weighting = 1)
	BEGIN

		declare @current_premium money;
		select @current_premium = uispetmis.dbo.get_premium_by_date(@policy_id, DATEADD(DAY, -1, @renewal_date));
	
		if(@premium > 2 * @current_premium)
		BEGIN
			select @premium = 2 * @current_premium;
		END
	END
	
	--in all cases limit the premium to 200
	if(@premium > 200)
	BEGIN
		select @premium = 200;
	END
	
	return @premium;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_calculate_prc]'
GO

CREATE PROCEDURE [dbo].[renewal_calculate_prc](
	@product_id int
	,@cover_id INT 
	,@animal_type_id int
	,@pet_dob date
	,@breed_id int
	,@postcode varchar(10)
	,@pet_count int
	,@offer_discount decimal(5,2) = 0
	,@multipet_discount decimal(5,2) = 0
	,@promotion_discount decimal(5,2) = 0
	,@sex char(1)
	,@clinic_id int
	,@policy_start_date date
	,@policy_id int = NULL
	,@show_options bit = 0 --used to return all levels of cover if switching at renewal - also suppresses cap function
	,@promo_code varchar(10) = NULL
	)
AS

	SET NOCOUNT ON
	
		--check its not on manual premium set list
		declare @renewal_date date
		select @renewal_date = renewaldate from uispetmis..policy where policyid = @policy_id

		--switch off premium cap if cover has been altered
		declare @old_cover_id int, @suppress_cap bit;
		select @old_cover_id = @cover_id, 
		@suppress_cap = @show_options;


		--############################# Logic coppied to the WebApi - Begin
		--We are updating the promocode for the premium override in the web api - so this loads the external id of the promocodes

		----check that the supplied promotion code is reflected in the override if one is present
		--if exists (select 1 from renewal_premium_override
		--	where policy_id = @policy_id and renewal_year = YEAR(@renewal_date)
		--	and not (cover_discount_id IS NULL AND ISNULL(@promo_code, '') = ''))
		--begin
		--	declare @new_cover_discount_id int
		--	select @new_cover_discount_id = cd.id
		--	from cover_discount cd
		--		join discount d on cd.discount_id = d.id
		--	where cd.cover_id = @cover_id
		--	and d.code = @promo_code
			
		--	update renewal_premium_override
		--	set cover_discount_id = @new_cover_discount_id
		--		,premium = NULL
		--	where policy_id = @policy_id and renewal_year = YEAR(@renewal_date)
		--	and ISNULL(cover_discount_id,0) != @new_cover_discount_id
		--end
		
		

		
		--if both premium and cover have been specified then go for it
		--if exists (select 1 from renewal_premium_override
		--	where policy_id = @policy_id and renewal_year = YEAR(@renewal_date) AND premium IS NOT NULL)
		--begin
		
		--	select ISNULL(cover_id, @cover_id) cover_id
		--			,premium
		--			,ISNULL(@multipet_discount,0.00) multipet_discount
		--			,premium premium_adjusted
		--			,premium premium_standard
		--			,@policy_id policy_id
		--			,w.claims_count
		--			,w.claims_value
		--			,w.recurrence
		--			,w.loading
		--			,w.loading_standard
		--			,c.description product
		--			,c.affinity_code
		--			,@promo_code promo_code
		--			,@promotion_discount promotion_discount
		--		from renewal_premium_override rpo
		--			join cover c on c.id = rpo.cover_id
		--			left join dbo.renewal_weighting(@policy_id) w on w.policy_id = @policy_id
		--		where rpo.policy_id = @policy_id and renewal_year = YEAR(@renewal_date)
		--		return
				
		--end
		--############################# Logic coppied to the WebApi - End

		declare @premium money;

		--if just cover has been specified then set the @cover_id and let it go
		if exists (select 1 from renewal_premium_override
			where policy_id = @policy_id and renewal_year = YEAR(@renewal_date) AND cover_id IS NOT NULL and premium IS NULL)
		begin
			select @cover_id = cover_id
				,@suppress_cap = 1
			from renewal_premium_override 
			where policy_id = @policy_id and renewal_year = YEAR(@renewal_date)
		end

		--or else we have added a promo code
		if exists (select 1 from renewal_premium_override
			where policy_id = @policy_id and renewal_year = YEAR(@renewal_date) AND cover_id IS NOT NULL and cover_discount_id IS NOT NULL)
		begin
			select @cover_id = rpo.cover_id
				,@promotion_discount = cd.discount
				,@promo_code = d.code
				,@suppress_cap = 1
			from renewal_premium_override rpo
				JOIN cover_discount cd on rpo.cover_discount_id = cd.id
				JOIN discount d on cd.discount_id = d.id
			where policy_id = @policy_id and renewal_year = YEAR(@renewal_date)
		end

		--check if cover was changed hence suppress premium cap
		if @old_cover_id != @cover_id
			select @suppress_cap = 1;

		

	declare --@premium money,
		@area_rating int,
		@breed_rating int,
		@age_rating	varchar(16),
		@area_id int,
		--@pet_age_at_policy_start_date float,
		@breed_group char(1)

	--SELECT	@pet_age_at_policy_start_date = convert(float, DATEDIFF(mm, @pet_dob, @policy_start_date))/12

	--Age
	select @age_rating = case 
		when DATEADD(yy,8, @pet_dob) < @policy_start_date and not @product_id in (29,30) then 'O8' 
		when DATEADD(yy,6, @pet_dob) < @policy_start_date and @product_id in (29,30) then 'O6' 
		when DATEADD(yy,6, @pet_dob) >= @policy_start_date and @product_id in (29,30) then 'U6' 				
		else 'U8' end

	--look for breed mapping
	SELECT @breed_id = ISNULL(map_to_breed_id, @breed_id) from breed where id = @breed_id

	--Breed
	IF @product_id in (3,13,14,1,9,11,6,22)
		SELECT @breed_rating = dbo.get_breed_rating(@product_id, @animal_type_id, @breed_id, @policy_start_date);
	ELSE
		SELECT @breed_group = dbo.get_breed_group(@product_id, @animal_type_id, @breed_id, @policy_start_date);
	
	--Postcode/Area
	select @area_rating = dbo.get_area_rating(@product_id, @postcode, @policy_start_date);

	/*********POSTCODES NOT LIABLE TO IPT ****************/
	/*****************************************************/
	declare @ipt_factor float;
	if(LEFT(@postcode, 2) IN ('GY','JE','IM') and @policy_start_date >= '2012-04-01')
		select @ipt_factor = 1/1.06;
	else
		select @ipt_factor = 1;	
	/*****************************************************/
	/*****************************************************/

	--------------------------------------------------
	-- TVIS brands
	--------------------------------------------------
	IF @product_id = 3 --village vet
	BEGIN
		select @area_id = area_id from clinic where id=@clinic_id and product_id = @product_id

		if @policy_start_date < '2013-06-12'
		begin	
			SELECT c.id cover_id,
				convert(decimal(10,2), 
					case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
					* case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
					* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
					,convert(decimal(10,2), 
					case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
					* case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
					* @ipt_factor) premium_adjusted
					,case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
				,c.description product
				,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM	pricing_matrix_VFV
				JOIN cover c ON cover_id = c.id
			WHERE	pricing_matrix_VFV.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND c.id != @cover_id

			order by ordinal
		end
		else
		begin
			SELECT c.id cover_id,
				convert(decimal(10,2), 
					case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
					* case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
					* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
					,convert(decimal(10,2), 
					case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end
					* case @pet_count when 1 then 1 else ((100-ISNULL(@multipet_discount, c.multipet_discount))/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
					* @ipt_factor) premium_adjusted
					,case @area_id when 1 then [value] when 2 then [value_2] when 3 then [value_3] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
				,c.description product
				,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM	pricing_matrix_VFV1
				JOIN cover c ON cover_id = c.id
			WHERE	pricing_matrix_VFV1.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)

			order by ordinal
		end;
	END
	
	ELSE IF @product_id = 6	--pet doctors 
	BEGIN

		select @area_id = area_id from clinic where id=@clinic_id and product_id = @product_id
			
		if @policy_start_date >= '2014-10-01'
		begin
			--OK, have all the factors, let's look up the value
			SELECT c.id cover_id,
				convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
				,convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor) premium_adjusted
					,case @area_id when 1 then [area_1] when 2 then [area_2] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount
			FROM	pricing_matrix_PDP
				JOIN cover c ON cover_id = c.id
			WHERE	pricing_matrix_PDP.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)

			order by ordinal
		end
		else
		begin
			--OK, have all the factors, let's look up the value
			SELECT c.id cover_id,
				convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
				,convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor) premium_adjusted
					,case @area_id when 1 then [area_1] when 2 then [area_2] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount
			FROM	pricing_matrix_PDP1
				JOIN cover c ON cover_id = c.id
			WHERE	pricing_matrix_PDP1.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)

			order by ordinal
		end;
	END
	
	ELSE IF @product_id = 11	--vetsure 
	BEGIN

		if @clinic_id is null or @clinic_id = 0
			select @area_id = @area_rating;
		else
			select @area_id = area_id from clinic where id = @clinic_id-- and product_id = @product_id

		if @policy_start_date < '2014-07-14'
			select @area_id = @area_id + 100


		--OK, have all the factors, let's look up the value
		SELECT	c.id cover_id,
				convert(decimal(10,2), 
				case @area_id 
					when 0 then [area_0]
					when 1 then [area_1] 
					when 2 then [area_2] 
					when 3 then [area_3] 
					when 4 then [area_4] 
					when 5 then [area_5] 
					when 6 then [area_6] 
					when 7 then [area_7]
					when 100 then [area_100]
					when 101 then [area_101] 
					when 102 then [area_102] 
					when 103 then [area_103] 
					when 104 then [area_104] 
					when 105 then [area_105] 
					when 106 then [area_106] 
					when 107 then [area_107] 
				end
				* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
					* ((100-@promotion_discount) /100) --promotional rate
					* ((100-@offer_discount) /100) --offer rate
					* @ipt_factor)
				as premium
				,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
				
				
				,convert(decimal(10,2), 
				case @area_id 
					when 0 then [area_0] 
					when 1 then [area_1] 
					when 2 then [area_2] 
					when 3 then [area_3] 
					when 4 then [area_4] 
					when 5 then [area_5] 
					when 6 then [area_6] 
					when 7 then [area_7] 
					when 100 then [area_100] 
					when 101 then [area_101] 
					when 102 then [area_102] 
					when 103 then [area_103] 
					when 104 then [area_104] 
					when 105 then [area_105] 
					when 106 then [area_106] 
					when 107 then [area_107] 
				end
				* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
					* ((100-@promotion_discount) /100) --promotional rate
					* ((100-@offer_discount) /100) --offer rate
					* @ipt_factor) as premium_adjusted
				,convert(decimal(10,2), 
				case @area_id 
					when 0 then [area_0] 
					when 1 then [area_1] 
					when 2 then [area_2] 
					when 3 then [area_3] 
					when 4 then [area_4] 
					when 5 then [area_5] 
					when 6 then [area_6] 
					when 7 then [area_7] 
					when 100 then [area_100] 
					when 101 then [area_101] 
					when 102 then [area_102] 
					when 103 then [area_103] 
					when 104 then [area_104] 
					when 105 then [area_105] 
					when 106 then [area_106]
					when 107 then [area_107] 
				end) premium_standard
				,@policy_id policy_id
				,0 claims_count
				,0 claims_value
				,0 recurrence
				,1 loading				
				,1 loading_standard
				,c.description product
				,c.affinity_code				
				,@promo_code promo_code
				,@promotion_discount promotion_discount
		FROM	pricing_matrix_vsr
		JOIN	cover c ON cover_id = c.id
		WHERE	pricing_matrix_vsr.animal_type_id = @animal_type_id 
		AND		age	= @age_rating 
		AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
		AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
	END
	
	ELSE IF @product_id in (13,14)	--BFG,VSA
	BEGIN
		select @area_id = area_id from clinic where id = @clinic_id and product_id = @product_id

		if @policy_start_date >= '2014-07-01'
		BEGIN
			SELECT	c.id cover_id,
					convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100)) --offer rate
						* @ipt_factor
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount

					,convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100)) --offer rate
						* @ipt_factor premium_adjusted
					,case @area_id when 1 then [area_1] when 2 then [area_2] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM pricing_matrix_bfg1
			JOIN cover c ON cover_id = c.id
			WHERE pricing_matrix_bfg1.animal_type_id = @animal_type_id 
			AND	age	= @age_rating 
			AND	ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
			and c.product_id = @product_id
		END
		ELSE
		BEGIN
			SELECT	c.id cover_id,
					convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100)) --offer rate
						* @ipt_factor
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount

					,convert(decimal(10,2), 
					case @area_id when 1 then [area_1] when 2 then [area_2] end
					* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100)) --offer rate
						* @ipt_factor premium_adjusted
					,case @area_id when 1 then [area_1] when 2 then [area_2] end premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM pricing_matrix_bfg
			JOIN cover c ON cover_id = c.id
			WHERE pricing_matrix_bfg.animal_type_id = @animal_type_id 
			AND	age	= @age_rating 
			AND	ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
			and c.product_id = @product_id
		END
	END
	
	ELSE IF @product_id = 22 --DON
	BEGIN
		--OK, have all the factors, let's look up the value
		if @policy_start_date < '2013-12-01'
		begin
			SELECT	c.id cover_id,
					convert(decimal(10,2), premium * case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
					,convert(decimal(10,2), premium * case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor) premium_adjusted
					,premium premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM	pricing_matrix_don
			JOIN	cover c ON cover_id = c.id
			WHERE	pricing_matrix_don.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
		END
		ELSE
		BEGIN
			SELECT	c.id cover_id,
					convert(decimal(10,2), premium * case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor)
					as premium
					,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
					
					,convert(decimal(10,2), premium * case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor) premium_adjusted
					,premium premium_standard
					,@policy_id policy_id
					,0 claims_count
					,0 claims_value
					,0 recurrence
					,1 loading
					,1 loading_standard
					,c.description product
					,c.affinity_code
					,@promo_code promo_code
					,@promotion_discount promotion_discount

			FROM	pricing_matrix_don1
			JOIN	cover c ON cover_id = c.id
			WHERE	pricing_matrix_don1.animal_type_id = @animal_type_id 
			AND		age	= @age_rating 
			AND		ISNULL(breed_rating,'') = ISNULL(@breed_rating,'')
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
		END	
	END
	
	--------------------------------------------------
	-- Inactive/Old brands
	--------------------------------------------------
	ELSE IF @product_id IN (28)	--kfi
	BEGIN	
			
		SELECT c.id cover_id
			,convert(decimal(10,2), 
				value * case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
					* ((100-@promotion_discount) /100) --promotional rate
					* ((100-@offer_discount) /100) --offer rate
					* @ipt_factor
					* (case when @policy_start_date < '2012-08-19' then POWER(CONVERT(float, 1.02), -1) else 1 end) --remove 2% rate increase temporarily
					)
				as premium
				,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
		FROM	pricing_matrix_kfi
		JOIN	cover c ON cover_id = c.id
		WHERE	animal_type_id = @animal_type_id 
		AND	age	= @age_rating 
		AND	area_rating	= @area_rating
		AND	
		CASE breed_rating
			WHEN 1 THEN 'H'
			WHEN 2 THEN 'L'
			WHEN 3 THEN 'M'
			WHEN 4 THEN 'S'
			ELSE ''
		END = ISNULL(@breed_group,'')
		and	c.active = 1
		--and c.product_id = @product_id
			AND	((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
		order by ordinal
	end
		
	--------------------------------------------------
	-- Other brands
	--------------------------------------------------
	ELSE
	BEGIN
		
		-- Neutered not used at the moment
		declare @neutered bit;
		select @neutered = null;
	
		-- Business Rules
		-- AAX/AAI: Cane Corso and Perro De Presa Canario are select for renewal rather than excluded
		if(@product_id in (29,30) and @breed_id in (294, 523, 535))
			select @breed_group = 'H';
	
		-- Look up the value
		SELECT c.id as cover_id,
			case when @premium IS NULL then
				convert(decimal(10,2),
						dbo.premium_cap(
							value 
							* dbo.get_pricing_factor(@policy_start_date, 'R', c.product_id, c.id, @animal_type_id, @sex, @age_rating, @area_rating, @breed_group, @neutered, u.effective_date)
							* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
							* ((100-@promotion_discount) /100) --promotional rate
							* ((100-@offer_discount) /100) --offer rate
							* @ipt_factor
							* w.loading, w.loading, @policy_id, @suppress_cap, @policy_start_date)
					) else @premium end as premium
			,case @pet_count when 1 then 0 else c.multipet_discount end multipet_discount
			,convert(decimal(10,2),
					dbo.premium_cap(
						value 
						* dbo.get_pricing_factor(@policy_start_date, 'R', c.product_id, c.id, @animal_type_id, @sex, @age_rating, @area_rating, @breed_group, @neutered, u.effective_date)
						* case @pet_count when 1 then 1 else ((100-c.multipet_discount)/100) end --multipet
						* ((100-@promotion_discount) /100) --promotional rate
						* ((100-@offer_discount) /100) --offer rate
						* @ipt_factor
						* w.loading, w.loading, @policy_id, @suppress_cap, @policy_start_date)
				) as premium_adjusted
			,value * dbo.get_pricing_factor(@policy_start_date, 'R', c.product_id, c.id, @animal_type_id, @sex, @age_rating, @area_rating, @breed_group, @neutered, u.effective_date) as premium_standard
			,w.*
			,c.description product
			,c.affinity_code
			,@promo_code promo_code
			,@promotion_discount promotion_discount
		FROM cover c 
			JOIN pricing_matrix u ON u.cover_id = c.id and u.id = ( select top 1 id
																	from pricing_matrix pm
																	where pm.cover_id = u.cover_id
																		and pm.animal_type_id = @animal_type_id
																		and pm.age = @age_rating
																		and ISNULL(pm.breed_group_code,'') 	= ISNULL(@breed_group,'')
																		and	area_rating	= @area_rating
																		and pm.effective_date <= @policy_start_date
																	order by pm.effective_date desc, id desc)
			LEFT JOIN dbo.[renewal_weighting](@policy_id) w on w.policy_id = @policy_id
		WHERE c.product_id = @product_id
			and ((@show_options = 0 AND c.id = @cover_id) OR @show_options = 1)
		order by ordinal
	END
	
SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[payment_typeL_prc]'
GO

CREATE PROCEDURE [dbo].[payment_typeL_prc] (@type INT=NULL)
AS

SET NOCOUNT ON

If @type = 0
BEGIN
	SELECT 	id, 
			description = REPLACE(REPLACE(description, ' Monthly', ''), ' Annual', '')
	FROM	payment_type
	where description like '%Monthly'
	ORDER BY id 
END

If @type = 1
BEGIN
	SELECT 	id, 
			description = REPLACE(REPLACE(description, ' Monthly', ''), ' Annual', '')
	FROM	payment_type
	where description like '%Annual'

	ORDER BY id 
END


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discount_staffL_prc]'
GO


CREATE PROCEDURE [dbo].[discount_staffL_prc] @policy_id INT

AS

SET NOCOUNT ON

	DECLARE @old_cover_id INT, @new_cover_id INT
	SELECT @old_cover_id = cover_id FROM uispetmis..policy WHERE policyid = @policy_id
	IF EXISTS(SELECT 1 FROM renewal_cover WHERE old_cover_id = @old_cover_id)
		SELECT @new_cover_id = new_cover_id FROM renewal_cover WHERE old_cover_id = @old_cover_id
	ELSE
		SELECT @new_cover_id = @old_cover_id

		SELECT DISTINCT [key] AS code,d.TotalDiscountRate AS discount FROM ZenithPricingEngine..discount d
		JOIN ZenithPricingEngine.dbo.DiscountRate dr ON dr.DiscountId = d.DiscountId
		WHERE d.ExternalCoverId = @old_cover_id 
		AND d.DESCRIPTION LIKE '%staf%'
		AND dr.EffectiveTo >= GETDATE()
			ORDER BY d.[key]

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_timeL_prc]'
GO


CREATE PROCEDURE [dbo].[lead_timeL_prc]
AS
BEGIN

	select	*
	from lead_time lt
	order by lt.start_time
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[upgrade_calculate_prc]'
GO



CREATE PROCEDURE [dbo].[upgrade_calculate_prc](
	@cover_id int
	,@pet_count int
	,@policy_id int
	,@product_id int
	,@is_renewal tinyint = 0
	,@get_calc_params bit = 0
	,@backdate bit = 0)
AS
begin
SET NOCOUNT ON

	declare @today date = getdate();

	declare @payment_type_id int
	select @payment_type_id = payment_type_id from uispetmis..payment_detail where policy_id = @policy_id
	declare @old_premium decimal(10,2)
	
	declare @max_effective_date datetime, @max_premium_id int;
	select @max_effective_date = MAX(isnull(mta_effective_date, effective_date)) from uispetmis..premium where policy_id = @policy_id;
	select @max_premium_id = uispetmis.dbo.get_premium_id_by_date(@policy_id, @max_effective_date) --max(id) from uispetmis..PREMIUM_PLUS_IMPORTED where policy_id = @policy_id
	select @old_premium = ISNULL(uispetmis.dbo.get_premium_by_date(@policy_id, ISNULL(@max_effective_date, @today)),0) --ISNULL(uispetmis.dbo.get_premium_plus_imported_by_date(@policy_id, ISNULL(@max_effective_date, CURRENT_TIMESTAMP)),0)

	declare @start_date datetime, @postcode varchar(20), @policynumber varchar(50), @sex char(1)
	select @start_date = startdate
		,@postcode = postcode
		,@policynumber = policynumber
		,@sex= CASE WHEN animalsexname = 'Male' THEN 'M' ELSE 'F' END 
	from uispetmis..policy where policyid = @policy_id

	declare @old_vouchers int
	if exists (select 1 from uispetmis..voucher_cover where cover_id = @cover_id)
		select @old_vouchers = 1
	else
		select @old_vouchers = 0

	-- calculate the premium effective date (not mta effective date)
	declare @effective_date date;
	/*if day(@start_date) >= day(@today)
        select @effective_date = map.period_start from uispetmis.dbo.get_monthly_anniversary_period_udf(@start_date, @today) map;
    else*/
		select @effective_date = dateadd(d, 1, map.period_end) from uispetmis.dbo.get_monthly_anniversary_period_udf(@start_date, @today) map;

	declare @new_product_prefix varchar(3), @old_product_prefix varchar(3)
	select @new_product_prefix = new_product_prefix
		,@old_product_prefix = old_product_prefix
	from renewal_product
	where old_product_id = @product_id

	declare @quotes table (
		pet varchar(32)
		,pet_id int
		,animal_type_id int
		,cover_id int
		,premium money
		,selected tinyint
		,promo_discount decimal(5,2)
		,offer_discount decimal(5,2)
		,multipet_discount decimal(5,2)
		,new_vouchers tinyint
		,product varchar(255)
		,product_id int
		,affinity_code varchar(4)
		,[description] varchar(255)
		,coinsurance float
		,excess money)

	if(@is_renewal > 0)
	begin
	
		declare @animal_type_id int
			,@pet_dob smalldatetime
			,@breed_id int
			,@opostcode varchar(10)
			,@offer_discount decimal(5,2)
			,@multipet_discount decimal(5,2)
			,@promotion_discount decimal(5,2)
			,@clinic_id int
			,@policy_start_date smalldatetime
			,@promo_code nvarchar(50)
	
		select @policy_start_date = startdate from uispetmis..POLICY where PolicyID = @policy_id;
		
		if(DATEADD(DAY, 14, @policy_start_date) > @today)
		begin
			select @animal_type_id = animal_type_id
				,@pet_dob = AnimalDateOfBirth
				,@breed_id = breed_id
				,@opostcode = postcode
				,@clinic_id = clinic_id
				,@policy_start_date = StartDate
			from uispetmis..policy
			where policyid = @policy_id
			
			--restore current discounts
			select @offer_discount = offer_discount
				,@multipet_discount = multipet_discount
				,@promotion_discount = promo_discount
				,@promo_code = promo_code
			FROM uispetmis..PREMIUM
			where id = @max_premium_id  --(select MAX(id) from uispetmis..PREMIUM WHERE policy_id = @policy_id)

		end
		else
		begin
			select @animal_type_id = animal_type_id
				,@pet_dob = AnimalDateOfBirth
				,@breed_id = breed_id
				,@opostcode = postcode
				,@offer_discount = 0
				,@multipet_discount = NULL
				,@promotion_discount = 0
				,@clinic_id = clinic_id
				,@policy_start_date = renewaldate	
			from uispetmis..policy 
			where policyid = @policy_id
			
			--keep any discounts which have been applied to existing renewal offer
			select @promotion_discount = promo_discount
				,@promo_code = promo_code
				,@multipet_discount = multipet_discount
			FROM uispetmis..PREMIUM
			where id = @max_premium_id	--(select MAX(id) from uispetmis..PREMIUM WHERE policy_id = @policy_id and renewal = 1)

		end

		--so if multipet is nothing force pet count to 1
		if(@multipet_discount = 0)
			select @pet_count = 1	

		select @cover_id = ISNULL(new_cover_id, @cover_id)
		from uispet..renewal_cover
		where old_cover_id = @cover_id

		--Promo code at renewal
		--Checking if the latest primium has a promo code for staff applied
		declare @staff bit
		select @staff = ISNULL(d.staff,0)
		from uispetmis..PREMIUM pr
			left join discount d on d.code collate SQL_Latin1_General_CP1_CI_AS = pr.promo_code
		where pr.id = @max_premium_id
		and d.staff = 1
		
		--the @is_renewal flag is 1 just when the policy will renewal in less then one month
		--validate if the promo code is persistent at renewal when it is not staff
		--if it is renewal=1 and staff=1 then the renewal options table on Control 80 should be hidden
		if not exists(
			select top 1 *
			from uispet..cover_discount cd
			inner join uispet..discount d on d.id = cd.discount_id
			where cd.cover_id = @cover_id
			and d.code = @promo_code
			and cd.persist_at_renewal = 1
			and d.staff = 0 -- there should not have persistent discount at renewal for staff members!
			and ISNULL(d.end_date, DATEADD(d, 1, getdate())) > getdate()
			and @is_renewal = 1 and isnull(@staff,0) = 0
		)
		begin
			select @promo_code = '', @promotion_discount = 0;
		end
		
		---- any promotion discount (staff) removes other discounts
		if isnull(@staff,0) = 1 and @promotion_discount > 0 
		begin
			select @multipet_discount=0, @pet_count=1;
		end
		
		--select @promo_code as promo_code, @promotion_discount as promo_discount;

		if @get_calc_params = 1
		begin
			select 1
			,@policy_id
			,@product_id
			,@cover_id
			,@animal_type_id
			,@pet_dob
			,@breed_id
			,@opostcode
			,@pet_count
			,@offer_discount
			,@multipet_discount
			,@promotion_discount
			,@sex
			,@clinic_id
			,@policy_start_date
			,@promo_code
			,ISNULL(@new_product_prefix, left(@policynumber, 3)) new_product_prefix
			,ISNULL(@old_product_prefix, left(@policynumber, 3)) old_product_prefix
			,@policynumber
			,@old_premium
			,@is_renewal;
		end
		else
		begin
			declare @renews table (
				cover_id int
				,premium decimal(10,2)                                
				,multipet_discount decimal(5,2)
				,premium_adjusted decimal(5,2)
				,premium_standard decimal(10,2)
				,policy_id int
				,claims_count int
				,claims_value money          
				,recurrence int
				,xloading decimal(5,2)
				,loading_standard decimal(5,2)
				,product varchar(50)
				,affinity_code varchar(50)
				,promo_code varchar(11)
				,promotion_discount decimal(5,2))
				
			insert into @renews
			exec uispet..[renewal_calculate_prc]
				@product_id
				,@cover_id
				,@animal_type_id
				,@pet_dob
				,@breed_id
				,@opostcode
				,@pet_count
				,@offer_discount
				,@multipet_discount
				,@promotion_discount
				,@sex
				,@clinic_id
				,@policy_start_date
				,@policy_id
				,1 --@show_options
				,@promo_code

			delete @renews
			where cover_id in (select c.id from uispet..cover c where not (@policy_start_date between c.cover_effective_date and ISNULL(c.cover_inactive_date,'2050-01-01')))

			declare @new_vouchers int, @new_cover_id int
			select @new_cover_id = cover_id from @renews
			if exists (select 1 from uispetmis..voucher_cover where cover_id = @cover_id)
				select @old_vouchers = 1
			else
				select @old_vouchers = 0

			select r.*
				,convert(int, (xloading - 1) * 100) loading
				,case @payment_type_id when 3 then 'AD' when 4 then 'DD' else 'CC' end payment_type
				,@promotion_discount promo_discount
				,@offer_discount offer_discount
				--,r.premium old_premium
				,@old_premium old_premium
				,@cover_id old_cover_id
				,@start_date start_date
				,@old_vouchers old_vouchers
				,case when v.cover_id is null then 0 else 1 end new_vouchers
				,@postcode postcode
				,@promo_code promo_code
				,@policynumber policynumber
				,ISNULL(@new_product_prefix, left(@policynumber, 3)) new_product_prefix
				,ISNULL(@old_product_prefix, left(@policynumber, 3)) old_product_prefix
				,@product_id product_id
				,@is_renewal is_renewal
				,ISNULL(cd.id,0) cover_discount_id
				,prem_adj.charge charge
				,@effective_date effective_date
				,prem_adj.total_payable total_payable
				,case when ISNULL(@staff, 0) = 1 and @is_renewal = 1 then 1 else 0 end as is_staff_and_renewal
				,c.renewable
			from @renews r
				JOIN dbo.cover c ON r.cover_id = c.id
				left join (select distinct cover_id from uispetmis..voucher_cover) v on v.cover_id = r.cover_id
				left join (select cd.* from discount d join cover_discount cd on cd.discount_id = d.id where code = @promo_code) cd on r.cover_id = cd.cover_id
				cross apply uispetmis.dbo.get_premium_adjustment(@policy_id, @old_premium, r.premium, @backdate) prem_adj
			WHERE c.renewable = 1
		end;
		
		return;
	end;
		
	
	declare @loading decimal(5,2)
	select @promo_code = ISNULL(promo_code,'') 
		,@loading = ISNULL(loading, 1)
		,@offer_discount = offer_discount
	from uispetmis..PREMIUM_PLUS_IMPORTED where id = @max_premium_id

	if @get_calc_params = 1
	begin
		select 2
		,@policy_id
		,@product_id
		,@cover_id
		,@animal_type_id
		,@pet_dob
		,@breed_id
		,@postcode
		,@pet_count
		,@offer_discount
		,@multipet_discount
		,@promotion_discount
		,@sex
		,@clinic_id
		,@policy_start_date
		,@promo_code
		,ISNULL(@new_product_prefix, left(@policynumber, 3))
		,ISNULL(@old_product_prefix, left(@policynumber, 3))
		,@policynumber
		,@old_premium
		,@is_renewal;
	end
	else
	begin
		insert into @quotes (pet, pet_id, animal_type_id, cover_id, premium, selected, promo_discount, offer_discount, multipet_discount, [description], product_id, coinsurance, excess)
		exec quick_quote_calculateU_prc @quote_pet_id=@policy_id,@pet_count=@pet_count, @promo_code = NULL, @is_aggregator = 2

		update @quotes
		set product_id = c.product_id
			,product = c.description
			,new_vouchers = case when v.cover_id is null then 0 else 1 end
			,affinity_code = c.affinity_code
		from @quotes q
			join uispet..cover c on q.cover_id = c.id
			left join (select distinct cover_id from uispetmis..voucher_cover) v on v.cover_id = c.id

		select 
			pet, pet_id, cover_id
			,((((((premium / (100.0 - promo_discount)) * 100) -- premium stripped of discounts to get matrix rate
			 / (100.0 - q.multipet_discount)) * 100)  
			 / (100.0 - q.offer_discount)) * 100) premium_standard			
			,convert(int, 100 * (@loading-1)) loading
			,@loading xloading
			,ROUND(@loading * ((premium) / ((100.0 - q.offer_discount) * 100) * ((100.00 - @offer_discount) * 100)),2) premium
			,selected, promo_discount, @offer_discount offer_discount, q.multipet_discount
			,new_vouchers
			,product
			,q.product_id
			,q.affinity_code
			,@old_vouchers old_vouchers
			,ISNULL(@new_product_prefix, left(@policynumber, 3)) new_product_prefix
			,ISNULL(@old_product_prefix, left(@policynumber, 3)) old_product_prefix
			,@policynumber policynumber
			,@promo_code promo_code
			,@postcode postcode
			,@start_date start_date
			,@cover_id old_cover_id
			,@old_premium old_premium
			,@payment_type_id payment_type_id 
			,case @payment_type_id when 3 then 'AD' when 4 then 'DD' else 'CC' end payment_type
			,@is_renewal is_renewal
			,prem_adj.charge charge
			,@effective_date effective_date
			,prem_adj.total_payable total_payable
		from @quotes q
			cross apply uispetmis.dbo.get_premium_adjustment(@policy_id, @old_premium, @loading * ((premium) / ((100.0 - q.offer_discount) * 100) * ((100.00 - @offer_discount) * 100)), @backdate) prem_adj;
	end;

SET NOCOUNT OFF
end


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[get_schedule_config_value]'
GO

Create FUNCTION [dbo].[get_schedule_config_value]
(
    @affinityCode VARCHAR(10),
	@typeId int
)
RETURNS VARCHAR(max)
AS
BEGIN

    RETURN (SELECT TOP 1 sc.Value FROM uispetmis..schedule_configuration sc WHERE sc.AffinityCode = 
	 @affinityCode AND sc.ScheduleTypeId = @typeId)	

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_scheduleS_prc]'
GO

CREATE PROCEDURE [dbo].[policy_scheduleS_prc] (@policy_id INT, @param_affinity_code VARCHAR(10))

AS 

SET NOCOUNT ON

BEGIN

	DECLARE @policy_start_date SMALLDATETIME
		,@effective_date SMALLDATETIME
		,@cover_id INT
		,@currency VARCHAR(1)
		--,@renewal int
		,@postcode VARCHAR(16)
		,@underwriter VARCHAR(3)
		,@foot VARCHAR(MAX)
		,@footer_colour VARCHAR(7)
		,@dual_footer VARCHAR(MAX)


		DECLARE @footer TABLE(footer VARCHAR(MAX), dual_footer VARCHAR(MAX), footer_colour VARCHAR(7));
		

		
	IF @policy_id <> 0
	BEGIN
	    SELECT @param_affinity_code = AffinityCode FROM uispetmis.dbo.POLICY WHERE PolicyID = @policy_id
	    IF(@param_affinity_code LIKE 'SLF%')
		BEGIN
		    SELECT @foot = [Text] FROM uispetmis.comms.Footer  WHERE FooterTypeID = 4
		END
		IF(@param_affinity_code LIKE 'PDS%')
		BEGIN
		    SELECT @foot = [Text] FROM uispetmis.comms.Footer  WHERE FooterTypeID = 6
		END
		--check most recent premium record in case of renewal invitation
		SELECT TOP 1 @effective_date = effective_date
			,@policy_start_date = CASE WHEN renewal > 0 THEN effective_date ELSE p.StartDate END
			,@cover_id = prm.cover_id
			--,@renewal = renewal
			,@postcode = p.postcode
			,@underwriter = c.underwriter
		FROM uispetmis..premium prm
			JOIN uispetmis..policy p ON p.policyid = prm.policy_id
			JOIN uispet..cover c ON p.cover_id = c.id
		WHERE policy_id = @policy_id
		AND renewal < 3
		AND ISNULL(mta_effective_date, effective_date) > startdate	--getdate() changed THS 2012-02-28
		ORDER BY ISNULL(mta_effective_date, effective_date) DESC;
		
		--otherwise we need the start date off of the policy record (not most recent effective date in case of MTA)
		IF(@policy_start_date IS NULL)	--(ISNULL(@renewal,0) = 0)
		BEGIN
			SELECT @policy_start_date = startdate 
				,@effective_date = startdate
				,@cover_id = cover_id
				,@postcode = postcode
				,@underwriter = c.underwriter
			FROM uispetmis..policy p 
					JOIN uispet..cover c ON p.cover_id = c.id
			WHERE p.policyid = @policy_id
		END
		
	
	END
	ELSE
	BEGIN
		SELECT @cover_id = c.id
		FROM cover c
		WHERE c.affinity_code = @param_affinity_code;
	END;

	
	-- use this wording only policy is from one TVIS affinity and for Accident & Illness Renewing Benefit (policy type = 12)
	DECLARE @PolicyTypeExtraForTVIS AS VARCHAR(300) = 'Public Liability benefit is per claim.  All other benefits applicable to the policy including the inner limits within Veterinary Fees are per Policy Period.';
	
	DECLARE @product_names AS TABLE(product_id INT, product_desc VARCHAR(300))
	INSERT INTO @product_names
		SELECT 1,'ULTIMATE INSURANCE SOLUTIONS IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	UNION
		SELECT 2,'ADMIRAL PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 19,'ADMIRAL PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 3,'VILLAGE VET TOTAL CARE INSURANCE'	union
		select 4,'INEEDTOINSURE IN ASSOCIATION WITH INSURANCE FACTORY'	union
		select 5,'COMPUTER QUOTE PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 25,'COMPUTER QUOTE IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 6,'PET DOCTORS PET INSURANCE'	union
		select 8,'FORCESDEAL.COM PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY'	union
		select 9,'ALAN BOSWELL GROUP IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 21,'ALAN BOSWELL GROUP IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 11,'VETSURE PET INSURANCE®'	union
		select 12,'THE GREEN INSURANCE COMPANY LIMITED PET INSURANCE'	union
		select 18, 
		CASE WHEN @cover_id in(391,392,393,394,395) 
			THEN  'KWIK FIT INSURANCE SERVICES IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'
		ELSE
			'THE GREEN INSURANCE COMPANY LIMITED PET INSURANCE'
		end	union
		select 13,'BEST FRIENDS4LIFE PET INSURANCE'	union
		select 14,'VET SAVERS PET INSURANCE'	union
		select 17,
		CASE WHEN @cover_id IN (287,288,289,290)
			THEN 'ULTIMATE INSURANCE SOLUTIONS'
			else
			'1ST CENTRAL IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'
		end	union
		select 20,'BHSF PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY'	union
		select 22,'DONALDSON & PARTNERS PET INSURANCE'	union

		select 23, CASE WHEN @cover_id < 550 THEN 'ULTIMATE INSURANCE SOLUTIONS IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	
			ELSE 'ULTIMATE INSURANCE SOLUTIONS'
			END union
		select 24, CASE WHEN @cover_id in(361,362,363,364) 
					THEN 'ePet IN ASSOCIATION WITH INSURANCE FACTORY' 
					ELSE 'ePET INSURANCE' END union
		select 10,'ULTIMATE INSURANCE SOLUTIONS IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 26,'PURELY PETS INSURANCE'	union
		select 27,'JBI INTERNATIONAL INSURANCE BROKERS LTD IN ASSOCIATION WITH INSURANCE FACTORY'	union
		select 28,'KWIK FIT PET INSURANCE'	union
		select 29,'AA INSURANCE SERVICES IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 30,'AA INSURANCE SERVICES IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'	union
		select 31,'PET INSURANCE FROM INSURANCE FACTORY LIMITED IN ASSOCIATION WITH LEGAL & GENERAL'	union
		select 32,		
			CASE WHEN @cover_id >= 396 THEN 'PAWS & CLAWS PET INSURANCE'-- IN ASSOCIATION WITH Insurance Factory Limited' 
			ELSE 'PAWS & CLAWS PET INSURANCE IN ASSOCIATION WITH INSURANCE FACTORY LIMITED'
		end	;
	-- END PRODUCT DESCRIPTION --


IF @policy_id <> 0
begin

	select 
		CASE 
			WHEN aff.underwriter = 'ATL'
				THEN 'In return for the premium shown below, Atlas Insurance PCC Limited in respect of its TVIS Cell (the Insurer) will provide the cover set out in this policy Schedule.  The Insurer has authorised Insurance Factory Limited (Ultimate) to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter = 'GLK' 
				THEN 'In return for the premium shown below, Great Lakes Reinsurance (UK) Plc (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory to issue Schedules and collect premium on its behalf.'
			--petwise wording for the underwriter in the html_header.
			WHEN aff.underwriter = 'ZEN' AND aff.AffinityCode LIKE 'PTW%'				
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy schedule. The Insurer has authorised Insurance Factory Limited to issue schedules and to collect premium on their behalf.'		
			WHEN aff.underwriter = 'ZEN' AND aff.AffinityCode LIKE 'SLF%'				
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy schedule. The Insurer has authorised Insurance Factory Limited to issue schedules and to collect premium on their behalf.'		
			WHEN aff.underwriter = 'ZEN' AND aff.AffinityCode LIKE 'PDS%'
				THEN 'In return for the premium shown below, Zenith Insurance plc. (the Insurer) will provide the cover set out in this policy schedule. The Insurer has authorised Insurance Factory Limited to issue schedules and to collect premiums on their behalf.'
			WHEN aff.underwriter = 'ZEN' AND aff.AffinityCode LIKE 'UIZ%'
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy schedule. The Insurer has authorised Insurance Factory Limited to issue schedules and to collect premiums on their behalf.'
			WHEN aff.underwriter = 'ZEN' 
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue schedules and collect premiums on their behalf.'
			WHEN aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE' and tplunder.annual_premium > 0 and affsc.id is not null 
				THEN 'In return for the premium shown below, Insurance Factory Limited and Ageas Insurance Limited (the Insurers) will provide the cover set out in this policy Schedule.  The Insurers have authorised Insurance Factory Limited to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter in ('HAN','HAD')
				THEN 'In return for the premium shown below, HDI Global Specialty SE UK Branch and Ageas Insurance Limited (the Insurers) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter = 'L&G' 
				THEN 'In return for the premium shown below, Legal & General Insurance Limited (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue Schedules and collect premium on their behalf.'
			ELSE 'In return for the premium shown below, Insurance Factory Ltd (the Insurer) will provide the cover set out in this Policy Schedule.  The Insurer has authorised Insurance Factory Ltd to issue Schedules and collect premium on its behalf.'
		END as header_html,
	pn.product_desc as product_desc,
	CASE WHEN aff.AffinityCode LIKE 'PTW%'  THEN
	'12 months'
	WHEN aff.AffinityCode LIKE 'PDS%' THEN
	'12 Months'
	WHEN aff.AffinityCode LIKE 'SLF%' THEN
	isnull(dbo.get_schedule_config_value(aff.AffinityCode,4),'')
	ELSE
	'ANNUAL'
	END AS period,
	@foot AS footer,
	p.quote_pet_id as id,
	p.quote_pet_id as quote_pet_id,
	UPPER(c.description +  case when c.product_id IN (18,32) and @cover_id IN (311,348,349,396,397,735,736) THEN ' (ACCIDENT ONLY)' 
		WHEN c.product_id = 32 THEN ' (ACCIDENT AND ILLNESS)' ELSE '' end +  ' Policy Schedule') as cover_desc,
	convert(varchar(11), @policy_start_date, 103) as policy_start_date,
	CASE WHEN aff.AffinityCode LIKE 'SLF%' THEN 
	P.Title + ' ' + P.LastName	ELSE p.FirstName + ' ' + p.LastName END  as policyholder, 
	p.title as title,
	p.Address1 + ' ' + p.Address2 as address_1,
	p.Address3 as address_2,
	p.Address4 as town,
	p.Address5 as county,
	p.postcode,
	p.AnimalPetName as pet_name,
	convert(varchar(11),p.AnimalDateOfBirth,103) as pet_dob,
	case when p.purchase_price > 0 then ISNULL(aff.currency_character, '£') + convert(varchar, ISNULL(convert(int, p.purchase_price),'')) else '' end purchase_price,
	p.BreedDescription as breed,
	--Vol excess value wording
	CASE WHEN affsc.affinity_code LIKE 'SLF%' OR affsc.affinity_code LIKE 'PTW%' OR affsc.affinity_code LIKE 'PDS%'
	THEN '£' + CONVERT(VARCHAR(100),ISNULL(p.vol_excess,'0.00'),1)
	ELSE isnull(p.ColourDescription,'')
	END AS colour_excess_config_value
	,
	CASE WHEN affsc.affinity_code LIKE 'SLF%'
	THEN 'This premium is fully inclusive of Insurance Premium Tax'
	ELSE 'This premium is fully inclusive of Insurance Premium Tax which is charged at the prevailing rate'
	END AS total_wording

	,p.AnimalSexName as sex,
	uispetmis.dbo.get_premium_by_date(p.policyid, @effective_date) as total_monthly_premium,
	p.exclusion as exclusion
	,ISNULL(@cover_id, p.cover_id) cover_id
	,p.policynumber opn
	,case when new_product_prefix is null then p.policynumber else replace(p.policynumber collate Latin1_General_CI_AS , old_product_prefix, new_product_prefix) end policynumber
	,ISNULL(aff.currency_character, '£') currency
	,case when pt.id in (11,12) and c.product_id IN (3,6,11,13,14,22) 
		THEN affsc.policy_type_description + ' ' + @PolicyTypeExtraForTVIS
		ELSE affsc.policy_type_description
	end as policy_type_html
	,ISNULL(affsc.excess_wording, '') as excess_copayment,
	case 
	WHEN affsc.affinity_code LIKE 'SLF%' THEN isnull(dbo.get_schedule_config_value(aff.AffinityCode,3),'')
	--Petwise require a different excess heading
	WHEN affsc.affinity_code LIKE 'PTW%' THEN 'EXCESS AND VARIABLE EXCESS' 
	WHEN affsc.affinity_code LIKE 'PDS%' THEN 'EXCESS AND VARIABLE EXCESS'
	WHEN affsc.has_co_payment = 1 then 'EXCESS AND CO-PAYMENT' 
	 ELSE 'EXCESS' 
	END as excess_copayment_title
	--Vol excess wording
	,CASE WHEN affsc.affinity_code LIKE 'SLF%' OR affsc.affinity_code LIKE 'PTW%' OR affsc.affinity_code LIKE 'PDS%'
		THEN 'VOLUNTARY EXCESS' 
		ELSE 'COLOUR'
	END AS colour_excess_config
	,CASE WHEN	affsc.affinity_code LIKE 'SLF%' THEN
    'hide'
	ELSE
	'show'
	END AS show_signature
	-- Third party or Public Liability
	-- all products except TVIS are TPL
	,CASE WHEN c.product_id not in (3,6,11,13,14,22) or ( aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE')
		then 
			case when aff.AffinityCode LIKE 'PDS%' THEN 'Third Party Legal'
			WHEN tplunder.annual_premium = 0 or affsc.id is null
			then 'Public'
			else 'Third Party' end
		else 'Public' end as liability_type

	,CASE WHEN aff.AffinityCode LIKE 'PTW%' THEN 'All claims will be settled from within the one policy limit for the period of insurance.'
	ELSE 'The Benefit Limit for ' +  CASE WHEN c.product_id not in (3,6,11,13,14,22) or ( aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE')
		then 
			case when aff.AffinityCode LIKE 'PDS%' THEN 'third party legal'
			WHEN tplunder.annual_premium = 0 or affsc.id is null
			then 'Public'
			else 'Third Party' end
		else 'Public' end + ' Liability is per claim and the Excess is payable per claim.' END AS [liability_wording]

	,case when aff.AffinityCode LIKE 'PDS%' THEN 1
	WHEN tplunder.annual_premium = 0 or affsc.id is NULL OR aff.AffinityCode IN ('SLF5','SLF6','SLF7','SLF8')  then 0  
	ELSE 1 end as show_liability_box
	-- Last Paragraph
	,
	CASE WHEN AFF.AffinityCode LIKE 'SLF%' THEN 

	isnull(dbo.get_schedule_config_value(aff.AffinityCode,2),'')
				
	 when c.product_id not in (3,6,11,13,14,22) and aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE' and tplunder.annual_premium > 0 then
					CASE WHEN tplunder.annual_premium = 0 THEN 
						'Signature in respect of all Benefits' +' other than Public Liability'
					
					ELSE 'Signature in respect of all Benefits' +' other than Third Party Liability' end
				else ''	end
		as last_para,
	 aff.AffinityCode as affinity_code,
	 isnull(affsc.benefit_limit_title,'BENEFIT LIMIT') as benefit_limit_title
	from uispetmis..policy p
		left join cover c on @cover_id = c.id
		inner join uispetmis..affinity aff on c.affinity_code = aff.AffinityCode collate SQL_Latin1_General_CP1_CI_AS
		inner join uispetmis..policy_type pt on aff.policy_type_id = pt.id
		left join renewal_product rp on rp.old_product_id = p.product_id
		left join @product_names pn on p.product_id = pn.product_id
		left join uispetmis.dbo.get_tpl_underwriter(@cover_id, 1, @policy_start_date) tplunder on 1=1
		left join uispetmis..affinity_schedule affsc on affsc.id = (select top 1 a.id
													from uispetmis..affinity_schedule a
													where a.affinity_code = aff.AffinityCode
													and a.valid_from <= @policy_start_date
													order by a.valid_from desc) --getting the last one by affinity and date
	where p.policyid = @policy_id;
	
END
ELSE
BEGIN
-- it will load by affinity code
	SET @policy_start_date = GETDATE();
	
	select @cover_id = c.id from uispet..cover c where c.affinity_code = @param_affinity_code;
	
	SELECT
	CASE 
			WHEN aff.underwriter = 'ATL'
				THEN 'In return for the premium shown below, Atlas Insurance PCC Limited in respect of its TVIS Cell (the Insurer) will provide the cover set out in this policy Schedule.  The Insurer has authorised Insurance Factory Limited (Ultimate) to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter = 'GLK' 				
				THEN 'In return for the premium shown below, Great Lakes Reinsurance (UK) Plc (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory to issue Schedules and collect premium on its behalf.'
			WHEN aff.underwriter = 'ZEN' AND aff.AffinityCode LIKE 'PTW%'				
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue Schedules and to collect premium on their behalf.'					
			WHEN aff.underwriter = 'ZEN' 
				THEN 'In return for the premium shown below, Zenith Insurance Plc. (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue Schedules and collect premiums on their behalf.'
			WHEN aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE' AND tplunder.annual_premium > 0 AND affsc.id IS NOT NULL 
				THEN 'In return for the premium shown below, Insurance Factory Limited and Ageas Insurance Limited (the Insurers) will provide the cover set out in this policy Schedule.  The Insurers have authorised Insurance Factory Limited to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter IN ('HAN','HAD') 
				THEN 'In return for the premium shown below, HDI Global Specialty SE UK Branch and Ageas Insurance Limited (the Insurers) will provide the cover set out in this policy Schedule. The Insurer has authorised Ultimate Insurance Solutions Limited to issue Schedules and collect premium on their behalf.'
			WHEN aff.underwriter = 'L&G' 
				THEN 'In return for the premium shown below, Legal & General Insurance Limited (the Insurer) will provide the cover set out in this policy Schedule. The Insurer has authorised Insurance Factory Limited to issue Schedules and collect premium on their behalf.'
			ELSE 'In return for the premium shown below, Insurance Factory Ltd (the Insurer) will provide the cover set out in this Policy Schedule.  The Insurer has authorised Insurance Factory Limited to issue Schedules and collect premium on its behalf.'
		END AS header_html,
	pn.product_desc AS product_desc,
	CASE WHEN aff.AffinityCode LIKE 'PTW%' OR aff.AffinityCode LIKE 'SLF%' THEN
	'12 months'
	ELSE
	'ANNUAL'
	END AS period,
	0 AS id,
	0 AS quote_pet_id,
	UPPER(c.description +  CASE WHEN c.product_id IN (18,32) AND @cover_id IN (311,348,349,396,397,735,736) THEN ' (ACCIDENT ONLY)' 
		WHEN c.product_id = 32 THEN ' (ACCIDENT AND ILLNESS)' ELSE '' END +  ' Policy Schedule') AS cover_desc,
	CONVERT(VARCHAR(10), GETDATE(), 103) AS policy_start_date,
	'' AS title,
	'' AS policyholder,
	'' AS address_1,
	'' AS address_2,
	'' AS town,
	'' AS county,
	'' AS postcode,
	'' AS pet_name,
	'' AS pet_dob,
	'' purchase_price,
	'' AS breed,
	'' AS colour,
	--added the voluntary excess for Petwise.
	'' AS vol_excess,
	'' AS sex,
	0 total_monthly_premium,
	'' AS exclusion,
	c.id AS cover_id,
	'' AS opn,
	'' AS policynumber,
	'£' AS currency,
	CASE WHEN pt.id IN (11,12) AND c.product_id IN (3,6,11,13,14,22) 
		THEN affsc.policy_type_description + ' ' + @PolicyTypeExtraForTVIS
		ELSE affsc.policy_type_description
	END AS policy_type_html,
	ISNULL(affsc.excess_wording, '') AS excess_copayment,
	CASE 
	WHEN affsc.affinity_code LIKE 'SLF%' THEN 'EXCESS AND VARIABLE EXCESS' +  NCHAR(0x00B9)
	WHEN affsc.affinity_code LIKE 'PTW%' THEN 'Excess and Variable excess'
	WHEN affsc.has_co_payment = 1 THEN 'EXCESS AND CO-PAYMENT' 
	 ELSE 'EXCESS' 
	END AS excess_copayment_title
		-- Third party or Public Liability
	-- all products except TVIS are TPL
	,CASE WHEN c.product_id NOT IN (3,6,11,13,14,22) OR ( aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE')
		THEN 
			CASE WHEN tplunder.annual_premium = 0 OR affsc.id IS NULL
			THEN 'Public'
			ELSE 'Third Party' END
		ELSE 'Public' END AS liability_type

	,CASE WHEN tplunder.annual_premium = 0 OR affsc.id IS NULL THEN 1
	ELSE 1
	END AS show_liability_box
	-- Last Paragraph
	,'Signature in respect of all Benefits' +
				CASE WHEN c.product_id NOT IN (3,6,11,13,14,22) AND aff.underwriter = 'UIC' AND tplunder.underwriter = 'AGE' AND tplunder.annual_premium > 0 THEN
					CASE WHEN tplunder.annual_premium = 0 THEN 
						' other than Public Liability'
					ELSE ' other than Third Party Liability' END
				ELSE ''	END
		AS last_para,
	 aff.AffinityCode AS affinity_code,
	ISNULL(affsc.benefit_limit_title,'BENEFIT LIMIT') AS benefit_limit_title
	FROM uispetmis..AFFINITY aff
	LEFT JOIN uispet..cover c ON aff.AffinityCode=c.affinity_code COLLATE Latin1_General_CI_AS
	INNER JOIN uispetmis..policy_type pt ON aff.policy_type_id = pt.id
	LEFT JOIN @product_names pn ON c.product_id = pn.product_id
	LEFT JOIN uispetmis.dbo.get_tpl_underwriter(@cover_id, 1, @policy_start_date) tplunder ON 1=1
	LEFT JOIN uispetmis..affinity_schedule affsc ON affsc.id = (SELECT TOP 1 a.id
													FROM uispetmis..affinity_schedule a
													WHERE a.affinity_code = aff.AffinityCode
													AND a.valid_from <= @policy_start_date
													ORDER BY a.valid_from DESC) --getting the last one by affinity and date
	WHERE aff.AffinityCode = @param_affinity_code
END
END


SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[AddBreakdown]'
GO

CREATE PROCEDURE [Pricing].[AddBreakdown]
	@id UNIQUEIDENTIFIER,
	@sourceId INT,
	@identifier INT,
	@coverId INT,
	@premium DECIMAL(10,2),
	@pricingEngineId INT,
	@sequenceId VARCHAR(50),
	@breakdown VARCHAR(4000),
	@createdDate DATETIME,
	@uploaded BIT,
	@assetId VARCHAR(50) = NULL
AS
BEGIN

	INSERT INTO Pricing.Breakdown ( 
			Id,
			SourceId,
			Identifier,
			CoverId,
			Premium,
			PricingEngineId,
			SequenceId,
			Breakdown,
			Created,
			Uploaded,
			AssetID
	) VALUES ( 
			@id,
			@sourceId,
			@identifier,
			@coverId,
			@premium,
			@pricingEngineId,
			@sequenceId,
			@breakdown,
			@createdDate,
			@uploaded,
			@assetId
		)

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetBreakdownSummariesForPolicy]'
GO

CREATE PROCEDURE [Pricing].[GetBreakdownSummariesForPolicy]
	@policyId INT
AS
BEGIN

	SELECT COALESCE(BQ.Id, BR.Id) [BreakdownId],
	                           COALESCE(BQ.SourceId, BR.SourceId) [SourceId],
	                           COALESCE(NULLIF(BQ.Identifier, 0), NULLIF(BAQ.Identifier, 0), NULLIF(BR.Identifier, 0), NULLIF(BAR.Identifier, 0)) [Identifier],
	                           COALESCE(BQ.Premium, BR.Premium) [Premium],
	                           C.affinity_code [AffinityCode],
	                           C.description [CoverDescription],
	                           COALESCE(BQ.Created, BR.Created) [Created]
                        FROM uispetmis..Premium P
                        LEFT JOIN uispetmis..mta_quote MQ ON MQ.premium_id = P.Id
                        LEFT JOIN uispet.Pricing.BreakdownAssociation BAQ ON BAQ.SourceId = 2 AND BAQ.Identifier = MQ.id -- MTA
                        LEFT JOIN uispet.Pricing.Breakdown BQ ON BQ.SourceId = 2 AND (BQ.Identifier = MQ.id OR BQ.Id = BAQ.BreakdownId)-- MTA
                        LEFT JOIN uispet.Pricing.BreakdownAssociation BAR ON BAR.SourceId = 3 AND BAR.Identifier = P.id -- Renewal
                        LEFT JOIN uispet.Pricing.Breakdown BR ON BR.SourceId = 3 AND (BR.Identifier = P.id OR BR.Id = BAR.BreakdownId) -- Renewal
                        JOIN uispet..cover C ON C.id = COALESCE(BQ.CoverId, BR.CoverId)
                        WHERE P.policy_id = @policyId
                        AND COALESCE(BQ.Id, BR.Id) IS NOT NULL
                        ORDER BY COALESCE(BQ.Created, BR.Created) DESC

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CoverConfiguration]'
GO
CREATE TABLE [dbo].[CoverConfiguration]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CoverId] [int] NULL,
[Configuration] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Effective] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CoverConfiguration_Id] on [dbo].[CoverConfiguration]'
GO
ALTER TABLE [dbo].[CoverConfiguration] ADD CONSTRAINT [PK_CoverConfiguration_Id] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetCoverConfiguration]'
GO




CREATE PROCEDURE [dbo].[GetCoverConfiguration]
( @coverId int)
AS
BEGIN

	SELECT TOP 1 * FROM dbo.CoverConfiguration
	WHERE CoverId = @coverId
	ORDER BY Effective desc
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetBreakdownSummariesForQuote]'
GO

CREATE PROCEDURE [Pricing].[GetBreakdownSummariesForQuote]
	@quoteId INT
AS
BEGIN

	SELECT BQ.Id [BreakdownId],
	                           BQ.SourceId [SourceId],
	                           COALESCE(NULLIF(BQ.Identifier, 0), NULLIF(BAQ.Identifier, 0)) [Identifier],
	                           BQ.Premium [Premium],
	                           C.affinity_code [AffinityCode],
	                           C.description [CoverDescription],
	                           BQ.Created [Created]
                        FROM uispet..quote_pet QP
                        LEFT JOIN uispet.Pricing.BreakdownAssociation BAQ ON BAQ.SourceId = 1 AND BAQ.Identifier = QP.id
                        LEFT JOIN uispet.Pricing.Breakdown BQ ON BQ.SourceId = 1 AND (BQ.Identifier = QP.id OR BQ.Id = BAQ.BreakdownId)
                        JOIN uispet..cover C ON C.id = BQ.CoverId
                        WHERE QP.quote_id = @quoteId
                        AND BQ.Id IS NOT NULL
                        ORDER BY BQ.Created DESC

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DuplicateQuoteS_prc]'
GO
CREATE PROCEDURE [dbo].[DuplicateQuoteS_prc] (@quoteIdToDuplicate INT, 
                                              @newProductId       INT) 
AS 
    SET NOCOUNT ON

    DECLARE @affinityCode      VARCHAR(3),
            @new_quote_ref     VARCHAR(16), 
            @new_agg_id        INT, 
            @new_agg_quote_ref VARCHAR(16),
			@new_id INT
    -- Get the new affinity code   
    SELECT @affinityCode = affinity_code 
    FROM   product 
    WHERE  id = @newProductId 

    -- Duplicate quote record 
    INSERT INTO [dbo].[quote] 
                ([product_id], 
                 [title_id], 
                 [forename], 
                 [surname], 
                 [dob], 
                 [postcode], 
                 [house_name_num], 
                 [street], 
                 [town], 
                 [county], 
                 [contact_num], 
                 [email], 
                 [quote_status_id], 
                 [create_timestamp], 
                 [update_timestamp], 
                 [product_excess_id], 
                 [occupation_id], 
                 [hear_about_us_id], 
                 [other_policies], 
                 [motor_insurance_due], 
                 [home_insurance_due], 
                 [street2], 
                 [policy_start_date], 
                 [agree_contact], 
                 [agree_terms], 
                 [payment_detail_id], 
                 [vfv_clinic], 
                 [vfv_clinicid], 
                 [referrerid], 
                 [clientipaddress], 
                 [schedule_doc], 
                 [promo_code], 
                 [email_sent], 
                 [created_systuser_id], 
                 [modified_systuser_id], 
                 [converted_systuser_id], 
                 [min_cover_ordinal], 
                 [opt_out_marketing], 
                 [agg_remote_ref], 
                 [affinity], 
                 [agree_preexisting_excl], 
                 [agree_waiting_excl], 
                 [agree_illvac_excl], 
                 [agree_vicious_excl], 
                 [enquiry_method_code], 
                 [all_pet_names], 
                 [confirm_excluded_breeds], 
                 [agree_contact_phone], 
                 [agree_contact_sms], 
                 [agree_contact_email], 
                 [agree_contact_letter], 
                 [campaign_code], 
                 [emailaddressconfirmation], 
                 [is_multi_pet], 
                 [agree_contact_mmpportal], 
                 [sent_by_post], 
                 [agreecoinsurance]) 
    SELECT @newProductId, 
           [title_id], 
           [forename], 
           [surname], 
           [dob], 
           [postcode], 
           [house_name_num], 
           [street], 
           [town], 
           [county], 
           [contact_num], 
           [email], 
           [quote_status_id], 
           [create_timestamp], 
           [update_timestamp], 
           [product_excess_id], 
           [occupation_id], 
           [hear_about_us_id], 
           [other_policies], 
           [motor_insurance_due], 
           [home_insurance_due], 
           [street2], 
           [policy_start_date], 
           [agree_contact], 
           [agree_terms], 
           [payment_detail_id], 
           [vfv_clinic], 
           [vfv_clinicid], 
           [referrerid], 
           [clientipaddress], 
           [schedule_doc], 
           [promo_code], 
           0, 
           [created_systuser_id], 
           [modified_systuser_id], 
           [converted_systuser_id], 
           [min_cover_ordinal], 
           [opt_out_marketing], 
           [agg_remote_ref], 
           @affinityCode, 
           [agree_preexisting_excl], 
           [agree_waiting_excl], 
           [agree_illvac_excl], 
           [agree_vicious_excl], 
           [enquiry_method_code], 
           [all_pet_names], 
           [confirm_excluded_breeds], 
           [agree_contact_phone], 
           [agree_contact_sms], 
           [agree_contact_email], 
           [agree_contact_letter], 
           [campaign_code], 
           [emailaddressconfirmation], 
           [is_multi_pet], 
           [agree_contact_mmpportal], 
           [sent_by_post], 
           [agreecoinsurance] 
    FROM   [dbo].[quote] 
    WHERE  id = @quoteIdToDuplicate; 

    SELECT @new_id = @@IDENTITY 

    SELECT @new_quote_ref = dbo.[Create_quote_ref](@new_id, @affinityCode) 

    -- Update the quote with the new ref 
    UPDATE [dbo].[quote] 
    SET    [quote_ref] = @new_quote_ref 
    WHERE  id = @new_id; 

    -- Look after quote pets 
    INSERT INTO [dbo].[quote_pet] 
                ([quote_id], 
                 [pet_name], 
                 [animal_type_id], 
                 [product_gender_id], 
                 [breed_id], 
                 [purchase_price], 
                 [chip_number], 
                 [colour], 
                 [pet_dob], 
                 [premium], 
                 [create_timestamp], 
                 [update_timestamp], 
                 [vet_visit], 
                 [incidcent], 
                 [incident], 
                 [microchip], 
                 [microchipnumber], 
                 [vetvisit], 
                 [vetvisitdetails], 
                 [accident], 
                 [accidentdetails], 
                 [exclusion], 
                 [promo_discount], 
                 [multipet_discount], 
                 [offer_discount], 
                 [exclusion_review], 
                 [not_working], 
                 [sick], 
                 [kfconfirm], 
                 [sickdetails], 
                 [owned], 
                 [kept_at_home], 
                 [not_breeding], 
                 [not_racing], 
                 [not_hunting], 
                 [neutered], 
                 [agg_cover_id], 
                 [ipt_absolute], 
                 [commission_affinity_absolute], 
                 [commission_administrator_absolute], 
                 [commission_underwriter_absolute], 
                 [credit_charge_absolute], 
                 [helpline_charge_absolute], 
                 [tpl_absolute], 
                 [pricing_engine_id], 
                 [vol_excess], 
                 [promo_discount_absolute], 
                 [multipet_discount_absolute], 
                 [offer_discount_absolute]) 
    SELECT @new_id, 
           [pet_name], 
           [animal_type_id], 
           [product_gender_id], 
           [breed_id], 
           [purchase_price], 
           [chip_number], 
           [colour], 
           [pet_dob], 
           [premium], 
           [create_timestamp], 
           [update_timestamp], 
           [vet_visit], 
           [incidcent], 
           [incident], 
           [microchip], 
           [microchipnumber], 
           [vetvisit], 
           [vetvisitdetails], 
           [accident], 
           [accidentdetails], 
           [exclusion], 
           [promo_discount], 
           [multipet_discount], 
           [offer_discount], 
           [exclusion_review], 
           [not_working], 
           [sick], 
           [kfconfirm], 
           [sickdetails], 
           [owned], 
           [kept_at_home], 
           [not_breeding], 
           [not_racing], 
           [not_hunting], 
           [neutered], 
           [agg_cover_id], 
           [ipt_absolute], 
           [commission_affinity_absolute], 
           [commission_administrator_absolute], 
           [commission_underwriter_absolute], 
           [credit_charge_absolute], 
           [helpline_charge_absolute], 
           [tpl_absolute], 
           [pricing_engine_id], 
           [vol_excess], 
           [promo_discount_absolute], 
           [multipet_discount_absolute], 
           [offer_discount_absolute] 
    FROM   [dbo].[quote_pet] 
    WHERE  [quote_id] = @quoteIdToDuplicate 

    IF EXISTS (SELECT 1 
               FROM   [dbo].[quote_optional_question] 
               WHERE  [quoteid] = @quoteIdToDuplicate) 
      BEGIN 
          -- Look after optional (SBS) questions 
          INSERT INTO [dbo].[quote_optional_question] 
                      ([quoteid], 
                       [coverforillness], 
                       [coverforongoingtreatment]) 
          SELECT @new_id, 
                 [coverforillness], 
                 [coverforongoingtreatment] 
          FROM   [dbo].[quote_optional_question] 
          WHERE  [quoteid] = @quoteIdToDuplicate 
      END 

    IF EXISTS (SELECT 1 
               FROM   [dbo].[aggregator] 
               WHERE  [quote_id] = @quoteIdToDuplicate) 
      BEGIN 
          -- Look after Aggregator stuff... 
          INSERT INTO [dbo].[aggregator] 
                      ([source], 
                       [affinity], 
                       [title], 
                       [forename], 
                       [surname], 
                       [postcode], 
                       [contact_num_day], 
                       [contact_num_eve], 
                       [email], 
                       [status_id], 
                       [create_timestamp], 
                       [update_timestamp], 
                       [policy_start_date], 
                       [client_ip_address], 
                       [promo_code], 
                       [quote_id], 
                       [lead_id], 
                       [date_of_birth], 
                       [house], 
                       [address1], 
                       [address2], 
                       [address3], 
                       [address4], 
                       [remote_reference], 
                       [suffix_id], 
                       [opt_out_marketing], 
                       [all_pet_names]) 
          SELECT [source], 
                 @affinityCode, 
                 [title], 
                 [forename], 
                 [surname], 
                 [postcode], 
                 [contact_num_day], 
                 [contact_num_eve], 
                 [email], 
                 [status_id], 
                 [create_timestamp], 
                 [update_timestamp], 
                 [policy_start_date], 
                 [client_ip_address], 
                 [promo_code], 
                 @new_id, 
                 [lead_id], 
                 [date_of_birth], 
                 [house], 
                 [address1], 
                 [address2], 
                 [address3], 
                 [address4], 
                 [remote_reference], 
                 [suffix_id], 
                 [opt_out_marketing], 
                 [all_pet_names] 
          FROM   [dbo].[aggregator] 
          WHERE  [quote_id] = @quoteIdToDuplicate 

          SELECT @new_agg_id = @@IDENTITY 

          SELECT @new_agg_quote_ref = dbo.[Create_aggregator_ref](@new_agg_id, 
                                      @affinityCode) 

          -- Update the quote with the new ref 
          UPDATE [dbo].[aggregator] 
          SET    [quote_ref] = @new_agg_quote_ref 
          WHERE  id = @new_agg_id; 
      END

	  SELECT @new_id Id;

SET NOCOUNT OFF
/****** Script for SelectTopNRows command from SSMS  ******/
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetBreakdown]'
GO


CREATE PROCEDURE [Pricing].[GetBreakdown]
	@breakdownId UNIQUEIDENTIFIER
AS
BEGIN

	SELECT  Id,
		    SourceId,
		    Identifier,
		    CoverId,
		    Premium,
		    PricingEngineId,
            SequenceId,
		    Breakdown,
		    Created,
            Uploaded,
			AssetID [AssetId]
            FROM uispet.Pricing.Breakdown
            WHERE id = @breakdownId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_petS_prc]'
GO



--quoteS_prc 1
CREATE PROCEDURE [dbo].[lead_petS_prc]  (@id INT)
		
AS

SET NOCOUNT ON


		SELECT	*
		FROM	lead_pet
		WHERE	id = @id


SET NOCOUNT OFF





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetBreakdownAssociations]'
GO

CREATE PROCEDURE [Pricing].[GetBreakdownAssociations]
AS
BEGIN

	SELECT BreakdownId,
            SourceId, 
            Identifier
    FROM Pricing.BreakdownAssociation

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_pricing_factorL_prc]'
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[product_pricing_factorL_prc]
	@affinity_code varchar(3),
	@date date,
	@applies_to varchar(2)
AS

SET NOCOUNT ON
	
	declare @product_id int;
	select top 1 @product_id = c.product_id
	from cover c
	where left(c.affinity_code,3) = @affinity_code;
	
	select @applies_to = ISNULL(@applies_to,'');
	
	select pf.*,
		   case when c.id is null then 'All' else c.affinity_code + ' - ' + c.description end cover_description,
		   case when at.id is null then 'All' else at.description end animal_type_description,
		   case when pf.factor = 0 then 'deactivate' else convert(varchar(10), convert(decimal(5,2), round((pf.factor - 1) * 100, 2))) + '%' end factor_percent,
		   case when isnull(pf.applies_to,'') = '' then 'All'
			    when pf.applies_to = 'NB' then 'New Business Only'
			    when pf.applies_to = 'R' then 'Renewal Only'
			    when pf.applies_to = 'M' then 'MTAs Only'
			    else 'Unknowwn' end applies_to_description
	from pricing_factor pf
		left join cover c on pf.cover_id = c.id
		left join animal_type at on pf.animal_type_id = at.id
	where isnull(pf.product_id, @product_id) = @product_id
		and (c.id is null or (c.id is not null and c.product_id = @product_id and left(c.affinity_code, 3) = @affinity_code and @date between c.cover_effective_date and isnull(c.cover_inactive_date, @date)))
		and @date between pf.factor_effective_date and ISNULL(pf.factor_inactive_date, @date)
		and ((@applies_to in ('', 'NB') and isnull(pf.applies_to,'') in('', 'NB')) or
			 (@applies_to in ('', 'R')  and isnull(pf.applies_to,'') in('', 'R')))
	order by pf.factor_effective_date desc, pf.animal_type_id, c.ordinal, pf.applies_to, pf.factor_inactive_date desc;
	
SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[UpdateBreakdownIdentifier]'
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Pricing].[UpdateBreakdownIdentifier]
	@identifier INT,
	@breakdownId UNIQUEIDENTIFIER
AS
BEGIN

	UPDATE Pricing.Breakdown
    SET Identifier = @identifier
    WHERE Id = @breakdownId

    SELECT @@ROWCOUNT

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breed_group]'
GO
CREATE TABLE [dbo].[breed_group]
(
[animal_type_id] [int] NOT NULL,
[breed_group_code] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL,
[description] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[ordinal] [tinyint] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_breed_group] on [dbo].[breed_group]'
GO
ALTER TABLE [dbo].[breed_group] ADD CONSTRAINT [PK_breed_group] PRIMARY KEY CLUSTERED  ([animal_type_id], [breed_group_code])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_pricing_matrixL_prc]'
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[product_pricing_matrixL_prc]
	@affinity_code varchar(3),
	@date date,
	@applies_to varchar(2),
	@sex char(1)
AS

SET NOCOUNT ON

	declare @product_id int;
	select top 1 @product_id = c.product_id
	from cover c
	where left(c.affinity_code,3) = @affinity_code;

	select  p.*,
			at.description animal_type_description,
		    bg.description breed_group_description,
			c.affinity_code + ' - ' + c.description cover_description,
			p.age age_description,
			'Zone&nbsp;' + CONVERT(varchar(10), p.area_rating) area_rating_description,
			p.value * dbo.get_pricing_factor(@date, @applies_to, @product_id, p.cover_id, p.animal_type_id, @sex, p.age, p.area_rating, p.breed_group_code, null, p.effective_date) as value_with_factors
	from pricing_matrix p
		join cover c on p.cover_id = c.id
		join animal_type at on p.animal_type_id = at.id
		join breed_group bg on isnull(p.breed_group_code,'') = bg.breed_group_code and p.animal_type_id = bg.animal_type_id
	where c.product_id = @product_id
		and left(c.affinity_code, 3) = @affinity_code
		and p.id = (select top 1 id
					from pricing_matrix pm
					where pm.cover_id = p.cover_id
						and pm.animal_type_id = p.animal_type_id
						and pm.age = p.age
						and ISNULL(pm.breed_group_code,'') 	= ISNULL(p.breed_group_code,'')
						and	area_rating	= p.area_rating
						and @date between pm.effective_date and ISNULL(pm.inactive_date, @date)
					order by pm.effective_date desc, id desc)
		and ((@applies_to = 'NB' and @date between c.cover_effective_date and isnull(c.cover_inactive_date, @date)) or
			 (@applies_to = 'R' and @date >= c.cover_effective_date))
	order by p.animal_type_id, p.age desc, bg.ordinal, c.cover_effective_date desc, c.ordinal, p.area_rating;
	
SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [TestDataExport].[GetAggregators]'
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [TestDataExport].[GetAggregators]
	@offset INT,
	@fetch INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[source]
      ,[affinity]
      ,[title]
      ,[forename]
      ,[surname]
      ,[quote_ref]
      ,[postcode]
      ,[contact_num_day]
      ,[contact_num_eve]
      ,[email]
      ,[status_id]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[policy_start_date]
      ,[client_ip_address]
      ,[promo_code]
      ,[quote_id]
      ,[lead_id]
      ,[date_of_birth]
      ,[house]
      ,[address1]
      ,[address2]
      ,[address3]
      ,[address4]
      ,[remote_reference]
      ,[suffix_id]
      ,[opt_out_marketing]
      ,[all_pet_names]
  FROM [uispet].[dbo].[aggregator]
  WHERE create_timestamp >= DATEADD(MONTH,-1,GETDATE())
  AND affinity IN
  ('AVZ',
'ASD',
'SLF',
'PTW',
'PAW',
'LAG',
'AAN',
'AAX',
'KFI',
'JBP',
'PUR',
'CPU',
'EPT',
'UIU',
'DON',
'ALU',
'BSF',
'ADU',
'GRU',
'FCE',
'VSA',
'BFG',
'GRN',
'VSR',
'HAS',
'ALB',
'FOR',
'PFL',
'PDP',
'CPQ',
'INT',
'VFV',
'ADM',
'ULT')
 
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;

  SELECT ap.[id]
      ,[aggregator_id]
      ,[pet_name]
      ,[animal_type]
      ,[sex]
      ,[breed_id]
      ,[purchase_price]
      ,[colour]
      ,[pet_DOB]
      ,ap.[create_timestamp]
      ,ap.[update_timestamp]
      ,[microchip]
      ,[original_breed_id]
      ,[vetvisit]
      ,[not_working]
      ,[sick]
      ,[owned]
      ,[kept_at_home]
      ,[not_breeding]
      ,[not_racing]
      ,[not_hunting]
      ,[neutered]
      ,[pricing_engine_id]
  FROM [uispet].[dbo].[aggregator_pet] ap
  WHERE ap.create_timestamp >= DATEADD(MONTH, -1, GETDATE())
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[DeleteBreakdownAssociation]'
GO

CREATE PROCEDURE [Pricing].[DeleteBreakdownAssociation]
	@breakdownId UNIQUEIDENTIFIER
AS
BEGIN

	DELETE FROM Pricing.BreakdownAssociation
    WHERE BreakdownId = @breakdownId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [TestDataExport].[GetLeads]'
GO
CREATE PROCEDURE [TestDataExport].[GetLeads]
	@offset INT,
	@fetch INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[product_id]
      ,[title_id]
      ,[forename]
      ,[surname]
      ,[house]
      ,[postcode]
      ,[telephone]
      ,[email_address]
      ,[lead_status_id]
      ,[contact_date]
      ,[contact_time]
      ,[clinic_id]
      ,[clinic_vet_name]
      ,[ip_address]
      ,[lead_reason_id]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[contact_attempt_count]
      ,[lead_failure_reason_id]
      ,[quote_id]
      ,[email_sent]
      ,[broker_referrer]
      ,[source]
      ,[mobile_telephone]
      ,[work_telephone]
      ,[lead_time_id]
  FROM [uispet].[dbo].[lead]
  WHERE create_timestamp >= DATEADD(MONTH, -1, GETDATE())
  AND source is null
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;

  SELECT lp.[id]
      ,[lead_id]
      ,[pet_name]
      ,[animal_type_id]
      ,[product_gender_id]
      ,[breed_id]
      ,[purchase_price]
      ,[colour]
      ,[pet_DOB]
      ,lp.[create_timestamp]
      ,lp.[update_timestamp]
  FROM [uispet].[dbo].[lead_pet] lp
  INNER JOIN [uispet].[dbo].[lead] l ON l.id = lp.lead_id
  WHERE l.create_timestamp >= DATEADD(MONTH, -1, GETDATE())
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetBreakdownLogEntriesForUpload]'
GO

-- With this release we also stop storing aggregator quotes

CREATE PROCEDURE [Pricing].[GetBreakdownLogEntriesForUpload]
AS
BEGIN

	SELECT TOP 10000
            Id,
		    SourceId,
		    Identifier,
		    CoverId,
		    Premium,
		    PricingEngineId,
            SequenceId,
		    Breakdown,
		    Created,
            Uploaded,
			AssetID [AssetId]
    FROM Pricing.Breakdown
    WHERE SourceId <> 4 -- Agg quotes are not uploaded as they are deleted
    AND Uploaded = 0
	AND Created < DATEADD(day, -1, GETDATE()) -- Only upload breakdowns once they are 24 hours old

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [TestDataExport].[GetQuote]'
GO
CREATE PROCEDURE [TestDataExport].[GetQuote]
    @quoteId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    SET NOCOUNT ON;

SELECT TOP 1000 [id]
      ,[product_id]
      ,[quote_ref]
      ,[title_id]
      ,[forename]
      ,[surname]
      ,[DOB]
      ,[postcode]
      ,[house_name_num]
      ,[street]
      ,[town]
      ,[county]
      ,[contact_num]
      ,[email]
      ,[quote_status_id]
      ,[cover_id]
      ,[premium]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[product_excess_id]
      ,[occupation_id]
      ,[hear_about_us_id]
      ,[other_policies]
      ,[motor_insurance_due]
      ,[home_insurance_due]
      ,[street2]
      ,[policy_start_date]
      ,[agree_contact]
      ,[agree_terms]
      ,[payment_detail_id]
      ,[VFV_clinic]
      ,[VFV_ClinicID]
      ,[ReferrerID]
      ,[ClientIpAddress]
      ,[schedule_doc]
      ,[promo_code]
      ,[email_sent]
      ,[created_systuser_id]
      ,[modified_systuser_id]
      ,[converted_systuser_id]
      ,[min_cover_ordinal]
      ,[opt_out_marketing]
      ,[agg_remote_ref]
      ,[affinity]
      ,[agree_preexisting_excl]
      ,[agree_waiting_excl]
      ,[agree_illvac_excl]
      ,[agree_vicious_excl]
      ,[enquiry_method_code]
      ,[all_pet_names]
      ,[confirm_excluded_breeds]
      ,[annual_premium]
      ,[agree_contact_phone]
      ,[agree_contact_SMS]
      ,[agree_contact_email]
      ,[agree_contact_letter]
      ,[campaign_code]
FROM[uispet].[dbo].[quote]
WHERE id = @quoteId
AND cover_id NOT IN (626,
627,
660,
661,
662,
663,
664,
630,
631,
632,
670,
671,
672,
673,
674,
616,
617,
618,
619,
620,
621
)

SELECT [id]
      ,[quote_id]
      ,[pet_name]
      ,[animal_type_id]
      ,[product_gender_id]
      ,[breed_id]
      ,[purchase_price]
      ,[chip_number]
      ,[colour]
      ,[pet_DOB]
      ,[premium]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[vet_visit]
      ,[cover_id]
      ,[incidcent]
      ,[incident]
      ,[microchip]
      ,[microchipnumber]
      ,[vetvisit]
      ,[vetvisitdetails]
      ,[accident]
      ,[accidentdetails]
      ,[exclusion]
      ,[promo_discount]
      ,[multipet_discount]
      ,[offer_discount]
      ,[exclusion_review]
      ,[not_working]
      ,[sick]
      ,[kfconfirm]
      ,[sickdetails]
      ,[owned]
      ,[kept_at_home]
      ,[not_breeding]
      ,[not_racing]
      ,[not_hunting]
      ,[neutered]
      ,[agg_cover_id]
      ,[IPT_absolute]
      ,[commission_affinity_absolute]
      ,[commission_administrator_absolute]
      ,[commission_underwriter_absolute]
      ,[credit_charge_absolute]
      ,[helpline_charge_absolute]
      ,[tpl_absolute]
      ,[pricing_engine_id]
      ,[vol_excess]
      ,[promo_discount_absolute]
      ,[multipet_discount_absolute]
      ,[offer_discount_absolute]
      ,[annual_premium]
  FROM [uispet].[dbo].[quote_pet]
  WHERE quote_id = @quoteId
  AND cover_id NOT IN (626,
627,
660,
661,
662,
663,
664,
630,
631,
632,
670,
671,
672,
673,
674,
616,
617,
618,
619,
620,
621
)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetCoInsuranceAndExcess]'
GO
--DECLARE @CoverId INT = 674,
--        @PetDOB DATETIME = '2019/01/01',
--        @PolicyStartDate DATETIME = GETDATE()


--DECLARE @affinity_code VARCHAR(4)

--SELECT @affinity_code = affinity_code FROM cover
--WHERE id = @CoverId

--SELECT * FROM get_coinsurance_and_excess (@affinity_code, @PetDOB, @PolicyStartDate)



CREATE PROCEDURE [dbo].[GetCoInsuranceAndExcess]
    @CoverId INT,
    @PetDOB DATETIME,
    @PolicyStartDate DATETIME
AS
BEGIN

    DECLARE @affinity_code VARCHAR(4)

    SELECT @affinity_code = affinity_code FROM cover
    WHERE id = @CoverId

    SELECT * FROM get_coinsurance_and_excess (@affinity_code, @PetDOB, @PolicyStartDate)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[SetBreakdownUploaded]'
GO

CREATE PROCEDURE [Pricing].[SetBreakdownUploaded]
	@breakdownId UNIQUEIDENTIFIER
AS
BEGIN

	UPDATE Pricing.Breakdown
    SET Breakdown = NULL,
        Uploaded = 1
    WHERE Id = @breakdownId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quoteS_prc]'
GO


CREATE PROCEDURE [dbo].[quoteS_prc]  (@id INT)
		
AS
SET NOCOUNT ON


		SELECT	q.id, 
				q.quote_ref,
				q.title_id,
				ISNULL(q.forename,'') as forename,
				ISNULL(q.surname,'') as surname,
				DATEPART(d, q.DOB) as DOB_dd,
				DATEPART(m, q.DOB) as DOB_mm,
				DATEPART(yyyy, q.DOB) as DOB_yyyy,
				ISNULL(q.postcode,'') as postcode,
				ISNULL(q.house_name_num,'') as house_name_num,
				ISNULL(q.street,'') as street ,
				ISNULL(q.street2,'') as street2 ,
				ISNULL(q.town,'') as town,
				ISNULL(q.county,'') as county,
				ISNULL(q.contact_num,'') as contact_num,
				ISNULL(q.email,'') as email,
				q.quote_status_id,
				q.cover_id,
				q.product_excess_id,
				q.occupation_id,
				q.hear_about_us_id,
				ISNULL(q.other_policies,'') as other_policies,
				q.motor_insurance_due,
				q.home_insurance_due,
				CONVERT(VARCHAR(11), q.policy_start_date,113) as policy_start_date,
				ISNULL(q.agree_contact, 0) as agree_contact,
				ISNULL(q.agree_contact_phone, ISNULL(q.agree_contact,0)) as agree_contact_phone,
				ISNULL(q.agree_contact_SMS, ISNULL(q.agree_contact,0)) as agree_contact_SMS,
				ISNULL(q.agree_contact_email, ISNULL(q.agree_contact,0)) as agree_contact_email,
				ISNULL(q.agree_contact_letter, ISNULL(q.agree_contact,0)) as agree_contact_letter,
				ISNULL(q.VFV_clinic,'') as VFV_clinic,
				ISNULL(q.VFV_clinicID,'') as VFV_clinicID,
				ISNULL(q.VFV_clinicID,'') as clinic_id,
				ISNULL(c.clinic_group_id,'') as clinic_group_id,
				q.agree_terms,
				q.promo_code,
				q.referrerID,
				q.agree_illvac_excl,
				q.agree_preexisting_excl,
				q.agree_vicious_excl,
				q.agree_waiting_excl,
				ISNULL(t.description,'') as title,
				p.description productdescription,
				q.product_id
		FROM	quote q
			left join clinic c on c.id = q.vfv_clinicID
			LEFT JOIN title t on q.title_id = t.id
			LEFT JOIN product p on p.id=q.product_id
		WHERE	q.id = @id

		select qp.id, ISNULL(qp.premium,0) premium, ISNULL(qpc.charity_id,0) charity_id
		from quote_pet qp
			LEFT JOIN quote_pet_charity qpc on qpc.quote_pet_id = qp.id
		where quote_id = @id

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [TestDataExport].[GetQuotes]'
GO
CREATE PROCEDURE [TestDataExport].[GetQuotes]
	@offset INT,
	@fetch INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[product_id]
      ,[quote_ref]
      ,[title_id]
      ,[forename]
      ,[surname]
      ,[DOB]
      ,[postcode]
      ,[house_name_num]
      ,[street]
      ,[town]
      ,[county]
      ,[contact_num]
      ,[email]
      ,[quote_status_id]
      ,[cover_id]
      ,[premium]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[product_excess_id]
      ,[occupation_id]
      ,[hear_about_us_id]
      ,[other_policies]
      ,[motor_insurance_due]
      ,[home_insurance_due]
      ,[street2]
      ,[policy_start_date]
      ,[agree_contact]
      ,[agree_terms]
      ,[payment_detail_id]
      ,[VFV_clinic]
      ,[VFV_ClinicID]
      ,[ReferrerID]
      ,[ClientIpAddress]
      ,[schedule_doc]
      ,[promo_code]
      ,[email_sent]
      ,[created_systuser_id]
      ,[modified_systuser_id]
      ,[converted_systuser_id]
      ,[min_cover_ordinal]
      ,[opt_out_marketing]
      ,[agg_remote_ref]
      ,[affinity]
      ,[agree_preexisting_excl]
      ,[agree_waiting_excl]
      ,[agree_illvac_excl]
      ,[agree_vicious_excl]
      ,[enquiry_method_code]
      ,[all_pet_names]
      ,[confirm_excluded_breeds]
      ,[annual_premium]
      ,[agree_contact_phone]
      ,[agree_contact_SMS]
      ,[agree_contact_email]
      ,[agree_contact_letter]
      ,[campaign_code]
  FROM [uispet].[dbo].[quote]
  WHERE create_timestamp >= DATEADD(MONTH,-1,GETDATE()) AND ReferrerID IS NULL
  AND (cover_id IS NULL OR cover_id NOT IN (626,
627,
660,
661,
662,
663,
664,
630,
631,
632,
670,
671,
672,
673,
674,
616,
617,
618,
619,
620,
621
))
  AND product_id IN (1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30,
31,
32,
33,
34,
36,
40)
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;

  SELECT qp.[id]
      ,[quote_id]
      ,[pet_name]
      ,[animal_type_id]
      ,[product_gender_id]
      ,[breed_id]
      ,[purchase_price]
      ,[chip_number]
      ,[colour]
      ,[pet_DOB]
      ,qp.[premium]
      ,qp.[create_timestamp]
      ,qp.[update_timestamp]
      ,[vet_visit]
      ,qp.[cover_id]
      ,[incidcent]
      ,[incident]
      ,[microchip]
      ,[microchipnumber]
      ,[vetvisit]
      ,[vetvisitdetails]
      ,[accident]
      ,[accidentdetails]
      ,[exclusion]
      ,[promo_discount]
      ,[multipet_discount]
      ,[offer_discount]
      ,[exclusion_review]
      ,[not_working]
      ,[sick]
      ,[kfconfirm]
      ,[sickdetails]
      ,[owned]
      ,[kept_at_home]
      ,[not_breeding]
      ,[not_racing]
      ,[not_hunting]
      ,[neutered]
      ,[agg_cover_id]
      ,[IPT_absolute]
      ,[commission_affinity_absolute]
      ,[commission_administrator_absolute]
      ,[commission_underwriter_absolute]
      ,[credit_charge_absolute]
      ,[helpline_charge_absolute]
      ,[tpl_absolute]
      ,[pricing_engine_id]
      ,[vol_excess]
      ,[promo_discount_absolute]
      ,[multipet_discount_absolute]
      ,[offer_discount_absolute]
      ,qp.[annual_premium]
  FROM [uispet].[dbo].[quote_pet] qp
  WHERE qp.create_timestamp >= DATEADD(MONTH, -1, GETDATE())
  AND qp.cover_id NOT IN (626,
627,
660,
661,
662,
663,
664,
630,
631,
632,
670,
671,
672,
673,
674,
616,
617,
618,
619,
620,
621
)
  ORDER BY id OFFSET @offset ROWS FETCH NEXT @fetch ROWS ONLY;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[charity]'
GO
CREATE TABLE [dbo].[charity]
(
[id] [int] NOT NULL,
[description] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[product_id] [int] NULL,
[active] [bit] NOT NULL CONSTRAINT [DF_charity_active] DEFAULT ((1)),
[ordinal] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_charity] on [dbo].[charity]'
GO
ALTER TABLE [dbo].[charity] ADD CONSTRAINT [PK_charity] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[charityL_prc]'
GO

CREATE PROCEDURE [dbo].[charityL_prc] (@product_id int)
AS
SET NOCOUNT ON

select id,description from charity
where product_id = @product_id and active = 1
order by ordinal, description

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Aggregator].[cover_configuration_override]'
GO
CREATE TABLE [Aggregator].[cover_configuration_override]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[aggregator_id] [int] NOT NULL,
[affinity] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[cover_id] [int] NOT NULL,
[excess] [decimal] (15, 5) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__cover_co__3213E83F8CC1D2B1] on [Aggregator].[cover_configuration_override]'
GO
ALTER TABLE [Aggregator].[cover_configuration_override] ADD CONSTRAINT [PK__cover_co__3213E83F8CC1D2B1] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Aggregator].[Aggregator]'
GO
CREATE TABLE [Aggregator].[Aggregator]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[agg_code] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__Aggregat__3213E83FFAA5E318] on [Aggregator].[Aggregator]'
GO
ALTER TABLE [Aggregator].[Aggregator] ADD CONSTRAINT [PK__Aggregat__3213E83FFAA5E318] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Aggregator].[GetAggregatorCoverConfigurationOverride]'
GO

CREATE PROCEDURE [Aggregator].[GetAggregatorCoverConfigurationOverride]
AS
BEGIN
	SET NOCOUNT ON

	SELECT a.agg_code, cco.aggregator_id, cco.affinity, cco.cover_id, c.description, c.cover_effective_date, c.isCoInsurance, cco.excess
	FROM Aggregator.cover_configuration_override cco
	INNER JOIN Aggregator.Aggregator a ON a.id = cco.aggregator_id
	INNER JOIN dbo.cover c ON cco.cover_id = c.id
	ORDER BY a.agg_code, cco.affinity, cco.cover_id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[DeleteBreakdown]'
GO

CREATE PROCEDURE [Pricing].[DeleteBreakdown]
	@breakdownId UNIQUEIDENTIFIER
AS
BEGIN

	DELETE FROM Pricing.Breakdown
    WHERE Id = @breakdownId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [TestDataExport].[GetLead]'
GO
CREATE PROCEDURE [TestDataExport].[GetLead]
	@leadId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
      ,[product_id]
      ,[title_id]
      ,[forename]
      ,[surname]
      ,[house]
      ,[postcode]
      ,[telephone]
      ,[email_address]
      ,[lead_status_id]
      ,[contact_date]
      ,[contact_time]
      ,[clinic_id]
      ,[clinic_vet_name]
      ,[ip_address]
      ,[lead_reason_id]
      ,[create_timestamp]
      ,[update_timestamp]
      ,[contact_attempt_count]
      ,[lead_failure_reason_id]
      ,[quote_id]
      ,[email_sent]
      ,[broker_referrer]
      ,[source]
      ,[mobile_telephone]
      ,[work_telephone]
      ,[lead_time_id]
  FROM [uispet].[dbo].[lead]
  WHERE id = @leadId;

  SELECT lp.[id]
      ,[lead_id]
      ,[pet_name]
      ,[animal_type_id]
      ,[product_gender_id]
      ,[breed_id]
      ,[purchase_price]
      ,[colour]
      ,[pet_DOB]
      ,lp.[create_timestamp]
      ,lp.[update_timestamp]
  FROM [uispet].[dbo].[lead_pet] lp
  WHERE lead_id = @leadId;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[error_listL_prc]'
GO

CREATE PROCEDURE [dbo].[error_listL_prc]
AS

SET NOCOUNT ON

select top 10 error_page,
			error_type,
			error_message,
			convert(varchar(11),time_stamp,113) as update_time
	from error_tracker el
	order by el.id desc

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[DeleteAllBreakdownsForSource]'
GO

CREATE PROCEDURE [Pricing].[DeleteAllBreakdownsForSource]
	@sourceId INT
AS
BEGIN

	DELETE FROM Pricing.Breakdown
    WHERE SourceId = @sourceId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DeleteHeartbeatTestpersonQuotes]'
GO
-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 Remove test quotes which are more than 50% in the live environment. Will be run by a scheduled job
-- =============================================

CREATE PROCEDURE [dbo].[DeleteHeartbeatTestpersonQuotes]
(
	@Debug BIT = 0
)
AS
BEGIN

DECLARE @Max_Rows INT
DECLARE @DeletedTotal_Rows INT
DECLARE @Deleted_Rows INT
DECLARE @MaxQuoteId INT
DECLARE @MinQuoteId INT
DECLARE @CountTestRecords INT = 0
DECLARE @MaxTestRecord INT = 0
SET @Deleted_Rows = 1
SET @DeletedTotal_Rows = 0

	SELECT @CountTestRecords = COUNT(*), @MaxTestRecord=MAX(Id)
		FROM uispet..quote
		WHERE forename = 'Heartbeat'
		AND surname = 'Testperson'

	IF (@Debug =1)
	BEGIN
		SELECT @CountTestRecords 'Remaining at start'
	END

	IF (@CountTestRecords > 0)
	BEGIN
		  SELECT TOP 5000 id 
		  INTO #tempQuotesToRemove
		  FROM uispet..quote	  
			WHERE id <= @MaxTestRecord
			AND forename = 'Heartbeat'
			AND surname = 'Testperson'

		IF (@Debug =1)
		BEGIN
			SELECT * FROM #tempQuotesToRemove
		END

		--output get deleted range
		--SELECT @MaxQuoteId = MAX(id),  @MinQuoteId = MIN(id) FROM uispet..quote
		--WHERE id IN (SELECT id FROM #tempQuotesToRemove)

		DELETE FROM uispet..quote_pet
		WHERE quote_id IN (SELECT id FROM #tempQuotesToRemove)

		DELETE FROM uispet..quote_optional_question
		WHERE quoteid IN (SELECT id FROM #tempQuotesToRemove)
		
		DELETE FROM uispet..iv_decision_result
		WHERE quote_id IN (SELECT id FROM #tempQuotesToRemove)
				
		/*DELETE FROM uispet..BlacklistedQuote
		WHERE QuoteId IN (SELECT id FROM #tempQuotesToRemove)*/

		DELETE FROM uispet..quote
		WHERE id IN (SELECT id FROM #tempQuotesToRemove)
	
		SET @Deleted_Rows = @@ROWCOUNT;
		SET @DeletedTotal_Rows = @DeletedTotal_Rows + @Deleted_Rows
	
		IF (@Debug =1)
		BEGIN
			SELECT @MinQuoteId '@MinQuoteId', @MaxQuoteId '@MaxQuoteId', @Deleted_Rows '@Deleted_Rows', @DeletedTotal_Rows '@DeletedTotal_Rows'
		END

		DROP TABLE #tempQuotesToRemove

		IF (@Debug =1)
		BEGIN
			SELECT COUNT(*) 'Remaining at end'
			FROM uispet..quote
			WHERE forename = 'Heartbeat'
			AND surname = 'Testperson'
		END
	END
	ELSE
	BEGIN
		SELECT 'No test records'
	END


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_premiumS_prc]'
GO

CREATE PROCEDURE [dbo].[quote_premiumS_prc]  (@quote_id INT) 

AS

SET NOCOUNT ON



	SELECT	premium = '£' + convert(varchar(8), premium * 12),
			premium_monthly =  '£' + convert(varchar(8), convert(decimal(8,2), premium) ),
			premium_monthly_advance_2m =  '£' + convert(varchar(8), premium * 2),
			premium_val = convert(varchar(8), premium * 12),
			premium_monthly_val = convert(varchar(8), convert(decimal(8,2), premium) ),
			premium_monthly_advance_2m_val = convert(varchar(8), premium * 2),
			CONVERT(VARCHAR(256),payment_detail_id) as payment_detail_id
	FROM	quote	
	WHERE	id = @quote_id
	


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinicL_prc]'
GO
CREATE PROCEDURE [dbo].[clinicL_prc] (@product_id int)
AS

SET NOCOUNT ON


	SELECT 	id, 
			description
	FROM	clinic
	WHERE	status = 1
	AND product_id = @product_id
	ORDER BY description 



SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_prefixL_prc]'
GO


CREATE PROCEDURE [dbo].[product_prefixL_prc] (@leads_only bit = 0)

AS

SET NOCOUNT ON

if(@leads_only=1)
BEGIN
		SELECT 	distinct prod_prefix id,
		prod_prefix description
		FROM PRODUCT p 
			join lead l on l.product_id = p.id
		order by description
END
ELSE
BEGIN

	SELECT 	distinct prod_prefix id,
		prod_prefix description
	FROM 	PRODUCT
	order by description
END
	

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[report_data_dump_v2_prc]'
GO






CREATE PROCEDURE [dbo].[report_data_dump_v2_prc]
(@status varchar(12) = '', @affinity varchar(3) = '', @from smalldatetime = '2008-01-01', @to smalldatetime = '2030-12-31')
AS
BEGIN
	SET NOCOUNT ON;

IF(@from > @to) return

select @to = DATEADD(n,-1,DATEADD(d,1,@to))

select ISNULL(policynumber, quote_ref) collate Latin1_General_CI_AS quote_ref 
	,case when quote_status_id = 6 then 'Declined'
		when p.cancelleddate < @to then 'Cancelled'
		when p.policyid is not null then 'Policy' 
		 else 'Quote' end outcome
	,ISNULL(vvc.description,'') collate Latin1_General_CI_AS practice
	,ISNULL(c.description,'') collate Latin1_General_CI_AS product
	,replace(isnull(qp.premium,''), '0.00','') premium
	,LOWER(pet_name) collate Latin1_General_CI_AS [pet name]
	--,convert(varchar, q.create_timestamp, 103)  collate Latin1_General_CI_AS [when]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,case when p.policyid is not null then
		ISNULL(convert(varchar, q.update_timestamp, 103),'')  
	else '' end collate Latin1_General_CI_AS  [policy sold]
	

	,at.description collate Latin1_General_CI_AS [species]
	,pg.gender_desc collate Latin1_General_CI_AS [gender]
	,b.description collate Latin1_General_CI_AS [breed]
	,convert(int, qp.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') collate Latin1_General_CI_AS chip
	,ISNULL(qp.colour,'') collate Latin1_General_CI_AS colour
	,convert(varchar, qp.pet_dob, 103) collate Latin1_General_CI_AS [pet dob]
	,ISNULL(convert(varchar(255), qp.vetvisitdetails),'') collate Latin1_General_CI_AS vetvisit
	,ISNULL(convert(varchar(255), qp.accidentdetails),'') collate Latin1_General_CI_AS accident
	,ISNULL(p.exclusion,'') collate Latin1_General_CI_AS exclusion
	,ISNULL(t.description,'') collate Latin1_General_CI_AS title
	,ISNULL(q.forename,'') collate Latin1_General_CI_AS firstname
	,ISNULL(q.surname,'') collate Latin1_General_CI_AS lastname
	,ISNULL(convert(varchar, q.dob, 103),'') collate Latin1_General_CI_AS dob
	
	,q.house_name_num +' '+ISNULL(q.street,'') collate Latin1_General_CI_AS address1
	,q.street2 collate Latin1_General_CI_AS address2
	,q.town collate Latin1_General_CI_AS address3
	,q.county collate Latin1_General_CI_AS address4
	,'' address5
	,q.postcode collate Latin1_General_CI_AS postcode
	,ISNULL(q.contact_num,'') collate Latin1_General_CI_AS telephone
	,q.email collate Latin1_General_CI_AS email
	,convert(varchar, q.policy_start_date, 103) collate Latin1_General_CI_AS [policy start date]
	,ISNULL(convert(varchar, qp.offer_discount),'') collate Latin1_General_CI_AS offer
	,ISNULL(convert(varchar, qp.multipet_discount),'') collate Latin1_General_CI_AS multipet
	,ISNULL(cr.description,'') collate Latin1_General_CI_AS [cancel reason]
	,ISNULL(convert(varchar, cancelleddate, 103),'') collate Latin1_General_CI_AS [cancel date]
	,case when s.ip_address is null then 'Y' else 'N' end collate Latin1_General_CI_AS internet
	,case when CHARINDEX('vfv', q.promo_code)>0 then 'Y' else 'N' end collate Latin1_General_CI_AS staff
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') policyinceptiondate
from quote_pet qp join quote q on q.id = qp.quote_id
	join animal_type at on qp.animal_type_id = at.id
	join product_gender pg on pg.id = qp.product_gender_id
	join breed b on b.id = qp.breed_id
	left join uispetmis..policy p on qp.id = p.quote_pet_id
	left join cover c on qp.cover_id = c.id
	left join title t on t.id = q.title_id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join clinic vvc on vfv_clinicid = vvc.id
	LEFT JOIN sales_ip_address s on s.ip_address = q.ClientIpAddress
where 
q.create_timestamp > '1 mar 2009'
and q.quote_ref like @affinity+'%'
and 
(
	1 = case when @status = '' and quote_status_id <> 3 then 1 
				when @status = 'quote' and quote_status_id in (1,2,3,4) then 1 
				when @status = 'policy' then 0
				when @status = 'referral' and quote_status_id in (3,5,6) then 1
				when @status = 'cancelled' then 0
				when @status = 'declined' and quote_status_id = 6 then 1 
				when @status = 'lapsed' then 0
				when @status = 'renewed' then 0
	end
)
and q.create_timestamp between @from and @to
and ISNULL(ISNULL(cancelleddate, renewaldate),'2030-12-31') >= @from


UNION

----plus the policies (quote_pet_id may be null if imported)
select case when affinitycode like '123%' then 'OTT'+policynumber else policynumber end quote_ref
	,case when p.cancelleddate < @to then 'Cancelled'

		when floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0 
		THEN 'Renewal'+convert(varchar, floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12))
		
when renewaldate < @to AND renewaldate < getdate() then 'Lapsed'

		 else 'Policy' end outcome

	,ISNULL(vvc.description,'') practice
	,ISNULL(c.description,'') product
	,case when startdate > getdate() then 
		convert(varchar, uispetmis.dbo.get_premium_by_date(policyid, @to)) 
		else convert(varchar, uispetmis.dbo.get_premium_by_date(policyid, getdate())) end
		premium
	,LOWER(animalpetname) [pet name]
--	,case when p.cancelleddate between @from and @to then convert(varchar, p.cancelleddate, 103)
--			when @to between policyinceptiondate and startdate then convert(varchar, policyinceptiondate, 103)
--			else convert(varchar, startdate, 103)
--			end [when]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,ISNULL(convert(varchar, q.update_timestamp, 103),'')  collate Latin1_General_CI_AS [policy sold]

	,ISNULL(ISNULL(at.description, AnimalTypeName),'Dog') [animal]
	,p.animalsexname [gender]
	,breeddescription [breed]
	,convert(int, p.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') chip
	,ISNULL(p.colourdescription,'') colour
	,convert(varchar, p.animaldateofbirth, 103) [pet dob]
	,'' vetvisit
	,'' accident
	,ISNULL(p.exclusion,'') exclusion
	,p.title
	,ISNULL(p.firstname,'') firstname
	,ISNULL(p.lastname,'') lastname
	,'' dob

	,case when LEN(p.Address1)> 4 then p.address1 else p.address1+' '+p.address2 end address1
	,case when LEN(p.Address1)> 4 then p.address2 else ' ' end address2
	,p.address3
	,p.address4
	,p.address5
	,p.postcode
	,ISNULL(p.telephone1,'') telephone
	,ISNULL(email_address,'') emailaddress
	,case when startdate > @to 
		then convert(varchar, DATEADD(yy, -floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12), startdate), 103)
	else
		convert(varchar, startdate, 103) 
	end [policy start date]


	,case when convert(float, ISNULL(prem.offer_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.offer_discount),'') end offer
	,case when convert(float, ISNULL(prem.multipet_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.multipet_discount),'') end multipet
	--,ISNULL(convert(varchar, prem.multipet_discount),'') multipet
	,ISNULL(cr.description,'') [cancel reason]
	--,ISNULL(convert(varchar, cancelleddate, 103),'') [cancel date]
	,case when p.cancelleddate < @to then convert(varchar, p.cancelleddate, 103) else '' end [cancel date]
	,case when ISNULL(q.ClientIpAddress,'') in (select ip_address from uispet..sales_ip_address) then 'N' else 'Y' end collate Latin1_General_CI_AS internet
	,case when CHARINDEX('vfv', ISNULL(q.promo_code,''))>0 then 'Y' else 'N' end collate Latin1_General_CI_AS staff
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') policyinceptiondate
from uispetmis..policy p
	left join animal_type at on p.animal_type_id = at.id
	left join cover c on p.cover_id = c.id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join clinic vvc on p.clinic_id = vvc.id
	left join quote_pet qp on p.quote_pet_id = qp.id
	left join quote q on q.id = qp.quote_id
	left JOIN uispetmis..LATEST_PREMIUM prem on p.policyid = prem.policy_id
	left join uispetmis..payment_detail pd on p.policyid = pd.policy_id
--where p.policynumber like @affinity+'%'
where p.affinitycode like @affinity+'%'
and p.affinitycode not like 'CC%' -- exclude caravan club

and 
(
	1 = case when @status = '' 
		and (
		(policyinceptiondate < DATEADD(n,-1,@to) AND renewaldate >= @from)
		OR (cancelleddate between @from and DATEADD(n,-1,@to))
		OR (renewaldate between @from and DATEADD(n,-1,@to))
		OR (floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0)
		) 
		THEN 1
				when @status = 'quote' then 0
				when @status = 'policy' and policyinceptiondate < @to and cancelleddate IS NULL and renewaldate >= @from then 1
				when @status = 'referral' then 0
				when @status = 'cancelled' and cancelleddate between @from and DATEADD(n,-1,@to) then 1
				when @status = 'declined' then 0
				when @status = 'lapsed' and renewaldate between @from and DATEADD(n,-1,@to) then 1
				when @status = 'renewed' and floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0 then 1
	end
)
order by 1

SET NOCOUNT OFF

END














GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BlacklistedLead]'
GO
CREATE TABLE [dbo].[BlacklistedLead]
(
[LeadId] [int] NOT NULL,
[UpdatedDateTime] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BlacklistedLead] on [dbo].[BlacklistedLead]'
GO
ALTER TABLE [dbo].[BlacklistedLead] ADD CONSTRAINT [PK_BlacklistedLead] PRIMARY KEY CLUSTERED  ([LeadId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwWhitelistedLead]'
GO




-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 View of whitelisted Leads excluding those in backlist table
-- =============================================
CREATE VIEW [dbo].[vwWhitelistedLead]
AS
	SELECT DISTINCT id, product_id, title_id, forename, surname, house, postcode, telephone, email_address, lead_status_id, 
	contact_date, contact_time, clinic_id, clinic_vet_name, ip_address, lead_reason_id, create_timestamp, 
	update_timestamp, contact_attempt_count, lead_failure_reason_id, quote_id, email_sent, broker_referrer, 
	source, mobile_telephone, work_telephone, lead_time_id

	--version3
	FROM uispet..Lead l
	LEFT JOIN uispet..BlacklistedLead bl ON l.id = bl.LeadId
	WHERE bl.LeadId IS NULL




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_email_queueL_prc]'
GO


CREATE PROCEDURE [dbo].[quote_email_queueL_prc]

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @today DATE = CONVERT(DATE, GETDATE());

	-- get all agg quote ids for last 13 months - everything else is irrelivant
	select * into #agg from (
		select quote_id, lead_id, quote_ref from
		aggregator where quote_id is not null or lead_id is not null
		union all
		select quote_id, lead_id, quote_ref from
		aggregator_archive where create_timestamp > DATEADD(MONTH, -13, GETDATE()) and (quote_id is not null or lead_id is not null)
	) as agg

	select id, quote_ref, create_timestamp, update_timestamp, [affinity], [email]
	into #quoteEmails
	from quote
	where 
	email_sent = 0
	and LEN(email) > 0
	and id in (select quote_id from quote_pet)
	and DATEDIFF(SECOND, update_timestamp, GETDATE()) > 300	--600 -- 10 minutes
	and quote_status_id in (1,2)
	--don't send if it's from the leads portal
	--and id not in (select quote_id from lead where quote_id is not null)
	--and ISNULL(agree_contact,0) = 0	-- if not declined contact -- ok for the sake if this quote	
	and id not in (select quote_id from lead where quote_id is not null and id in (select lead_id from #agg where lead_id is not null))	-- and source in ('AGGBEATQ','AGGCTM')))
	and id not in (select quote_id from #agg where quote_id is not null )	-- and source in ('AGGBEATQ','AGGCTM'))	
	
	--making sure we dont send out quote emails for policy conversion quotes (unless mis user forces it, which will picked up in the next statement)
	AND id NOT IN (SELECT QuoteId FROM uispetmis.Renewals.PolicyConversion)

	--We should not send the quote email to testing quotes, so lets use the QuoteListExclusion logic (same as used in [quote_listL_prc])
	and (ClientIpAddress not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 1) or ClientIpAddress is null)
	and surname not in (select Value from QuoteListExclusion where QuoteListExclusionTypeId = 4)

	union 

	select id, quote_ref, create_timestamp, update_timestamp, [affinity], [email]
	from quote where id IN (SELECT QuoteId FROM QuoteEmailQueue qeq
							inner join quote q on q.id = qeq.QuoteId
							where  quote_status_id in (1,2))
	order by id

	select top 100 * from #quoteEmails
		-- Temporary prophylactic measure to ensure that very old quotes are not re-emailed out as a result of something or someone updating the update_timestamp, as happened in September 2018
		WHERE DATEDIFF(DAY, create_timestamp, update_timestamp) <= 31
	drop table #quoteEmails



	--leads
	select top 10 l.id, a.quote_ref, l.create_timestamp, l.update_timestamp, l.source, email_address, p.affinity_code AffinityCode
	from lead l		
	join #agg a on a.lead_id = l.id
	join product p on p.id = l.product_id
	where ISNULL(email_sent,0) = 0
	and l.id in (select lead_id from lead_pet)
	and lead_status_id = 100
	and l.quote_id IS NULL
	and l.[source] not in ('GOCOMP')
	 -- used to disable this element of the query to stop automated emails on GoCompare leads

	 --5 day reminder
	SELECT TOP 20 id, quote_ref, create_timestamp, update_timestamp, surname, email_sent
	FROM quote
	WHERE email_sent = 1
	AND id IN (SELECT quote_id FROM quote_pet)
	AND 
	(DATEADD(DAY, 5, CONVERT(DATE, create_timestamp)) = @today)
		--OR DATEADD(DAY, 6, CONVERT(DATE, create_timestamp)) = @today 
		--OR DATEADD(DAY, 8, CONVERT(DATE, create_timestamp)) = @today)
	AND quote_status_id IN (1,2)
	--and ISNULL(agree_contact,0) = 0	-- if not declined contact -- ok for the sake if this quote
	AND LEFT(quote_ref, 3) IN ('GRU','ADU')
	AND id NOT IN (SELECT quote_id FROM lead WHERE quote_id IS NOT NULL AND id IN (SELECT lead_id FROM #agg WHERE lead_id IS NOT NULL))	-- and source in ('AGGBEATQ','AGGCTM')))
	AND id NOT IN (SELECT quote_id FROM #agg WHERE quote_id IS NOT NULL )	-- and source in ('AGGBEATQ','AGGCTM'))	
	--next clause added to prevent sending reminder email where products have changed
	AND EXISTS (SELECT 1 FROM cover WHERE product_id = quote.product_id AND cover_inactive_date IS NULL AND cover_effective_date <= quote.policy_start_date)
	ORDER BY id

	--11 month reminder
	SELECT TOP 5 id, quote_ref, create_timestamp, update_timestamp, surname, email_sent, policy_start_date
		FROM quote
		WHERE email_sent IN (1,5)
		AND id IN (SELECT quote_id FROM quote_pet)
		AND DATEADD(MONTH, 11, CONVERT(DATE, policy_start_date)) = @today
		AND quote_status_id = 4
		AND ISNULL(agree_contact,0) = 0	-- if not declined contact
		AND LEFT(quote_ref, 3) IN ('GRU','ADM','ADU')	
		AND id NOT IN (SELECT quote_id FROM lead WHERE quote_id IS NOT NULL AND id IN (SELECT lead_id FROM #agg WHERE lead_id IS NOT NULL))	-- and source in ('AGGBEATQ','AGGCTM')))
		AND id NOT IN (SELECT quote_id FROM #agg WHERE quote_id IS NOT NULL )	-- and source in ('AGGBEATQ','AGGCTM'))	
		AND 1 = 0 -- Disabled this for all brands: EOD-4270
		ORDER BY id

	DROP TABLE #agg

/*SD Removed Post Inception Docs as per UC1196*/
--	--post-inception emails
--	SELECT TOP 5 PolicyID policy_id, PolicyNumber policy_reference, email_address
--	FROM uispetmis..policy 
--	WHERE policynumber LIKE 'pur%' 
--	AND startdate BETWEEN DATEADD(WEEK, -6, GETDATE()) AND DATEADD(SECOND, -1, DATEADD(DAY, 1, DATEADD(WEEK, -6, GETDATE()))) 
--	AND startdate =  policyinceptiondate
--	AND ISNULL(email_address, '') != '' 
--	AND email_address  NOT LIKE '%ultimate%'
--	AND cancelleddate IS NULL
--	AND ISNULL(email_sent,0) & 1 = 0 --test for the post-inception bit
----and 1 = 0
--	ORDER BY policyinceptiondate

--	--pre-renewal emails
	SELECT TOP 5 PolicyID policy_id, PolicyNumber policy_reference, AffinityCode [affinity], email_address
	FROM uispetmis..policy p
	LEFT JOIN 
		(
			SELECT * FROM 
				(SELECT
					Source,
					Identifier,
					Method,
					Purpose,
					[Enabled], 
					Ordinal,
					[Domain],
					Username,
					Channel,
					Created,
					ROW_NUMBER() OVER (PARTITION BY identifier,Purpose ORDER BY Created DESC) RowNumber
				FROM
					uispetmis.Contact.Preference
				WHERE 
					Purpose = 'Contact'
					AND Method = 'Email'

				) a
			WHERE
				a.RowNumber = 1

		) cp ON cp.Identifier = p.policyid

	WHERE LEFT(policynumber,3) = 'LIV' --SD All other affinities have been removed from this UC-1196 IN ('PUR','PAW','UIU')
	AND renewaldate BETWEEN DATEADD(WEEK, 6, GETDATE()) AND DATEADD(SECOND, 1, DATEADD(DAY, 1, DATEADD(WEEK, 6, GETDATE()))) 
	AND ISNULL(email_address, '') != '' 
	--AND email_address  NOT LIKE '%ultimate%'
	AND cancelleddate IS NULL
	AND ISNULL(email_sent,0) & 2 = 0 --test for the post-inception bit
	AND cp.Enabled = 1
--and 1 = 0
	ORDER BY policyinceptiondate

	SET NOCOUNT OFF

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TEMP_BREED_CHECKER]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[TEMP_BREED_CHECKER]
(
	@product_id int,
	@animal_type_id int,
	@breed_id int,
	@date date
	
)
RETURNS char(1)
AS
BEGIN

declare @breed_group as char(1);
declare @breed_rating as int = 0;

	-- Declare the return variable here
	IF @animal_type_id = 1 and @product_id in (3,13,14,1,9,11,6,22)
    BEGIN
          SELECT      @breed_rating = rating 
          FROM  product_breed_score 
          WHERE product_id = case  
                      when @product_id = 3 then 103 
                      --when @product_id = 2 then 2
                      when @product_id IN (13,14) then 13
                      when @product_id IN (1,9) then 9
                      when @product_id IN (11) then 11
                      when @product_id IN (6,22) then 22  --DON has one change! now PDP too
                      --when @product_id in (12,18,28) then 18 
                      else 1 end
          AND         breed_id = @breed_id 
    END
	ELSE IF @animal_type_id = 2 and @product_id in (26) and @date >= '2013-10-31'		-- needed because PURx covers will stay on PURx (same underwriter) and on that date there were no pedigree/non-pedigree cats
	BEGIN
		select @breed_group = rating from product_breed_group
		where breed_id = @breed_id
		and product_id = 1
	END
	ELSE IF @animal_type_id = 2 and @product_id in (32)
	BEGIN
		select @breed_group = rating from product_breed_group
		where breed_id = @breed_id
		and product_id = 32
	END
	ELSE IF @animal_type_id = 2 and @product_id in (1,2,5,9,10,12,17,18,19,20,21,23,24,25,27,29,30) and @date >= '2014-04-17'
	BEGIN
		select @breed_group = rating from product_breed_group
		where breed_id = @breed_id
		and product_id = 32
	END
	ELSE IF @animal_type_id = 1
	BEGIN
		select @breed_group = rating from product_breed_group
		where breed_id = @breed_id
		and product_id = 
			CASE 
				  WHEN @product_id in (26,31) THEN 1 
				  WHEN @date < '2013-10-01' THEN 1
				  ELSE 32 END
	END
	ELSE --We don't care about breed of cats
	BEGIN
		SELECT @breed_rating = NULL
	END

	if @breed_rating > 0 
	begin
		select @breed_group =
			CASE @breed_rating
			when 1 then 'H'
			when 2 then 'L'
			when 3 then 'M'
			when 4 then 'S'
			when 5 then 'X'
			else 'U'
			end 
	end
	-- Return the result of the function
	RETURN @breed_group

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ReportBlacklist]'
GO
CREATE TABLE [dbo].[ReportBlacklist]
(
[FieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BlacklistedData] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwBlacklistedLead]'
GO
	
	CREATE VIEW [dbo].[vwBlacklistedLead]
	AS
	
	SELECT DISTINCT id, product_id, title_id, forename, surname, house, postcode, telephone, email_address, lead_status_id, contact_date, contact_time, clinic_id, clinic_vet_name, ip_address, lead_reason_id, create_timestamp, update_timestamp, contact_attempt_count, lead_failure_reason_id, quote_id, email_sent, broker_referrer, source, mobile_telephone, work_telephone, lead_time_id, '' [petName], ISNULL(tEmail.BlacklistedData,'') [EmailExclusionReason], ISNULL(tPostcode.BlacklistedData,'') [PostcodeExclusionReason], ISNULL(tForeName.BlacklistedData,'') [ForenameExclusionReason], ISNULL(tLastName.BlacklistedData,'') [LastnameExclusionReason], '' [PetnameExclusionReason], bl.UpdatedDateTime 'BlacklistedDate'
	FROM uispet..Lead l
		LEFT JOIN uispet..BlackListedLead bl ON l.id = bl.Leadid
		LEFT JOIN [UISPET].[dbo].[ReportBlacklist] tEmail ON tEmail.FieldName = 'email' AND l.email_address COLLATE DATABASE_DEFAULT LIKE '%' + tEmail.BlacklistedData + '%'
		LEFT JOIN [UISPET].[dbo].[ReportBlacklist] tPostcode ON tPostcode.FieldName = 'postcode' AND REPLACE(l.postcode, ' ', '') COLLATE DATABASE_DEFAULT LIKE REPLACE(tPostcode.BlacklistedData, ' ', '') + '%'	
		LEFT JOIN [UISPET].[dbo].[ReportBlacklist] tForeName ON tForeName.FieldName = 'forename' AND l.forename COLLATE DATABASE_DEFAULT LIKE tForeName.BlacklistedData + '%'
		LEFT JOIN [UISPET].[dbo].[ReportBlacklist] tLastName ON tLastName.FieldName = 'surname' AND l.surname COLLATE DATABASE_DEFAULT LIKE tLastName.BlacklistedData + '%'
		WHERE 

		--forename & surname 
		(tForeName.FieldName IS NOT NULL
		AND tLastName.FieldName IS NOT NULL)

		OR
		--forename or surname or petname
		((tForeName.FieldName IS NOT NULL
		OR tLastName.FieldName IS NOT NULL)
		AND
		--with email or postcode
		(tEmail.FieldName IS NOT NULL
		OR tPostcode.FieldName IS NOT NULL))
		
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[InsBlacklistedLead]'
GO


-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 Insert Leads with blacklist criteria into the blacklist table. Will be run by a scheduled job
-- =============================================
CREATE PROCEDURE [dbo].[InsBlacklistedLead]
(
	@ReProcessAll BIT = 0
)
AS
BEGIN

	DECLARE @MaxLeadId INT 
	DECLARE @ProcessedCount INT 
	DECLARE @TotalBlacklistedCount INT 

	IF (@ReProcessAll = 0)
	BEGIN
		SELECT @MaxLeadId = MAX(LeadId) FROM [BlacklistedLead]
	END

	INSERT INTO [BlacklistedLead](LeadId, UpdatedDateTime)
	SELECT DISTINCT q.id, GETDATE()
	FROM uispet..vwBlacklistedLead q
	LEFT JOIN [BlacklistedLead] bq ON q.Id = bq.LeadId 
	WHERE bq.LeadId IS NULL  --not already added
	AND (@MaxLeadId IS NULL OR q.Id > @MaxLeadId)

	SELECT @ProcessedCount = @@ROWCOUNT 
	SELECT @TotalBlacklistedCount = COUNT(*) FROM [BlacklistedLead]
	SELECT @MaxLeadId '@MaxLeadId', @ProcessedCount 'ProcessedCount', @TotalBlacklistedCount '@TotalBlacklistedCount'

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[report_data_dump_prc_debug]'
GO





CREATE PROCEDURE [dbo].[report_data_dump_prc_debug]
(@status varchar(12) = '', @affinity varchar(3) = '', @from smalldatetime = '2008-01-01', @to smalldatetime = '2030-12-31')
AS
BEGIN
	SET NOCOUNT ON;

IF(@from > @to) return

select @to = DATEADD(n,-1,DATEADD(d,1,@to))

select ISNULL(policynumber, quote_ref) collate Latin1_General_CI_AS [quote_ref]
	,case when quote_status_id = 6 then 'Declined'
		when p.cancelleddate < @to then 'Cancelled'
		when p.policyid is not null then 'Policy' 
		 else 'Quote' end [outcome]
	,ISNULL(cl.description,'') collate Latin1_General_CI_AS [practice]
	,ISNULL(cg.description,'') collate Latin1_General_CI_AS [vet group]
	,ISNULL(c.description,'') collate Latin1_General_CI_AS [product]
	,replace(isnull(qp.premium,''), '0.00','') [premium]
	,LOWER(pet_name) collate Latin1_General_CI_AS [pet name]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,case when p.policyid is not null then
		ISNULL(convert(varchar, q.update_timestamp, 103),'')  
	else '' end collate Latin1_General_CI_AS  [policy sold date]
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') [inception date]

	,convert(varchar, q.policy_start_date, 103) collate Latin1_General_CI_AS [policy start date]

	,ISNULL(convert(varchar, cancelleddate, 103),'') collate Latin1_General_CI_AS [cancellation date]
	,ISNULL(convert(varchar, p.cancellation_instruction_date, 103),'') [cancellation instruction date]
	,ISNULL(cr.description,'') collate Latin1_General_CI_AS [cancel reason]

	,'' [lapse effective date]
	,'' [lapse instruction date]

	,at.description collate Latin1_General_CI_AS [species]
	,pg.gender_desc collate Latin1_General_CI_AS [gender]
	,b.description collate Latin1_General_CI_AS [breed]
	,convert(int, qp.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') collate Latin1_General_CI_AS chip
	,ISNULL(qp.colour,'') collate Latin1_General_CI_AS colour
	,convert(varchar, qp.pet_dob, 103) collate Latin1_General_CI_AS [pet dob]
	,ISNULL(convert(varchar(255), qp.vetvisitdetails),'') collate Latin1_General_CI_AS vetvisit
	,ISNULL(convert(varchar(255), qp.accidentdetails),'') collate Latin1_General_CI_AS accident
	,ISNULL(p.exclusion,'') collate Latin1_General_CI_AS exclusion
	,ISNULL(t.description,'') collate Latin1_General_CI_AS title
	,ISNULL(q.forename,'') collate Latin1_General_CI_AS firstname
	,ISNULL(q.surname,'') collate Latin1_General_CI_AS lastname
	,ISNULL(convert(varchar, q.dob, 103),'') collate Latin1_General_CI_AS dob
	
	,q.house_name_num +' '+ISNULL(q.street,'') collate Latin1_General_CI_AS address1
	,q.street2 collate Latin1_General_CI_AS address2
	,q.town collate Latin1_General_CI_AS address3
	,q.county collate Latin1_General_CI_AS address4
	,'' address5
	,q.postcode collate Latin1_General_CI_AS postcode
	,ISNULL(q.contact_num,'') collate Latin1_General_CI_AS telephone
	,q.email collate Latin1_General_CI_AS email
	,ISNULL(convert(varchar, qp.offer_discount),'') collate Latin1_General_CI_AS offer
	,ISNULL(convert(varchar, qp.multipet_discount),'') collate Latin1_General_CI_AS multipet
	,case when s.ip_address is null then 'Y' else 'N' end collate Latin1_General_CI_AS internet
	,case when d.staff = 1 then 'Y' else 'N' end [staff]
from uispet..quote_pet qp 
	join uispet..quote q on q.id = qp.quote_id
	join uispet..animal_type at on qp.animal_type_id = at.id
	join uispet..product_gender pg on pg.id = qp.product_gender_id
	join uispet..breed b on b.id = qp.breed_id
	left join uispetmis..policy p on qp.id = p.quote_pet_id
	left join uispet..cover c on qp.cover_id = c.id
	left join uispet..title t on t.id = q.title_id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join uispet..clinic cl on vfv_clinicid = cl.id
	left join uispet..clinic_group cg on cg.id = cl.clinic_group_id
	LEFT JOIN uispet..sales_ip_address s on s.ip_address = q.ClientIpAddress
	left join uispet..discount d on d.code = q.promo_code

where q.create_timestamp > '1 mar 2009'
and q.quote_ref like @affinity+'%'
and 
(
	1 = case when @status = '' and quote_status_id <> 3 then 1 
				when @status = 'quote' and quote_status_id in (1,2,3,4) then 1 
				when @status = 'policy' then 0
				when @status = 'referral' and quote_status_id in (3,5,6) then 1
				when @status = 'cancelled' then 0
				when @status = 'declined' and quote_status_id = 6 then 1 
				when @status = 'lapsed' then 0
				when @status = 'renewed' then 0
	end
)
and q.create_timestamp between @from and @to
and ISNULL(ISNULL(cancellation_instruction_date, renewaldate),'2030-12-31') >= @from


UNION
----plus the policies (quote_pet_id may be null if imported)
select case when affinitycode like '123%' then 'OTT'+policynumber else policynumber end [quote_ref]
	,case 
		when p.cancelleddate < @to then 'Cancelled'
		when floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0 
			THEN 'Renewal'+convert(varchar, floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12))
		when renewaldate < @to AND renewaldate < getdate() then 'Lapsed'
		when policyinceptiondate > @to AND q.update_timestamp < getdate() then 'Pending'
		else 'Policy' end [outcome]

	,ISNULL(cl.description,'') [practice]
	,ISNULL(cg.description,'') collate Latin1_General_CI_AS [vet group]
	,ISNULL(c.description,'') [product]

	,ISNULL(case when startdate > @to then 
		convert(varchar, uispetmis.dbo.get_premium_by_date(policyid, startdate)) 
		else convert(varchar, ps.premium) end,0)
		[premium]

	,LOWER(animalpetname) [pet name]

	,ISNULL(convert(varchar, q.create_timestamp, 103),'')  collate Latin1_General_CI_AS [quote date]
	,ISNULL(convert(varchar, q.update_timestamp, 103),'')  collate Latin1_General_CI_AS [policy sold date]
	,ISNULL(convert(varchar, p.policyinceptiondate, 103),'') [inception date]

	,case when startdate > @to 
		then convert(varchar, DATEADD(yy, -floor(convert(float,DATEDIFF(mm, policyinceptiondate, startdate))/12), startdate), 103)
	else
		convert(varchar, startdate, 103) 
	end [policy start date]

	,case when p.cancelleddate < @to then convert(varchar, p.cancelleddate, 103) else '' end [cancellation date]
	,case when p.cancellation_instruction_date < @to then convert(varchar, p.cancellation_instruction_date, 103) else '' end
	,case when p.cancellation_instruction_date < @to then ISNULL(cr.description,'') else '' end [cancel reason]

	,case when cancelleddate is null and renewaldate < @to AND renewaldate < getdate() then ISNULL(convert(varchar, p.renewaldate, 103),'') else '' end [lapse effective date]
	,case when cancelleddate is null and renewaldate < @to AND renewaldate < getdate() then ISNULL(convert(varchar, p.renewaldate, 103),'') else '' end  [lapse instruction date]


	,ISNULL(ISNULL(at.description, AnimalTypeName),'Dog') [animal]
	,p.animalsexname [gender]
	,breeddescription [breed]
	,convert(int, p.purchase_price) [purchase price]
	,ISNULL(qp.microchipnumber,'') chip
	,ISNULL(p.colourdescription,'') colour
	,convert(varchar, p.animaldateofbirth, 103) [pet dob]
	,'' vetvisit
	,'' accident
	,ISNULL(p.exclusion,'') exclusion
	,p.title
	,ISNULL(p.firstname,'') firstname
	,ISNULL(p.lastname,'') lastname
	,'' dob

	,case when LEN(p.Address1)> 4 then p.address1 else p.address1+' '+p.address2 end address1
	,case when LEN(p.Address1)> 4 then p.address2 else ' ' end address2
	,p.address3
	,p.address4
	,p.address5
	,p.postcode
	,ISNULL(p.telephone1,'') telephone
	,ISNULL(email_address,'') emailaddress

	,case when convert(float, ISNULL(prem.offer_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.offer_discount),'') end offer
	,case when convert(float, ISNULL(prem.multipet_discount,0)) = 0 then '' else ISNULL(convert(varchar, prem.multipet_discount),'') end multipet
	,case when ISNULL(q.ClientIpAddress,'') in (select ip_address from uispet..sales_ip_address) then 'N' else 'Y' end collate Latin1_General_CI_AS internet
	,case when d.staff = 1 then 'Y' else 'N' end [staff]
from uispetmis..policy p
	left join uispet..animal_type at on p.animal_type_id = at.id
	left join uispet..cover c on p.cover_id = c.id
	left join uispetmis..cancellation_reason cr on cr.id = p.cancellation_reason_id
	left join uispet..clinic cl on p.clinic_id = cl.id
	left join uispet..clinic_group cg on cg.id = cl.clinic_group_id
	left join uispet..quote_pet qp on p.quote_pet_id = qp.id
	left join uispet..quote q on q.id = qp.quote_id
	left JOIN uispetmis..LATEST_PREMIUM prem on p.policyid = prem.policy_id
	--left join uispetmis..payment_detail pd on p.policyid = pd.policy_id
	left join uispetmis.dbo.premium_snapshot(@to) ps on ps.policy_id = p.policyid
	left join uispet..discount d on d.code = q.promo_code

where p.affinitycode like @affinity+'%'
and p.affinitycode not like 'CC%' -- exclude caravan club

and 
(
	1 = case when @status = '' 
		and (
		--policies sold
		(ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND q.update_timestamp between @from and DATEADD(n,-1,@to))	--policyinceptiondate < DATEADD(n,-1,@to) AND renewaldate >= @from)
		--cancelled
		OR (cancellation_instruction_date between @from and DATEADD(n,-1,@to))
		--renewals
		OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND startdate between @from and DATEADD(n,-1,@to)) and policyinceptiondate < startdate --renewaldate between @from and DATEADD(n,-1,@to))
		--lapsed
		OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND renewaldate between @from and DATEADD(n,-1,@to)) 
		--and policyinceptiondate < startdate --renewaldate between @from and DATEADD(n,-1,@to))

		--OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0)
		--OR (ISNULL(CancelledDate, '2030-01-01') > DATEADD(n,-1,@to) AND policyinceptiondate > @to and q.update_timestamp <= @to)
		) 
		THEN 1
				when @status = 'quote' then 0
				when @status = 'policy' and policyinceptiondate < @to and cancelleddate IS NULL and renewaldate >= @from then 1
				when @status = 'referral' then 0
				when @status = 'cancelled' and cancelleddate between @from and DATEADD(n,-1,@to) then 1
				when @status = 'declined' then 0
				when @status = 'lapsed' and renewaldate between @from and DATEADD(n,-1,@to) then 1
				when @status = 'renewed' and floor(convert(float,DATEDIFF(mm, policyinceptiondate, case when startdate < @to then startdate else @to end))/12) > 0 then 1
	end
)

order by 2

SET NOCOUNT OFF

END


















GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_emailU_prc]'
GO

CREATE PROCEDURE [dbo].[quote_emailU_prc] (@quote_id int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE QUOTE SET email_sent = 1 WHERE id = @quote_id

	exec [dbo].[SetQuoteEmailSent] @quoteId=@quote_id

	SET NOCOUNT OFF

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwBlacklistedQuote]'
GO


	
	CREATE VIEW [dbo].[vwBlacklistedQuote]
	AS

	SELECT DISTINCT q.id, product_id, quote_ref, title_id, forename, surname, DOB, postcode, house_name_num, street, town, county, contact_num, email, quote_status_id, q.cover_id, q.premium, q.create_timestamp, q.update_timestamp, product_excess_id, occupation_id, hear_about_us_id, other_policies, motor_insurance_due, home_insurance_due, street2, policy_start_date, agree_contact, agree_terms, payment_detail_id, VFV_clinic, VFV_ClinicID, ReferrerID, ClientIpAddress, NULL [schedule_doc], promo_code, email_sent, created_systuser_id, modified_systuser_id, converted_systuser_id, min_cover_ordinal, opt_out_marketing, agg_remote_ref, affinity, agree_preexisting_excl, agree_waiting_excl, agree_illvac_excl, agree_vicious_excl, enquiry_method_code, all_pet_names, confirm_excluded_breeds, q.annual_premium, agree_contact_phone, agree_contact_SMS, agree_contact_email, agree_contact_letter, campaign_code, qp.pet_name, ISNULL(tEmail.BlacklistedData,'') [EmailExclusionReason], ISNULL(tPostcode.BlacklistedData,'') [PostcodeExclusionReason], ISNULL(tForeName.BlacklistedData,'') [ForenameExclusionReason], ISNULL(tLastName.BlacklistedData,'') [LastnameExclusionReason], ISNULL(tPetName.BlacklistedData,'') [PetnameExclusionReason], bq.UpdatedDateTime 'BlacklistedDate'
	FROM uispet..Quote q
		LEFT JOIN uispet..BlackListedQuote bq ON q.Id = bq.QuoteId
		LEFT JOIN uispet..Quote_pet qp ON qp.quote_id = q.id
		LEFT JOIN [UISPet].[dbo].[ReportBlacklist] tEmail ON tEmail.FieldName = 'email' AND q.email COLLATE DATABASE_DEFAULT LIKE '%' + tEmail.BlacklistedData + '%'
		LEFT JOIN [UISPet].[dbo].[ReportBlacklist] tPostcode ON tPostcode.FieldName = 'postcode' AND REPLACE(q.postcode, ' ', '') COLLATE DATABASE_DEFAULT LIKE REPLACE(tPostcode.BlacklistedData, ' ', '') + '%'	
		LEFT JOIN [UISPet].[dbo].[ReportBlacklist] tForeName ON tForeName.FieldName = 'forename' AND q.forename COLLATE DATABASE_DEFAULT LIKE tForeName.BlacklistedData + '%'
		LEFT JOIN [UISPet].[dbo].[ReportBlacklist] tLastName ON tLastName.FieldName = 'surname' AND q.surname COLLATE DATABASE_DEFAULT LIKE tLastName.BlacklistedData + '%'
		LEFT JOIN [UISPet].[dbo].[ReportBlacklist] tPetName ON tPetName.FieldName = 'petname' AND qp.pet_name COLLATE DATABASE_DEFAULT LIKE tPetName.BlacklistedData + '%'
		WHERE 

		--forename & surname 
		(tForeName.FieldName IS NOT NULL
		AND tLastName.FieldName IS NOT NULL)

		OR
		--forename or surname or petname
		((tForeName.FieldName IS NOT NULL
		OR tLastName.FieldName IS NOT NULL
		OR tPetname.FieldName IS NOT NULL)
		AND
		--with email or postcode
		(tEmail.FieldName IS NOT NULL
		OR tPostcode.FieldName IS NOT NULL))

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[InsBlacklistedQuote]'
GO


-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 Insert Quotes with blacklist criteria into the blacklist table. Will be run by a scheduled job
-- =============================================
CREATE PROCEDURE [dbo].[InsBlacklistedQuote]
(
	@ReProcessAll BIT = 0
)
AS
BEGIN

	DECLARE @MaxQuoteId INT 
	DECLARE @ProcessedCount INT 
	DECLARE @TotalBlacklistedCount INT 

	IF (@ReProcessAll = 0)
	BEGIN
		SELECT @MaxQuoteId = MAX(QuoteId) FROM [BlacklistedQuote]
	END

	INSERT INTO [BlacklistedQuote](QuoteId, UpdatedDateTime)
	SELECT DISTINCT q.id, GETDATE()
	FROM uispet..vwBlacklistedQuote q
	LEFT JOIN [BlacklistedQuote] bq ON q.Id = bq.QuoteId 
	WHERE bq.QuoteId IS NULL  --not already added
	AND (@MaxQuoteId IS NULL OR q.Id > @MaxQuoteId)
	
	SELECT @ProcessedCount = @@ROWCOUNT 
	SELECT @TotalBlacklistedCount = COUNT(*) FROM [BlacklistedQuote]
	SELECT @MaxQuoteId '@MaxQuoteId', @ProcessedCount 'ProcessedCount', @TotalBlacklistedCount '@TotalBlacklistedCount'

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_email_contentS_prc]'
GO

CREATE PROCEDURE [dbo].[lead_email_contentS_prc]  (@lead_id int)
AS

SET NOCOUNT ON

declare @product_id int, @aggregator_id int, @source varchar(50)
select @product_id = product_id from lead where id = @lead_id
select @aggregator_id = id
	,@source = source
from aggregator where lead_id = @lead_id

declare @product_affinity_code varchar(3);
select @product_affinity_code=affinity_code from product where id=@product_id;

-- select the footer
declare @footer table(footer varchar(max), dual_footer varchar(max), footer_colour VARCHAR(7))
insert @footer(footer,dual_footer, footer_colour)
exec uispetmis..affinity_footer_selectorS_prc @product_affinity_code, @aggregator_id, 0;

select t.description+' '+ISNULL(dbo.PROPERCASE(q.forename),'')+' '+ISNULL(dbo.PROPERCASE(q.surname),'') owner
	,quote_ref
	,DATEADD(d,30, q.create_timestamp) date_expires
	,q.email_address email
	,email_subject = 
		case 
			when @product_id = 19 then 'Admiral Pet Insurance Quote. Quote Ref: ' + ISNULL(a.quote_ref, 'Not Supplied')
			else 'Pet Insurance Quote from ' + ISNULL(p.description,'') + '. Quote Ref: ' + ISNULL(a.quote_ref, 'Not Supplied')
		end
	,email_to = q.email_address
	,email_from = p.email
	,email_blind_cc = p.admin_email
	,'' footnotes
	,ISNULL(f.footer, '') footer
	,isnull(rl.letter_start,'') letter_start
	,isnull(rl.middle_table_title,'') middle_table_title
	,isnull(rl.letter_end,'') letter_end
	,rl.table_back_color
	,rl.table_fore_color
	,isnull(rl.signature, 'Sales Department') signature
	,isnull(rl.signature_image_url,'') signature_image_url
	,p.description prod_name
	,isnull(p.web_address,'') prod_web_address
	,case when p.id in(23,26,32) then '<p>We are constantly looking at ways we can improve our service and appreciate your feedback on your experience with our Company. It would be extremely helpful if you could spare 5 minutes to visit the link below to complete our online survey to help us improve not just your journey but that of our future customers.</p>' +
				case when p.id = 23 then
					'<p><a href="https://www.surveymonkey.com/s/uispetinsurancecustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/uispetinsurancecustomers</a></p>'
				when p.id = 26 then
					'<p><a href="https://www.surveymonkey.com/s/purelypetscustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/purelypetscustomers</a></p>'
				when p.id = 32 then
					'<p><a href="https://www.surveymonkey.com/s/pawsandclawscustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/pawsandclawscustomers</a></p>'
				end
			else '' end survey_para
	,phone.phone_affinity_customer_services cust_services_num
	,phone.phone_sales sales_num
from lead q 
	join title t on t.id = q.title_id 
	join product p on p.id = q.product_id
	join aggregator a on a.lead_id = q.id
	left join @footer f on 1=1
	left join uispetmis..report_letter rl on rl.id = uispetmis.dbo.get_report_letter_id_udf(LEFT(a.quote_ref,3), 1, DATEADD(dd, 0, DATEDIFF(dd, 0, a.policy_start_date)))
	CROSS APPLY uispetmis.dbo.get_telephone_numbers_by_affinity_udf(p.affinity_code) phone
where q.id = @lead_id;

if (@product_id = 25 and @source = 'GOCOMP')
begin
	exec cover_benefitL_prc @product_id = @product_id, @min_cover_ordinal = 2, @context = 'E'
end
else
begin
	exec cover_benefitL_prc @product_id, @context = 'E'
end


DECLARE @pet_count int
SELECT @pet_count = COUNT(id) FROM aggregator_pet WHERE aggregator_id = @aggregator_id
declare @promo_code nvarchar(10)
select @promo_code = promo_code from aggregator where id = @aggregator_id

DECLARE CURSOR1 CURSOR FOR
SELECT id FROM aggregator_pet WHERE aggregator_id = @aggregator_id

DECLARE @id int 

OPEN CURSOR1 
FETCH NEXT FROM  CURSOR1 INTO  @id
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
	IF (@@FETCH_STATUS <> - 2)

	exec quick_quote_calculateU_prc @id, @pet_count, @promo_code, @is_aggregator=1

	FETCH NEXT FROM CURSOR1 INTO @id
END 
CLOSE CURSOR1 
DEALLOCATE CURSOR1



SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[clinic_by_group_referrerL_prc]'
GO


CREATE PROCEDURE [dbo].[clinic_by_group_referrerL_prc] (@referrer_code varchar(10))
AS
BEGIN
	SET NOCOUNT ON;

	select c.id, c.description
	from clinic c
		join clinic_group cg on c.clinic_group_id = cg.id
	where cg.referrer_code = @referrer_code
	order by c.description
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_email_contentS_prc]'
GO




CREATE PROCEDURE [dbo].[quote_email_contentS_prc]  (@quote_id INT, @replaceSups bit = 0)
AS

SET NOCOUNT ON

declare @product_id int
select @product_id = product_id from quote where id = @quote_id

declare @product_affinity_code varchar(3);
select @product_affinity_code=affinity_code from product where id=@product_id;

-- select the footer
declare @footer table(footer varchar(max) NULL, dual_footer varchar(max) NULL,footer_colour VARCHAR(7) NULL)
insert @footer(footer,dual_footer,footer_colour)
exec uispetmis..affinity_footer_selectorS_prc @product_affinity_code, @quote_id, 1;

select t.description+' '+ISNULL(dbo.PROPERCASE(forename),'')+' '+ISNULL(dbo.PROPERCASE(surname),'') owner
	,quote_ref
	,DATEADD(d,30, q.create_timestamp) date_expires
	,q.email
	,email_subject = 
		case 
			when @product_id = 3 then 'Your Village Vet Total Care Pet Insurance Quote Ref: ' + ISNULL(q.quote_ref, 'Not Supplied')
			when @product_id = 19 then 'Admiral Pet Insurance Quote. Quote Ref: ' + ISNULL(q.quote_ref, 'Not Supplied')
			else 'Pet Insurance Quote from ' + ISNULL(p.description,'') + '. Quote Ref: ' + ISNULL(q.quote_ref, 'Not Supplied')
		end
	,email_to = q.email
	,email_from = p.email
	,email_blind_cc = p.admin_email
	--,p.cust_services_num
	,phone.phone_affinity_customer_services AS cust_services_num
	--,p.tel sales_num
	,phone.phone_sales AS sales_num
	,case 
		when q.product_id = 3 and policy_start_date >= '2010-10-20' then '<sup>1</sup> An excess of £79 is deducted from your benefit for each illness or accidental injury condition treated. If you use a non-Village Vet practice the excess is increased to £99.<br />For pets over the age of 8 years, a 15% co-payment will apply to each and every claim payment for Veterinary Fees, Special Diet and Complementary Medicine, which is in addition to the deduction of the applicable standard excess.<br /><sup>2</sup> A one off payment up to the Maximum Benefit Limit upon the Death of Your Pet<br /><sup>3</sup> An excess of £250 applies to Public Liability'
		when q.product_id = 3 then '<sup>1</sup> An excess of £69 is deducted from your benefit for each illness or accidental injury condition treated during the policy period. If you use a non-Village Vet practice the excess is increased to £89.<br />For pets over the age of 8 years, a 15% co-payment will apply to each claim payment for Veterinary Fees, Special Diet and Complementary Medicine, which is in addition to the deduction of the applicable standard excess.<br /><sup>2</sup> A one off payment up to the Maximum Benefit Limit upon the Death of Your Pet<br /><sup>3</sup> An excess of £250 applies to Public Liability'
		when q.product_id = 11 and cg.service_profile = 1 then 'Cover & Care, ' 
		when q.product_id = 6 and policy_start_date >= '2011-04-01' then '* An excess of £79 is deducted from your benefit for each illness or accidental injury condition treated. If you use a non-Pet Doctors practice the excess is increased to £99. For pets over the age of 8 years, a 15% co-payment will apply to each and every claim payment for Veterinary Fees, Special Diet and Complementary Medicine, which is in addition to the deduction of the applicable standard excess.<br />** A one off payment up to the Maximum Benefit Limit upon the Death of Your Pet<br />***A £250 excess applies once per claim for Public Liability cover.'
		when q.product_id = 6 then '* An excess of £49 is deducted from your benefit for each illness or accidental injury condition treated during the policy period. If you use a non-Pet Doctors practice the excess is increased to £69. For pets over the age of 8 years, a 15% co-payment will apply to each claim payment for Veterinary Fees, Special Diet and Complementary Medicine, which is in addition to the deduction of the applicable standard excess.<br />** A one off payment up to the Maximum Benefit Limit upon the Death of Your Pet<br />*** An excess of £250 applies to Public Liability'
		else '' end footnotes
	,ISNULL(f.footer, '') footer,
	f.footer_colour footer_colour,
	CASE WHEN @@SERVERNAME LIKE '%SQLEXPRESS%' THEN
						'https://sandbox.uispet.co.uk/services/v1.3.7/images/'+LEFT(@product_affinity_code,3)
								
			ELSE 
				'https://services.uispet.co.uk/production/v1.3.7/images/'+LEFT(@product_affinity_code,3)
			END image_url,
			@product_affinity_code affinity_code
	,t.description title
	,q.forename
	,q.surname
	,q.house_name_num
	,q.street
	,q.street2
	,q.town
	,q.county
	,q.postcode
	,isnull(rl.letter_start,'') letter_start
	,isnull(rl.middle_table_title,'') middle_table_title
	,isnull(rl.letter_end,'') letter_end
	,rl.table_back_color
	,rl.table_fore_color
	,isnull(rl.signature, 'Sales Department') signature
	,isnull(rl.signature_image_url,'') signature_image_url
	,p.description prod_name
	,isnull(p.web_address,'') prod_web_address
	,case when p.id in(23,26,32) then '<p>We are constantly looking at ways we can improve our service and appreciate your feedback on your experience with our Company. It would be extremely helpful if you could spare 5 minutes to visit the link below to complete our online survey to help us improve not just your journey but that of our future customers.</p>' +
				case when p.id = 23 then
					'<p><a href="https://www.surveymonkey.com/s/uispetinsurancecustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/uispetinsurancecustomers</a></p>'
				when p.id = 26 then
					'<p><a href="https://www.surveymonkey.com/s/purelypetscustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/purelypetscustomers</a></p>'
				when p.id = 32 then
					'<p><a href="https://www.surveymonkey.com/s/pawsandclawscustomers?c=q|||quote_id|||">https://www.surveymonkey.com/s/pawsandclawscustomers</a></p>'
				end
			else '' end survey_para
	, qp.vol_excess as voluntary_excess
from quote q 
	join title t on t.id = q.title_id 
	join product p on p.id = q.product_id
	left join clinic c on q.vfv_clinicid = c.id
	left join clinic_group cg on cg.id = c.clinic_group_id
	left join @footer f on 1=1
	left join uispetmis..report_letter rl on rl.id = uispetmis.dbo.get_report_letter_id_udf(LEFT(q.quote_ref,3), 1, DATEADD(dd, 0, DATEDIFF(dd, 0, q.policy_start_date)))
	CROSS APPLY uispetmis.dbo.get_telephone_numbers_by_affinity_udf(LEFT(q.quote_ref,3)) phone
	left join quote_pet qp on q.id = qp.quote_id
where q.id = @quote_id;

if (@product_id=3)
begin
	exec cover_benefit_vfvL_prc @quote_id
end
else if (@product_id in (11,12,18,21,23,25,26))
begin
	exec cover_benefitL_prc @product_id, @quote_id, NULL, @replaceSups, NULL, NULL, NULL, 'E'
end
else
begin
	exec cover_benefitL_prc @product_id, NULL, NULL, @replaceSups, NULL, NULL, NULL, 'E'
end

exec quote_calculateU_prc @quote_id = @quote_id


SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_emailU_prc]'
GO

CREATE PROCEDURE [dbo].[lead_emailU_prc] (@lead_id int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE LEAD SET email_sent = 1 WHERE id = @lead_id

	SET NOCOUNT OFF

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[helptext]'
GO
CREATE TABLE [dbo].[helptext]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[page_id] [int] NOT NULL,
[description] [varchar] (4000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__helptext__690797E6] on [dbo].[helptext]'
GO
ALTER TABLE [dbo].[helptext] ADD CONSTRAINT [PK__helptext__690797E6] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[helptextL_prc]'
GO

CREATE PROCEDURE [dbo].[helptextL_prc](@product_id INT, @page_id int = NULL, @quote_id int = NULL)
AS

SET NOCOUNT ON

	if(@page_id is null)
	begin
		SELECT 	page_id,
				description
		FROM	helptext
		where product_id = @product_id
		ORDER BY page_id 
	end
	else
	begin
		SELECT 	page_id,
				description
		FROM	helptext
		where product_id = @product_id
		and page_id = @page_id
	end

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_update_enquiry_method_prc]'
GO



CREATE PROCEDURE [dbo].[quote_update_enquiry_method_prc]  (
		@id int = NULL,
		@enquiry_method_code varchar(1) = NULL
) 
AS
SET NOCOUNT ON

	UPDATE [dbo].[quote]
	SET enquiry_method_code = @enquiry_method_code
	WHERE id = @id

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DeleteHeartbeatTestpersonAggArchiveQuotes]'
GO

-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 25/05/2018
-- Description:	UC-229/EOD-7652 Remove test agg quotes. 
--				In batches of 5000
--				Will be run by a scheduled job
-- =============================================

CREATE PROCEDURE [dbo].[DeleteHeartbeatTestpersonAggArchiveQuotes]
(
	@Debug BIT = 0
)
AS
BEGIN

DECLARE @Max_Rows INT
DECLARE @DeletedTotal_Rows INT
DECLARE @Deleted_Rows INT
DECLARE @MaxQuoteId INT
DECLARE @MinQuoteId INT
DECLARE @CountTestRecords INT = 0
DECLARE @MaxTestRecord INT = 0
SET @Deleted_Rows = 1
SET @DeletedTotal_Rows = 0

	SELECT @CountTestRecords = COUNT(*), @MaxTestRecord=MAX(Id)
		FROM uispet..Aggregator_Archive
		WHERE Forename IN ('Heartbeat', 'Test', 'Petlife')
			AND Surname IN ('Testpeson', 'Test', 'Confidence')
			AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')

	IF (@Debug =1)
	BEGIN
		SELECT @CountTestRecords 'Remaining at start', @MaxTestRecord 'MaxTestRecord'
	END

	IF (@CountTestRecords > 0)
	BEGIN
		  SELECT TOP 5000 id 
		  INTO #tempQuotesToRemove
		  	FROM uispet..Aggregator_Archive
			WHERE id <= @MaxTestRecord
			AND Forename IN ('Heartbeat', 'Test', 'Petlife')
			AND Surname IN ('Testpeson', 'Test', 'Confidence')
			AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')

		IF (@Debug =1)
		BEGIN
			SELECT * FROM #tempQuotesToRemove
		END

		--output get deleted range
		--SELECT @MaxQuoteId = MAX(id),  @MinQuoteId = MIN(id) FROM uispet..quote
		--WHERE id IN (SELECT id FROM #tempQuotesToRemove)

		DELETE FROM uispet..Aggregator_Pet_Archive
		WHERE aggregator_id IN (SELECT id FROM #tempQuotesToRemove)
		
		DELETE FROM uispet..Aggregator_Archive
		WHERE id IN (SELECT id FROM #tempQuotesToRemove)
	
		SET @Deleted_Rows = @@ROWCOUNT;
		SET @DeletedTotal_Rows = @DeletedTotal_Rows + @Deleted_Rows
	
		IF (@Debug =1)
		BEGIN
			SELECT @MinQuoteId '@MinQuoteId', @MaxQuoteId '@MaxQuoteId', @Deleted_Rows '@Deleted_Rows', @DeletedTotal_Rows '@DeletedTotal_Rows'
		END

		DROP TABLE #tempQuotesToRemove

		IF (@Debug =1)
		BEGIN
			SELECT COUNT(*) 'Remaining at end'
			FROM uispet..Aggregator_Archive
			WHERE Forename IN ('Heartbeat', 'Test', 'Petlife')
			AND Surname IN ('Testpeson', 'Test', 'Confidence')
			AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')
		END
	END
	ELSE
	BEGIN
		SELECT 'No test records'
	END


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[create_aggregator_ref1]'
GO

CREATE FUNCTION [dbo].[create_aggregator_ref1] (@aggregator_id int, @affinity varchar(3))  
RETURNS varchar(16) AS  
	BEGIN 

	declare @next int
	SELECT TOP 1 @next = a1.suffix_id + 1
	FROM aggregator a1
		left join aggregator a2 on a1.suffix_id + 1 = a2.suffix_id
	where a2.quote_ref IS NULL
	and a1.suffix_id is not null
	order by a1.suffix_id

		RETURN  UPPER(@affinity + CONVERT(varchar, @next))	--(9*@aggregator_id) + 10000001))
	END






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DeleteHeartbeatTestpersonAggQuotes]'
GO

-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 25/05/2018
-- Description:	UC-229/EOD-7652 Remove test agg quotes. 
--				In batches of 5000
--				Will be run by a scheduled job
-- =============================================

CREATE PROCEDURE [dbo].[DeleteHeartbeatTestpersonAggQuotes]
(
	@Debug BIT = 0
)
AS
BEGIN

DECLARE @Max_Rows INT
DECLARE @DeletedTotal_Rows INT
DECLARE @Deleted_Rows INT
DECLARE @MaxQuoteId INT
DECLARE @MinQuoteId INT
DECLARE @CountTestRecords INT = 0
DECLARE @MaxTestRecord INT = 0
SET @Deleted_Rows = 1
SET @DeletedTotal_Rows = 0

	SELECT @CountTestRecords = COUNT(*), @MaxTestRecord=MAX(Id)
		FROM uispet..aggregator
		WHERE Forename IN ('Heartbeat', 'Test', 'Petlife')
		AND Surname IN ('Testpeson', 'Test', 'Confidence')
		AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')

	IF (@Debug =1)
	BEGIN
		SELECT @CountTestRecords 'Remaining at start', @MaxTestRecord 'MaxTestRecord'
	END

	IF (@CountTestRecords > 0)
	BEGIN
		  SELECT TOP 5000 id 
		  INTO #tempQuotesToRemove
		  	FROM uispet..aggregator  
			WHERE id <= @MaxTestRecord
			AND Forename IN ('Heartbeat', 'Test', 'Petlife')
			AND Surname IN ('Testpeson', 'Test', 'Confidence')
			AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')

		IF (@Debug =1)
		BEGIN
			SELECT * FROM #tempQuotesToRemove
		END

		--output get deleted range
		--SELECT @MaxQuoteId = MAX(id),  @MinQuoteId = MIN(id) FROM uispet..quote
		--WHERE id IN (SELECT id FROM #tempQuotesToRemove)

		DELETE FROM uispet..aggregator_pet
		WHERE aggregator_id IN (SELECT id FROM #tempQuotesToRemove)
		
		DELETE FROM uispet..aggregator
		WHERE id IN (SELECT id FROM #tempQuotesToRemove)
	
		SET @Deleted_Rows = @@ROWCOUNT;
		SET @DeletedTotal_Rows = @DeletedTotal_Rows + @Deleted_Rows
	
		IF (@Debug =1)
		BEGIN
			SELECT @MinQuoteId '@MinQuoteId', @MaxQuoteId '@MaxQuoteId', @Deleted_Rows '@Deleted_Rows', @DeletedTotal_Rows '@DeletedTotal_Rows'
		END

		DROP TABLE #tempQuotesToRemove

		IF (@Debug =1)
		BEGIN
			SELECT COUNT(*) 'Remaining at end'
			FROM uispet..aggregator
			WHERE Forename IN ('Heartbeat', 'Test', 'Petlife')
			AND Surname IN ('Testpeson', 'Test', 'Confidence')
			AND (email IN ('test@Test.com', 'uptrends@test.com')
		    OR email LIKE '%emailreaction.org')

		END
	END
	ELSE
	BEGIN
		SELECT 'No test records'
	END


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CreateClinic]'
GO

create PROCEDURE [dbo].[CreateClinic]
@vetName VARCHAR(255),
@productId VARCHAR(255),
@clinicGroupId VARCHAR(255),
@misVetId VARCHAR(255)
AS
BEGIN
	DECLARE @nextClinicId AS INT;

	SET @nextClinicId = (SELECT MAX(id)+1 FROM [uispet].[dbo].[clinic]);

	INSERT INTO [uispet].[dbo].[clinic]
           ([id],[product_id],[description],[area_id],[status],[clinic_group_id],[claim_vet_id])
     VALUES
           (@nextClinicId,@productId,@vetName,0,1,@clinicGroupId,@misVetId);

	return @nextClinicId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[reminder_emailU_prc]'
GO

CREATE PROCEDURE [dbo].[reminder_emailU_prc] (@quote_id int, @reminder int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE QUOTE SET email_sent = @reminder WHERE id = @quote_id

	SET NOCOUNT OFF

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[claim_emailS_prc]'
GO

CREATE PROCEDURE [dbo].[claim_emailS_prc] (@claim_id INT)
AS

SET NOCOUNT ON

	DECLARE @email_subject VARCHAR(128)
	DECLARE @email_body	VARCHAR(8000)
	DECLARE @email_to	VARCHAR(64)
	DECLARE @email_blind_cc	VARCHAR(64)
	DECLARE @email_from VARCHAR(64)
	DECLARE	@email_header VARCHAR(8000)
	DECLARE	@pet_details VARCHAR(8000)
	DECLARE	@policy_details VARCHAR(8000)
	DECLARE	@cust_details	VARCHAR(8000)
	DECLARE	@price_details	VARCHAR(256)
	DECLARE	@price		MONEY
	DECLARE	@cover_required	VARCHAR(256)
	DECLARE @DD INT


			SELECT 	@email_subject = 'Pet Insurance Claim',
					@email_to	= q.email,
					@email_from = p.email,
					@email_blind_cc = p.admin_email,
					@email_body = 'Dear ' + ISNULL(t.description,'') + ' ' + ISNULL(q.surname,'') + ',' + CHAR(10) + CHAR(13) 
								+ 'Thank you for submitting a claim for your pet <pet name>' + ISNULL(p.description,'') + '.' + CHAR(10) 
								+ 'Should you wish to check on the progress of your claim, please log onto the website and enter your postcode and claim reference number.  Alternatively you can call us on <claims line number> to speak to a member of  our customer services team who will be happy to help you.' + CHAR(10) 
								+ 'Please remember that you will need to return a copy of your claim form which has been signed by the policy holder and the vet together with any receipts and documentation relevant to your claim.' + CHAR(10) + CHAR(13) 
								+ 'Your claim will be assessed by one of our veterinary nurses and you will be hearing from us in due course.' + CHAR(10) + CHAR(13) 
								+ 'Kind regards' + CHAR(10) + CHAR(13) 
								+ 'Claims Department' + CHAR(10) + CHAR(13) 
					
			FROM	quote q
			LEFT JOIN	title t 
				ON	t.id = q.title_id
			JOIN	product p 
				ON	p.id = q.product_id
			left JOIN	cover c 
				ON	c.id = q.cover_id
			WHERE	q.id = @claim_id



		SELECT	@email_subject as email_subject,
				@email_body as email_body,
				@email_to as email_to,
				@email_from as email_from,
				@email_blind_cc as email_blind_cc,
				@DD as DD

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[discount_fundL_prc]'
GO



CREATE PROCEDURE [dbo].[discount_fundL_prc]  

AS

SET NOCOUNT ON

	SELECT * FROM discount_fund

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[fnGetPostcodeArea]'
GO





-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 25/04/18
-- Description:	GDPR Get Postcode Area
-- =============================================
CREATE FUNCTION [dbo].[fnGetPostcodeArea]
(
	@Postcode VARCHAR(16)
)
RETURNS VARCHAR(4)
AS
BEGIN
	DECLARE @Underwriter VARCHAR(3)

	SELECT @Postcode =
		CASE
		 -- IF CHARINDEX(' ', LTRIM(@Postcode)) > 0 THEN DO THIS
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) > 5 THEN LEFT(LTRIM(@Postcode),4) 
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 2 THEN LEFT(LTRIM(@Postcode),1)  
		 WHEN CHARINDEX(' ', LTRIM(@Postcode))= 3 THEN LEFT(LTRIM(@Postcode),2) 
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 4 THEN LEFT(LTRIM(@Postcode),3) 
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 5 THEN LEFT(LTRIM(@Postcode),4) 

		 -- IF CHARINDEX(' ', LTRIM(@Postcode)) = 0 THEN DO THIS
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 0 AND LEN(LTRIM(@Postcode)) = 5 THEN LEFT(LTRIM(@Postcode),2) 
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 0 AND LEN(LTRIM(@Postcode)) = 6 THEN LEFT(LTRIM(@Postcode),3) 
		 WHEN CHARINDEX(' ', LTRIM(@Postcode)) = 0 AND LEN(LTRIM(@Postcode)) = 7 THEN LEFT(LTRIM(@Postcode),4) 

		 ELSE LEFT(@Postcode,4)  END 

	RETURN @Postcode

END



 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwWhitelistedQuote_Pricing]'
GO






-- =============================================
-- Author:		Yvonne Hawley
-- Create date: 06/07/2017
-- Description:	UC-229 View of whitelisted Quotes excluding those in backlist table
-- =============================================
CREATE VIEW [dbo].[vwWhitelistedQuote_Pricing]
AS
	SELECT DISTINCT q.id, product_id, quote_ref, DOB, uispet.dbo.fnGetPostcodeArea(postcode) as Postcodearea, town, county, 
	quote_status_id, q.cover_id, q.premium, q.create_timestamp, q.update_timestamp, product_excess_id, occupation_id, hear_about_us_id, other_policies, 
	motor_insurance_due, home_insurance_due, policy_start_date, agree_contact, agree_terms, payment_detail_id, VFV_clinic, VFV_ClinicID, 
	ReferrerID, ClientIpAddress, NULL [schedule_doc], promo_code, email_sent, created_systuser_id, modified_systuser_id, converted_systuser_id, 
	min_cover_ordinal, opt_out_marketing, agg_remote_ref, affinity, agree_preexisting_excl, agree_waiting_excl, agree_illvac_excl, 
	agree_vicious_excl, enquiry_method_code, all_pet_names, confirm_excluded_breeds, q.annual_premium, agree_contact_phone, agree_contact_SMS, 
	agree_contact_email, agree_contact_letter, campaign_code, EmailAddressConfirmation, is_multi_pet, agree_contact_mmpportal

	FROM uispet..Quote q
	LEFT JOIN uispet..BlacklistedQuote bq ON q.id = bq.QuoteId
	WHERE bq.QuoteId IS NULL


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwWhitelistedQuote_Pet_Pricing]'
GO


create VIEW [dbo].[vwWhitelistedQuote_Pet_Pricing]

as

select 

id,quote_id,pet_name,animal_type_id,product_gender_id,breed_id,purchase_price
,chip_number,colour,pet_DOB,premium,create_timestamp,update_timestamp,vet_visit
,cover_id,incidcent,incident,microchip,microchipnumber,vetvisit,vetvisitdetails
,accident,accidentdetails,exclusion,promo_discount,multipet_discount,offer_discount
,exclusion_review,not_working,sick,kfconfirm,sickdetails,owned,kept_at_home
,not_breeding,not_racing,not_hunting,neutered,agg_cover_id,IPT_absolute,commission_affinity_absolute
,commission_administrator_absolute,commission_underwriter_absolute,credit_charge_absolute
,helpline_charge_absolute,tpl_absolute,pricing_engine_id,vol_excess,promo_discount_absolute
,multipet_discount_absolute,offer_discount_absolute,annual_premium

from uispet..quote_pet qp

LEFT JOIN uispet..BlacklistedQuote bq ON qp.quote_id = bq.QuoteId
	WHERE bq.QuoteId IS NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_documentS_prc]'
GO





CREATE PROCEDURE [dbo].[product_documentS_prc] (
	@docid integer,
	@doctype varchar(4)
)
AS

SET NOCOUNT ON
	
IF @doctype = 'pw' OR @doctype = 'kf'
BEGIN
	SELECT 
		p.description + ' ' + c.description + 
		CASE 
			WHEN @doctype = 'pw' THEN ' Policy Wording' 
			WHEN @doctype = 'kf' THEN ' Key Facts'
		END doc_name,
		CASE 
			WHEN @doctype = 'pw' THEN ad.document 
			WHEN @doctype = 'kf' THEN ad.key_facts
		END blob
	FROM uispetmis..affinity_document ad
	JOIN cover c on ad.affinity_code = c.affinity_code COLLATE Latin1_General_CI_AS
	JOIN product p on c.product_id = p.id 
	WHERE ad.id = @docid 	
END
IF @doctype = 'ii'
BEGIN
	SELECT 
		p.description + ' ' + c.description + ' Important Information' doc_name,
		aii.important_information blob
	FROM uispetmis..affinity_ii aii
	JOIN cover c on aii.affinity_code = c.affinity_code COLLATE Latin1_General_CI_AS
	JOIN product p on c.product_id = p.id 
	WHERE aii.id = @docid
END
ELSE IF @doctype = 'toba'
BEGIN
	SELECT top 1 a.description + ' Terms of Business' doc_name, at.toba blob 
	FROM uispetmis..affinity_toba at 
	JOIN uispetmis..AFFINITY a on at.affinity_code = LEFT(a.affinitycode,3)
	WHERE at.id = @docid
END
ELSE IF @doctype = 'ddf' OR @doctype = 'ddg'
BEGIN
	SELECT top 1
		a.description + ' ' + 
		CASE 
			WHEN @doctype = 'ddf' THEN ' Direct Debit Form' 
			WHEN @doctype = 'ddg' THEN ' Direct Debit Guarantee'
		END doc_name,
		CASE 
			WHEN @doctype = 'ddf' THEN ad.dd_form 
			WHEN @doctype = 'ddg' THEN ad.dd_guarantee
		END blob
	FROM uispetmis..affinity_dd ad 
	JOIN uispetmis..AFFINITY a on ad.affinity_code = LEFT(a.affinitycode,3)
	WHERE ad.id = @docid
END
	
SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dd_service_organisationS_prc]'
GO

CREATE PROCEDURE [dbo].[dd_service_organisationS_prc]  
(
	@product_id int
)

AS
SET NOCOUNT ON

	select *
	from dbo.get_dd_service_organisation(@product_id, GETDATE());

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_documentL_prc]'
GO
SET QUOTED_IDENTIFIER OFF
GO





CREATE PROCEDURE [dbo].[product_documentL_prc] (
	@product_id AS INT,
	@quote_id AS INT = NULL,
	@all AS BIT = 1,
	@effective_date AS DATETIME = null
)
AS

SET NOCOUNT ON

	CREATE TABLE #selected_products (
		pet_id INT,
		pet_name VARCHAR(64),
		cover_id INT
	)
	
	DECLARE @missing_documents INT
	DECLARE @min_cover_ordinal INT
	
	-- If customer has got a quote, find out required inception date, otherwise default to today
	IF @quote_id is not null 
	BEGIN 
		-- Incase quote has been deleted
		IF EXISTS (SELECT 1 FROM quote WHERE id = @quote_id)
		BEGIN	
			SELECT 
				@effective_date = ISNULL(q.policy_start_date,CURRENT_TIMESTAMP),
				@min_cover_ordinal = 
					CASE 
						WHEN ISNULL(cg.service_profile,1) = 1 THEN 1
						ELSE 2
					END 
			FROM quote q 
				left join clinic c on q.vfv_clinicid = c.id
				left join clinic_group cg on c.clinic_group_id = cg.id
			WHERE q.id = @quote_id	
			
			INSERT INTO #selected_products
			SELECT 
				qp.id pet_id,
				qp.pet_name,
				qp.cover_id		
			FROM quote q 		
			JOIN quote_pet qp ON q.id = qp.quote_id 
			WHERE q.id = @quote_id	
		END
		ELSE SELECT @effective_date = ISNULL(@effective_date,CURRENT_TIMESTAMP)
	END
	ELSE SELECT @effective_date = ISNULL(@effective_date,CURRENT_TIMESTAMP)

	-- Lookup id for relevant pw/kf/toba/dd/cf documents based on product id and inception date.  
	SELECT c.id cover_id, c.[description] cover_description, ad.id pw_document_id, at.id toba_document_id, sp.pet_id, sp.pet_name--, affdd.id dd_document_id
	INTO #documents
	FROM cover c 
	LEFT JOIN uispetmis..affinity_document ad 
		ON  c.affinity_code = ad.affinity_code COLLATE Latin1_General_CI_AS  
		AND ad.valid_from = 
			(SELECT MAX(ad2.valid_from) FROM uispetmis..affinity_document ad2 
				WHERE ad.affinity_code = ad2.affinity_code AND ad2.valid_from <= @effective_date)
	LEFT JOIN uispetmis..affinity_toba at
		ON LEFT(c.affinity_code,3) = at.affinity_code COLLATE Latin1_General_CI_AS
		AND at.valid_from_date = 
			(SELECT MAX(at2.valid_from_date) FROM uispetmis..affinity_toba at2
				WHERE at.affinity_code = at2.affinity_code and at2.valid_from_date <= @effective_date)
	--LEFT JOIN uispetmis..affinity_dd affdd
	--	ON LEFT(c.affinity_code,3) = affdd.affinity_code COLLATE Latin1_General_CI_AS
	--	AND affdd.valid_from_date = 
	--		(SELECT MAX(affdd2.valid_from_date) FROM uispetmis..affinity_dd affdd2
	--			WHERE affdd.affinity_code = affdd2.affinity_code and affdd2.valid_from_date <= @effective_date)
	LEFT JOIN #selected_products sp ON sp.cover_id = c.id and @all = 0
	WHERE c.product_id = @product_id AND @effective_date BETWEEN c.cover_effective_date AND ISNULL(c.cover_inactive_date,'2050-01-01')
		AND (@all = 1 OR sp.pet_id IS NOT NULL)
		AND c.ordinal >= ISNULL(@min_cover_ordinal,1)
	ORDER BY c.ordinal
	
	IF (SELECT COUNT(*) FROM #documents WHERE pw_document_id IS NULL OR toba_document_id IS NULL) > 0 OR (SELECT COUNT(*) FROM #documents) = 0
	BEGIN
		DECLARE @error VARCHAR(255)
		SELECT @error = 'Documents missing - Product ID: ' + CONVERT(VARCHAR,@product_id) + ' Quote ID: ' + ISNULL(CONVERT(VARCHAR,@quote_id),'N/A')
		RAISERROR (@error, 10,1)
	END
	ELSE SELECT * FROM #documents
	
	DROP TABLE #selected_products, #documents

	
SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[max_benefitL_prc]'
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[max_benefitL_prc]
		(	
			@product_id INT
		)
AS

SET NOCOUNT ON

	SELECT 	affinity_code,
			vets_fees,
			claiming_limitation,
			excess,
			compl_alt_treatment,
			clinical_diet,
			death_illness_accident,
			boarding_kennels_catteries,
			travel_quarantine,
			replacement_healthcare_cert,
			travel_repeat_tick_worming,
			third_party_liability,
			quarantine,
			healthcare_certificate,
			flea_tick_worm
	FROM	max_benefit
	WHERE	product_id = @product_id
	ORDER BY affinity_code	

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[report_MI_Purely_Hourly_Sales]'
GO


--exec report_MI_Purely_Hourly_Sales

CREATE PROCEDURE [dbo].[report_MI_Purely_Hourly_Sales]
(
	@recipients VARCHAR(200) = 'dbraiden@markerstudy.com',
	@SendEmail BIT = 1
)
AS
BEGIN

	DECLARE @outdir NVARCHAR(200)
	DECLARE @Body NVARCHAR(250)

	SET @outdir = '\\markerstudy.local\data\Transfers\UltimateMISData\PROD\MIOutputs\PurelyPetsSales.csv'
--	SET @Body = 'See file: ' + @outdir
	SET @Body = 'Please find attached Purely Pets Hourly Sales'
	DECLARE @Subject VARCHAR(200) = 'Purely Pets Hourly Sales is complete from ' + @@servername

	DECLARE @profile_name VARCHAR(256) = 'UIS Mail Profile' ----UIS Mail Profile/UIS Mail Profile
	IF (@@servername = 'dc1pinfsql02') SELECT @profile_name = 'UIS Mail Profile'


--declare
declare
@DynamicPivotQuery AS NVARCHAR(MAX),
@PivotColumnNames AS NVARCHAR(MAX),
@PivotSelectColumnNames AS NVARCHAR(MAX)

--create dummy table for all hours and a total

create table #times (times varchar(20))

insert into #times
values
('00:00:00'),
('01:00:00'),
('02:00:00'),
('03:00:00'),
('04:00:00'),
('05:00:00'),
('06:00:00'),
('07:00:00'),
('08:00:00'),
('09:00:00'),
('10:00:00'),
('11:00:00'),
('12:00:00'),
('13:00:00'),
('14:00:00'),
('15:00:00'),
('16:00:00'),
('17:00:00'),
('18:00:00'),
('19:00:00'),
('20:00:00'),
('21:00:00'),
('22:00:00'),
('23:00:00'),
('Total')


--Get Sales by day rounded down to the nearest hour

select 

cast(q.update_timestamp as date) AS DATE 
,convert(varchar,dateadd(hour, datediff(hour, 0, q.update_timestamp), 0),108) 'rounded_hr'
,COUNT(*) AS 'SALES'
into #sales
from
uispet..vwwhitelistedquote q
join uispet..quote_pet qp on q.id = qp.quote_id
where q.quote_status_id = 3 
and cast(q.update_timestamp as date) 
	between '2019-08-19' and getdate() 
and q.product_id = 48
and (q.surname <> 'Test' AND q.postcode NOT IN ('PO2 8QL','PO2 8DE'))
GROUP BY 
cast(q.update_timestamp as date)
,convert(varchar,dateadd(hour, datediff(hour, 0, q.update_timestamp), 0),108)

ORDER BY 
cast(q.update_timestamp as date)
,convert(varchar,dateadd(hour, datediff(hour, 0, q.update_timestamp), 0),108)





--Get total sales by day and insert into above temp table to give summary total

insert into #sales
(date,rounded_hr,sales)



select 

cast(q.update_timestamp as date) AS DATE 
,'Total'
,COUNT(*) AS 'SALES'
from
uispet..vwwhitelistedquote q
join uispet..quote_pet qp on q.id = qp.quote_id
where q.quote_status_id = 3 
and cast(q.update_timestamp as date) 
	between '2019-08-19' and getdate() 
and q.product_id = 48
and (q.surname <> 'Test' AND q.postcode NOT IN ('PO2 8QL','PO2 8DE'))
GROUP BY 
cast(q.update_timestamp as date)


ORDER BY 
cast(q.update_timestamp as date)




select s.date,COALESCE(S.ROUNDED_HR,t.times) AS ROUNDED_HR,sum(ISNULL(s.sales,0)) as sales

INTO #TEMP
from #sales s
full outer join #times t

on  s.rounded_hr = t.times

group by s.date,COALESCE(S.ROUNDED_HR,t.times)





--Pivot to give dates as columns, times as rows, sales as measure

SELECT @PivotColumnNames= ISNULL(@PivotColumnNames + ',','')
+ QUOTENAME(Date)
FROM (SELECT DISTINCT Date FROM #temp) AS Date  order by date asc


SELECT @PivotSelectColumnNames 
    = ISNULL(@PivotSelectColumnNames + ',','')
    + 'ISNULL(' + QUOTENAME(Date) + ', 0) AS '
    + QUOTENAME(Date)
FROM (SELECT DISTINCT Date FROM #temp) AS Date  order by date asc

SET @DynamicPivotQuery =
N'SELECT rounded_hr, ' + @PivotSelectColumnNames + '
into ##temp
FROM #temp

PIVOT(SUM([Sales])
FOR Date IN (' + @PivotColumnNames + ')) AS PVTTable ORDER BY rounded_hr ASC
'
exec(@DynamicPivotQuery)




EXEC [UPPMIS].dbo.[proc_generate_csv_with_columns] 'UPPMIS','##temp','',@outdir,'','',1

	IF (@SendEmail = 1)
	BEGIN
		EXEC MSDB..sp_send_dbmail 		
			@profile_name = @profile_name,
			@recipients=@recipients,		
			@subject=@Subject,
			@body=@Body,
			@file_attachments = @outdir
	END



drop table #temp
drop table ##temp
drop table #times
drop table #sales


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breed_categoryL_prc]'
GO


CREATE PROCEDURE [dbo].[breed_categoryL_prc] (@animal_type_id int)
as
select * from breed_category where animal_type_id = @animal_type_id

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_failure_reason]'
GO
CREATE TABLE [dbo].[lead_failure_reason]
(
[id] [int] NOT NULL IDENTITY(200, 1),
[description] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[active] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_lead_failure_reason] on [dbo].[lead_failure_reason]'
GO
ALTER TABLE [dbo].[lead_failure_reason] ADD CONSTRAINT [PK_lead_failure_reason] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_failure_reasonL_prc]'
GO

CREATE PROCEDURE [dbo].[lead_failure_reasonL_prc]
AS

SET NOCOUNT ON
	
select * from lead_failure_reason where active = 1

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breed_searchL_prc]'
GO


CREATE PROCEDURE [dbo].[breed_searchL_prc] (@animal_type_id int, @product_id int, @search_text as varchar(50), @record_count as int)
as


	--inserting the results from the proc into a temp table
	declare @temp table
	(
		id int,
		description varchar(255)
	);

	--figuring out what is the breed category
	--as this proc does not have this info in the params, we will use the logic from the old version to retrieve it
	--this case we are assuming that this proc will be used just to perform searches on cat and dog pedigree
	declare @breedCategoryId int 
	set @breedCategoryId = case when @animal_type_id = 1 then 1 else 11 end

	INSERT @temp  
		Exec [breedL_prc] @animal_type_id =@animal_type_id, @product_id = @product_id, @breed_category_id = @breedCategoryId;


	--setting the limit of the query and passing the text to lower..
	set rowcount @record_count
	select @search_text = LOWER(@search_text)

	--selecting the breed by the provided text
	select * 
	from @temp t
	where LOWER([description]) like '%' + @search_text + '%';

--rolling back the limit of records
set rowcount 0


	--OLD PROC content - Begin
	--set rowcount @record_count
	--select @search_text = LOWER(@search_text)

	--if(@animal_type_id in (1,2))
	--BEGIN
			
	--	IF @product_id IN (1,5,9,10,12,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32)
	--	BEGIN
	--		SELECT breed_id id
	--			,LOWER(breed) AS description
	--		FROM product_breed_group pbg
	--			JOIN breed b on b.id = pbg.breed_id
	--		where product_id = CASE WHEN @product_id in (26,31) THEN 1 ELSE 32 END
	--		and rating in ('S','M','L','H','G','P','N')
	--		and LOWER(breed) like '%' + @search_text + '%'
	--		and b.breed_category_id = case when @animal_type_id = 1 then 1 else 11 end
	--		and animal_type_id = @animal_type_id 
	--		order by breed
	--	END
	--	ELSE
	--	BEGIN
	--		IF @product_id not in (3,6,11,13,14,22,103) --2
	--			SELECT @product_id = 1
	--		IF @product_id = 14
	--			SELECT @product_id = 13				

	--		IF(@animal_type_id = 1)
	--		BEGIN
	--			SELECT 	min(b.id) id, 
	--					LOWER(b.description) description
	--			FROM	breed b 
	--				join product_breed_score pbs
	--						ON b.id = pbs.breed_id
	--			WHERE	b.animal_type_id = 1
	--			AND pbs.product_id = @product_id
	--			AND pbs.rating BETWEEN 1 AND 4
	--			and LOWER(b.[description]) like '%' + @search_text + '%'
	--			and b.breed_category_id = case when @animal_type_id = 1 then 1 else 11 end
	--			and animal_type_id = @animal_type_id 
	--			GROUP BY  description
	--			ORDER BY description 
	--		END
	--		ELSE
	--		BEGIN
	--			SELECT 	min(id) id, 
	--					LOWER(description) description
	--			FROM	breed b
	--			WHERE	animal_type_id = @animal_type_id 
	--			and LOWER([description]) like '%' + @search_text + '%'
	--			and b.breed_category_id = case when @animal_type_id = 1 then 1 else 11 end
	--			GROUP BY  description
	--			ORDER BY description 
	--		END
	--	END
	--END
	--ELSE
	--BEGIN
	--	SELECT 	min(id) id, 
	--			LOWER(description) description
	--	FROM	breed b
	--	WHERE	animal_type_id = @animal_type_id 
	--	and LOWER([description]) like '%' + @search_text + '%'
	--	and b.breed_category_id = case when @animal_type_id = 1 then 1 else 11 end
	--	GROUP BY  description
	--	ORDER BY description 
	--END

	--set rowcount 0

--OLD PROC content - END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[blog]'
GO
CREATE TABLE [dbo].[blog]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[heading] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[blog] [ntext] COLLATE Latin1_General_CI_AS NULL,
[show] [bit] NOT NULL CONSTRAINT [DF_blog_show] DEFAULT ((1)),
[blogyear] [int] NULL,
[blogmonth] [char] (3) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_blog] on [dbo].[blog]'
GO
ALTER TABLE [dbo].[blog] ADD CONSTRAINT [PK_blog] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[blogL_prc]'
GO


CREATE PROCEDURE [dbo].[blogL_prc] (@affinity varchar(3), @latest bit = 0)
AS
BEGIN
	SET NOCOUNT ON;

	if(@latest=1)
		select top 1 * from blog where affinity = @affinity and show = 1 order by id desc
	else
		select * from blog where affinity = @affinity and show = 1 order by id desc

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_petU_prc]'
GO
CREATE PROCEDURE [dbo].[lead_petU_prc]  (
		@action			CHAR(1),
		@id				INT = NULL,
		@lead_id		INT = NULL,
		@pet_name		VARCHAR(64)	= NULL,
		@animal_type_id	INT	= NULL,
		@breed_id		INT	= NULL,
		@pet_DOB	date = NULL,
		@product_gender_id	 INT= NULL,
		@purchase_price	MONEY = NULL,
		@colour		varchar(32)= NULL
)		

AS
SET NOCOUNT ON

	SET DATEFORMAT DMY

	DECLARE @new_id int

	IF @action = 'X'
	BEGIN
		DELETE lead_pet WHERE id = @id
	END

	IF @action = 'I'
		BEGIN
			INSERT	lead_pet (
					lead_id,
					pet_name,
					animal_type_id,
					breed_id,
					pet_DOB,
					product_gender_id,
					purchase_price,
					colour
 ) 
			SELECT	@lead_id,
					@pet_name,
					@animal_type_id,
					@breed_id,
					@pet_DOB,
					@product_gender_id,
					@purchase_price,
					@colour

			SELECT @new_id = @@IDENTITY
						
			SELECT  @new_id as new_id,
					@pet_name as pet_name
			RETURN

		END 

	IF @action = 'U'
		BEGIN
			IF (@id IS NULL)
				BEGIN
					RAISERROR ('Incomplete Details', 10,1)
					RETURN
				END 
			ELSE
				BEGIN
					UPDATE	lead_pet
					SET		pet_name		= ISNULL(@pet_name, pet_name),
							animal_type_id	= ISNULL(@animal_type_id,animal_type_id),
							breed_id		= ISNULL(@breed_id, breed_id),
							pet_DOB			= ISNULL(@pet_DOB, pet_DOB),
							product_gender_id = ISNULL(@product_gender_id, product_gender_id),
							purchase_price	= ISNULL(@purchase_price,purchase_price),
							update_timestamp	= CURRENT_TIMESTAMP,
							colour			= isnull(@colour,colour)
					WHERE	id = @id 

				END 
			SELECT @lead_id = lead_id FROM lead_pet WHERE id=@id
		END 

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[productL_prc]'
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[productL_prc]
AS

SET NOCOUNT ON

	select id, prod_prefix + ' - ' + description description_alt,
	prod_prefix
	from product
	where id not in(3,4,6,7,8,11,13,14,22,28)
	order by description_alt;
	
SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_clinicsL_prc]'
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[lead_clinicsL_prc]
AS

SET NOCOUNT ON

	SELECT 	distinct c.id, c.description
	FROM clinic c
		join lead l on l.clinic_id = c.id
	ORDER BY description 



SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_reason]'
GO
CREATE TABLE [dbo].[lead_reason]
(
[id] [int] NOT NULL,
[description] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[product_id] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_lead_reason] on [dbo].[lead_reason]'
GO
ALTER TABLE [dbo].[lead_reason] ADD CONSTRAINT [PK_lead_reason] PRIMARY KEY CLUSTERED  ([id], [product_id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_reasonL_prc]'
GO
CREATE PROCEDURE [dbo].[lead_reasonL_prc] (@product_id int = 3)
AS
SET NOCOUNT ON

select id,description 
from lead_reason 
where product_id = @product_id
and description <> 'aggregator'
	
SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[testimonialS_prc]'
GO

CREATE PROCEDURE [dbo].[testimonialS_prc]  
(
	@id int
) 
AS

SET NOCOUNT ON

	select * from testimonial t
	where t.id = @id;


SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_statusL_prc]'
GO



CREATE PROCEDURE [dbo].[lead_statusL_prc]
AS
SET NOCOUNT ON

select * from lead_status
	

SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[testimonialU_prc]'
GO

CREATE PROCEDURE [dbo].[testimonialU_prc]  (
	@id int = null,
	@date date = null,
	@product_prefix  varchar(3) = null,
	@testimonial varchar(1000) = null,
	@testimonial_footer varchar(200)=null,
	@ordinal int = null,
	@active bit = null,
	@user_last_change_id int = null,
	@product_id int = null
) 
AS

SET NOCOUNT ON

if @product_id is not null
	select @product_prefix = p.prod_prefix from product p where p.id = @product_id;

if @date is null
	set @date = GETDATE();

if @id is not null
BEGIN
	
	declare @HasOrdinalChanged bit;
	declare @oldOrdinal int;
	select 
		@HasOrdinalChanged = case when t.ordinal = @ordinal then 0 else 1 end,
		@oldOrdinal = t.ordinal,
		@product_prefix = t.product_prefix
	from testimonial t
	where t.id = @id;
	
	
	-- adjusting the ORDINAL field
	if(@HasOrdinalChanged = 1)
	begin
		if @oldOrdinal < @ordinal
		begin
			-- it is decreasing the ordinal
			update testimonial
			set ordinal = ordinal -1
			where ordinal > @oldOrdinal and ordinal <= @ordinal
			and product_prefix = @product_prefix
			and id <> @id;
		end
		else
		begin
			-- it is increasing the ordinal
			update testimonial
			set ordinal = ordinal +1
			where ordinal < @oldOrdinal and ordinal >= @ordinal
			and product_prefix = @product_prefix;
		end
	end
	
	-- update
	update testimonial
	set date = ISNULL(@date, date),
	product_prefix = ISNULL(@product_prefix, product_prefix), 
	testimonial = ISNULL(@testimonial, testimonial), 
	testimonial_footer = ISNULL(@testimonial_footer, testimonial_footer),
	ordinal = ISNULL(@ordinal, ordinal),
	active = ISNULL(@active,active),
	user_last_change_id = ISNULL(@user_last_change_id, user_last_change_id),
	update_timestamp = CURRENT_TIMESTAMP
	where id = @id;
	
END
ELSE
BEGIN
	-- insert
	insert into testimonial(date,product_prefix, testimonial, testimonial_footer,ordinal,active,user_last_change_id,update_timestamp)
	values (@date,@product_prefix, @testimonial, @testimonial_footer, @ordinal, @active, @user_last_change_id, CURRENT_TIMESTAMP);
	
	declare @lastId int =  SCOPE_IDENTITY();
	
	-- adjusting the ORDINAL field
	update testimonial
	set ordinal = ordinal + 1
	where ordinal >= @ordinal
	and product_prefix = @product_prefix
	and id <> @lastId;
	
	select @lastId;
END



SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[testimonialX_prc]'
GO

CREATE PROCEDURE [dbo].[testimonialX_prc]  
(
	@id int
) 
AS

SET NOCOUNT ON

	declare @currentOrdinal int;
	declare @productPrefix varchar(3);
	
	select 
		@currentOrdinal = t.ordinal,
		@productPrefix = t.product_prefix
	from testimonial t where t.id = @id;

	delete from testimonial
	where id = @id;

	-- alter the ordinal
	if(@currentOrdinal is not null)
	begin
		update testimonial
		set ordinal = ordinal - 1
		where ordinal > @currentOrdinal
		and product_prefix = @productPrefix;
	
	end

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[postcodeS_prc]'
GO

CREATE PROCEDURE [dbo].[postcodeS_prc]  (@postcode varchar(10))
		
AS
SET NOCOUNT ON
		
		SELECT top 0 ISNULL(street,'') as street
				,ISNULL(street2,'') as street2
				,ISNULL(town,'') as town
				,ISNULL(county,'') as county
		FROM quote 
		WHERE ISNULL(street,'') <> ''
		AND REPLACE(postcode, ' ','') = REPLACE(@postcode, ' ', '')

SET NOCOUNT OFF




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[auditU_prc]'
GO


CREATE PROCEDURE [dbo].[auditU_prc]  (@lead_id int, @user_id int, @description nvarchar(4000) )
		
AS
SET NOCOUNT ON


INSERT INTO [dbo].[lead_audit]
           ([lead_id]
           ,[user_id]
           ,[description]
           ,[datetimestamp])
     VALUES
           (@lead_id
           ,@user_id
           ,@description
           ,CURRENT_TIMESTAMP)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_historyL_prc]'
GO



CREATE PROCEDURE [dbo].[lead_historyL_prc] @lead_id int
AS

SET NOCOUNT ON

	SELECT case when e.id is null then 'd'+convert(varchar, d.diaryid) else 'e'+convert(varchar, e.id) end collate Latin1_General_CI_AS id 
		,CreatedDate As DateTimeStamp
		,ISNULL(su.Forename + ' ' + su.Surname, 'System') collate Latin1_General_CI_AS As InteractiveUser
		,Description collate Latin1_General_CI_AS description
		,t.tracking_status_id
		,t.tracking_status_description
		,e.process_count [process_count]
	FROM uispetmis..DIARY d 
		LEFT JOIN uispetmis..systuser su on d.UserId = su.id
		LEFT JOIN uispetmis..EMAIL e on e.diary_id = d.diaryid
		CROSS APPLY (select * from uispetmis..get_email_tracking_info_udf(e.tracking_id)) t
	WHERE d.lead_id = @lead_id
UNION
	SELECT convert(varchar, d.id)
		,DateTimeStamp
		,ISNULL(su.Forename + ' ' + su.Surname, 'System') As InteractiveUser
		,Description
		,null [tracking_status_id]
		,null [tracking_status_description]
		,null [process_count]
	FROM lead_audit d 
		LEFT JOIN uispetmis..systuser su on d.user_id = su.id
	WHERE d.lead_id = @lead_id
	ORDER BY DateTimeStamp DESC
	

SET NOCOUNT OFF










GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_contact_failedU_prc]'
GO
CREATE PROCEDURE [dbo].[lead_contact_failedU_prc](@id int, @user_id int)
AS

SET NOCOUNT ON
	
	update lead set contact_attempt_count = contact_attempt_count + 1
	where id = @id

	insert lead_audit (lead_id, [user_id], description, datetimestamp)
	values (@id, @user_id, 'Failed attempt to contact owner', CURRENT_TIMESTAMP)

SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[renewal_premium_overrideX_prc]'
GO


CREATE PROCEDURE [dbo].[renewal_premium_overrideX_prc](@id int)
as
begin

		--get the prmium id that this override was used to create
		declare @premium_id int;
		select @premium_id = pr.id
		from renewal_premium_override rpo
			join uispetmis..PREMIUM pr on rpo.policy_id = pr.policy_id			
		where rpo.id = @id
		and renewal = 1;

		delete renewal_premium_override 
		where id = @id
		
		--select @premium_id  premium_id
		
		if(@premium_id IS NOT NULL)
		begin
			exec uispetmis..[premiumX_prc] @id = @premium_id
		end

end

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[lead_quoteU_prc]'
GO

CREATE PROCEDURE [dbo].[lead_quoteU_prc] @id int, @client_ip nvarchar(20), @user_id int, @enquiry_method_code char(1) = NULL
AS

SET NOCOUNT ON

declare @product_id int
	,@title_id int
	,@forename varchar(32)
	,@surname varchar(32)
	,@postcode varchar(16)
	,@house varchar(50)
	,@telephone varchar(32)
	,@email_address varchar(64)
	,@clinic_id int
	,@policy_start_date smalldatetime	--default policy start date to today
	,@referrer_id nvarchar(50)
	,@min_cover_ordinal int

select @product_id = l.product_id 
	,@title_id= l.title_id
	,@forename = l.forename
	,@surname = l.surname
	,@postcode = l.postcode
	,@house = l.house
	,@telephone = l.telephone
	,@email_address = l.email_address
	,@clinic_id = l.clinic_id
	,@policy_start_date = convert(varchar, getdate(),106)
	,@referrer_id = CASE
						WHEN (l.broker_referrer IS NOT NULL) THEN l.broker_referrer
						WHEN (a.source IS NOT NULL) THEN a.source
						ELSE ''
					END
	,@min_cover_ordinal = NULL
from LEAD l
	left join aggregator a on a.lead_id = l.id
where l.id = @id

declare @quote_record table (new_quote_ref varchar(16), new_id int, prod_prefix varchar(3), referrer varchar(50))

--if vetsure suppress the cover and care product by setting the min cover ordinal
if(@product_id=11)	--vetsure
begin
	declare @service_profile int;
	select @service_profile = service_profile
	from clinic_group cg
		join clinic c on c.clinic_group_id = cg.id
	where c.id = @clinic_id

	if(@service_profile=0)
		select @min_cover_ordinal=2
end


insert into @quote_record
exec [dbo].[quoteU_prc]
		@action='I'
		,@product_id = @product_id
		,@title_id = @title_id
		,@forename = @forename
		,@surname = @surname
		,@postcode = @postcode
		,@house_name_num = @house
		,@contact_num = @telephone
		,@email = @email_address
		,@clinic_id	= @clinic_id
		,@policy_start_date = @policy_start_date
		,@promo_code = ''
		,@client_ip = @client_ip
		,@created_systuser_id = @user_id
		,@referrer_id = @referrer_id
		,@min_cover_ordinal = @min_cover_ordinal
		,@enquiry_method_code = @enquiry_method_code

declare @quote_id int
	,@pet_name	VARCHAR(64)
	,@animal_type_id INT
	,@breed_id INT
	,@pet_DOB date
	,@product_gender_id INT
	,@purchase_price MONEY
	,@colour varchar(32)

select @quote_id = new_id from @quote_record
select @quote_id quote_id

--loop for pets
DECLARE CURSOR1 CURSOR FOR
SELECT pet_name, animal_type_id, breed_id, pet_dob, product_gender_id, purchase_price, colour
FROM lead_pet
WHERE lead_id = @id

OPEN CURSOR1 
FETCH NEXT FROM CURSOR1 INTO @pet_name,@animal_type_id,@breed_id,@pet_DOB,@product_gender_id,@purchase_price,@colour
WHILE (@@FETCH_STATUS <> - 1) 
BEGIN 
IF (@@FETCH_STATUS <> - 2)

	exec [dbo].[quote_petU_prc]
		@action='I'
		,@quote_id = @quote_id
		,@pet_name = @pet_name
		,@animal_type_id = @animal_type_id
		,@breed_id = @breed_id
		,@pet_DOB = @pet_DOB
		,@product_gender_id = @product_gender_id
		,@purchase_price	= @purchase_price
		,@colour = @colour

FETCH NEXT FROM CURSOR1 INTO @pet_name,@animal_type_id,@breed_id,@pet_DOB,@product_gender_id,@purchase_price,@colour
END 
CLOSE CURSOR1 
DEALLOCATE CURSOR1

SET NOCOUNT OFF







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[minder_prc]'
GO

CREATE PROCEDURE [dbo].[minder_prc] (@when bigint, @sid int, @field char(2), @exited bit = 0, @qref varchar(50))
AS

SET NOCOUNT ON

declare @ext int
select @ext = uispetmis.dbo.get_systuser_extension(@sid)

if(@exited=0)
begin
	insert into minder (started, ext, field, qref) values (@when, @ext, @field, @qref)
end
else
begin
	update minder 
	set ended = @when, duration = @when - started
	where qref = @qref 
	and field = @field 
	and ext = @ext
	and ended is null
end


SET NOCOUNT OFF



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[leadS_prc]'
GO


CREATE PROCEDURE [dbo].[leadS_prc]
           (@id int)
AS
SET NOCOUNT ON

declare @lead_product_id int
select @lead_product_id = product_id from lead where id = @id
select @lead_product_id = ISNULL(max(product_id),12) from lead_reason where product_id = @lead_product_id

	select l.* 
		,t.description+' '+l.forename+' '+l.surname owner
		,ls.description lead_status
		,lr.description lead_reason
		,ISNULL(q.quote_ref,'') quote_ref
		,ISNULL(c.description,'') clinic
		,CASE
			WHEN (l.broker_referrer IS NOT NULL) THEN l.broker_referrer
			WHEN (l.source IS NOT NULL) THEN l.source
			ELSE ''
		END as referrer
		,COALESCE(l.contact_time, lt.time_description) as lead_time_description
		,isnull(p.prod_prefix,'') as [affinity]
	from lead l
		LEFT JOIN title t on l.title_id = t.id
		JOIN lead_reason lr on l.lead_reason_id = lr.id and lr.product_id = @lead_product_id
		JOIN lead_status ls on l.lead_status_id = ls.id
		LEFT JOIN quote q on l.quote_id = q.id
		LEFT JOIN clinic c on l.clinic_id = c.id
		left join lead_time lt on l.lead_time_id = lt.id
		left join product p on p.id = l.product_id
	where l.id = @id

	SELECT	b.description breed, 
			qp.*,
			at.description animal,
			pg.gender_desc gender
	FROM lead_pet qp
	join breed b on b.id = qp.breed_id
	join animal_type at on at.id = qp.animal_type_id
	join product_gender pg on pg.id = qp.product_gender_id
	WHERE	lead_id = @id

SET NOCOUNT OFF





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cover_discountL_prc]'
GO

CREATE PROCEDURE [dbo].[cover_discountL_prc] (@cover_id int, @date datetime = NULL)
as
begin

IF @date IS NULL
	select @date = GETDATE()

select cd.id, d.code + ' (' + convert(varchar, cd.discount) + '%)' + case when d.staff = 1 then ' STAFF' else '' end description
from cover_discount cd 
	join discount d on d.id = cd.discount_id
where cover_id = @cover_id
AND @date BETWEEN ISNULL(d.start_date, '2010-01-01') and ISNULL(d.end_date, '2050-01-01')

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [RedGate].[MigrationHistory]'
GO
CREATE FUNCTION [RedGate].[MigrationHistory] ()
RETURNS @Tbl TABLE (PropertyKey VARCHAR(30) UNIQUE, PropertyValue NVARCHAR(MAX))
AS 
BEGIN
    
INSERT  @Tbl  VALUES  ('MigrationHistory' , N'[]')
    RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FromUtcToLocalTime]'
GO
CREATE FUNCTION [dbo].[FromUtcToLocalTime]
(
	@utcDate DATETIME
) RETURNS DATETIME As 
BEGIN

	DECLARE @timeZoneOffsetMinutes INT = DATEDIFF(mi, GETUTCDATE(), GETDATE());
	RETURN DATEADD(mi, @timeZoneOffsetMinutes, @utcDate) 

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ToProperCase]'
GO
CREATE FUNCTION [dbo].[ToProperCase](@string VARCHAR(255)) RETURNS VARCHAR(255)
AS
BEGIN
  DECLARE @i INT           -- index
  DECLARE @l INT           -- input length
  DECLARE @c NCHAR(1)      -- current char
  DECLARE @f INT           -- first letter flag (1/0)
  DECLARE @o VARCHAR(255)  -- output string
  DECLARE @w VARCHAR(10)   -- characters considered as white space

  SET @w = '[' + CHAR(13) + CHAR(10) + CHAR(9) + CHAR(160) + ' ' + ']'
  SET @i = 1
  SET @l = LEN(@string)
  SET @f = 1
  SET @o = ''

  WHILE @i <= @l
  BEGIN
    SET @c = SUBSTRING(@string, @i, 1)
    IF @f = 1 
    BEGIN
     SET @o = @o + @c
     SET @f = 0
    END
    ELSE
    BEGIN
     SET @o = @o + LOWER(@c)
    END

    IF @c LIKE @w SET @f = 1

    SET @i = @i + 1
  END

  RETURN @o
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[create_aggregator_ref2]'
GO



CREATE FUNCTION [dbo].[create_aggregator_ref2] (@aggregator_id int, @affinity varchar(3))  
RETURNS varchar(16) AS  
	BEGIN 

--	declare @next int
--	SELECT TOP 1 @next = a1.suffix_id + 1
--	FROM aggregator a1
--		left join aggregator a2 on a1.suffix_id + 1 = a2.suffix_id
--	where a2.quote_ref IS NULL
--	and a1.suffix_id is not null
--	order by a1.suffix_id

		RETURN  UPPER(@affinity + CONVERT(varchar, 10000000 + @aggregator_id))	--(9*@aggregator_id) + 10000001))
	END








GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[premium_floor]'
GO

CREATE FUNCTION [dbo].[premium_floor](@new_premium money, @policy_id int, @load int, @renewal_date datetime, @suppress_floor int)
RETURNS money
AS
BEGIN

	declare @premium money;
	select @premium = @new_premium;

	if @suppress_floor = 1
		return @premium;

	declare @load_val float;
	select @load_val = 	(100 + @load)/ convert(float, 100)

	declare @current_premium money;
	select @current_premium = uispetmis.dbo.get_premium_plus_imported_by_date(@policy_id, DATEADD(DAY, -1, @renewal_date));

	if(@current_premium > @new_premium)
	BEGIN
		select @premium = ROUND(@load_val * @current_premium,2);
	END
	
	--in all cases limit the premium to 200
	if(@premium > 200)
	BEGIN
		select @premium = 200;
	END
	
	return @premium;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[proper_case_string_hyphen]'
GO


CREATE FUNCTION [dbo].[proper_case_string_hyphen] 
(
	@string VARCHAR(128)
)  
RETURNS VARCHAR(128) AS  
	BEGIN 
		DECLARE @retval		VARCHAR(128)
		DECLARE @count		INT 
		DECLARE @count2		INT

		--Make everything lowercase to begin with.
		SET @string = REPLACE(LOWER(@string),'-','- ')
		
		SET @count = LEN(@string)
		SET @count2 = 1 

		SET @retval = UPPER(LEFT(@string, 1))
		
		WHILE @count > @count2			
			BEGIN
				
				IF SUBSTRING(@string,@count2 , 1) = ' ' 
					SET @retval = @retval + UPPER(SUBSTRING(@string,@count2 + 1  , 1))
				ELSE
					SET @retval = @retval + SUBSTRING(@string,@count2 + 1  , 1)
				SET @count2 = @count2  + 1  
			END 

		RETURN REPLACE(@retval,'- ','-')
		
	END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MSMBreeds]'
GO
CREATE TABLE [dbo].[MSMBreeds]
(
[animal_id] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[animal_breed] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[animal_type] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[animal_pedigree] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[id_upp] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[QuoteSentToThirdParty]'
GO
CREATE TABLE [dbo].[QuoteSentToThirdParty]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[QuoteId] [int] NOT NULL,
[SentToThirdParty] [bit] NOT NULL CONSTRAINT [DF_QuoteSentToThirdParty_SentToThirdParty] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_QuoteSentToThirdParty] on [dbo].[QuoteSentToThirdParty]'
GO
ALTER TABLE [dbo].[QuoteSentToThirdParty] ADD CONSTRAINT [PK_QuoteSentToThirdParty] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[azure_storage_quote_pet]'
GO
CREATE TABLE [dbo].[azure_storage_quote_pet]
(
[azure_storage_id] [uniqueidentifier] NOT NULL,
[quote_pet_id] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[bad_quotes_adu_20160722]'
GO
CREATE TABLE [dbo].[bad_quotes_adu_20160722]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NOT NULL,
[quote_ref] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[title_id] [int] NULL,
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[DOB] [smalldatetime] NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[house_name_num] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[street] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[town] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[county] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[contact_num] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[quote_status_id] [int] NULL,
[cover_id] [int] NULL,
[premium] [money] NULL,
[create_timestamp] [smalldatetime] NOT NULL,
[update_timestamp] [smalldatetime] NOT NULL,
[product_excess_id] [int] NULL,
[occupation_id] [int] NULL,
[hear_about_us_id] [int] NULL,
[other_policies] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[motor_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[home_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[street2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[policy_start_date] [smalldatetime] NULL,
[agree_contact] [bit] NULL,
[agree_terms] [bit] NULL,
[payment_detail_id] [uniqueidentifier] NULL,
[VFV_clinic] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[VFV_ClinicID] [int] NULL,
[ReferrerID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientIpAddress] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[schedule_doc] [image] NULL,
[promo_code] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[email_sent] [tinyint] NULL,
[created_systuser_id] [int] NULL,
[modified_systuser_id] [int] NULL,
[converted_systuser_id] [int] NULL,
[min_cover_ordinal] [int] NULL,
[opt_out_marketing] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[agg_remote_ref] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[agree_preexisting_excl] [bit] NULL,
[agree_waiting_excl] [bit] NULL,
[agree_illvac_excl] [bit] NULL,
[agree_vicious_excl] [bit] NULL,
[enquiry_method_code] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[all_pet_names] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[confirm_excluded_breeds] [bit] NOT NULL,
[annual_premium] [money] NULL,
[agree_contact_phone] [bit] NULL,
[agree_contact_SMS] [bit] NULL,
[agree_contact_email] [bit] NULL,
[agree_contact_letter] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[error_tracker_archive]'
GO
CREATE TABLE [dbo].[error_tracker_archive]
(
[id] [int] NOT NULL,
[error_page] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[error_type] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[error_message] [varchar] (8000) COLLATE Latin1_General_CI_AS NOT NULL,
[stack_trace] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[time_stamp] [datetime] NOT NULL,
[product_id] [int] NULL,
[notification_sent] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_error_tracker_archive_id] on [dbo].[error_tracker_archive]'
GO
CREATE CLUSTERED INDEX [idx_error_tracker_archive_id] ON [dbo].[error_tracker_archive] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[error_tracker_claim_dump]'
GO
CREATE TABLE [dbo].[error_tracker_claim_dump]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[error_page] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[error_type] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[error_message] [varchar] (8000) COLLATE Latin1_General_CI_AS NOT NULL,
[stack_trace] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[time_stamp] [datetime] NOT NULL,
[product_id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[excess]'
GO
CREATE TABLE [dbo].[excess]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[amount] [date] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__excess__7E6CC920] on [dbo].[excess]'
GO
ALTER TABLE [dbo].[excess] ADD CONSTRAINT [PK__excess__7E6CC920] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[exclusion]'
GO
CREATE TABLE [dbo].[exclusion]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__exclusion__78B3EFCA] on [dbo].[exclusion]'
GO
ALTER TABLE [dbo].[exclusion] ADD CONSTRAINT [PK__exclusion__78B3EFCA] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[linkedCovers]'
GO
CREATE TABLE [dbo].[linkedCovers]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[affinityCode] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[coverId1] [int] NOT NULL,
[coverId2] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[occupation]'
GO
CREATE TABLE [dbo].[occupation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__occupation__22751F6C] on [dbo].[occupation]'
GO
ALTER TABLE [dbo].[occupation] ADD CONSTRAINT [PK__occupation__22751F6C] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[preexisting_condition]'
GO
CREATE TABLE [dbo].[preexisting_condition]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[description] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__preexisting_cond__7A9C383C] on [dbo].[preexisting_condition]'
GO
ALTER TABLE [dbo].[preexisting_condition] ADD CONSTRAINT [PK__preexisting_cond__7A9C383C] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_factor_25062015]'
GO
CREATE TABLE [dbo].[pricing_factor_25062015]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NULL,
[cover_id] [int] NULL,
[animal_type_id] [int] NULL,
[factor_effective_date] [date] NOT NULL,
[factor_inactive_date] [date] NULL,
[description] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[metric] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[factor] [decimal] (5, 4) NOT NULL,
[applies_to] [varchar] (2) COLLATE Latin1_General_CI_AS NULL,
[apply_on_override] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_AAI1]'
GO
CREATE TABLE [dbo].[pricing_matrix_AAI1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_AAI]'
GO
CREATE TABLE [dbo].[pricing_matrix_AAI]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_ADM]'
GO
CREATE TABLE [dbo].[pricing_matrix_ADM]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_ALB]'
GO
CREATE TABLE [dbo].[pricing_matrix_ALB]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__pricing_matrix_A__53F76C67] on [dbo].[pricing_matrix_ALB]'
GO
ALTER TABLE [dbo].[pricing_matrix_ALB] ADD CONSTRAINT [PK__pricing_matrix_A__53F76C67] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_GRN1]'
GO
CREATE TABLE [dbo].[pricing_matrix_GRN1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_HAS]'
GO
CREATE TABLE [dbo].[pricing_matrix_HAS]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_LAG1]'
GO
CREATE TABLE [dbo].[pricing_matrix_LAG1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_LAG]'
GO
CREATE TABLE [dbo].[pricing_matrix_LAG]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PAW1]'
GO
CREATE TABLE [dbo].[pricing_matrix_PAW1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PAW]'
GO
CREATE TABLE [dbo].[pricing_matrix_PAW]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PUR1]'
GO
CREATE TABLE [dbo].[pricing_matrix_PUR1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_PUR]'
GO
CREATE TABLE [dbo].[pricing_matrix_PUR]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_UIS]'
GO
CREATE TABLE [dbo].[pricing_matrix_UIS]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_adm1]'
GO
CREATE TABLE [dbo].[pricing_matrix_adm1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_has1]'
GO
CREATE TABLE [dbo].[pricing_matrix_has1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[pricing_matrix_uis1]'
GO
CREATE TABLE [dbo].[pricing_matrix_uis1]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[animal_type_id] [int] NOT NULL,
[age] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[area_rating] [int] NOT NULL,
[breed_rating] [int] NULL,
[cover_id] [int] NOT NULL,
[value] [money] NOT NULL,
[breed_group] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_breed_group_25062015]'
GO
CREATE TABLE [dbo].[product_breed_group_25062015]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[product_id] [int] NULL,
[breed_id] [int] NULL,
[rating] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL,
[breed] [nvarchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_postcode_score_new]'
GO
CREATE TABLE [dbo].[product_postcode_score_new]
(
[product_id] [int] NOT NULL,
[postcode_id] [int] NOT NULL,
[rating] [int] NOT NULL,
[value] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[quote_quarantine]'
GO
CREATE TABLE [dbo].[quote_quarantine]
(
[id] [int] NOT NULL,
[product_id] [int] NOT NULL,
[quote_ref] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[title_id] [int] NULL,
[forename] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[surname] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[DOB] [date] NULL,
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[house_name_num] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[street] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[town] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[county] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[contact_num] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[email] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[quote_status_id] [int] NULL,
[cover_id] [int] NULL,
[premium] [money] NULL,
[create_timestamp] [smalldatetime] NULL,
[update_timestamp] [smalldatetime] NULL,
[product_excess_id] [int] NULL,
[occupation_id] [int] NULL,
[hear_about_us_id] [int] NULL,
[other_policies] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[motor_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[home_insurance_due] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[street2] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[policy_start_date] [date] NULL,
[agree_contact] [bit] NULL,
[agree_terms] [bit] NULL,
[payment_detail_id] [uniqueidentifier] NULL,
[VFV_clinic] [varchar] (32) COLLATE Latin1_General_CI_AS NULL,
[VFV_ClinicID] [int] NULL,
[ReferrerID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientIpAddress] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[schedule_doc] [image] NULL,
[promo_code] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[email_sent] [tinyint] NULL,
[created_systuser_id] [int] NULL,
[modified_systuser_id] [int] NULL,
[converted_systuser_id] [int] NULL,
[min_cover_ordinal] [int] NULL,
[opt_out_marketing] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[agg_remote_ref] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[affinity] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[agree_preexisting_excl] [bit] NULL,
[agree_waiting_excl] [bit] NULL,
[agree_illvac_excl] [bit] NULL,
[agree_vicious_excl] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_quote_quarantine] on [dbo].[quote_quarantine]'
GO
ALTER TABLE [dbo].[quote_quarantine] ADD CONSTRAINT [PK_quote_quarantine] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[qz breeds]'
GO
CREATE TABLE [dbo].[qz breeds]
(
[id] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[animal_type_id] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[breed_id] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[section_ROB_BOWMAN_SAFE_COPY]'
GO
CREATE TABLE [dbo].[section_ROB_BOWMAN_SAFE_COPY]
(
[id] [int] NOT NULL,
[product_id] [int] NOT NULL,
[description] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[ordinal] [int] NOT NULL,
[section_id] [int] NULL,
[alternative_description] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[help_content] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__section__0A688BB1_ROB_BOWMAN_SAFE_COPY] on [dbo].[section_ROB_BOWMAN_SAFE_COPY]'
GO
ALTER TABLE [dbo].[section_ROB_BOWMAN_SAFE_COPY] ADD CONSTRAINT [PK__section__0A688BB1_ROB_BOWMAN_SAFE_COPY] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[temp_postcodes]'
GO
CREATE TABLE [dbo].[temp_postcodes]
(
[postcode] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[rating] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[temp_vet]'
GO
CREATE TABLE [dbo].[temp_vet]
(
[postcode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[longitude] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[latitude] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[geoaccuracy] [int] NULL,
[geoerror] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [idx_temp_vet_postcode] on [dbo].[temp_vet]'
GO
CREATE CLUSTERED INDEX [idx_temp_vet_postcode] ON [dbo].[temp_vet] ([postcode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Core].[GetAffinities]'
GO
CREATE PROCEDURE [Core].[GetAffinities]
AS
BEGIN

DECLARE @userName varchar(50) = null
	
	SET NOCOUNT ON;

	IF (@userName IS NULL)
		BEGIN
			SELECT DISTINCT 
				LEFT(af.[AffinityCode], 3)  AS 'Code'
				,af.[Description] AS 'Name'
				,af.active [Enabled]
			FROM [uispetmis].dbo.AFFINITY as af
			WHERE af.[Description] IS NOT NULL
				AND ((af.active IS NOT NULL) AND (af.active = 1))
			ORDER BY LEFT(af.[AffinityCode], 3) ASC;
		END
	ELSE
		BEGIN
			SELECT DISTINCT [Code], [Name], [Enabled] FROM (
				SELECT LEFT(sua.[affinity_code], 3) AS 'Code'
					,af.[Description] AS 'Name'
					,af.active [Enabled]
				FROM [uispetmis].[dbo].[systuser_affinity] AS sua
					INNER JOIN [uispetmis].[dbo].[systuser] AS su ON su.id = sua.systuser_id
					INNER JOIN [uispetmis].[dbo].[AFFINITY] AS af ON LEFT(sua.[affinity_code], 3) = LEFT(af.AffinityCode, 3)
				WHERE ((af.active IS NOT NULL) AND (af.active = 1))
					AND su.username = @userName

				UNION

				SELECT LEFT(sga.[affinity_code], 3)  AS 'Code'
					,af.[Description] AS 'Name'
					,af.active [Enabled]
				FROM [uispetmis].[dbo].[systuser] as su 
					INNER JOIN [uispetmis].[dbo].systuser_group as sug ON su.id = sug.user_id
					INNER JOIN [uispetmis].[dbo].[systgroup_affinity] as sga ON sug.group_id = sga.systgroup_id
					INNER JOIN [uispetmis].[dbo].[AFFINITY] as af ON LEFT(sga.[affinity_code], 3) = LEFT(af.AffinityCode, 3)
				WHERE af.[Description] IS NOT NULL
				AND ((af.active IS NOT NULL) AND (af.active = 1))
					AND su.username = @userName
				) as af
			ORDER BY [Code] ASC;
		END    
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Correspondence].[GetQuotePet]'
GO


CREATE PROCEDURE [Correspondence].[GetQuotePet]
(	
	@quotePetId INT 
)

AS

SELECT qp.id, abd.maximum_benefit_amount AS vetFees,ISNULL(ade.min_excess_amt,0) + ISNULL(qp.vol_excess,0) AS totalExcess
INTO ##quotePetVetExcessFees
FROM uispet..quote q
JOIN uispet..quote_pet qp ON qp.quote_id = q.id
JOIN uispet..cover c ON c.id = qp.cover_id
JOIN uispetmis..affinity_document ad ON ad.affinity_code = c.affinity_code COLLATE DATABASE_DEFAULT
LEFT JOIN uispetmis..[affinity_benefit_detail] abd ON ad.id = abd.affinity_document_id
JOIN uispetmis..affinity_document_excess ade ON ade.affinity_document_id = ad.id
WHERE qp.id = @quotePetId
AND abd.benefit_type_id = 1 
AND ad.valid_from < q.policy_start_date
AND ade.[default] = 1


SELECT 
	qp.id AS Id,
	qp.cover_id AS SelectedCoverId,
	ISNULL(b.description,'') AS Breed,
	ISNULL(pg.gender_desc,'') AS Gender,
	CAST(CASE WHEN pg.gender_desc = 'Male' THEN 1 ELSE 0 END AS BIT) AS IsMale,
	qp.pet_name AS PetName,
 	CAST(1 AS BIT) AS IsAlive,
	qp.pet_DOB PetDateOfBirth,
	ISNULL(qp.chip_number, '') MicrochipNumber,
	ISNULL(qp.purchase_price, 0) PurchasePrice,
	ISNULL(qp.accidentdetails, '') PreviousAccidents,
	ISNULL(qp.vetvisitdetails, '') PreviousTreatment,
	ISNULL(qp.exclusion, '') SignsAndSymptoms,
	ISNULL(qp.not_working, 1) NotWorking,
	ISNULL(qp.not_breeding, 1) NotBreeding,
	ISNULL(qp.not_racing, 1) NotRacing,
	ISNULL(qp.not_hunting, 1) NotHunting,
	ISNULL(qp.neutered, 0) Neutered,
	ISNULL(qp.kept_at_home, 0) KeptAtResidence,
	ISNULL(at.description, '') PetType,
	ISNULL(qp.colour, '') Colour,
	ISNULL(qpf.vetFees,0) AS VetFees,
	ISNULL(qpf.totalExcess,0) AS TotalExcess,
	ISNULL(c.description,'') AS CoverDescription

FROM uispet..quote q
INNER JOIN uispet..quote_pet qp ON q.id = qp.quote_id
LEFT JOIN uispet..cover c ON c.id = qp.cover_id
LEFT JOIN uispet..breed b ON qp.breed_id = b.id 
LEFT JOIN uispet..product_gender pg ON pg.id = qp.product_gender_id
LEFT JOIN uispet..animal_type at ON at.id = qp.animal_type_id
LEFT JOIN ##quotePetVetExcessFees qpf ON qpf.id = qp.id
WHERE qp.id = @quotePetId

DROP TABLE ##quotePetVetExcessFees
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Pricing].[GetPricingEngineBreedID]'
GO

CREATE PROCEDURE [Pricing].[GetPricingEngineBreedID]
	@UisBreedID INT,
	@CoverID INT,
	@Effective DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1 zenith_breed_id FROM uispet..zenith_breed_mapping
	WHERE upp_breed_id = @UisBreedID
	AND cover_id = @CoverID
	AND effective_date <= @Effective
	ORDER BY effective_date DESC;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DOBL_prc]'
GO


CREATE PROCEDURE [dbo].[DOBL_prc]				
AS
SET NOCOUNT ON

	DECLARE @temp table(ID int NOT NULL,Description INT)
	    
	declare @liCount INT
	set @liCount = year(current_timestamp) - 17

	while @liCount > year(current_timestamp) - 100
	BEGIN
		set @liCount = @liCount - 1
		insert @temp(ID,Description) select @licOunt, @licOunt
	end

	select * from @temp order by id desc

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[monthL_prc]'
GO


CREATE PROCEDURE [dbo].[monthL_prc] 
AS

SET NOCOUNT ON

	
	SELECT 	0		as id,  
			''	as description
UNION
	SELECT 	1		as id,  
			'Jan'	as description
UNION
	SELECT 	2		as id,  
			'Feb'	as description
UNION
	SELECT 	3		as id,  
			'Mar'	as description
UNION
	SELECT 	4		as id,  
			'Apr'	as description
UNION
	SELECT 	5		as id,  
			'May'	as description
UNION
	SELECT 	6		as id,  
			'Jun'	as description
UNION
	SELECT 	7		as id,  
			'Jul'	as description
UNION
	SELECT 	8		as id,  
			'Aug'	as description
UNION
	SELECT 	9		as id,  
			'Sep'	as description
UNION
	SELECT 	10		as id,  
			'Oct'	as description
UNION
	SELECT 	11		as id,  
			'Nov'	as description
UNION
	SELECT 	12		as id,  
			'Dec'	as description

ORDER BY id
	

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_ID_retrieveS_prc]'
GO

CREATE PROCEDURE [dbo].[policy_ID_retrieveS_prc] (@policy_num varchar(32), @postcode varchar(12), @product_id int = NULL)				

AS
SET NOCOUNT ON

-- Hack for AA affinity where we have two product codes
IF @product_id = 30 AND @policy_num like 'AAX%' BEGIN SET @product_id = 29 END

select policyid 
from uispetmis..policy
where UPPER(REPLACE(policynumber,' ','')) =  @policy_num
and UPPER(REPLACE(postcode,' ','')) = @postcode
and ISNULL(@product_id, product_id)=product_id

SET NOCOUNT OFF

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_claimU_prc]'
GO

CREATE PROCEDURE [dbo].[policy_claimU_prc] (	@uis_policy_id				INT,
										@uis_treatment_date_from	SMALLDATETIME,
										@uis_diagnosis				VARCHAR(255),
										@uis_user_id				INT = 63 --Hard code for the mo



)				
AS
SET NOCOUNT ON

	DECLARE @uis_treatment_date_to SMALLDATETIME
	SET @uis_treatment_date_to = CURRENT_TIMESTAMP

	EXEC UISPetMIS..claimU_prc 		@claim_id=0,
										@policy_id=@uis_policy_id,
										@status_id=120, --previously "new from customer" now "awaiting documentation" (since now send email with claim form attached)
										@treatment_date_from = @uis_treatment_date_from,
										@treatment_date_to = @uis_treatment_date_to,
										@diagnosis= @uis_diagnosis,
										@treatment='',
										@treatment_cost=0,
										@diet_name='',
										@diet_tins_count=0,
										@diet_tins_cost=0.53,
										@diet_bags_count=0,
										@diet_bags_cost=1,
										@pay_vet=0,
										@total=0,
										@CostDietFees=0,
										@CostDeductions=0,
										@CostAdjustment=0,
										@CostExcess=0,
										@user_id=@uis_user_id,	
										@loss_code_section_id=NULL,
										@loss_code_specific_id=NULL,
										@DeathBenefitCost=0,
										@StolenLostCost=0,
										@AdvertCost=0,
										@deduction_reason1='',
										@deduction_amount1=0,
										@deduction_reason2='',
										@deduction_amount2=0,
										@deduction_reason3='',
										@deduction_amount3=0,
										@sub_total=0,
										@pay_to_vet_amt=0,
										@pay_to_claimant_amt=0,
										@previous_claim_id='0',
										@previous_imported_claim_id=NULL,
										@claim_entered_date='27 Nov 2008',  --TODAY
										@cheque_number_vet='',
										@cheque_number_insured='',
										@cheque_date=NULL,
										@declined_reason='',
										@cancelled_holiday_cost=0,
										@boarding_kennel_cost=0,
										@transport_cost=0,
										@expenses_cost=0,
										@ExcessExcludedCost=0,
										@vet_id=NULL,
										@is_no_excess=0,
										@dead_lost=0,
										@cheque_number_affinity='',
										@affinity_excess_id=299,  --hard code this for now too
										@LimitDeduction=0

										 



SET NOCOUNT OFF


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[policy_retrieveS_prc]'
GO


CREATE PROCEDURE [dbo].[policy_retrieveS_prc] (@policy_id INT)				
AS
SET NOCOUNT ON


	EXEC [UISPetMIS]..policyS_prc   @policy_id 

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_coverL_prc]'
GO


CREATE PROCEDURE [dbo].[product_coverL_prc] (@product_id  INT)
AS

SET NOCOUNT ON

	SELECT 	id, 
			cover_desc as description
	FROM	product_cover
	WHERE	product_id = @product_id 
	ORDER BY id

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[product_excessL_prc]'
GO


CREATE PROCEDURE [dbo].[product_excessL_prc] (@product_id  INT)
AS

SET NOCOUNT ON

	SELECT 	id, 
			 description
	FROM	product_excess
	WHERE	product_id = @product_id 
	ORDER BY id

SET NOCOUNT OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[cover_discount]'
GO
ALTER TABLE [dbo].[cover_discount] ADD CONSTRAINT [CHK_cover_discount_uniqueness] CHECK (([dbo].[cover_discount_uniqueness]([cover_id],[discount_id])<=(1)))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[sagepay_transaction]'
GO
ALTER TABLE [dbo].[sagepay_transaction] WITH NOCHECK  ADD CONSTRAINT [FK_sagepay_transaction_ProductPaymentProviderConfig_ProductPaymentProviderConfigId] FOREIGN KEY ([ProductPaymentProviderConfigId]) REFERENCES [dbo].[ProductPaymentProviderConfig] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[aggregator_archive]'
GO
ALTER TABLE [dbo].[aggregator_archive] WITH NOCHECK  ADD CONSTRAINT [FK_aggregator_quote_id_quote_id] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[zenith_breed_mapping3]'
GO
ALTER TABLE [dbo].[zenith_breed_mapping3] WITH NOCHECK  ADD CONSTRAINT [FK_zenith_breed_mapping3_breed] FOREIGN KEY ([upp_breed_id]) REFERENCES [dbo].[breed] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote]'
GO
ALTER TABLE [dbo].[quote] WITH NOCHECK  ADD CONSTRAINT [FK_vfv_clinic_id_id] FOREIGN KEY ([VFV_ClinicID]) REFERENCES [dbo].[clinic] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[clinic]'
GO
ALTER TABLE [dbo].[clinic] WITH NOCHECK  ADD CONSTRAINT [FK_clinic_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cover_benefit]'
GO
ALTER TABLE [dbo].[cover_benefit] WITH NOCHECK  ADD CONSTRAINT [FK_section_id_id] FOREIGN KEY ([section_id]) REFERENCES [dbo].[section] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote_pet]'
GO
ALTER TABLE [dbo].[quote_pet] WITH NOCHECK  ADD CONSTRAINT [FK_quote_pet_quote_id] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Debenhams].[VoucherConfig]'
GO
ALTER TABLE [Debenhams].[VoucherConfig] ADD CONSTRAINT [FK__VoucherCo__Campa__0E990F48] FOREIGN KEY ([CampaignId]) REFERENCES [Voucher].[Campaign] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Pricing].[Breakdown-8-8-18]'
GO
ALTER TABLE [Pricing].[Breakdown-8-8-18] ADD CONSTRAINT [[FK_Pricing.Breakdown_Pricing.Source_SourceId-8-8-18]]] FOREIGN KEY ([SourceId]) REFERENCES [Pricing].[Source] ([Id])
GO
ALTER TABLE [Pricing].[Breakdown-8-8-18] ADD CONSTRAINT [[FK_Pricing.Breakdown_dbo.Cover_CoverId-8-8-18]]] FOREIGN KEY ([CoverId]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [Pricing].[Breakdown-8-8-18] ADD CONSTRAINT [[FK_Pricing.Breakdown_dbo.pricing_engine_PricingEngineId-8-8-18]]] FOREIGN KEY ([PricingEngineId]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Pricing].[BreakdownAssociation]'
GO
ALTER TABLE [Pricing].[BreakdownAssociation] ADD CONSTRAINT [FK_Pricing.BreakdownAssociation_Pricing.Breakdown_BreakdownId] FOREIGN KEY ([BreakdownId]) REFERENCES [Pricing].[Breakdown] ([Id])
GO
ALTER TABLE [Pricing].[BreakdownAssociation] ADD CONSTRAINT [FK_Pricing.BreakdownAssociation_Pricing.Source_SourceId] FOREIGN KEY ([SourceId]) REFERENCES [Pricing].[Source] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Pricing].[Breakdown]'
GO
ALTER TABLE [Pricing].[Breakdown] ADD CONSTRAINT [FK_Pricing.Breakdown_Pricing.Source_SourceId] FOREIGN KEY ([SourceId]) REFERENCES [Pricing].[Source] ([Id])
GO
ALTER TABLE [Pricing].[Breakdown] ADD CONSTRAINT [FK_Pricing.Breakdown_dbo.Cover_CoverId] FOREIGN KEY ([CoverId]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [Pricing].[Breakdown] ADD CONSTRAINT [FK_Pricing.Breakdown_dbo.pricing_engine_PricingEngineId] FOREIGN KEY ([PricingEngineId]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Voucher].[Allocation]'
GO
ALTER TABLE [Voucher].[Allocation] ADD CONSTRAINT [FK__Allocatio__Vouch__0BBCA29D] FOREIGN KEY ([VoucherId]) REFERENCES [Voucher].[Voucher] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Voucher].[UnallocatedVouchers]'
GO
ALTER TABLE [Voucher].[UnallocatedVouchers] ADD CONSTRAINT [FK__Unallocat__Campa__43CBF196] FOREIGN KEY ([CampaignId]) REFERENCES [Voucher].[Campaign] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Voucher].[Voucher]'
GO
ALTER TABLE [Voucher].[Voucher] ADD CONSTRAINT [FK__Voucher__Campaig__041B80D5] FOREIGN KEY ([CampaignId]) REFERENCES [Voucher].[Campaign] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [Voucher].[Property]'
GO
ALTER TABLE [Voucher].[Property] ADD CONSTRAINT [FK__Property__Vouche__06F7ED80] FOREIGN KEY ([VoucherId]) REFERENCES [Voucher].[Voucher] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PaymentProviderCallBack]'
GO
ALTER TABLE [dbo].[PaymentProviderCallBack] ADD CONSTRAINT [FK_PaymentProviderCallBack_ApplicationType_ApplicationTypeID] FOREIGN KEY ([ApplicationTypeID]) REFERENCES [dbo].[ApplicationType] ([Id])
GO
ALTER TABLE [dbo].[PaymentProviderCallBack] ADD CONSTRAINT [FK_PaymentProviderCallBack_ProductPaymentProviderConfig_ProductPaymentProviderConfigId] FOREIGN KEY ([ProductPaymentProviderConfigId]) REFERENCES [dbo].[ProductPaymentProviderConfig] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[sagepay_transaction]'
GO
ALTER TABLE [dbo].[sagepay_transaction] ADD CONSTRAINT [FK_sagepay_transaction_ApplicationType_ApplicationTypeId] FOREIGN KEY ([ApplicationTypeId]) REFERENCES [dbo].[ApplicationType] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ProductPaymentProviderConfig]'
GO
ALTER TABLE [dbo].[ProductPaymentProviderConfig] ADD CONSTRAINT [FK_ProductPaymentProviderConfig_PaymentProviderDetail_PaymentProviderDetailId] FOREIGN KEY ([PaymentProviderDetailId]) REFERENCES [dbo].[PaymentProviderDetail] ([Id])
GO
ALTER TABLE [dbo].[ProductPaymentProviderConfig] ADD CONSTRAINT [FK_ProductPaymentProviderConfig_PaymentProvider_PaymentProviderId] FOREIGN KEY ([PaymentProviderId]) REFERENCES [dbo].[PaymentProvider] ([Id])
GO
ALTER TABLE [dbo].[ProductPaymentProviderConfig] ADD CONSTRAINT [FK_ProductPaymentProviderConfig_Product_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [products].[ProductQBActionMethod]'
GO
ALTER TABLE [products].[ProductQBActionMethod] ADD CONSTRAINT [FK_ProductQBActionMethod_QBActionMethod] FOREIGN KEY ([QBActionMethodID]) REFERENCES [dbo].[QBActionMethod] ([ID])
GO
ALTER TABLE [products].[ProductQBActionMethod] ADD CONSTRAINT [FK_ProductQBActionMethod_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[QuoteEmailQueue]'
GO
ALTER TABLE [dbo].[QuoteEmailQueue] ADD CONSTRAINT [FK__QuoteEmai__Quote__0015E5C7] FOREIGN KEY ([QuoteId]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[QuoteListExclusion]'
GO
ALTER TABLE [dbo].[QuoteListExclusion] ADD CONSTRAINT [FK_QuoteListExclusion_TypeId] FOREIGN KEY ([QuoteListExclusionTypeId]) REFERENCES [dbo].[QuoteListExclusionType] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [products].[BaseSite]'
GO
ALTER TABLE [products].[BaseSite] ADD CONSTRAINT [FK_BaseSite_SiteType] FOREIGN KEY ([SiteTypeID]) REFERENCES [dbo].[SiteType] ([ID])
GO
ALTER TABLE [products].[BaseSite] ADD CONSTRAINT [FK_BaseSite_SystEnvironment] FOREIGN KEY ([SystEnvironmentID]) REFERENCES [dbo].[SystEnvironment] ([ID])
GO
ALTER TABLE [products].[BaseSite] ADD CONSTRAINT [FK_BaseSite_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[aggregator_pet]'
GO
ALTER TABLE [dbo].[aggregator_pet] ADD CONSTRAINT [FK_aggregator_pet_aggregator] FOREIGN KEY ([aggregator_id]) REFERENCES [dbo].[aggregator] ([id])
GO
ALTER TABLE [dbo].[aggregator_pet] ADD CONSTRAINT [FK_aggregator_pet_pricing_engine_id] FOREIGN KEY ([pricing_engine_id]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[aggregator]'
GO
ALTER TABLE [dbo].[aggregator] ADD CONSTRAINT [FK_quote_id_id] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cover]'
GO
ALTER TABLE [dbo].[cover] ADD CONSTRAINT [fk_animal_cover_type] FOREIGN KEY ([animal_type_id]) REFERENCES [dbo].[animal_type] ([id])
GO
ALTER TABLE [dbo].[cover] ADD CONSTRAINT [FK_cover_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[breed]'
GO
ALTER TABLE [dbo].[breed] ADD CONSTRAINT [FK_breed_animal_type] FOREIGN KEY ([animal_type_id]) REFERENCES [dbo].[animal_type] ([id])
GO
ALTER TABLE [dbo].[breed] ADD CONSTRAINT [FK_breed_category_id] FOREIGN KEY ([breed_category_id]) REFERENCES [dbo].[breed_category] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[breed_category]'
GO
ALTER TABLE [dbo].[breed_category] ADD CONSTRAINT [FK_breed_category_animal_type_id] FOREIGN KEY ([animal_type_id]) REFERENCES [dbo].[animal_type] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[pricing_matrix_GRN]'
GO
ALTER TABLE [dbo].[pricing_matrix_GRN] ADD CONSTRAINT [FK_green_pricing_matrix_animal_type] FOREIGN KEY ([animal_type_id]) REFERENCES [dbo].[animal_type] ([id])
GO
ALTER TABLE [dbo].[pricing_matrix_GRN] ADD CONSTRAINT [FK_pricing_matrix_grn_animal_type] FOREIGN KEY ([animal_type_id]) REFERENCES [dbo].[animal_type] ([id])
GO
ALTER TABLE [dbo].[pricing_matrix_GRN] ADD CONSTRAINT [FK_green_pricing_matrix_cover] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [dbo].[pricing_matrix_GRN] ADD CONSTRAINT [FK_pricing_matrix_grn_cover] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote_pet]'
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [FK_quote_pet_breed_id] FOREIGN KEY ([breed_id]) REFERENCES [dbo].[breed] ([id])
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [FK_quote_pet_cover] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [FK_quote_pet_cover_id] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [FK_quote_pet_pricing_engine] FOREIGN KEY ([pricing_engine_id]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
ALTER TABLE [dbo].[quote_pet] ADD CONSTRAINT [FK_product_gender_id] FOREIGN KEY ([product_gender_id]) REFERENCES [dbo].[product_gender] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[zenith_breed_mapping]'
GO
ALTER TABLE [dbo].[zenith_breed_mapping] ADD CONSTRAINT [FK_zenith_breed_mapping_breed] FOREIGN KEY ([upp_breed_id]) REFERENCES [dbo].[breed] ([id])
GO
ALTER TABLE [dbo].[zenith_breed_mapping] ADD CONSTRAINT [FK_zenith_breed_mapping_cover] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[payment_detail]'
GO
ALTER TABLE [dbo].[payment_detail] ADD CONSTRAINT [FK_payment_detail_card_type] FOREIGN KEY ([card_type_id]) REFERENCES [dbo].[card_type] ([id])
GO
ALTER TABLE [dbo].[payment_detail] ADD CONSTRAINT [FK_payment_detail_payment_type] FOREIGN KEY ([payment_type_id]) REFERENCES [dbo].[payment_type] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[surcharge]'
GO
ALTER TABLE [dbo].[surcharge] ADD CONSTRAINT [FK_surcharge_card_type_id] FOREIGN KEY ([card_type_id]) REFERENCES [dbo].[card_type] ([id])
GO
ALTER TABLE [dbo].[surcharge] ADD CONSTRAINT [FK_surcharge_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[clinic]'
GO
ALTER TABLE [dbo].[clinic] ADD CONSTRAINT [FK_clinic_group_id_id] FOREIGN KEY ([clinic_group_id]) REFERENCES [dbo].[clinic_group] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cover_discount]'
GO
ALTER TABLE [dbo].[cover_discount] ADD CONSTRAINT [FK_cover_discount_cover_id] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [dbo].[cover_discount] ADD CONSTRAINT [FK_cover_discount_discount_id] FOREIGN KEY ([discount_id]) REFERENCES [dbo].[discount] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cover_benefit]'
GO
ALTER TABLE [dbo].[cover_benefit] ADD CONSTRAINT [FK_cover_id_id] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote]'
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [FK_quote_cover] FOREIGN KEY ([cover_id]) REFERENCES [dbo].[cover] ([id])
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [FK_quote_payment_detail] FOREIGN KEY ([payment_detail_id]) REFERENCES [dbo].[payment_detail] ([id])
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [FK_quote_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [FK_title_id] FOREIGN KEY ([title_id]) REFERENCES [dbo].[title] ([id])
GO
ALTER TABLE [dbo].[quote] ADD CONSTRAINT [FK_quote_quote_status] FOREIGN KEY ([quote_status_id]) REFERENCES [dbo].[quote_status] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[customer]'
GO
ALTER TABLE [dbo].[customer] ADD CONSTRAINT [FK_customer_title] FOREIGN KEY ([title_id]) REFERENCES [dbo].[title] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[dd_service_product]'
GO
ALTER TABLE [dbo].[dd_service_product] ADD CONSTRAINT [FK_dd_service_product_service_id] FOREIGN KEY ([dd_service_id]) REFERENCES [dbo].[dd_service] ([id])
GO
ALTER TABLE [dbo].[dd_service_product] ADD CONSTRAINT [FK_dd_service_product_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[dd_service]'
GO
ALTER TABLE [dbo].[dd_service] ADD CONSTRAINT [FK_dd_service_organisation_id] FOREIGN KEY ([dd_service_organisation_id]) REFERENCES [dbo].[dd_service_organisation] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[iv_decision_result]'
GO
ALTER TABLE [dbo].[iv_decision_result] ADD CONSTRAINT [FK_iv_decision_result_quote_id] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
ALTER TABLE [dbo].[iv_decision_result] ADD CONSTRAINT [FK_iv_decision_result_iv_request_log_id] FOREIGN KEY ([iv_request_log_id]) REFERENCES [dbo].[iv_request_log] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[lead]'
GO
ALTER TABLE [dbo].[lead] ADD CONSTRAINT [lead_time_fk] FOREIGN KEY ([lead_time_id]) REFERENCES [dbo].[lead_time] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[max_benefit]'
GO
ALTER TABLE [dbo].[max_benefit] ADD CONSTRAINT [FK_max_benefit_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_payment_type]'
GO
ALTER TABLE [dbo].[product_payment_type] ADD CONSTRAINT [FK_product_payment_type_payment_type_id] FOREIGN KEY ([payment_type_id]) REFERENCES [dbo].[payment_type] ([id])
GO
ALTER TABLE [dbo].[product_payment_type] ADD CONSTRAINT [FK_product_payment_type_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_postcode_score]'
GO
ALTER TABLE [dbo].[product_postcode_score] ADD CONSTRAINT [FK_product_postcode_score_postcode] FOREIGN KEY ([postcode_id]) REFERENCES [dbo].[postcode] ([id])
GO
ALTER TABLE [dbo].[product_postcode_score] ADD CONSTRAINT [FK_product_postcode_score_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_pricing_engine]'
GO
ALTER TABLE [dbo].[product_pricing_engine] ADD CONSTRAINT [FK_product_pricing_engine_pricing_engine] FOREIGN KEY ([pricing_engine_id]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
ALTER TABLE [dbo].[product_pricing_engine] ADD CONSTRAINT [FK_product_pricing_engine_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[renewal_premium_override]'
GO
ALTER TABLE [dbo].[renewal_premium_override] ADD CONSTRAINT [FK_renewal_premium_override_pricing_engine] FOREIGN KEY ([pricing_engine_id]) REFERENCES [dbo].[pricing_engine] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_gender]'
GO
ALTER TABLE [dbo].[product_gender] ADD CONSTRAINT [FK_product_gender_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_site_configuration]'
GO
ALTER TABLE [dbo].[product_site_configuration] ADD CONSTRAINT [FK_product_site_configuration_product_id] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_underwriting_check]'
GO
ALTER TABLE [dbo].[product_underwriting_check] ADD CONSTRAINT [FK_product_underwriting_check_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[underwriting_check]'
GO
ALTER TABLE [dbo].[underwriting_check] ADD CONSTRAINT [FK_underwriting_check_product] FOREIGN KEY ([product_id]) REFERENCES [dbo].[product] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[product_underwriting_check_quote]'
GO
ALTER TABLE [dbo].[product_underwriting_check_quote] ADD CONSTRAINT [FK_product_underwriting_check_puc] FOREIGN KEY ([product_underwriting_check_id]) REFERENCES [dbo].[product_underwriting_check] ([id])
GO
ALTER TABLE [dbo].[product_underwriting_check_quote] ADD CONSTRAINT [FK_product_underwriting_check_quote] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[pw_history]'
GO
ALTER TABLE [dbo].[pw_history] ADD CONSTRAINT [FK_pw_history_systuser] FOREIGN KEY ([systuser_id]) REFERENCES [dbo].[systuser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote_optional_question]'
GO
ALTER TABLE [dbo].[quote_optional_question] ADD CONSTRAINT [FK_quote_optional_question_quoteId] FOREIGN KEY ([quoteId]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[quote_payment_detail]'
GO
ALTER TABLE [dbo].[quote_payment_detail] ADD CONSTRAINT [FK_quote_payment_detail_quote_id] FOREIGN KEY ([quote_id]) REFERENCES [dbo].[quote] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[schedule_section_config]'
GO
ALTER TABLE [dbo].[schedule_section_config] ADD CONSTRAINT [fk_section] FOREIGN KEY ([SectionId]) REFERENCES [dbo].[section] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[systuser]'
GO
ALTER TABLE [dbo].[systuser] ADD CONSTRAINT [FK_systuser_systgroup] FOREIGN KEY ([systgroup_id]) REFERENCES [dbo].[systgroup] ([id])
GO
ALTER TABLE [dbo].[systuser] ADD CONSTRAINT [FK_systuser_title] FOREIGN KEY ([title_id]) REFERENCES [dbo].[title] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[systuser_logon_history]'
GO
ALTER TABLE [dbo].[systuser_logon_history] ADD CONSTRAINT [FK_systuser_logon_history_systuser] FOREIGN KEY ([systuser_id]) REFERENCES [dbo].[systuser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Disabling constraints on [dbo].[aggregator_archive]'
GO
ALTER TABLE [dbo].[aggregator_archive] NOCHECK CONSTRAINT [FK_aggregator_quote_id_quote_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating extended properties'
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'Quotes', 'VIEW', N'QuoteLinks', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'Quotes', 'VIEW', N'QuoteLinks', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = -982
      End
      Begin Tables = 
         Begin Table = "x"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 99
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 227
               Bottom = 114
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cb"
            Begin Extent = 
               Top = 6
               Left = 416
               Bottom = 114
               Right = 567
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vw_cover_excess', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vw_cover_excess', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
PRINT N'Altering permissions on  [dbo].[DeriveSalesSource]'
GO
GRANT EXECUTE ON  [dbo].[DeriveSalesSource] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[create_quote_ref]'
GO
GRANT EXECUTE ON  [dbo].[create_quote_ref] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[fnRemoveNonNumericCharacters]'
GO
GRANT EXECUTE ON  [dbo].[fnRemoveNonNumericCharacters] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_breed_group]'
GO
GRANT EXECUTE ON  [dbo].[get_breed_group] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_breed_group_ZEN]'
GO
GRANT EXECUTE ON  [dbo].[get_breed_group_ZEN] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[get_breed_group_ZEN] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[get_breed_group_ZEN] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_dd_service_organisation]'
GO
GRANT SELECT ON  [dbo].[get_dd_service_organisation] TO [m-user]
GO
GRANT SELECT ON  [dbo].[get_dd_service_organisation] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[get_dd_service_organisation] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[get_dd_service_organisation] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_sales_source]'
GO
GRANT EXECUTE ON  [dbo].[get_sales_source] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[get_sales_source] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_schedule_config_value]'
GO
GRANT EXECUTE ON  [dbo].[get_schedule_config_value] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[get_schedule_section_config_value]'
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [ssrs-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [s-user]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [ult-admin]
GO
GRANT EXECUTE ON  [dbo].[get_schedule_section_config_value] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[Breakdown-8-8-18]'
GO
GRANT INSERT ON  [Pricing].[Breakdown-8-8-18] TO [h-framework-user]
GO
GRANT SELECT ON  [Pricing].[Breakdown-8-8-18] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[BreakdownAssociation]'
GO
GRANT INSERT ON  [Pricing].[BreakdownAssociation] TO [h-framework-user]
GO
GRANT SELECT ON  [Pricing].[BreakdownAssociation] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[Source]'
GO
GRANT SELECT ON  [Pricing].[Source] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[BlacklistedLead]'
GO
GRANT SELECT ON  [dbo].[BlacklistedLead] TO [m-user]
GO
GRANT SELECT ON  [dbo].[BlacklistedLead] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[BlacklistedQuote]'
GO
GRANT SELECT ON  [dbo].[BlacklistedQuote] TO [m-user]
GO
GRANT SELECT ON  [dbo].[BlacklistedQuote] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[ReportBlacklist]'
GO
GRANT SELECT ON  [dbo].[ReportBlacklist] TO [m-user]
GO
GRANT SELECT ON  [dbo].[ReportBlacklist] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator]'
GO
GRANT SELECT ON  [dbo].[aggregator] TO [m-user]
GO
GRANT SELECT ON  [dbo].[aggregator] TO [s-user]
GO
GRANT SELECT ON  [dbo].[aggregator] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_archive]'
GO
GRANT SELECT ON  [dbo].[aggregator_archive] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_detail]'
GO
GRANT SELECT ON  [dbo].[aggregator_detail] TO [m-user]
GO
GRANT SELECT ON  [dbo].[aggregator_detail] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[animal_type]'
GO
GRANT SELECT ON  [dbo].[animal_type] TO [m-user]
GO
GRANT SELECT ON  [dbo].[animal_type] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[animal_type] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[breed]'
GO
GRANT SELECT ON  [dbo].[breed] TO [m-user]
GO
GRANT SELECT ON  [dbo].[breed] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[breed] TO [s-user]
GO
GRANT SELECT ON  [dbo].[breed] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[card_type]'
GO
GRANT SELECT ON  [dbo].[card_type] TO [m-user]
GO
GRANT SELECT ON  [dbo].[card_type] TO [q-user]
GO
GRANT SELECT ON  [dbo].[card_type] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[card_type] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[charity]'
GO
GRANT SELECT ON  [dbo].[charity] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic]'
GO
GRANT SELECT ON  [dbo].[clinic] TO [h-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [m-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [p-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [q-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [s-user]
GO
GRANT SELECT ON  [dbo].[clinic] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic_group]'
GO
GRANT SELECT ON  [dbo].[clinic_group] TO [m-user]
GO
GRANT SELECT ON  [dbo].[clinic_group] TO [q-user]
GO
GRANT SELECT ON  [dbo].[clinic_group] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[clinic_group] TO [s-user]
GO
GRANT SELECT ON  [dbo].[clinic_group] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover]'
GO
GRANT SELECT ON  [dbo].[cover] TO [h-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [m-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [p-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [quoteandbuy-asda]
GO
GRANT SELECT ON  [dbo].[cover] TO [q-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [s-user]
GO
GRANT SELECT ON  [dbo].[cover] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit]'
GO
GRANT SELECT ON  [dbo].[cover_benefit] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_discount]'
GO
GRANT SELECT ON  [dbo].[cover_discount] TO [m-user]
GO
GRANT SELECT ON  [dbo].[cover_discount] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[cover_discount] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[dd_service]'
GO
GRANT SELECT ON  [dbo].[dd_service] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[dd_service_organisation]'
GO
GRANT SELECT ON  [dbo].[dd_service_organisation] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[dd_service_product]'
GO
GRANT SELECT ON  [dbo].[dd_service_product] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discount]'
GO
GRANT SELECT ON  [dbo].[discount] TO [m-user]
GO
GRANT SELECT ON  [dbo].[discount] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[discount] TO [s-user]
GO
GRANT SELECT ON  [dbo].[discount] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[hear_about_us]'
GO
GRANT SELECT ON  [dbo].[hear_about_us] TO [m-user]
GO
GRANT SELECT ON  [dbo].[hear_about_us] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead]'
GO
GRANT SELECT ON  [dbo].[lead] TO [m-user]
GO
GRANT SELECT ON  [dbo].[lead] TO [s-user]
GO
GRANT SELECT ON  [dbo].[lead] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_audit]'
GO
GRANT SELECT ON  [dbo].[lead_audit] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_failure_reason]'
GO
GRANT SELECT ON  [dbo].[lead_failure_reason] TO [m-user]
GO
GRANT SELECT ON  [dbo].[lead_failure_reason] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_pet]'
GO
GRANT SELECT ON  [dbo].[lead_pet] TO [m-user]
GO
GRANT SELECT ON  [dbo].[lead_pet] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_reason]'
GO
GRANT SELECT ON  [dbo].[lead_reason] TO [m-user]
GO
GRANT SELECT ON  [dbo].[lead_reason] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_status]'
GO
GRANT SELECT ON  [dbo].[lead_status] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[linkedCovers]'
GO
GRANT SELECT ON  [dbo].[linkedCovers] TO [a-user]
GO
GRANT SELECT ON  [dbo].[linkedCovers] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[occupation]'
GO
GRANT SELECT ON  [dbo].[occupation] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound]'
GO
GRANT SELECT ON  [dbo].[outbound] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_audit]'
GO
GRANT SELECT ON  [dbo].[outbound_audit] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_status]'
GO
GRANT SELECT ON  [dbo].[outbound_status] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[payment_type]'
GO
GRANT SELECT ON  [dbo].[payment_type] TO [h-framework-user]
GO
GRANT SELECT ON  [dbo].[payment_type] TO [m-user]
GO
GRANT SELECT ON  [dbo].[payment_type] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[payment_type] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[payment_type] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[postcode]'
GO
GRANT SELECT ON  [dbo].[postcode] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[pricing_engine]'
GO
GRANT SELECT ON  [dbo].[pricing_engine] TO [h-user]
GO
GRANT SELECT ON  [dbo].[pricing_engine] TO [m-user]
GO
GRANT SELECT ON  [dbo].[pricing_engine] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product]'
GO
GRANT SELECT ON  [dbo].[product] TO [m-user]
GO
GRANT SELECT ON  [dbo].[product] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[product] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[product] TO [s-user]
GO
GRANT SELECT ON  [dbo].[product] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_gender]'
GO
GRANT SELECT ON  [dbo].[product_gender] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_postcode_score]'
GO
GRANT SELECT ON  [dbo].[product_postcode_score] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_pricing_engine]'
GO
GRANT SELECT ON  [dbo].[product_pricing_engine] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote]'
GO
GRANT UPDATE ON  [dbo].[quote] TO [h-framework-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [h-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [m-user]
GO
GRANT UPDATE ON  [dbo].[quote] TO [m-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[quote] TO [q-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [s-user]
GO
GRANT SELECT ON  [dbo].[quote] TO [ult-admin]
GO
GRANT SELECT ON  [dbo].[quote] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_pet]'
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [h-user]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [m-user]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [quoteandbuy]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [q-user]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [s-user]
GO
GRANT SELECT ON  [dbo].[quote_pet] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_pet_charity]'
GO
GRANT SELECT ON  [dbo].[quote_pet_charity] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_pet_introductory_discount]'
GO
GRANT SELECT ON  [dbo].[quote_pet_introductory_discount] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_cover]'
GO
GRANT SELECT ON  [dbo].[renewal_cover] TO [m-user]
GO
GRANT SELECT ON  [dbo].[renewal_cover] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[renewal_cover] TO [s-user]
GO
GRANT SELECT ON  [dbo].[renewal_cover] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_premium_override]'
GO
GRANT SELECT ON  [dbo].[renewal_premium_override] TO [m-user]
GO
GRANT SELECT ON  [dbo].[renewal_premium_override] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[renewal_premium_override] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_product]'
GO
GRANT SELECT ON  [dbo].[renewal_product] TO [m-user]
GO
GRANT SELECT ON  [dbo].[renewal_product] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[renewal_product] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[sales_ip_address]'
GO
GRANT SELECT ON  [dbo].[sales_ip_address] TO [m-user]
GO
GRANT SELECT ON  [dbo].[sales_ip_address] TO [s-user]
GO
GRANT SELECT ON  [dbo].[sales_ip_address] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[section]'
GO
GRANT SELECT ON  [dbo].[section] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[title]'
GO
GRANT SELECT ON  [dbo].[title] TO [m-user]
GO
GRANT SELECT ON  [dbo].[title] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[title] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Aggregator].[GetAggregatorCoverConfigurationOverride]'
GO
GRANT EXECUTE ON  [Aggregator].[GetAggregatorCoverConfigurationOverride] TO [a-user]
GO
GRANT EXECUTE ON  [Aggregator].[GetAggregatorCoverConfigurationOverride] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Core].[GetAffinitiesByUsername]'
GO
GRANT EXECUTE ON  [Core].[GetAffinitiesByUsername] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Core].[GetAffinities]'
GO
GRANT EXECUTE ON  [Core].[GetAffinities] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Correspondence].[GetQuote]'
GO
GRANT EXECUTE ON  [Correspondence].[GetQuote] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[AddBreakdown]'
GO
GRANT EXECUTE ON  [Pricing].[AddBreakdown] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[DeleteAllBreakdownsForSource]'
GO
GRANT EXECUTE ON  [Pricing].[DeleteAllBreakdownsForSource] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[DeleteBreakdownAssociation]'
GO
GRANT EXECUTE ON  [Pricing].[DeleteBreakdownAssociation] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[DeleteBreakdown]'
GO
GRANT EXECUTE ON  [Pricing].[DeleteBreakdown] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[GetBreakdownAssociations]'
GO
GRANT EXECUTE ON  [Pricing].[GetBreakdownAssociations] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[GetBreakdownLogEntriesForUpload]'
GO
GRANT EXECUTE ON  [Pricing].[GetBreakdownLogEntriesForUpload] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[GetBreakdownSummariesForPolicy]'
GO
GRANT EXECUTE ON  [Pricing].[GetBreakdownSummariesForPolicy] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[GetBreakdownSummariesForQuote]'
GO
GRANT EXECUTE ON  [Pricing].[GetBreakdownSummariesForQuote] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[GetBreakdown]'
GO
GRANT EXECUTE ON  [Pricing].[GetBreakdown] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[SetBreakdownUploaded]'
GO
GRANT EXECUTE ON  [Pricing].[SetBreakdownUploaded] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Pricing].[UpdateBreakdownIdentifier]'
GO
GRANT EXECUTE ON  [Pricing].[UpdateBreakdownIdentifier] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[BreakdownsBySequenceId]'
GO
GRANT EXECUTE ON  [dbo].[BreakdownsBySequenceId] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[CreateClinic]'
GO
GRANT EXECUTE ON  [dbo].[CreateClinic] TO [cvs-iframe]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[DOBL_prc]'
GO
GRANT EXECUTE ON  [dbo].[DOBL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[DOBL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[DocumentIdsFromCoverId]'
GO
GRANT EXECUTE ON  [dbo].[DocumentIdsFromCoverId] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[DuplicateQuoteS_prc]'
GO
GRANT EXECUTE ON  [dbo].[DuplicateQuoteS_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[GetCoverConfiguration]'
GO
GRANT EXECUTE ON  [dbo].[GetCoverConfiguration] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[ProductDetailsFromCoverId]'
GO
GRANT EXECUTE ON  [dbo].[ProductDetailsFromCoverId] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[ProductIdFromCode]'
GO
GRANT EXECUTE ON  [dbo].[ProductIdFromCode] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[QueueQuoteEmail]'
GO
GRANT EXECUTE ON  [dbo].[QueueQuoteEmail] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[QueueQuoteEmail] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[SetQuoteEmailSent]'
GO
GRANT EXECUTE ON  [dbo].[SetQuoteEmailSent] TO [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[TEMPlead_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[TEMPlead_listL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[VV_ClinicL_prc]'
GO
GRANT EXECUTE ON  [dbo].[VV_ClinicL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[affinity_activeL_prc]'
GO
GRANT EXECUTE ON  [dbo].[affinity_activeL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[affinity_activeL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[agg_quote_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[agg_quote_listL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[agg_quote_listL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregatorU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregatorU_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[aggregatorU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_archive_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_archive_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_detailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_detailS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_detailS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_leadU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_leadU_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_leadU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_petU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_petU_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_petU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_quoteS_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_quoteU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_quoteidU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteidU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_quoteidU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_requestU_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_requestU_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_requestU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[aggregator_sourceL_prc]'
GO
GRANT EXECUTE ON  [dbo].[aggregator_sourceL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[aggregator_sourceL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[animal_typeL_prc]'
GO
GRANT EXECUTE ON  [dbo].[animal_typeL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[animal_typeL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[animal_typeL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[animal_typeL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[auditU_prc]'
GO
GRANT EXECUTE ON  [dbo].[auditU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[auditU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[blogL_prc]'
GO
GRANT EXECUTE ON  [dbo].[blogL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[blogL_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[blogL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[breedL_prc]'
GO
GRANT EXECUTE ON  [dbo].[breedL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[breedL_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[breedL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[breedL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[breedL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[breed_categoryL_prc]'
GO
GRANT EXECUTE ON  [dbo].[breed_categoryL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[breed_categoryL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[breed_categoryL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[breed_searchL_prc]'
GO
GRANT EXECUTE ON  [dbo].[breed_searchL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[breed_searchL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[breed_searchL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[card_typeL_prc]'
GO
GRANT EXECUTE ON  [dbo].[card_typeL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[card_typeL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[charityL_prc]'
GO
GRANT EXECUTE ON  [dbo].[charityL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[charityL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[claim_emailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[claim_emailS_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinicL_prc]'
GO
GRANT EXECUTE ON  [dbo].[clinicL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[clinicL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[clinicL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic_by_groupL_prc]'
GO
GRANT EXECUTE ON  [dbo].[clinic_by_groupL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_by_groupL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_by_groupL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_by_groupL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic_by_group_referrerL_prc]'
GO
GRANT EXECUTE ON  [dbo].[clinic_by_group_referrerL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic_groupL_prc]'
GO
GRANT EXECUTE ON  [dbo].[clinic_groupL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_groupL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_groupL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_groupL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[clinic_services_by_group]'
GO
GRANT EXECUTE ON  [dbo].[clinic_services_by_group] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[clinic_services_by_group] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cookie_dumpU_prc]'
GO
GRANT EXECUTE ON  [dbo].[cookie_dumpU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cookie_dumpU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[coverL_prc]'
GO
GRANT EXECUTE ON  [dbo].[coverL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[coverL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefitL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefitL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefitL_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefitL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefitL_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit_HTML_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_HTML_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_HTML_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit_plus_premL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_plus_premL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit_scheduleL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_scheduleL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_scheduleL_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_scheduleL_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit_specificL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_specificL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_specificL_prc] TO [ssrs-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_specificL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_benefit_vfvL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_vfvL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_benefit_vfvL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_discountL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_discountL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_discountL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_introductory_discountL_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_introductory_discountL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_introductory_discountL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[cover_introductory_discountU_prc]'
GO
GRANT EXECUTE ON  [dbo].[cover_introductory_discountU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[cover_introductory_discountU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[dd_service_organisationS_prc]'
GO
GRANT EXECUTE ON  [dbo].[dd_service_organisationS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[dd_service_organisationS_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[dd_service_organisationS_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discountL_prc]'
GO
GRANT EXECUTE ON  [dbo].[discountL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discountL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discountU_prc]'
GO
GRANT EXECUTE ON  [dbo].[discountU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discountU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discountX_prc]'
GO
GRANT EXECUTE ON  [dbo].[discountX_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discountX_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discount_fundL_prc]'
GO
GRANT EXECUTE ON  [dbo].[discount_fundL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discount_fundL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discount_staffL_prc]'
GO
GRANT EXECUTE ON  [dbo].[discount_staffL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discount_staffL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[discount_valid_prc]'
GO
GRANT EXECUTE ON  [dbo].[discount_valid_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[discount_valid_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[error_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[error_listL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[error_listL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[error_trackerI_prc]'
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [ssrs-user]
GO
GRANT EXECUTE ON  [dbo].[error_trackerI_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[hear_about_usL_prc]'
GO
GRANT EXECUTE ON  [dbo].[hear_about_usL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[hear_about_usL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[helptextL_prc]'
GO
GRANT EXECUTE ON  [dbo].[helptextL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[helptextL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[housekeeping_prc]'
GO
GRANT EXECUTE ON  [dbo].[housekeeping_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_decision_resultI_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_decision_resultI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_decision_resultI_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_profile_productS_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_profile_productS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_profile_productS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_reportL_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_reportL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_reportL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_request_logI_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_request_logI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_request_logI_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_request_logS_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_request_logS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_request_logS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[iv_update_profile_passwordU_prc]'
GO
GRANT EXECUTE ON  [dbo].[iv_update_profile_passwordU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[iv_update_profile_passwordU_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[leadS_prc]'
GO
GRANT EXECUTE ON  [dbo].[leadS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[leadS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[leadS_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[leadU_prc]'
GO
GRANT EXECUTE ON  [dbo].[leadU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[leadU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[leadU_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_clinicsL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_clinicsL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_clinicsL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_contact_failedU_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_contact_failedU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_contact_failedU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_emailU_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_emailU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_email_contentS_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_email_contentS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_email_contentS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_failure_reasonL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_failure_reasonL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_failure_reasonL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_historyL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_historyL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_historyL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_listL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_listL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_petS_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_petS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_petS_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_petU_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_petU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_petU_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_quoteU_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_quoteU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_quoteU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_reasonL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_reasonL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_reasonL_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_statusL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_statusL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_statusL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[lead_timeL_prc]'
GO
GRANT EXECUTE ON  [dbo].[lead_timeL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[lead_timeL_prc] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[leads_today_countS_prc]'
GO
GRANT EXECUTE ON  [dbo].[leads_today_countS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[leads_today_countS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[max_benefitL_prc]'
GO
GRANT EXECUTE ON  [dbo].[max_benefitL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[minderL_prc]'
GO
GRANT EXECUTE ON  [dbo].[minderL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[minderL_prc] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[minder_prc]'
GO
GRANT EXECUTE ON  [dbo].[minder_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[minder_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[minder_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[monthL_prc]'
GO
GRANT EXECUTE ON  [dbo].[monthL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[monthL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[occupationL_prc]'
GO
GRANT EXECUTE ON  [dbo].[occupationL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[occupationL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outboundS_prc]'
GO
GRANT EXECUTE ON  [dbo].[outboundS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outboundS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outboundU_prc]'
GO
GRANT EXECUTE ON  [dbo].[outboundU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outboundU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_auditL_prc]'
GO
GRANT EXECUTE ON  [dbo].[outbound_auditL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outbound_auditL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[outbound_listL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outbound_listL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_quoteU_prc]'
GO
GRANT EXECUTE ON  [dbo].[outbound_quoteU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outbound_quoteU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[outbound_statusL_prc]'
GO
GRANT EXECUTE ON  [dbo].[outbound_statusL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[outbound_statusL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[page_trackerI_prc]'
GO
GRANT EXECUTE ON  [dbo].[page_trackerI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[page_trackerI_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[page_trackerI_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[payment_billing_addressS_prc]'
GO
GRANT EXECUTE ON  [dbo].[payment_billing_addressS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[payment_billing_addressS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[payment_billing_addressS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[payment_detailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[payment_detailS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[payment_detailS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[payment_detail_createI_prc]'
GO
GRANT EXECUTE ON  [dbo].[payment_detail_createI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[payment_detail_createI_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[payment_typeL_prc]'
GO
GRANT EXECUTE ON  [dbo].[payment_typeL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[payment_typeL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_ID_retrieveS_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_ID_retrieveS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[policy_ID_retrieveS_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[policy_ID_retrieveS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_claimU_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_claimU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_createI_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_createI_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[policy_createI_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[policy_createI_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[policy_createI_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_create_detailsI_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_create_detailsI_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_details_from_quote]'
GO
GRANT EXECUTE ON  [dbo].[policy_details_from_quote] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_retrieveS_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_retrieveS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[policy_retrieveS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_scheduleS_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_scheduleS_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[policy_schedule_get_signatureS_prc]'
GO
GRANT EXECUTE ON  [dbo].[policy_schedule_get_signatureS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[policy_schedule_get_signatureS_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[postcodeS_prc]'
GO
GRANT EXECUTE ON  [dbo].[postcodeS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[postcodeS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[productL_prc]'
GO
GRANT EXECUTE ON  [dbo].[productL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[productL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[productS_prc]'
GO
GRANT EXECUTE ON  [dbo].[productS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[productS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[productS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_coverL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_coverL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_documentL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_documentL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_documentL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_documentS_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_documentS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_documentS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_excessL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_excessL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_genderL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_genderL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_genderL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[product_genderL_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[product_genderL_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_prefixL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_prefixL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_pricing_factorL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_pricing_factorL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_pricing_factorL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_pricing_matrixL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_pricing_matrixL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_pricing_matrixL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_underwriting_check_quoteL_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_underwriting_check_quoteL_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[product_underwriting_check_quoteU_prc]'
GO
GRANT EXECUTE ON  [dbo].[product_underwriting_check_quoteU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[product_underwriting_check_quoteU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quick_quote_calculateU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculateU_prc] TO [a-user]
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculateU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculateU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quick_quote_calculate_BFG_U_prc]'
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculate_BFG_U_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quick_quote_calculate_DON_U_prc]'
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculate_DON_U_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quick_quote_calculate_VFV_U_prc]'
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculate_VFV_U_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quick_quote_calculate_VSR_U_prc]'
GO
GRANT EXECUTE ON  [dbo].[quick_quote_calculate_VSR_U_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quoteS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quoteS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quoteS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quoteS_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[quoteS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quoteS_prc_test]'
GO
GRANT EXECUTE ON  [dbo].[quoteS_prc_test] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quoteU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quoteU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quoteU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quoteU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_animal_typeS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_animal_typeS_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_by_referenceS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_by_referenceS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_by_referenceS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_calculateU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_calculateU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_calculateU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_calculate_allU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_calculate_allU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_calculate_allU_prc] TO [ssrs-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_cover_checkS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_cover_checkS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_cover_checkS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_detailsS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_detailsS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_detailsS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_emailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_emailS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_emailS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_emailS_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[quote_emailS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_emailU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_emailU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_email_contentS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_email_contentS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_email_contentS_prc] TO [ssrs-user]
GO
GRANT EXECUTE ON  [dbo].[quote_email_contentS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_email_detailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_email_detailS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_email_detailS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_email_queueL_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_email_queueL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_email_queueL_prc] TO [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_expireU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_expireU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_initialsS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_initialsS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_initialsS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_initialsU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_initialsU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_initialsU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_listL_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_listL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_listL_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_listL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_list_referralL_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_list_referralL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_list_referralL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_payment_detailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailS_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailS_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailS_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_payment_detailU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailU_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[quote_payment_detailU_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_petS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_petS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_petS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_petU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_petU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_petU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_petU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_pet_checkU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_pet_checkU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_pet_checkU_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_premiumS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_premiumS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_premiumS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_quarantine_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_quarantine_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_retrieveS_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_retrieveS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_retrieveS_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[quote_retrieveS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_statusL_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_statusL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_statusL_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_statusU_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_statusU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[quote_statusU_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[quote_statusU_prc] TO [q-user]
GO
GRANT EXECUTE ON  [dbo].[quote_statusU_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[quote_update_enquiry_method_prc]'
GO
GRANT EXECUTE ON  [dbo].[quote_update_enquiry_method_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[reminder_emailU_prc]'
GO
GRANT EXECUTE ON  [dbo].[reminder_emailU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_calculate_prc]'
GO
GRANT EXECUTE ON  [dbo].[renewal_calculate_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[renewal_calculate_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[renewal_calculate_prc] TO [ssrs-user]
GO
GRANT EXECUTE ON  [dbo].[renewal_calculate_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_premium_overrideU_prc]'
GO
GRANT EXECUTE ON  [dbo].[renewal_premium_overrideU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[renewal_premium_overrideU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[renewal_premium_overrideX_prc]'
GO
GRANT EXECUTE ON  [dbo].[renewal_premium_overrideX_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[renewal_premium_overrideX_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[report_data_dump_prc]'
GO
GRANT EXECUTE ON  [dbo].[report_data_dump_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[report_data_dump_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[report_data_dump_prc_debug]'
GO
GRANT EXECUTE ON  [dbo].[report_data_dump_prc_debug] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[report_data_dump_v2_prc]'
GO
GRANT EXECUTE ON  [dbo].[report_data_dump_v2_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[sagepay_transactionS_prc]'
GO
GRANT EXECUTE ON  [dbo].[sagepay_transactionS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[sagepay_transactionS_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[sagepay_transactionU]'
GO
GRANT EXECUTE ON  [dbo].[sagepay_transactionU] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[sagepay_transactionU_prc]'
GO
GRANT EXECUTE ON  [dbo].[sagepay_transactionU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[sagepay_transactionU_prc] TO [s-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[schedule_blobS_prc]'
GO
GRANT EXECUTE ON  [dbo].[schedule_blobS_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[schedule_blobU_prc]'
GO
GRANT EXECUTE ON  [dbo].[schedule_blobU_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[schedule_emailS_prc]'
GO
GRANT EXECUTE ON  [dbo].[schedule_emailS_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[testimonialL_prc]'
GO
GRANT EXECUTE ON  [dbo].[testimonialL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialL_prc] TO [h-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialL_prc] TO [m-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[testimonialS_prc]'
GO
GRANT EXECUTE ON  [dbo].[testimonialS_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialS_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[testimonialU_prc]'
GO
GRANT EXECUTE ON  [dbo].[testimonialU_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialU_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[testimonialX_prc]'
GO
GRANT EXECUTE ON  [dbo].[testimonialX_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[testimonialX_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[titleL_prc]'
GO
GRANT EXECUTE ON  [dbo].[titleL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[titleL_prc] TO [p-user]
GO
GRANT EXECUTE ON  [dbo].[titleL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[underwriting_checkL_prc]'
GO
GRANT EXECUTE ON  [dbo].[underwriting_checkL_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[underwriting_checkL_prc] TO [q-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[update_quote_pet_introductory_discount_prc]'
GO
GRANT EXECUTE ON  [dbo].[update_quote_pet_introductory_discount_prc] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[upgrade_calculate_prc]'
GO
GRANT EXECUTE ON  [dbo].[upgrade_calculate_prc] TO [h-framework-user]
GO
GRANT EXECUTE ON  [dbo].[upgrade_calculate_prc] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[vwBlacklistedLead]'
GO
GRANT SELECT ON  [dbo].[vwBlacklistedLead] TO [m-user]
GO
GRANT SELECT ON  [dbo].[vwBlacklistedLead] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[vwWhitelistedLead]'
GO
GRANT SELECT ON  [dbo].[vwWhitelistedLead] TO [m-user]
GO
GRANT SELECT ON  [dbo].[vwWhitelistedLead] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[vwWhitelistedQuote]'
GO
GRANT SELECT ON  [dbo].[vwWhitelistedQuote] TO [m-user]
GO
GRANT SELECT ON  [dbo].[vwWhitelistedQuote] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[vw_cover_excess]'
GO
GRANT SELECT ON  [dbo].[vw_cover_excess] TO [p-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Core]'
GO
GRANT EXECUTE ON SCHEMA:: [Core] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Correspondence]'
GO
GRANT EXECUTE ON SCHEMA:: [Correspondence] TO [quoteandbuy]
GO
GRANT EXECUTE ON SCHEMA:: [Correspondence] TO [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Debenhams]'
GO
GRANT EXECUTE ON SCHEMA:: [Debenhams] TO [h-framework-user]
GO
GRANT SELECT ON SCHEMA:: [Debenhams] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [TestDataExport]'
GO
GRANT EXECUTE ON SCHEMA:: [TestDataExport] TO [test-data-export-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Voucher]'
GO
GRANT EXECUTE ON SCHEMA:: [Voucher] TO [h-framework-user]
GO
GRANT SELECT ON SCHEMA:: [Voucher] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
