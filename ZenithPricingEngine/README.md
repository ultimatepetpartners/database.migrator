﻿************************ Migrate new changes ******************
1) Create a database called uispet on your local instance

2) Run this command prompt on your local machine, from the uispet project folder.

"bin\Debug\Migrate.exe" /connection "server=.;User ID=UltimateJobOwner;Password=LS82ChSzgvuJch8gjZWD;database=ZenithPricingEngine" /assembly "bin\Debug\ZenithPricingEngine.dll" /provider sqlserver2016 /verbose=true

If you want to run in powershell, just include & as a prefix.

3) You're up to date :) 

Note: If you would like to preview the script first then simply append the following arguments to the end of the above command line string.

	--preview=true --output --outputFilename migrateUp.sql

******************  Rollback Changes to specific version ******************
1) Run this command prompt on your local machine, from the ADP.Database.Data project folder.

"bin\Debug\Migrate.exe" /connection "server=.;User ID=UltimateJobOwner;Password=LS82ChSzgvuJch8gjZWD;database=ZenithPricingEngine" /task rollback --version={versionnumber} /assembly "bin\Debug\ZenithPricingEngine.dll" /provider sqlserver2016 /verbose=true

Note: version number is the identity used along with Migration attribute on top of the Migration class.

**************************  Creating a migration **************************
1) Create migration folder in SQLFiles, with sub folders Up & Down. Add modification scripts to the Up folder. Include a rollback script to the Down folder.

2) Create class in Scripts with the same migration name and reference your scripts in the releveant Up & Down methods.

Note: Naming conventions (removing curly braces)...
	1) for migration folder\class is M{YYYYMMDDHHMM}_{Description}. 
		YYYYMMDDHHMM = DateTime, 
		Description = Work item description

	2) for sql script files is {Sequence}-{YYYYMMDD}-{Description}-Up.sql
		Sequence = 2 digit number in order of which scripts will be executed, 
		YYYYMMDD = Date Only, 
		Description = Brief description of what sql is doing eg. Alter-spGetSomething