﻿USE [master]
GO

/****** Object:  Database [ZenithPricingEngine]    Script Date: 20/12/2019 13:49:03 ******/
CREATE DATABASE [ZenithPricingEngine]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ZenithContext', FILENAME = N'D:\SQLServer\MSSQL14.MSSQLSERVER\MSSQL\DATA\ZenithPricingEngine.mdf' , SIZE = 942080KB , MAXSIZE = UNLIMITED, FILEGROWTH = 204800KB )
 LOG ON 
( NAME = N'ZenithContext_log', FILENAME = N'E:\SQLServer\MSSQL14.MSSQLSERVER\MSSQL\Logs\ZenithPricingEngine_log.ldf' , SIZE = 519040KB , MAXSIZE = 2048GB , FILEGROWTH = 512000KB )
GO

ALTER DATABASE [ZenithPricingEngine] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ZenithPricingEngine].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ZenithPricingEngine] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET ARITHABORT OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ZenithPricingEngine] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ZenithPricingEngine] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET  DISABLE_BROKER 
GO

ALTER DATABASE [ZenithPricingEngine] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ZenithPricingEngine] SET READ_COMMITTED_SNAPSHOT ON 
GO

ALTER DATABASE [ZenithPricingEngine] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [ZenithPricingEngine] SET  MULTI_USER 
GO

ALTER DATABASE [ZenithPricingEngine] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ZenithPricingEngine] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ZenithPricingEngine] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [ZenithPricingEngine] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [ZenithPricingEngine] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [ZenithPricingEngine] SET QUERY_STORE = ON
GO

ALTER DATABASE [ZenithPricingEngine] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = ALL, SIZE_BASED_CLEANUP_MODE = AUTO)
GO

ALTER DATABASE [ZenithPricingEngine] SET  READ_WRITE 
GO

USE ZenithPricingEngine
GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING')
CREATE LOGIN [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING] FOR LOGIN [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\ADMIN_MIS_UAT_Servers')
CREATE LOGIN [MARKERSTUDY\ADMIN_MIS_UAT_Servers] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\ADMIN_MIS_UAT_Servers] FOR LOGIN [MARKERSTUDY\ADMIN_MIS_UAT_Servers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\USR_GRP_Retail_MI')
CREATE LOGIN [MARKERSTUDY\USR_GRP_Retail_MI] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\USR_GRP_Retail_MI] FOR LOGIN [MARKERSTUDY\USR_GRP_Retail_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MARKERSTUDY\USR_GRP_Ultimate_MI')
CREATE LOGIN [MARKERSTUDY\USR_GRP_Ultimate_MI] FROM WINDOWS
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [MARKERSTUDY\USR_GRP_Ultimate_MI] FOR LOGIN [MARKERSTUDY\USR_GRP_Ultimate_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''UltimateJobOwner''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [UltimateJobOwner] and mapping to the login [UltimateJobOwner]'
    CREATE USER [UltimateJobOwner] FOR LOGIN [UltimateJobOwner]
END
ELSE
BEGIN
    PRINT N'Creating user [UltimateJobOwner] without login'
    CREATE USER [UltimateJobOwner] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE USER [UltimateMIJobOwner] WITHOUT LOGIN
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''h-framework-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [h-framework-user] and mapping to the login [h-framework-user]'
    CREATE USER [h-framework-user] FOR LOGIN [h-framework-user]
END
ELSE
BEGIN
    PRINT N'Creating user [h-framework-user] without login'
    CREATE USER [h-framework-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''m-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [m-user] and mapping to the login [m-user]'
    CREATE USER [m-user] FOR LOGIN [m-user]
END
ELSE
BEGIN
    PRINT N'Creating user [m-user] without login'
    CREATE USER [m-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''s-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [s-user] and mapping to the login [s-user]'
    CREATE USER [s-user] FOR LOGIN [s-user]
END
ELSE
BEGIN
    PRINT N'Creating user [s-user] without login'
    CREATE USER [s-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''ssrs-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [ssrs-user] and mapping to the login [ssrs-user]'
    CREATE USER [ssrs-user] FOR LOGIN [ssrs-user]
END
ELSE
BEGIN
    PRINT N'Creating user [ssrs-user] without login'
    CREATE USER [ssrs-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''ult-admin''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [ult-admin] and mapping to the login [ult-admin]'
    CREATE USER [ult-admin] FOR LOGIN [ult-admin]
END
ELSE
BEGIN
    PRINT N'Creating user [ult-admin] without login'
    CREATE USER [ult-admin] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''x-user''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [x-user] and mapping to the login [x-user]'
    CREATE USER [x-user] FOR LOGIN [x-user]
END
ELSE
BEGIN
    PRINT N'Creating user [x-user] without login'
    CREATE USER [x-user] WITHOUT LOGIN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datareader'
GO
ALTER ROLE [db_datareader] ADD MEMBER [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [MARKERSTUDY\USR_GRP_Retail_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datawriter'
GO
ALTER ROLE [db_datawriter] ADD MEMBER [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_owner'
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\ADMIN_DEV_ULTIMATE_WEBHOSTING]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\ADMIN_MIS_UAT_Servers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [MARKERSTUDY\USR_GRP_Ultimate_MI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [UltimateJobOwner]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_owner] ADD MEMBER [UltimateMIJobOwner]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [Core]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Discount]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Management]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Rating]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [Validation]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating types'
GO
CREATE TYPE [Core].[ListOfInt] AS TABLE
(
[Id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ExternalCover]'
GO
CREATE TABLE [dbo].[ExternalCover]
(
[ExternalCoverId] [int] NOT NULL,
[AccidentOnly] [bit] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.ExternalCover] on [dbo].[ExternalCover]'
GO
ALTER TABLE [dbo].[ExternalCover] ADD CONSTRAINT [PK_dbo.ExternalCover] PRIMARY KEY CLUSTERED  ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreedGroupAreaRate]'
GO
CREATE TABLE [dbo].[BreedGroupAreaRate]
(
[BreedGroupAreaRateId] [int] NOT NULL IDENTITY(1, 1),
[AreaRatingId] [int] NOT NULL,
[BreedGroupId] [int] NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BreedGroupAreaRate] on [dbo].[BreedGroupAreaRate]'
GO
ALTER TABLE [dbo].[BreedGroupAreaRate] ADD CONSTRAINT [PK_BreedGroupAreaRate] PRIMARY KEY CLUSTERED  ([BreedGroupAreaRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedGroupAreaRate_AreaRatingId_BreedGroupId_ExternalCoverId_EffectiveFrom_3EA90] on [dbo].[BreedGroupAreaRate]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedGroupAreaRate_AreaRatingId_BreedGroupId_ExternalCoverId_EffectiveFrom_3EA90] ON [dbo].[BreedGroupAreaRate] ([AreaRatingId], [BreedGroupId], [ExternalCoverId], [EffectiveFrom]) INCLUDE ([BreedGroupAreaRateId], [Factor])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Breed]'
GO
CREATE TABLE [dbo].[Breed]
(
[BreedId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Breed] on [dbo].[Breed]'
GO
ALTER TABLE [dbo].[Breed] ADD CONSTRAINT [PK_dbo.Breed] PRIMARY KEY CLUSTERED  ([BreedId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Breed]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Breed] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[Breed]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[Breed] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreedLifeSpan]'
GO
CREATE TABLE [dbo].[BreedLifeSpan]
(
[BreedLifeSpanId] [int] NOT NULL IDENTITY(1, 1),
[BreedId] [int] NOT NULL,
[LifeSpanId] [int] NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedLifeSpan_BreedId_ExternalCoverId_85B8F] on [dbo].[BreedLifeSpan]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedLifeSpan_BreedId_ExternalCoverId_85B8F] ON [dbo].[BreedLifeSpan] ([BreedId], [ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedLifeSpan_ExternalCoverId_413B5] on [dbo].[BreedLifeSpan]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedLifeSpan_ExternalCoverId_413B5] ON [dbo].[BreedLifeSpan] ([ExternalCoverId]) INCLUDE ([BreedId], [EffectiveFrom], [LifeSpanId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedLifeSpan_LifeSpanId_ExternalCoverId_103B9] on [dbo].[BreedLifeSpan]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedLifeSpan_LifeSpanId_ExternalCoverId_103B9] ON [dbo].[BreedLifeSpan] ([LifeSpanId], [ExternalCoverId]) INCLUDE ([BreedId], [BreedLifeSpanId], [EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LifeSpan]'
GO
CREATE TABLE [dbo].[LifeSpan]
(
[LifeSpanId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.LifeSpan] on [dbo].[LifeSpan]'
GO
ALTER TABLE [dbo].[LifeSpan] ADD CONSTRAINT [PK_dbo.LifeSpan] PRIMARY KEY CLUSTERED  ([LifeSpanId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[LifeSpan]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[LifeSpan] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AdminFeeRecipient]'
GO
CREATE TABLE [dbo].[AdminFeeRecipient]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.AdminFeeRecipient] on [dbo].[AdminFeeRecipient]'
GO
ALTER TABLE [dbo].[AdminFeeRecipient] ADD CONSTRAINT [PK_dbo.AdminFeeRecipient] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AdminFee]'
GO
CREATE TABLE [dbo].[AdminFee]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Amount] [decimal] (18, 2) NOT NULL,
[CurrencyId] [int] NOT NULL,
[AdminFeeRecipientId] [int] NOT NULL,
[PetTypeId] [int] NOT NULL,
[SalesChannelId] [int] NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.AdminFee] on [dbo].[AdminFee]'
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [PK_dbo.AdminFee] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Currency]'
GO
CREATE TABLE [dbo].[Currency]
(
[CurrencyId] [int] NOT NULL IDENTITY(1, 1),
[Code] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Symbol] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Currency] on [dbo].[Currency]'
GO
ALTER TABLE [dbo].[Currency] ADD CONSTRAINT [PK_dbo.Currency] PRIMARY KEY CLUSTERED  ([CurrencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Currency]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Currency] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PetType]'
GO
CREATE TABLE [dbo].[PetType]
(
[PetTypeId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.PetType] on [dbo].[PetType]'
GO
ALTER TABLE [dbo].[PetType] ADD CONSTRAINT [PK_dbo.PetType] PRIMARY KEY CLUSTERED  ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[PetType]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[PetType] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SalesChannel]'
GO
CREATE TABLE [dbo].[SalesChannel]
(
[SalesChannelId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.SalesChannel] on [dbo].[SalesChannel]'
GO
ALTER TABLE [dbo].[SalesChannel] ADD CONSTRAINT [PK_dbo.SalesChannel] PRIMARY KEY CLUSTERED  ([SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[SalesChannel]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[SalesChannel] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AgeAtInceptionFactor]'
GO
CREATE TABLE [dbo].[AgeAtInceptionFactor]
(
[AgeAtInceptionFactorId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[PetAgeYears] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.AgeAtInceptionFactor] on [dbo].[AgeAtInceptionFactor]'
GO
ALTER TABLE [dbo].[AgeAtInceptionFactor] ADD CONSTRAINT [PK_dbo.AgeAtInceptionFactor] PRIMARY KEY CLUSTERED  ([AgeAtInceptionFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[AgeAtInceptionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[AgeAtInceptionFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[AgeAtInceptionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[AgeAtInceptionFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeAtInceptionFactor_ExternalCoverId_DF13D] on [dbo].[AgeAtInceptionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeAtInceptionFactor_ExternalCoverId_DF13D] ON [dbo].[AgeAtInceptionFactor] ([ExternalCoverId]) INCLUDE ([AgeAtInceptionFactorId], [EffectiveFrom], [Factor], [PetAgeYears], [PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeYears] on [dbo].[AgeAtInceptionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeYears] ON [dbo].[AgeAtInceptionFactor] ([PetAgeYears])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[AgeAtInceptionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[AgeAtInceptionFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AgeFactor]'
GO
CREATE TABLE [dbo].[AgeFactor]
(
[AgeFactorId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[LifeSpanId] [int] NOT NULL,
[PetAgeMonthsFrom] [decimal] (18, 2) NOT NULL,
[PetAgeMonthsTo] [decimal] (18, 2) NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL,
[SalesTypeId] [int] NOT NULL,
[SalesChannelId] [int] NOT NULL CONSTRAINT [DF__AgeFactor__Sales__02C769E9] DEFAULT ((9999)),
[EffectiveTo] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.AgeFactor] on [dbo].[AgeFactor]'
GO
ALTER TABLE [dbo].[AgeFactor] ADD CONSTRAINT [PK_dbo.AgeFactor] PRIMARY KEY CLUSTERED  ([AgeFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[AgeFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[AgeFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeFactor_ExternalCoverId_CDC18] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeFactor_ExternalCoverId_CDC18] ON [dbo].[AgeFactor] ([ExternalCoverId]) INCLUDE ([AgeFactorId], [EffectiveFrom], [Factor], [LifeSpanId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeFactor_ExternalCoverId_EB1F1] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeFactor_ExternalCoverId_EB1F1] ON [dbo].[AgeFactor] ([ExternalCoverId]) INCLUDE ([AgeFactorId], [EffectiveFrom], [EffectiveTo], [Factor], [LifeSpanId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId], [SalesChannelId], [SalesTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LifeSpanId] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_LifeSpanId] ON [dbo].[AgeFactor] ([LifeSpanId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsFrom] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsFrom] ON [dbo].[AgeFactor] ([PetAgeMonthsFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsTo] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsTo] ON [dbo].[AgeFactor] ([PetAgeMonthsTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[AgeFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeFactor_PetTypeId_LifeSpanId_ExternalCoverId_SalesTypeId_PetAgeMonthsFrom_PetAgeMonthsT_A0F48] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeFactor_PetTypeId_LifeSpanId_ExternalCoverId_SalesTypeId_PetAgeMonthsFrom_PetAgeMonthsT_A0F48] ON [dbo].[AgeFactor] ([PetTypeId], [LifeSpanId], [ExternalCoverId], [SalesTypeId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [EffectiveFrom], [SalesChannelId], [EffectiveTo]) INCLUDE ([AgeFactorId], [Factor])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeFactor_PetTypeId_PetAgeMonthsFrom_SalesTypeId_ExternalCoverId_F963A] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeFactor_PetTypeId_PetAgeMonthsFrom_SalesTypeId_ExternalCoverId_F963A] ON [dbo].[AgeFactor] ([PetTypeId], [PetAgeMonthsFrom], [SalesTypeId], [ExternalCoverId]) INCLUDE ([AgeFactorId], [EffectiveFrom], [EffectiveTo], [Factor], [LifeSpanId], [PetAgeMonthsTo], [SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_AgeFactor_PetTypeId_PetAgeMonthsFrom_SalesTypeId_ExternalCoverId_BA201] on [dbo].[AgeFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_AgeFactor_PetTypeId_PetAgeMonthsFrom_SalesTypeId_ExternalCoverId_BA201] ON [dbo].[AgeFactor] ([PetTypeId], [PetAgeMonthsFrom], [SalesTypeId], [ExternalCoverId]) INCLUDE ([AgeFactorId], [PetAgeMonthsTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AreaRatingFactor]'
GO
CREATE TABLE [dbo].[AreaRatingFactor]
(
[AreaRatingFactorId] [int] NOT NULL IDENTITY(1, 1),
[AreaRating] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.AreaRatingFactor] on [dbo].[AreaRatingFactor]'
GO
ALTER TABLE [dbo].[AreaRatingFactor] ADD CONSTRAINT [PK_dbo.AreaRatingFactor] PRIMARY KEY CLUSTERED  ([AreaRatingFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_AreaRating] on [dbo].[AreaRatingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_AreaRating] ON [dbo].[AreaRatingFactor] ([AreaRating])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[AreaRatingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[AreaRatingFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[AreaRatingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[AreaRatingFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BaseRate]'
GO
CREATE TABLE [dbo].[BaseRate]
(
[BaseRateId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Price] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.BaseRate] on [dbo].[BaseRate]'
GO
ALTER TABLE [dbo].[BaseRate] ADD CONSTRAINT [PK_dbo.BaseRate] PRIMARY KEY CLUSTERED  ([BaseRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CurrencyId] on [dbo].[BaseRate]'
GO
CREATE NONCLUSTERED INDEX [IX_CurrencyId] ON [dbo].[BaseRate] ([CurrencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[BaseRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[BaseRate] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[BaseRate]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[BaseRate] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[BaseRate]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[BaseRate] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreedGroup]'
GO
CREATE TABLE [dbo].[BreedGroup]
(
[BreedFactorId] [int] NOT NULL IDENTITY(1, 1),
[BreedId] [int] NOT NULL,
[NewBusinessBreedGroupId] [int] NULL,
[RenewalBreedGroupId] [int] NULL,
[OptimalAgeYears] [int] NULL,
[Excess] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Decline] [bit] NULL,
[ExternalCoverId] [int] NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.BreedGroup] on [dbo].[BreedGroup]'
GO
ALTER TABLE [dbo].[BreedGroup] ADD CONSTRAINT [PK_dbo.BreedGroup] PRIMARY KEY CLUSTERED  ([BreedFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BreedId] on [dbo].[BreedGroup]'
GO
CREATE NONCLUSTERED INDEX [IX_BreedId] ON [dbo].[BreedGroup] ([BreedId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedGroup_BreedId_6939E] on [dbo].[BreedGroup]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedGroup_BreedId_6939E] ON [dbo].[BreedGroup] ([BreedId]) INCLUDE ([BreedFactorId], [Decline], [EffectiveFrom], [Excess], [ExternalCoverId], [NewBusinessBreedGroupId], [OptimalAgeYears], [RenewalBreedGroupId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[BreedGroup]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[BreedGroup] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[BreedGroup]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[BreedGroup] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_BreedGroup_ExternalCoverId_6FD06] on [dbo].[BreedGroup]'
GO
CREATE NONCLUSTERED INDEX [IDX_BreedGroup_ExternalCoverId_6FD06] ON [dbo].[BreedGroup] ([ExternalCoverId]) INCLUDE ([BreedId], [Decline], [EffectiveFrom], [Excess], [NewBusinessBreedGroupId], [OptimalAgeYears], [RenewalBreedGroupId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreedGroupFactor]'
GO
CREATE TABLE [dbo].[BreedGroupFactor]
(
[BreedGroupFactorId] [int] NOT NULL IDENTITY(1, 1),
[BreedGroupId] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL,
[BreedGroup_BreedFactorId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.BreedGroupFactor] on [dbo].[BreedGroupFactor]'
GO
ALTER TABLE [dbo].[BreedGroupFactor] ADD CONSTRAINT [PK_dbo.BreedGroupFactor] PRIMARY KEY CLUSTERED  ([BreedGroupFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BreedGroup_BreedFactorId] on [dbo].[BreedGroupFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_BreedGroup_BreedFactorId] ON [dbo].[BreedGroupFactor] ([BreedGroup_BreedFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BreedGroupId] on [dbo].[BreedGroupFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_BreedGroupId] ON [dbo].[BreedGroupFactor] ([BreedGroupId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[BreedGroupFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[BreedGroupFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[BreedGroupFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[BreedGroupFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CappingRate]'
GO
CREATE TABLE [dbo].[CappingRate]
(
[CappingRateId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[PetAgeMonthsFrom] [decimal] (18, 2) NOT NULL,
[PetAgeMonthsTo] [decimal] (18, 2) NOT NULL,
[CleanCapRate] [decimal] (12, 5) NOT NULL,
[ClaimCapRate] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.CappingRate] on [dbo].[CappingRate]'
GO
ALTER TABLE [dbo].[CappingRate] ADD CONSTRAINT [PK_dbo.CappingRate] PRIMARY KEY CLUSTERED  ([CappingRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[CappingRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[CappingRate] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[CappingRate]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[CappingRate] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CappingRate_ExternalCoverId_1C2C1] on [dbo].[CappingRate]'
GO
CREATE NONCLUSTERED INDEX [IDX_CappingRate_ExternalCoverId_1C2C1] ON [dbo].[CappingRate] ([ExternalCoverId]) INCLUDE ([CappingRateId], [ClaimCapRate], [CleanCapRate], [EffectiveFrom], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[CappingRate]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[CappingRate] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CappingRate_PetTypeId_ExternalCoverId_PetAgeMonthsFrom_PetAgeMonthsTo_EffectiveFrom_49B5A] on [dbo].[CappingRate]'
GO
CREATE NONCLUSTERED INDEX [IDX_CappingRate_PetTypeId_ExternalCoverId_PetAgeMonthsFrom_PetAgeMonthsTo_EffectiveFrom_49B5A] ON [dbo].[CappingRate] ([PetTypeId], [ExternalCoverId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ClaimsLoadingFactor]'
GO
CREATE TABLE [dbo].[ClaimsLoadingFactor]
(
[ClaimsLoadingFactorId] [int] NOT NULL IDENTITY(1, 1),
[QuatersFromRenewal] [int] NOT NULL,
[ReoccuranceLikelihoodId] [int] NOT NULL,
[ClaimsValueFrom] [decimal] (18, 2) NOT NULL,
[ClaimsValueTo] [decimal] (18, 2) NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.ClaimsLoadingFactor] on [dbo].[ClaimsLoadingFactor]'
GO
ALTER TABLE [dbo].[ClaimsLoadingFactor] ADD CONSTRAINT [PK_dbo.ClaimsLoadingFactor] PRIMARY KEY CLUSTERED  ([ClaimsLoadingFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClaimsValueFrom] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ClaimsValueFrom] ON [dbo].[ClaimsLoadingFactor] ([ClaimsValueFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClaimsValueTo] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ClaimsValueTo] ON [dbo].[ClaimsLoadingFactor] ([ClaimsValueTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[ClaimsLoadingFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[ClaimsLoadingFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_ClaimsLoadingFactor_ExternalCoverId_374E4] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_ClaimsLoadingFactor_ExternalCoverId_374E4] ON [dbo].[ClaimsLoadingFactor] ([ExternalCoverId]) INCLUDE ([ClaimsLoadingFactorId], [ClaimsValueFrom], [ClaimsValueTo], [EffectiveFrom], [Factor], [QuatersFromRenewal], [ReoccuranceLikelihoodId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QuatersFromRenewal] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_QuatersFromRenewal] ON [dbo].[ClaimsLoadingFactor] ([QuatersFromRenewal])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_ClaimsLoadingFactor_QuatersFromRenewal_ReocLikelikId_ExtCoverId_ClaimValFr_ClaimValTo_EffeFrom_0D429] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_ClaimsLoadingFactor_QuatersFromRenewal_ReocLikelikId_ExtCoverId_ClaimValFr_ClaimValTo_EffeFrom_0D429] ON [dbo].[ClaimsLoadingFactor] ([QuatersFromRenewal], [ReoccuranceLikelihoodId], [ExternalCoverId], [ClaimsValueFrom], [ClaimsValueTo], [EffectiveFrom]) INCLUDE ([ClaimsLoadingFactorId], [Factor])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ReoccuranceLikelihoodId] on [dbo].[ClaimsLoadingFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ReoccuranceLikelihoodId] ON [dbo].[ClaimsLoadingFactor] ([ReoccuranceLikelihoodId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ReoccuranceLikelihood]'
GO
CREATE TABLE [dbo].[ReoccuranceLikelihood]
(
[ReoccuranceLikelihoodId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Order] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.ReoccuranceLikelihood] on [dbo].[ReoccuranceLikelihood]'
GO
ALTER TABLE [dbo].[ReoccuranceLikelihood] ADD CONSTRAINT [PK_dbo.ReoccuranceLikelihood] PRIMARY KEY CLUSTERED  ([ReoccuranceLikelihoodId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Description] on [dbo].[ReoccuranceLikelihood]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Description] ON [dbo].[ReoccuranceLikelihood] ([Description])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[ReoccuranceLikelihood]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[ReoccuranceLikelihood] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CollaringRate]'
GO
CREATE TABLE [dbo].[CollaringRate]
(
[CollaringRateId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[PetAgeMonthsFrom] [decimal] (18, 2) NOT NULL,
[PetAgeMonthsTo] [decimal] (18, 2) NOT NULL,
[CleanCollarRate] [decimal] (12, 5) NOT NULL,
[ClaimCollarRate] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.CollaringRate] on [dbo].[CollaringRate]'
GO
ALTER TABLE [dbo].[CollaringRate] ADD CONSTRAINT [PK_dbo.CollaringRate] PRIMARY KEY CLUSTERED  ([CollaringRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[CollaringRate] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[CollaringRate] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CollaringRate_ExternalCoverId_F60CD] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IDX_CollaringRate_ExternalCoverId_F60CD] ON [dbo].[CollaringRate] ([ExternalCoverId]) INCLUDE ([ClaimCollarRate], [CleanCollarRate], [CollaringRateId], [EffectiveFrom], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsFrom] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsFrom] ON [dbo].[CollaringRate] ([PetAgeMonthsFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsTo] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsTo] ON [dbo].[CollaringRate] ([PetAgeMonthsTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[CollaringRate] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CollaringRate_PetTypeId_ExternalCoverId_PetAgeMonthsFrom_PetAgeMonthsTo_EffectiveFrom_53CE0] on [dbo].[CollaringRate]'
GO
CREATE NONCLUSTERED INDEX [IDX_CollaringRate_PetTypeId_ExternalCoverId_PetAgeMonthsFrom_PetAgeMonthsTo_EffectiveFrom_53CE0] ON [dbo].[CollaringRate] ([PetTypeId], [ExternalCoverId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CommissionRecipient]'
GO
CREATE TABLE [dbo].[CommissionRecipient]
(
[CommissionRecipientId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.CommissionRecipient] on [dbo].[CommissionRecipient]'
GO
ALTER TABLE [dbo].[CommissionRecipient] ADD CONSTRAINT [PK_dbo.CommissionRecipient] PRIMARY KEY CLUSTERED  ([CommissionRecipientId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[CommissionRecipient]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[CommissionRecipient] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CommissionFactor]'
GO
CREATE TABLE [dbo].[CommissionFactor]
(
[CommissionFactorId] [int] NOT NULL IDENTITY(1, 1),
[CommissionTypeId] [int] NOT NULL,
[CommissionRecipientId] [int] NOT NULL,
[SalesChannelId] [int] NOT NULL,
[PetTypeId] [int] NOT NULL,
[PetAgeMonthsFrom] [decimal] (18, 2) NOT NULL,
[PetAgeMonthsTo] [decimal] (18, 2) NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.CommissionFactor] on [dbo].[CommissionFactor]'
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [PK_dbo.CommissionFactor] PRIMARY KEY CLUSTERED  ([CommissionFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CommissionRecipientId] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_CommissionRecipientId] ON [dbo].[CommissionFactor] ([CommissionRecipientId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CommissionTypeId] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_CommissionTypeId] ON [dbo].[CommissionFactor] ([CommissionTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CommissionFactor_CommissionTypeId_CommissRecId_SalesChId_PetTypeId_ExtCoverId_PetAgeMthFm_PetAgeMthTo_EffFrm_33814] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_CommissionFactor_CommissionTypeId_CommissRecId_SalesChId_PetTypeId_ExtCoverId_PetAgeMthFm_PetAgeMthTo_EffFrm_33814] ON [dbo].[CommissionFactor] ([CommissionTypeId], [CommissionRecipientId], [SalesChannelId], [PetTypeId], [ExternalCoverId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [EffectiveFrom]) INCLUDE ([CommissionFactorId], [Factor])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CommissionFactor_CommTyId_SalChanId_PeTyId_ExtCoverId_PetAgeMonFr_PetAgeMosTo_EffecFrom_F0714] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_CommissionFactor_CommTyId_SalChanId_PeTyId_ExtCoverId_PetAgeMonFr_PetAgeMosTo_EffecFrom_F0714] ON [dbo].[CommissionFactor] ([CommissionTypeId], [SalesChannelId], [PetTypeId], [ExternalCoverId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [EffectiveFrom]) INCLUDE ([CommissionFactorId], [CommissionRecipientId], [Factor])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[CommissionFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[CommissionFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CommissionFactor_ExternalCoverId_B943C] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_CommissionFactor_ExternalCoverId_B943C] ON [dbo].[CommissionFactor] ([ExternalCoverId]) INCLUDE ([CommissionFactorId], [CommissionRecipientId], [CommissionTypeId], [EffectiveFrom], [Factor], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId], [SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CommissionFactor_ExternalCoverId_C66A5] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_CommissionFactor_ExternalCoverId_C66A5] ON [dbo].[CommissionFactor] ([ExternalCoverId]) INCLUDE ([CommissionFactorId], [CommissionRecipientId], [CommissionTypeId], [EffectiveFrom], [Factor], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId], [SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_CommissionFactor_Factor_A5ACC] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IDX_CommissionFactor_Factor_A5ACC] ON [dbo].[CommissionFactor] ([Factor]) INCLUDE ([CommissionFactorId], [CommissionRecipientId], [CommissionTypeId], [EffectiveFrom], [ExternalCoverId], [PetAgeMonthsFrom], [PetAgeMonthsTo], [PetTypeId], [SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsFrom] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsFrom] ON [dbo].[CommissionFactor] ([PetAgeMonthsFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetAgeMonthsTo] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetAgeMonthsTo] ON [dbo].[CommissionFactor] ([PetAgeMonthsTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[CommissionFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SalesChannelId] on [dbo].[CommissionFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_SalesChannelId] ON [dbo].[CommissionFactor] ([SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CommissionType]'
GO
CREATE TABLE [dbo].[CommissionType]
(
[CommissionTypeId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.CommissionType] on [dbo].[CommissionType]'
GO
ALTER TABLE [dbo].[CommissionType] ADD CONSTRAINT [PK_dbo.CommissionType] PRIMARY KEY CLUSTERED  ([CommissionTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[CommissionType]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[CommissionType] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Discount]'
GO
CREATE TABLE [dbo].[Discount]
(
[DiscountId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Key] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[TotalDiscountRate] [decimal] (12, 5) NULL,
[EffectiveFrom] [datetime] NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[SalesChannelId] [int] NULL,
[CanApplyAtRenewal] [bit] NOT NULL CONSTRAINT [DF__Discount__CanApp__4D5F7D71] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Discount] on [dbo].[Discount]'
GO
ALTER TABLE [dbo].[Discount] ADD CONSTRAINT [PK_dbo.Discount] PRIMARY KEY CLUSTERED  ([DiscountId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Discount]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Discount] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[Discount]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[Discount] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DiscountRate]'
GO
CREATE TABLE [dbo].[DiscountRate]
(
[DiscountRateId] [int] NOT NULL IDENTITY(1, 1),
[DiscountId] [int] NOT NULL,
[Rate] [decimal] (12, 5) NOT NULL,
[CommissionRecipientId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL,
[EffectiveTo] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.DiscountRate] on [dbo].[DiscountRate]'
GO
ALTER TABLE [dbo].[DiscountRate] ADD CONSTRAINT [PK_dbo.DiscountRate] PRIMARY KEY CLUSTERED  ([DiscountRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CommissionRecipientId] on [dbo].[DiscountRate]'
GO
CREATE NONCLUSTERED INDEX [IX_CommissionRecipientId] ON [dbo].[DiscountRate] ([CommissionRecipientId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_DiscountId] on [dbo].[DiscountRate]'
GO
CREATE NONCLUSTERED INDEX [IX_DiscountId] ON [dbo].[DiscountRate] ([DiscountId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[DiscountRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[DiscountRate] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveTo] on [dbo].[DiscountRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveTo] ON [dbo].[DiscountRate] ([EffectiveTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FeePercentage]'
GO
CREATE TABLE [dbo].[FeePercentage]
(
[FeePercentageId] [int] NOT NULL IDENTITY(1, 1),
[FeeId] [int] NOT NULL,
[Percentage] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.FeePercentage] on [dbo].[FeePercentage]'
GO
ALTER TABLE [dbo].[FeePercentage] ADD CONSTRAINT [PK_dbo.FeePercentage] PRIMARY KEY CLUSTERED  ([FeePercentageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[FeePercentage]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[FeePercentage] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[FeePercentage]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[FeePercentage] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FeeId] on [dbo].[FeePercentage]'
GO
CREATE NONCLUSTERED INDEX [IX_FeeId] ON [dbo].[FeePercentage] ([FeeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Fee]'
GO
CREATE TABLE [dbo].[Fee]
(
[FeeId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Fee] on [dbo].[Fee]'
GO
ALTER TABLE [dbo].[Fee] ADD CONSTRAINT [PK_dbo.Fee] PRIMARY KEY CLUSTERED  ([FeeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Fee]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Fee] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HelplineCharge]'
GO
CREATE TABLE [dbo].[HelplineCharge]
(
[HelplineChargeId] [int] NOT NULL IDENTITY(1, 1),
[HelplineId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Charge] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.HelplineCharge] on [dbo].[HelplineCharge]'
GO
ALTER TABLE [dbo].[HelplineCharge] ADD CONSTRAINT [PK_dbo.HelplineCharge] PRIMARY KEY CLUSTERED  ([HelplineChargeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CurrencyId] on [dbo].[HelplineCharge]'
GO
CREATE NONCLUSTERED INDEX [IX_CurrencyId] ON [dbo].[HelplineCharge] ([CurrencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[HelplineCharge]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[HelplineCharge] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[HelplineCharge]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[HelplineCharge] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HelplineId] on [dbo].[HelplineCharge]'
GO
CREATE NONCLUSTERED INDEX [IX_HelplineId] ON [dbo].[HelplineCharge] ([HelplineId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Helpline]'
GO
CREATE TABLE [dbo].[Helpline]
(
[HelplineId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Helpline] on [dbo].[Helpline]'
GO
ALTER TABLE [dbo].[Helpline] ADD CONSTRAINT [PK_dbo.Helpline] PRIMARY KEY CLUSTERED  ([HelplineId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Helpline]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Helpline] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MicroChipFactor]'
GO
CREATE TABLE [dbo].[MicroChipFactor]
(
[MicroChipFactorId] [int] NOT NULL IDENTITY(1, 1),
[MicroChipStatusId] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.MicroChipFactor] on [dbo].[MicroChipFactor]'
GO
ALTER TABLE [dbo].[MicroChipFactor] ADD CONSTRAINT [PK_dbo.MicroChipFactor] PRIMARY KEY CLUSTERED  ([MicroChipFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[MicroChipFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[MicroChipFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[MicroChipFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[MicroChipFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_MicroChipStatusId] on [dbo].[MicroChipFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_MicroChipStatusId] ON [dbo].[MicroChipFactor] ([MicroChipStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MicroChipStatus]'
GO
CREATE TABLE [dbo].[MicroChipStatus]
(
[MicroChipStatusId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.MicroChipStatus] on [dbo].[MicroChipStatus]'
GO
ALTER TABLE [dbo].[MicroChipStatus] ADD CONSTRAINT [PK_dbo.MicroChipStatus] PRIMARY KEY CLUSTERED  ([MicroChipStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[MicroChipStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[MicroChipStatus] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NeuteringFactor]'
GO
CREATE TABLE [dbo].[NeuteringFactor]
(
[NeuteringFactorId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[GenderId] [int] NOT NULL,
[IsNeutered] [bit] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.NeuteringFactor] on [dbo].[NeuteringFactor]'
GO
ALTER TABLE [dbo].[NeuteringFactor] ADD CONSTRAINT [PK_dbo.NeuteringFactor] PRIMARY KEY CLUSTERED  ([NeuteringFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[NeuteringFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[NeuteringFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[NeuteringFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[NeuteringFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GenderId] on [dbo].[NeuteringFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_GenderId] ON [dbo].[NeuteringFactor] ([GenderId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[NeuteringFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[NeuteringFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Gender]'
GO
CREATE TABLE [dbo].[Gender]
(
[GenderId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Gender] on [dbo].[Gender]'
GO
ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [PK_dbo.Gender] PRIMARY KEY CLUSTERED  ([GenderId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Gender]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Gender] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PostcodeAreaRating]'
GO
CREATE TABLE [dbo].[PostcodeAreaRating]
(
[PostcodeAreaRatingId] [int] NOT NULL IDENTITY(1, 1),
[Postcode] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AreaRating] [int] NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL,
[SalesTypeId] [int] NOT NULL,
[Decline] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.PostcodeAreaRating] on [dbo].[PostcodeAreaRating]'
GO
ALTER TABLE [dbo].[PostcodeAreaRating] ADD CONSTRAINT [PK_dbo.PostcodeAreaRating] PRIMARY KEY CLUSTERED  ([PostcodeAreaRatingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_PostcodeAreaRating_ExternalCoverId_14F4F] on [dbo].[PostcodeAreaRating]'
GO
CREATE NONCLUSTERED INDEX [IDX_PostcodeAreaRating_ExternalCoverId_14F4F] ON [dbo].[PostcodeAreaRating] ([ExternalCoverId]) INCLUDE ([AreaRating], [Decline], [EffectiveFrom], [Postcode], [SalesTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_PostcodeAreaRating_ExternalCoverId_EffectiveFrom_A1EAE] on [dbo].[PostcodeAreaRating]'
GO
CREATE NONCLUSTERED INDEX [IDX_PostcodeAreaRating_ExternalCoverId_EffectiveFrom_A1EAE] ON [dbo].[PostcodeAreaRating] ([ExternalCoverId], [EffectiveFrom]) INCLUDE ([AreaRating], [Decline], [Postcode], [SalesTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_PostcodeAreaRating_Postcode_ExternalCoverId_EffectiveFrom_AD713] on [dbo].[PostcodeAreaRating]'
GO
CREATE NONCLUSTERED INDEX [IDX_PostcodeAreaRating_Postcode_ExternalCoverId_EffectiveFrom_AD713] ON [dbo].[PostcodeAreaRating] ([Postcode], [ExternalCoverId], [EffectiveFrom]) INCLUDE ([AreaRating], [PostcodeAreaRatingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_PostcodeAreaRating_Postcode_ExternalCoverId_EffectiveFrom_49E7C] on [dbo].[PostcodeAreaRating]'
GO
CREATE NONCLUSTERED INDEX [IDX_PostcodeAreaRating_Postcode_ExternalCoverId_EffectiveFrom_49E7C] ON [dbo].[PostcodeAreaRating] ([Postcode], [ExternalCoverId], [EffectiveFrom]) INCLUDE ([AreaRating], [Decline], [PostcodeAreaRatingId], [SalesTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SalesType]'
GO
CREATE TABLE [dbo].[SalesType]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SalesType] on [dbo].[SalesType]'
GO
ALTER TABLE [dbo].[SalesType] ADD CONSTRAINT [PK_SalesType] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PremiumMinimum]'
GO
CREATE TABLE [dbo].[PremiumMinimum]
(
[PremiumMinimumId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Price] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.PremiumMinimum] on [dbo].[PremiumMinimum]'
GO
ALTER TABLE [dbo].[PremiumMinimum] ADD CONSTRAINT [PK_dbo.PremiumMinimum] PRIMARY KEY CLUSTERED  ([PremiumMinimumId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CurrencyId] on [dbo].[PremiumMinimum]'
GO
CREATE NONCLUSTERED INDEX [IX_CurrencyId] ON [dbo].[PremiumMinimum] ([CurrencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[PremiumMinimum]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[PremiumMinimum] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[PremiumMinimum]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[PremiumMinimum] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[PremiumMinimum]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[PremiumMinimum] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PriceFactor]'
GO
CREATE TABLE [dbo].[PriceFactor]
(
[PriceFactorId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[PriceFrom] [decimal] (18, 2) NOT NULL,
[PriceTo] [decimal] (18, 2) NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.PriceFactor] on [dbo].[PriceFactor]'
GO
ALTER TABLE [dbo].[PriceFactor] ADD CONSTRAINT [PK_dbo.PriceFactor] PRIMARY KEY CLUSTERED  ([PriceFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CurrencyId] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_CurrencyId] ON [dbo].[PriceFactor] ([CurrencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[PriceFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[PriceFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[PriceFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PriceFrom] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PriceFrom] ON [dbo].[PriceFactor] ([PriceFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PriceTo] on [dbo].[PriceFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PriceTo] ON [dbo].[PriceFactor] ([PriceTo])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SalesChannelFactor]'
GO
CREATE TABLE [dbo].[SalesChannelFactor]
(
[SalesChannelFactorId] [int] NOT NULL IDENTITY(1, 1),
[SalesChannelId] [int] NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.SalesChannelFactor] on [dbo].[SalesChannelFactor]'
GO
ALTER TABLE [dbo].[SalesChannelFactor] ADD CONSTRAINT [PK_dbo.SalesChannelFactor] PRIMARY KEY CLUSTERED  ([SalesChannelFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[SalesChannelFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[SalesChannelFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[SalesChannelFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[SalesChannelFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SalesChannelId] on [dbo].[SalesChannelFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_SalesChannelId] ON [dbo].[SalesChannelFactor] ([SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ScopeConfiguration]'
GO
CREATE TABLE [dbo].[ScopeConfiguration]
(
[ScopeConfigurationId] [int] NOT NULL IDENTITY(1, 1),
[ExternalCoverId] [int] NOT NULL,
[ClaimsQuarters] [int] NOT NULL,
[CapQuarters] [int] NOT NULL,
[CollarQuarters] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.ScopeConfiguration] on [dbo].[ScopeConfiguration]'
GO
ALTER TABLE [dbo].[ScopeConfiguration] ADD CONSTRAINT [PK_dbo.ScopeConfiguration] PRIMARY KEY CLUSTERED  ([ScopeConfigurationId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TaxRate]'
GO
CREATE TABLE [dbo].[TaxRate]
(
[TaxRateId] [int] NOT NULL IDENTITY(1, 1),
[TaxId] [int] NOT NULL,
[Rate] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.TaxRate] on [dbo].[TaxRate]'
GO
ALTER TABLE [dbo].[TaxRate] ADD CONSTRAINT [PK_dbo.TaxRate] PRIMARY KEY CLUSTERED  ([TaxRateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[TaxRate]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[TaxRate] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[TaxRate]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[TaxRate] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TaxId] on [dbo].[TaxRate]'
GO
CREATE NONCLUSTERED INDEX [IX_TaxId] ON [dbo].[TaxRate] ([TaxId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Tax]'
GO
CREATE TABLE [dbo].[Tax]
(
[TaxId] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.Tax] on [dbo].[Tax]'
GO
ALTER TABLE [dbo].[Tax] ADD CONSTRAINT [PK_dbo.Tax] PRIMARY KEY CLUSTERED  ([TaxId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[Tax]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[Tax] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TplCharge]'
GO
CREATE TABLE [dbo].[TplCharge]
(
[TplChargeId] [int] NOT NULL IDENTITY(1, 1),
[CurrencyId] [int] NOT NULL,
[PetTypeId] [int] NOT NULL,
[Charge] [decimal] (18, 2) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.TplCharge] on [dbo].[TplCharge]'
GO
ALTER TABLE [dbo].[TplCharge] ADD CONSTRAINT [PK_dbo.TplCharge] PRIMARY KEY CLUSTERED  ([TplChargeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[VoluntaryExcessFactor]'
GO
CREATE TABLE [dbo].[VoluntaryExcessFactor]
(
[VoluntaryExcessFactorId] [int] NOT NULL IDENTITY(1, 1),
[PetTypeId] [int] NOT NULL,
[ExcessAmount] [decimal] (18, 2) NOT NULL,
[Factor] [decimal] (12, 5) NOT NULL,
[ExternalCoverId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.VoluntaryExcessFactor] on [dbo].[VoluntaryExcessFactor]'
GO
ALTER TABLE [dbo].[VoluntaryExcessFactor] ADD CONSTRAINT [PK_dbo.VoluntaryExcessFactor] PRIMARY KEY CLUSTERED  ([VoluntaryExcessFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[VoluntaryExcessFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[VoluntaryExcessFactor] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ExternalCoverId] on [dbo].[VoluntaryExcessFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_ExternalCoverId] ON [dbo].[VoluntaryExcessFactor] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PetTypeId] on [dbo].[VoluntaryExcessFactor]'
GO
CREATE NONCLUSTERED INDEX [IX_PetTypeId] ON [dbo].[VoluntaryExcessFactor] ([PetTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ExternalCoverSalesType]'
GO
CREATE TABLE [dbo].[ExternalCoverSalesType]
(
[ExternalCoverSalesTypeId] [int] NOT NULL IDENTITY(1, 1),
[ExternalCoverId] [int] NOT NULL,
[SalesChannelId] [int] NOT NULL,
[EffectiveFrom] [datetime] NOT NULL CONSTRAINT [DF_ExternalCoverSalesType_EffectiveFrom] DEFAULT (getdate()),
[EffectiveTo] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_ExternalCoverSalesType] on [dbo].[ExternalCoverSalesType]'
GO
ALTER TABLE [dbo].[ExternalCoverSalesType] ADD CONSTRAINT [PK_ExternalCoverSalesType] PRIMARY KEY CLUSTERED  ([ExternalCoverSalesTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Discount].[GetAll]'
GO

CREATE PROCEDURE [Discount].[GetAll]
	
AS
BEGIN
	SELECT [DiscountId]
      ,[Description]
      ,[Key]
      ,[TotalDiscountRate]
      ,[EffectiveFrom]
      ,[ExternalCoverId]
      ,[SalesChannelId]
      ,[CanApplyAtRenewal]
  FROM [dbo].[Discount]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Discount].[GetByCoverOnDate]'
GO

/*
		  var discounts = ZenithContext.Discounts.Where(f =>
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .ToList();
*/

CREATE PROCEDURE [Discount].[GetByCoverOnDate]
	@externalCoverId INT,
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT 
	[DiscountId]
      ,[Description]
      ,[Key]
      ,[TotalDiscountRate]
      ,[EffectiveFrom]
      ,[ExternalCoverId]
      ,[SalesChannelId]
      ,[CanApplyAtRenewal]
  FROM [dbo].[Discount]
  WHERE 
		ExternalCoverId = @externalCoverId
		AND EffectiveFrom <= @onThisDate
  ORDER BY EffectiveFrom desc

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Discount].[GetById]'
GO

/*
			  var discount = ZenithContext.Discounts.Single(f => f.DiscountId == discountId);
                return discount;
*/

CREATE PROCEDURE [Discount].[GetById]
	@discountId int
AS
BEGIN
	
	SELECT TOP 1
		[DiscountId]
      ,[Description]
      ,[Key]
      ,[TotalDiscountRate]
      ,[EffectiveFrom]
      ,[ExternalCoverId]
      ,[SalesChannelId]
      ,[CanApplyAtRenewal]
  FROM [dbo].[Discount]
  WHERE DiscountId = @discountId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Discount].[GetByKeyCoverSalesChannelAndDate]'
GO

/*
		     var discount = ZenithContext.Discounts.Where(f =>
                    f.Key == key &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate &&
                    (f.SalesChannelId == salesChannelId || !f.SalesChannelId.HasValue)
                ).OrderByDescending(x => x.EffectiveFrom).First();
*/

CREATE PROCEDURE [Discount].[GetByKeyCoverSalesChannelAndDate]
	@key VARCHAR(100), 
	@externalCoverId int, 
	@onThisDate DATETIME,
	@salesChannelId INT = NULL
AS
BEGIN
	
	SELECT TOP 1
		[DiscountId]
      ,[Description]
      ,[Key]
      ,[TotalDiscountRate]
      ,[EffectiveFrom]
      ,[ExternalCoverId]
      ,[SalesChannelId]
      ,[CanApplyAtRenewal]
  FROM [dbo].[Discount]
  WHERE 
	    [Key] = @key
    AND ExternalCoverId = @externalCoverId
    AND EffectiveFrom <= @onThisDate
	AND (ISNULL(SalesChannelId,-1) = ISNULL(@salesChannelId,-1) OR SalesChannelId IS null)
  ORDER BY EffectiveFrom desc

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Discount].[GetByKeyListOfCoversSalesChannelAndDate]'
GO

/****** Object:  StoredProcedure  [Discount].[GetByKeyListOfCoversSalesChannelAndDate]    ******/
CREATE PROCEDURE [Discount].[GetByKeyListOfCoversSalesChannelAndDate]
	@key VARCHAR(100), 
	@coverIds Core.ListOfInt READONLY, 
	@onThisDate DATETIME,
	@salesChannelId INT = null
AS
BEGIN
	
	SELECT 
		d.[DiscountId]
      ,d.[Description]
      ,d.[Key]
      ,d.[TotalDiscountRate]
      ,d.[EffectiveFrom]
      ,d.[ExternalCoverId]
      ,d.[SalesChannelId]
      ,d.[CanApplyAtRenewal]
  FROM [dbo].[Discount] d
  INNER join [dbo].DiscountRate dr ON dr.DiscountId = d.DiscountId
  WHERE 
	    d.[Key] = @key
    AND d.ExternalCoverId IN (SELECT Id from @coverIds)
    AND d.EffectiveFrom <= @onThisDate
	AND (d.SalesChannelId IS NULL OR d.SalesChannelId = @salesChannelId)
	AND dr.EffectiveFrom <= @onThisDate
	AND dr.EffectiveTo >= @onThisDate
  ORDER BY EffectiveFrom desc

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetAllCurrencies]'
GO

CREATE PROCEDURE [Management].[GetAllCurrencies]
	
AS
BEGIN
	SELECT [CurrencyId]
      ,[Code]
      ,[Symbol]
      ,[Description]
      ,[EffectiveFrom]
  FROM [ZenithPricingEngine].[dbo].[Currency]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetBreeds]'
GO

CREATE PROCEDURE [Management].[GetBreeds]
	
AS
BEGIN
	SELECT  [BreedId]
      ,[PetTypeId]
      ,[Description]
      ,[EffectiveFrom]
	FROM [dbo].[Breed]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetCommissionRecipients]'
GO

CREATE PROCEDURE [Management].[GetCommissionRecipients]
	
AS
BEGIN
	SELECT [CommissionRecipientId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[CommissionRecipient]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetCommissionTypes]'
GO

CREATE PROCEDURE [Management].[GetCommissionTypes]
	
AS
BEGIN
	SELECT [CommissionTypeId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[CommissionType]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetExternalCovers]'
GO

CREATE PROCEDURE [Management].[GetExternalCovers]
	
AS
BEGIN
	SELECT [ExternalCoverId]
      ,[AccidentOnly]
      ,[EffectiveFrom]
  FROM [dbo].[ExternalCover]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetFees]'
GO

CREATE PROCEDURE [Management].[GetFees]
	
AS
BEGIN
	SELECT [FeeId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[Fee]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetGenders]'
GO

CREATE PROCEDURE [Management].[GetGenders]
	
AS
BEGIN
	SELECT [GenderId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[Gender]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetHelplines]'
GO

CREATE PROCEDURE [Management].[GetHelplines]
	
AS
BEGIN
	SELECT [HelplineId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[Helpline]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetLifeSpans]'
GO

CREATE PROCEDURE [Management].[GetLifeSpans]
	
AS
BEGIN
	SELECT [LifeSpanId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[LifeSpan]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetMicroChipStatuses]'
GO

CREATE PROCEDURE [Management].[GetMicroChipStatuses]
	
AS
BEGIN
	SELECT[MicroChipStatusId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[MicroChipStatus]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetPetTypes]'
GO

CREATE PROCEDURE [Management].[GetPetTypes]
	
AS
BEGIN
	SELECT [PetTypeId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[PetType]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PostcodeTaxExemption]'
GO
CREATE TABLE [dbo].[PostcodeTaxExemption]
(
[PostcodeTaxExemptionId] [int] NOT NULL IDENTITY(1, 1),
[Postcode] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[EffectiveFrom] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo.PostcodeTaxExemption] on [dbo].[PostcodeTaxExemption]'
GO
ALTER TABLE [dbo].[PostcodeTaxExemption] ADD CONSTRAINT [PK_dbo.PostcodeTaxExemption] PRIMARY KEY CLUSTERED  ([PostcodeTaxExemptionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EffectiveFrom] on [dbo].[PostcodeTaxExemption]'
GO
CREATE NONCLUSTERED INDEX [IX_EffectiveFrom] ON [dbo].[PostcodeTaxExemption] ([EffectiveFrom])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Postcode] on [dbo].[PostcodeTaxExemption]'
GO
CREATE NONCLUSTERED INDEX [IX_Postcode] ON [dbo].[PostcodeTaxExemption] ([Postcode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetPostcodeTaxExemptions]'
GO

CREATE PROCEDURE [Management].[GetPostcodeTaxExemptions]
	
AS
BEGIN
	SELECT  [PostcodeTaxExemptionId]
      ,[Postcode]
      ,[EffectiveFrom]
  FROM [dbo].[PostcodeTaxExemption]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetReoccuranceLikelihoods]'
GO

CREATE PROCEDURE [Management].[GetReoccuranceLikelihoods]
	
AS
BEGIN
	SELECT [ReoccuranceLikelihoodId]
      ,[Description]
      ,[Order]
      ,[EffectiveFrom]
  FROM [dbo].[ReoccuranceLikelihood]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetSalesChannels]'
GO

CREATE PROCEDURE [Management].[GetSalesChannels]
	
AS
BEGIN
	SELECT [SalesChannelId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[SalesChannel]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Management].[GetTaxes]'
GO

CREATE PROCEDURE [Management].[GetTaxes]
	
AS
BEGIN
	SELECT [TaxId]
      ,[Description]
      ,[EffectiveFrom]
  FROM [dbo].[Tax]
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetAdminFee]'
GO

/*
	    var adminFees = ZenithContext.AdminFees.Where(n => n.EffectiveFrom <= onThisDate
                        && n.ExternalCoverId == externalCoverId
                        && (!petTypeId.HasValue || n.PetTypeId == petTypeId)
                        && (!salesChannelId.HasValue || n.SalesChannelId == salesChannelId));

                return adminFees == null || !adminFees.Any(am => am != null)
                       ? 0
                       : adminFees.Sum(am => am.Amount);
*/

CREATE PROCEDURE [Rating].[GetAdminFee]
	@externalCoverId int, 
	@onThisDate DATETIME,
	@petTypeId INT =null,
	@salesChannelId INT =null
AS
BEGIN
	
	SELECT
		ISNULL(SUM(af.Amount),0) as Factor
	FROM dbo.AdminFee af
	WHERE  
		af.ExternalCoverId = @externalCoverId
		AND af.EffectiveFrom <= @onThisDate
		AND (@salesChannelId IS NULL OR (@salesChannelId IS NOT NULL AND af.SalesChannelId = @salesChannelId))
		AND (@petTypeId IS NULL OR (@petTypeId IS NOT NULL AND af.PetTypeId = @petTypeId))


END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetAgeAtInceptionFactor]'
GO

/*
 decimal GetAgeAtInceptionFactor(int petTypeId, int petAgeAtInceptionInYears, int externalCoverId, DateTime onThisDate)

 var ageAtInceptionFactors = ZenithContext.AgeAtInceptionFactors.Where(f =>
                    f.PetTypeId == petTypeId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate);

                var ageAtInceptionFactorYear = ageAtInceptionFactors.Where(x => x.PetAgeYears <= petAgeAtInceptionInYears).OrderByDescending(x => x.PetAgeYears).First().PetAgeYears;

                var ageAtInceptionFactor = ageAtInceptionFactors.Where(x => x.PetAgeYears == ageAtInceptionFactorYear).OrderByDescending(x => x.EffectiveFrom).First();

                return ageAtInceptionFactor.Factor;
*/

CREATE PROCEDURE [Rating].[GetAgeAtInceptionFactor]
	@petTypeId int,
	@petAgeAtInceptionInYears int, 
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	--var ageAtInceptionFactorYear = ageAtInceptionFactors.Where(x => x.PetAgeYears <= petAgeAtInceptionInYears).OrderByDescending(x => x.PetAgeYears).First().PetAgeYears;
	DECLARE @ageAtInceptionFactorYear INT
	SELECT TOP 1
		@ageAtInceptionFactorYear = a.PetAgeYears
	FROM dbo.AgeAtInceptionFactor a
	WHERE a.PetTypeId = @petTypeId
	AND a.ExternalCoverId = @externalCoverId
	AND a.EffectiveFrom <= @onThisDate
	AND PetAgeYears <= @petAgeAtInceptionInYears
	ORDER BY a.PetAgeYears desc

	SELECT TOP 1
		a.Factor Factor
	FROM dbo.AgeAtInceptionFactor a
	WHERE a.PetTypeId = @petTypeId
	AND a.ExternalCoverId = @externalCoverId
	AND a.EffectiveFrom <= @onThisDate
	AND a.PetAgeYears = @ageAtInceptionFactorYear
	ORDER BY a.EffectiveFrom desc




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetAgeFactor]'
GO

/*
  var breedLifeSpan = ZenithContext.BreedLifeSpans.Single(x => x.BreedId == breedId && x.ExternalCoverId == externalCoverId);

                var ageFactors = ZenithContext.AgeFactors.Where(f =>
                    f.PetAgeMonthsFrom <= petAgeInMonths &&
                    f.PetAgeMonthsTo >= petAgeInMonths &&
                    f.PetTypeId == petTypeId &&
                    f.LifeSpanId == breedLifeSpan.LifeSpanId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.SalesTypeId == (int)salesType &&
                    (f.SalesChannelId == salesChannelId || f.SalesChannelId == (int)SalesChannel.Any) &&
                    f.EffectiveFrom <= onThisDate &&
                    (f.EffectiveTo == null || f.EffectiveTo > onThisDate))
                .OrderByDescending(r => r.PetAgeMonthsFrom).ThenByDescending(r => r.EffectiveFrom)
                .ToList();

                var ageFactor = ageFactors.FirstOrDefault(x => x.SalesChannelId == salesChannelId) ??
                                ageFactors.FirstOrDefault(x => x.SalesChannelId == (int)SalesChannel.Any);
*/

CREATE PROCEDURE [Rating].[GetAgeFactor]
	@petTypeId int,
	@breedId int, 
	@petAgeInMonths decimal(12,5),
	@externalCoverId int, 
	@onThisDate DateTime, 
	@salesTypeId int,
	@salesChannelId INT,
    @anySalesChannelId int
AS
BEGIN
	DECLARE @breedLifeSpanId int
	--var breedLifeSpan = ZenithContext.BreedLifeSpans.Single(x => x.BreedId == breedId && x.ExternalCoverId == externalCoverId);
	DECLARE @ageAtInceptionFactorYear INT
	SELECT TOP 1
		@breedLifeSpanId = b.LifeSpanId
	FROM dbo.BreedLifeSpan b
	WHERE b.BreedId = @breedId
	AND b.ExternalCoverId = @externalCoverId
	ORDER BY b.EffectiveFrom desc

	DECLARE @factor DECIMAL(12,5)

	SELECT TOP 1 @factor = a.Factor
	FROM dbo.AgeFactor a
	WHERE a.PetAgeMonthsFrom <= @petAgeInMonths
				AND a.PetAgeMonthsTo >= @petAgeInMonths 
				AND a.PetTypeId = @petTypeId 
				AND a.LifeSpanId = @breedLifeSpanId 
				AND a.ExternalCoverId = @externalCoverId 
				AND a.SalesTypeId = @salesTypeId
				AND a.SalesChannelId = @salesChannelId
				AND a.EffectiveFrom <= @onThisDate 
				AND ISNULL(a.EffectiveTo, DATEADD(DAY, 1, @onThisDate))> @onThisDate
	ORDER BY a.PetAgeMonthsFrom desc, a.EffectiveFrom desc

    IF(@factor IS NULL)
	begin
		SELECT TOP 1 @factor = a.Factor
		FROM dbo.AgeFactor a
		WHERE a.PetAgeMonthsFrom <= @petAgeInMonths
					AND a.PetAgeMonthsTo >= @petAgeInMonths 
					AND a.PetTypeId = @petTypeId 
					AND a.LifeSpanId = @breedLifeSpanId 
					AND a.ExternalCoverId = @externalCoverId 
					AND a.SalesTypeId = @salesTypeId
					AND a.SalesChannelId = @anySalesChannelId
					AND a.EffectiveFrom <= @onThisDate 
					AND ISNULL(a.EffectiveTo, DATEADD(DAY, 1, @onThisDate))> @onThisDate
		ORDER BY a.PetAgeMonthsFrom desc, a.EffectiveFrom desc
	END
    
	SELECT @factor Factor


END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetAreaRating]'
GO

/*
var postcodeAreaRatings = ZenithContext.PostcodeAreaRatings.Where(r =>
        r.Postcode == petPostcode &&
        r.ExternalCoverId == externalCoverId &&
        r.EffectiveFrom <= onThisDate)
    .OrderByDescending(x => x.EffectiveFrom);

if (!postcodeAreaRatings.Any())
{
    postcodeAreaRatings = ZenithContext.PostcodeAreaRatings.Where(r =>
            r.Postcode == PostcodeAreaRating.OTHER_POSTCODE_STING &&
            r.ExternalCoverId == externalCoverId &&
            r.EffectiveFrom <= onThisDate)
        .OrderByDescending(x => x.EffectiveFrom);
}

var postcodeAreaRating = postcodeAreaRatings.First();

if (postcodeAreaRating.Decline)
{
    throw new DeclineException($"The postcode {petPostcode} for cover {externalCoverId} has been declined.");
}

*/

CREATE PROCEDURE [Rating].[GetAreaRating]
	@postcode varchar(10),
	@externalCoverId int,
	@onThisDate DATETIME
AS
BEGIN 
	
	--on the code we will have to call this proc twice
	--if not found for the current postocde, then make another call to the DB using PostcodeAreaRating.OTHER_POSTCODE_STING

	SELECT 
		[PostcodeAreaRatingId]
      ,[Postcode]
      ,[AreaRating]
      ,[ExternalCoverId]
      ,[EffectiveFrom]
      ,[SalesTypeId]
      ,[Decline]
	FROM dbo.PostcodeAreaRating
	WHERE Postcode = @postcode
	AND ExternalCoverId = @externalCoverId
	AND EffectiveFrom <= @onThisDate
	ORDER BY EffectiveFrom desc

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetAreaRatingFactor]'
GO

/*
var postcodeAreaRatingId = GetAreaRatingId(petPostcode, externalCoverId, onThisDate, isRenewal);

AreaRatingFactor areaRatingFactor = ZenithContext.AreaRatingFactors.Where(f =>
        f.AreaRating == postcodeAreaRatingId &&
        f.ExternalCoverId == externalCoverId &&
        f.EffectiveFrom <= onThisDate)
    .OrderByDescending(r => r.EffectiveFrom)
    .First();

return areaRatingFactor.Factor;
*/

CREATE PROCEDURE [Rating].[GetAreaRatingFactor]
	@postcodeAreaRatingId int,
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1 
		a.Factor Factor
	FROM dbo.AreaRatingFactor a
	WHERE a.AreaRating = @postcodeAreaRatingId
	AND a.ExternalCoverId = @externalCoverId
	AND a.EffectiveFrom <= @onThisDate
	ORDER BY a.EffectiveFrom DESC




END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetBaseRate]'
GO

/*
decimal GetBaseRate(int petTypeId, int externalCoverId, DateTime onThisDate, int salesChannelId)

 var baseRate = ZenithContext.BaseRates
                    .Join(ZenithContext.ExternalCoverSalesTypes, x => x.ExternalCoverId, y => y.ExternalCoverId, (x, y) => new { BaseRates = x, ExternalCovers = y })
                    .Where(r =>
                    r.BaseRates.PetTypeId == petTypeId &&
                    r.BaseRates.ExternalCoverId == externalCoverId &&
                    r.BaseRates.EffectiveFrom <= onThisDate &&
                    r.ExternalCovers.EffectiveFrom <= onThisDate &&
                    (r.ExternalCovers.EffectiveTo == null || r.ExternalCovers.EffectiveTo > onThisDate) &&
                    (r.ExternalCovers.SalesChannelId == salesChannelId || r.ExternalCovers.SalesChannelId == (int)SalesChannel.Any))
                    .OrderByDescending(r => r.BaseRates.EffectiveFrom)
                    .First();

                return baseRate.BaseRates.Price;
*/

CREATE PROCEDURE [Rating].[GetBaseRate]
	@petTypeId int, 
	@externalCoverId int, 
	@onThisDate datetime,
	@salesChannelId INT,
	@anySalesChannelId INT --This one will related to the enum: SalesChannel.Any
AS
BEGIN
	SELECT TOP 1
		br.Price AS Factor
	FROM dbo.BaseRate br
	INNER JOIN dbo.ExternalCoverSalesType ecst ON ecst.ExternalCoverId = br.ExternalCoverId
	WHERE 
		br.PetTypeId = @petTypeId AND 
		br.ExternalCoverId = @externalCoverId and
		br.EffectiveFrom <= @onThisDate and
		ecst.EffectiveFrom <= @onThisDate and
		ISNULL(ecst.EffectiveTo, DATEADD(dd,1,@onThisDate)) > @onThisDate  --( r.ExternalCovers.EffectiveTo == null || r.ExternalCovers.EffectiveTo > onThisDate) &&
		AND (ecst.SalesChannelId = @salesChannelId OR ecst.SalesChannelId = @anySalesChannelId)  --(r.ExternalCovers.SalesChannelId == salesChannelId || r.ExternalCovers.SalesChannelId == (int)SalesChannel.Any)
	ORDER BY br.EffectiveFrom DESC
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetBreedGroupAreaFactor]'
GO

/*
var breedGroupId = GetBreedGroupId(breedId, isRenewal, externalCoverId, onThisDate);
var areaId = GetAreaRatingId(postcode, externalCoverId, onThisDate, isRenewal);
				
var breedGroupAreaFactor = ZenithContext.BreedGroupAreaRates.Where(x =>
                        x.AreaRatingId == areaId
                        && x.ExternalCoverId == externalCoverId
                        && x.BreedGroupId == breedGroupId
                        && x.EffectiveFrom <= onThisDate
                    ).OrderByDescending(x => x.EffectiveFrom)
                    .FirstOrDefault();

*/

CREATE PROCEDURE [Rating].[GetBreedGroupAreaFactor]
	@breedGroupId int, 
	@areaId int, 
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		b.Factor Factor
	FROM dbo.BreedGroupAreaRate b
	WHERE   b.AreaRatingId = @areaId
            and b.ExternalCoverId = @externalCoverId
            and b.BreedGroupId = @breedGroupId
            and b.EffectiveFrom <= @onThisDate
    ORDER BY b.EffectiveFrom desc




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetBreedGroupFactor]'
GO

/*
int breedGroupId = GetBreedGroupId(breedId, isRenewal, externalCoverId, onThisDate);

var breedGroupFactor = ZenithContext.BreedGroupFactors.Where(f =>
        f.BreedGroupId == breedGroupId &&
        f.ExternalCoverId == externalCoverId &&
        f.EffectiveFrom <= onThisDate)
    .OrderByDescending(r => r.EffectiveFrom).First();
*/

CREATE PROCEDURE [Rating].[GetBreedGroupFactor]
	@breedGroupId int, 
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		b.Factor Factor
	FROM dbo.BreedGroupFactor b
	WHERE b.BreedGroupId = @breedGroupId
		AND b.ExternalCoverId = @externalCoverId    
		AND b.EffectiveFrom <= @onThisDate
    ORDER BY b.EffectiveFrom desc




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetBreedGroupId]'
GO

/*
var breedFactor = ZenithContext.BreedGroups.Where(f =>
    f.BreedId == breedId &&
    f.ExternalCoverId == externalCoverId
).OrderByDescending(r => r.EffectiveFrom).FirstOrDefault();
*/

CREATE PROCEDURE [Rating].[GetBreedGroupId]
	@breedId int, 
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		b.RenewalBreedGroupId, b.NewBusinessBreedGroupId
	FROM dbo.BreedGroup b
	WHERE b.BreedId = @breedId
        AND b.ExternalCoverId = @externalCoverId
		AND b.EffectiveFrom <= @onThisDate --original query did not filter by date
    ORDER BY b.EffectiveFrom desc




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetCappingRate]'
GO

/*
	 var cappingRate = ZenithContext.CappingRates.Where(f =>
                    f.PetAgeMonthsFrom <= petAgeInMonths &&
                    f.PetAgeMonthsTo > petAgeInMonths &&
                    f.ExternalCoverId == externalCoverId &&
                    f.PetTypeId == petTypeId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();

                return isCleanCap ? cappingRate.CleanCapRate : cappingRate.ClaimCapRate;
*/

CREATE PROCEDURE [Rating].[GetCappingRate]
	@petTypeId int, 
	@externalCoverId int, 
	@petAgeInMonths decimal(12,5), 
	@isCleanCap bit, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		CASE WHEN @isCleanCap = 1 THEN c.CleanCapRate ELSE c.ClaimCapRate END as Factor
	FROM dbo.CappingRate c
	WHERE  
		c.PetAgeMonthsFrom <= @petAgeInMonths and
		c.PetAgeMonthsTo > @petAgeInMonths and
		c.ExternalCoverId = @externalCoverId and
		c.PetTypeId = @petTypeId and
		c.EffectiveFrom <= @onThisDate
	ORDER BY c.EffectiveFrom desc



END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetClaimsLoadingFactor]'
GO

/*
  var voluntaryExcessFactor = ZenithContext.VoluntaryExcessFactors.Where(f =>
                    f.PetTypeId == petTypeId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.ExcessAmount == excessAmount &&
                    f.EffectiveFrom <= onThisDate)
                    .OrderByDescending(r => r.EffectiveFrom)
                    .First().Factor;
*/

CREATE PROCEDURE [Rating].[GetClaimsLoadingFactor]
	@lastClaimQuarter int,
	@reoccuranceLikeihoodId int,
	@claimValue decimal(12,5),
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1 
		c.Factor Factor
	FROM dbo.ClaimsLoadingFactor c
	WHERE c.QuatersFromRenewal = @lastClaimQuarter
		AND c.ReoccuranceLikelihoodId = @reoccuranceLikeihoodId
		AND c.ClaimsValueFrom <= @claimValue
		AND c.ClaimsValueTo > @claimValue
		AND c.ExternalCoverId = @externalCoverId
		AND c.EffectiveFrom <= @onThisDate
	ORDER BY c.EffectiveFrom DESC




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetCollaringRate]'
GO

/*
	  var collaringRate = ZenithContext.CollaringRates.Where(f =>
                    f.PetAgeMonthsFrom <= petAgeInMonths &&
                    f.PetAgeMonthsTo > petAgeInMonths &&
                    f.ExternalCoverId == externalCoverId &&
                    f.PetTypeId == petTypeId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();

                return isCleanCap ? collaringRate.CleanCollarRate : collaringRate.ClaimCollarRate;
*/

CREATE PROCEDURE [Rating].[GetCollaringRate]
	@petTypeId int, 
	@externalCoverId int, 
	@petAgeInMonths decimal(12,5), 
	@isCleanCap bit, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		CASE WHEN @isCleanCap = 1 THEN c.CleanCollarRate ELSE c.ClaimCollarRate END as Factor
	FROM dbo.CollaringRate c
	WHERE  
		c.PetAgeMonthsFrom <= @petAgeInMonths and
		c.PetAgeMonthsTo > @petAgeInMonths and
		c.ExternalCoverId = @externalCoverId and
		c.PetTypeId = @petTypeId and
		c.EffectiveFrom <= @onThisDate
	ORDER BY c.EffectiveFrom desc



END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetCommissionRates]'
GO

/*
 var commissionFactors = ZenithContext.CommissionFactors.Include(x=>x.CommissionRecipient).Where(f =>
                    f.PetAgeMonthsFrom <= petAgeInMonths &&
                    f.PetAgeMonthsTo > petAgeInMonths &&
                    f.ExternalCoverId == externalCoverId &&
                    f.PetTypeId == petTypeId &&
                    f.CommissionTypeId == commissionTypeId &&
                    f.SalesChannelId == salesChannelId &&
                    //f.CommissionRecipientId == commissionRecipientId &&
                    f.EffectiveFrom <= onThisDate
                )
                .OrderByDescending(r => r.EffectiveFrom)
                .ToList();

                if (commissionFactors.Count == 0)
                    throw new InvalidOperationException("No commission factors found");

                var idList = new List<int>();

                foreach (var commissionFactor in commissionFactors.ToList())
                {
                    if (idList.Contains(commissionFactor.CommissionRecipientId))
                        commissionFactors.Remove(commissionFactor);

                    idList.Add(commissionFactor.CommissionRecipientId);
                }

                return commissionFactors;
*/

CREATE PROCEDURE [Rating].[GetCommissionRates]
	@petAgeInMonths decimal(12,5),
	@petTypeId int,
	@commissionTypeId int,
	@salesChannelId INT,
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN

;WITH CommissionFactorCte AS
(
	SELECT 
		*, 
		 --partiting this by recipient id, order by effective
		 --so the most recent record for certain CommissionRecipientId will have the RowNumber 1
		 ROW_NUMBER() OVER (PARTITION BY CommissionRecipientId ORDER BY c.EffectiveFrom DESC, c.CommissionFactorId desc) AS RowNumber 
	FROM dbo.CommissionFactor c
	WHERE c.PetAgeMonthsFrom <= @petAgeInMonths 
		AND  c.PetAgeMonthsTo > @petAgeInMonths               
		AND c.PetTypeId = @petTypeId
		AND c.CommissionTypeId = @commissionTypeId
		AND c.SalesChannelId = @salesChannelId
		AND c.ExternalCoverId = @externalCoverId
		AND c.EffectiveFrom <= @onThisDate
)

SELECT
	  cf.[CommissionFactorId]
      ,cf.[CommissionTypeId]
      ,cf.[CommissionRecipientId]
      ,cf.[SalesChannelId]
      ,cf.[PetTypeId]
      ,cf.[PetAgeMonthsFrom]
      ,cf.[PetAgeMonthsTo]
      ,cf.[Factor]
      ,cf.[ExternalCoverId]
      ,cf.[EffectiveFrom]
	  ,cr.[CommissionRecipientId] AS CommissionRecipient_Id
	  ,cr.[Description] AS CommissionRecipient_Description
      ,cr.[EffectiveFrom] AS CommissionRecipient_EffectiveFrom
FROM CommissionFactorCte cf
INNER JOIN dbo.CommissionRecipient cr ON cr.CommissionRecipientId = cf.CommissionRecipientId
WHERE RowNumber = 1 --now we filter out just the first rows so we can get rid of duplicated recipients in our list



END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetCommissionRecipientById]'
GO

/*
	To be used in the lazy loading
*/

CREATE PROCEDURE [Rating].[GetCommissionRecipientById]
	@commissionRecipientId int
AS
BEGIN
	
SELECT [CommissionRecipientId]
    ,[Description]
    ,[EffectiveFrom]
FROM [dbo].[CommissionRecipient]
WHERE CommissionRecipientId = @commissionRecipientId




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetDiscountRates]'
GO



/*
			 var discountRates = ZenithContext.DiscountRates.Where(dr =>
                    dr.DiscountId == discountId &&
                    dr.Discount.ExternalCoverId == externalCoverId &&
                    dr.EffectiveFrom <= onThisDate &&
                    (ignoreDiscountExpiry || dr.EffectiveTo >= DateTime.Now)
                ).OrderByDescending(f => f.EffectiveFrom).ToList();

                decimal discountRateSum = 0;
                int i = 0;

                while (i < discountRates.Count && discountRateSum < 1)
                {
                    discountRateSum += discountRates.ElementAt(i).Rate;
                    i++;
                }

                return discountRates.Take(i).ToList();*/

CREATE PROCEDURE [Rating].[GetDiscountRates]
	@discountId INT, 
	@externalCoverId INT, 
	@onThisDate DATETIME, 
	@ignoreDiscountExpiry BIT
AS
BEGIN
	
	--we wont be checking if the discounts are more than 100% as in the EF query. It will just return all data that is set up and then the service layer will make the check

SELECT dr.[DiscountRateId]
      ,dr.[DiscountId]
      ,dr.[Rate]
      ,dr.[CommissionRecipientId]
      ,dr.[EffectiveFrom]
      ,dr.[EffectiveTo]
  FROM [dbo].[DiscountRate] dr
  INNER JOIN dbo.Discount d ON d.DiscountId = dr.DiscountId
  WHERE 
		dr.DiscountId = @discountId 
		AND d.ExternalCoverId = @externalCoverId 
		AND dr.EffectiveFrom <= @onThisDate 
		--AND (@ignoreDiscountExpiry = 1 OR dr.EffectiveTo >= GETDATE())
		AND (@ignoreDiscountExpiry = 1 OR dr.EffectiveTo >= @onThisDate )
   ORDER BY dr.EffectiveFrom DESC


END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetFee]'
GO

/*
	     var pclFeePercentage = ZenithContext.FeePercentages.Where(f =>
                    f.FeeId == feeId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();

                return pclFeePercentage.Percentage;
*/

CREATE PROCEDURE [Rating].[GetFee]
	@feeId int, 
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		p.Percentage as [Percentage]
	FROM dbo.FeePercentage p
	WHERE  
		p.FeeId = @feeId and
		p.ExternalCoverId = @externalCoverId and
		p.EffectiveFrom <= @onThisDate
	ORDER BY p.EffectiveFrom desc



END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetHelplineCharge]'
GO


/*
	     var helplineCharge = 0.00m;

                var query = ZenithContext.Helplines                     // source
                            .Join(ZenithContext.HelplineCharges.Where(n => n.EffectiveFrom <= onThisDate && n.ExternalCoverId == externalCoverId),        // target
                                c => c.HelplineId,                      // FK
                                cm => cm.HelplineId,                    // PK
                                (c, cm) => new { c.Description, cm.Charge, }) // project result
                            .Select(x => x);                            // select result

                foreach (var x in query)
                {
                    helplineCharge += x.Charge;
                }

                return helplineCharge;
*/

CREATE PROCEDURE [Rating].[GetHelplineCharge]
	@externalCoverId INT, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT
		ISNULL(SUM(hc.Charge),0) AS Charge
	FROM dbo.Helpline h
		INNER JOIN dbo.HelplineCharge hc ON hc.HelplineId = h.HelplineId--not sure why this join is needed because just the Charge column is used. Kept here just in case it's needed for integrity check (it ensures the HelplineId is valid)
	
	--EOD-15027 get latest row for cover and date, grouped by HelplineId 
	INNER JOIN 
	(
		 SELECT HelplineId, MAX(EffectiveFrom) [MaxEffectiveFrom]
		 FROM dbo.HelplineCharge 
		 WHERE ExternalCoverId = @externalCoverId 
		 AND EffectiveFrom <=  @onThisDate
		 GROUP BY HelplineId
	) MaxCharge ON hc.HelplineId = MaxCharge.HelplineId AND hc.[EffectiveFrom] = MaxCharge.[MaxEffectiveFrom]
	AND hc.ExternalCoverId = @externalCoverId
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetMicroChipFactor]'
GO

/*
   var microchipFactor = ZenithContext.MicroChipFactors.Where(f =>
                    f.MicroChipStatusId == microChipStatusId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();
*/

CREATE PROCEDURE [Rating].[GetMicroChipFactor]
	@microChipStatusId int, 
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		m.Factor Factor
	FROM dbo.MicroChipFactor m
	WHERE m.MicroChipStatusId = @microChipStatusId
		AND m.ExternalCoverId = @externalCoverId    
		AND m.EffectiveFrom <= @onThisDate
    ORDER BY m.EffectiveFrom desc




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetMinimumGrossPremium]'
GO

/*
	   var premiumMinimum = ZenithContext.PremiumMinimums.Where(m =>
                    m.PetTypeId == petTypeId &&
                    m.ExternalCoverId == externalCoverId &&
                    m.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();

                return premiumMinimum.Price;
*/

CREATE PROCEDURE [Rating].[GetMinimumGrossPremium]
	@petTypeId int, 
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		p.Price as Price
	FROM dbo.PremiumMinimum p
	WHERE  
		p.ExternalCoverId = @externalCoverId and
		p.PetTypeId = @petTypeId and
		p.EffectiveFrom <= @onThisDate
	ORDER BY p.EffectiveFrom desc



END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetNeuteringFactor]'
GO

/*
   var neuteringFactor = ZenithContext.NeuteringFactors.Where(f =>
                    f.PetTypeId == petTypeId &&
                    f.GenderId == genderId &&
                    f.IsNeutered == isNeutered &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();
*/

CREATE PROCEDURE [Rating].[GetNeuteringFactor]
	@petTypeId int, 
	@genderId int, 
	@isNeutered BIT,
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		n.Factor Factor
	FROM dbo.NeuteringFactor n
	WHERE n.PetTypeId = @petTypeId
        AND n.GenderId = @genderId
        AND n.IsNeutered = @isNeutered
		AND n.ExternalCoverId = @externalCoverId    
		AND n.EffectiveFrom <= @onThisDate
    ORDER BY n.EffectiveFrom desc




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetPriceFactor]'
GO

/*
var priceFactor = ZenithContext.PriceFactors.Where(f =>
        f.PetTypeId == petTypeId &&
        f.PriceFrom <= petPrice &&
        f.PriceTo > petPrice &&
        f.ExternalCoverId == externalCoverId &&
        f.EffectiveFrom <= onThisDate)
    .OrderByDescending(r => r.EffectiveFrom)
    .First();
*/

CREATE PROCEDURE [Rating].[GetPriceFactor]
	@petTypeId int, 
	@petPrice decimal(12,5), 
	@externalCoverId int, 
	@onThisDate datetime
AS
BEGIN
	
    SELECT TOP 1
		p.Factor Factor
	FROM dbo.PriceFactor p
	WHERE p.PetTypeId = @petTypeId
		AND p.PriceFrom <= @petPrice
        AND p.PriceTo > @petPrice
		AND p.ExternalCoverId = @externalCoverId    
		AND p.EffectiveFrom <= @onThisDate
    ORDER BY p.EffectiveFrom desc




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetReoccuranceLikelihoodByDescription]'
GO

/*
			  var reoccuranceLikelihood = ZenithContext.ReoccuranceLikelihoods.Where(
                    r => r.Description == description)
                    .OrderByDescending(x => x.EffectiveFrom).First();

                return reoccuranceLikelihood;
*/

CREATE PROCEDURE [Rating].[GetReoccuranceLikelihoodByDescription]
	@description VARCHAR(100)
AS
BEGIN
	
	SELECT TOP 1
		[ReoccuranceLikelihoodId]
      ,[Description]
      ,[Order]
      ,[EffectiveFrom]
	FROM dbo.ReoccuranceLikelihood r
	WHERE  
		r.Description = @description
	ORDER BY r.EffectiveFrom DESC	


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetReoccuranceLikelihoodById]'
GO

/*
		   var reoccuranceLikelihood = ZenithContext.ReoccuranceLikelihoods.First(f => f.ReoccuranceLikelihoodId == id);

                return reoccuranceLikelihood;
*/

CREATE PROCEDURE [Rating].[GetReoccuranceLikelihoodById] 
	@reoccuranceLikelihoodId INT
AS
BEGIN
	SELECT TOP 1
	 [ReoccuranceLikelihoodId]
      ,[Description]
      ,[Order]
      ,[EffectiveFrom]
  FROM [dbo].[ReoccuranceLikelihood]
  WHERE ReoccuranceLikelihoodId = @reoccuranceLikelihoodId

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetReoccuranceLikelihoodByOrder]'
GO

/*
			    var reoccuranceLikelihood = ZenithContext.ReoccuranceLikelihoods.Where(
                    r => r.Order == order &&
                        r.EffectiveFrom <= onThisDate)
                        .OrderByDescending(x => x.EffectiveFrom).First();

                return reoccuranceLikelihood;
*/

CREATE PROCEDURE [Rating].[GetReoccuranceLikelihoodByOrder]
	@order int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		[ReoccuranceLikelihoodId]
      ,[Description]
      ,[Order]
      ,[EffectiveFrom]
	FROM dbo.ReoccuranceLikelihood r
	WHERE  
		r.[Order] = @order
		AND r.EffectiveFrom <= @onThisDate
	ORDER BY r.EffectiveFrom DESC	


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetSalesChannelFactor]'
GO

/*
       var salesChannelFactor = ZenithContext.SalesChannelFactors.Where(f =>
                    f.SalesChannelId == salesChannelId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();
*/

CREATE PROCEDURE [Rating].[GetSalesChannelFactor]
	@salesChannelId int,
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1 
		s.Factor Factor
	FROM dbo.SalesChannelFactor s
	WHERE s.SalesChannelId = @salesChannelId 
	AND s.ExternalCoverId = @externalCoverId
	AND s.EffectiveFrom <= @onThisDate
	ORDER BY s.EffectiveFrom DESC




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetScopeConfiguration]'
GO

/*
ScopeConfiguration GetScopeConfiguration(int externalCoverId, DateTime onThisDate)

var scopeConfig = ZenithContext.ScopeConfigurations.Where(f =>
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate)
                .OrderByDescending(x => x.EffectiveFrom)
                .First();
*/

CREATE PROCEDURE [Rating].[GetScopeConfiguration]
	@externalCoverId INT,
	@onThisDate DATETIME
AS
BEGIN
	SELECT TOP 1
		[ScopeConfigurationId]
		,[ExternalCoverId]
		,[ClaimsQuarters]
		,[CapQuarters]
		,[CollarQuarters]
		,[EffectiveFrom]
	FROM dbo.ScopeConfiguration
	WHERE  ExternalCoverId = @externalCoverId AND EffectiveFrom <= @onThisDate
	ORDER BY EffectiveFrom desc
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetTax]'
GO

/*
			   var tax = ZenithContext.Taxes.Single(r => r.TaxId == taxId);

                return tax;
*/

CREATE PROCEDURE [Rating].[GetTax]
	@taxId int
AS
BEGIN
	
	SELECT TOP 1
		[TaxId]
      ,[Description]
      ,[EffectiveFrom]
	FROM dbo.Tax t
	WHERE  t.TaxId = @taxId


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetTaxRate]'
GO

/*
			   var iptRate = ZenithContext.TaxRates.Where(r =>
                     r.TaxId == taxId &&
                     r.ExternalCoverId == externalCoverId &&
                     r.EffectiveFrom <= onThisDate)
                .OrderByDescending(r => r.EffectiveFrom)
                .First();
*/

CREATE PROCEDURE [Rating].[GetTaxRate]
	@taxId int, 
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1
		t.Rate as Factor
	FROM dbo.TaxRate t
	WHERE  
		t.TaxId = @taxId
		AND t.ExternalCoverId = @externalCoverId
		AND t.EffectiveFrom <= @onThisDate
	ORDER BY t.EffectiveFrom DESC	


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetTplCharge]'
GO

/*
			decimal tpl_charge = 0.00m;

                var query = ZenithContext.TplCharges                     // source
                            .Join(ZenithContext.TplCharges.Where(n => n.EffectiveFrom <= onThisDate && n.ExternalCoverId == externalCoverId && n.PetTypeId == petTypeId),        // target
                                c => c.TplChargeId,                      // FK
                                cm => cm.TplChargeId,                    // PK
                                (c, cm) => new { Charge = cm.Charge, }) // project result
                            .Select(x => x);                            // select result

                foreach (var x in query)
                {

                    tpl_charge += x.Charge;
                }

                return tpl_charge;*/

CREATE PROCEDURE [Rating].[GetTplCharge]
	@externalCoverId int, 
	@onThisDate DATETIME,
	@petTypeId INT
AS
BEGIN
	
	--above linq query does not make much sense. It's self joining the table TplCharge and getting just the Charge value
	SELECT
		ISNULL(SUM(tc.Charge),0) as Factor
	FROM dbo.TplCharge tc
	WHERE  
		tc.ExternalCoverId = @externalCoverId
		AND tc.EffectiveFrom <= @onThisDate
		AND tc.PetTypeId = @petTypeId
		


END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetVoluntaryExcessFactor]'
GO

/*
  var voluntaryExcessFactor = ZenithContext.VoluntaryExcessFactors.Where(f =>
                    f.PetTypeId == petTypeId &&
                    f.ExternalCoverId == externalCoverId &&
                    f.ExcessAmount == excessAmount &&
                    f.EffectiveFrom <= onThisDate)
                    .OrderByDescending(r => r.EffectiveFrom)
                    .First().Factor;
*/

CREATE PROCEDURE [Rating].[GetVoluntaryExcessFactor]
	@petTypeId int,
	@excessAmount decimal(12,5),
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT TOP 1 
		v.Factor Factor
	FROM dbo.VoluntaryExcessFactor v
	WHERE v.PetTypeId = @petTypeId 
		AND v.ExternalCoverId = @externalCoverId
		AND v.ExcessAmount = @excessAmount
		AND v.ExternalCoverId = @externalCoverId
		AND v.EffectiveFrom <= @onThisDate
	ORDER BY v.EffectiveFrom DESC




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Rating].[GetVoluntaryExcessValuesByCoverIdsAndDate]'
GO

/*
	GetZenithContext().VoluntaryExcessFactors.Where(x => coverIds.Contains(x.ExternalCoverId) && x.EffectiveFrom <= onThisDate).ToList();
*/

CREATE PROCEDURE [Rating].[GetVoluntaryExcessValuesByCoverIdsAndDate]
	@coverIds Core.ListOfInt READONLY, 
	@onThisDate DATETIME
AS
BEGIN
	
	SELECT DISTINCT 
		ExcessAmount AS [ExcessAmount]
	FROM dbo.VoluntaryExcessFactor
	WHERE ExternalCoverId IN (SELECT Id FROM @coverIds)
	AND EffectiveFrom <= @onThisDate

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsABreed]'
GO

/*
		   var breeds = ZenithContext.Breeds.Where(r => r.BreedId == breedId);

                return breeds.Any();
*/

CREATE PROCEDURE [Validation].[IsABreed] 
	@breedId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.Breed
		WHERE BreedId = @breedId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAccidentOnlyCover]'
GO

/*
		    var exemptions = ZenithContext.ExternalCovers.Where(f =>
                    f.ExternalCoverId == externalCoverId &&
                    f.AccidentOnly &&
                    f.EffectiveFrom <= onThisDate);

                return exemptions.Any();
*/

CREATE PROCEDURE [Validation].[IsAccidentOnlyCover] 
	@externalCoverId INT,
	@onThisDate DATETIME
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.ExternalCover e
		WHERE 
			e.ExternalCoverId = @externalCoverId
			AND e.AccidentOnly = 1
            AND e.EffectiveFrom <= @onThisDate
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAGender]'
GO

/*
		   var genders = ZenithContext.Genders.Where(r => r.GenderId == genderId);

                return genders.Any();
*/

CREATE PROCEDURE [Validation].[IsAGender] 
	@genderId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.Gender
		WHERE GenderId = @genderId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAMicroChipStatus]'
GO

/*
		   var status = ZenithContext.MicroChipStatuses.Where(r => r.MicroChipStatusId == microChipStatusId);

                return status.Any();
*/

CREATE PROCEDURE [Validation].[IsAMicroChipStatus] 
	@microChipStatusId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.MicroChipStatus
		WHERE MicroChipStatusId = @microChipStatusId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAnExternalCover]'
GO

/*
		   var level = ZenithContext.ExternalCovers.Where(r => r.ExternalCoverId == externalCoverId);

                return level.Any();
*/

CREATE PROCEDURE [Validation].[IsAnExternalCover] 
	@externalCoverId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.ExternalCover
		WHERE ExternalCoverId = @externalCoverId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAPetType]'
GO

/*
		   var petTypes = ZenithContext.PetTypes.Where(r => r.PetTypeId == petTypeId);

                return petTypes.Any();
*/

CREATE PROCEDURE [Validation].[IsAPetType] 
	@petTypeId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.PetType
		WHERE PetTypeId = @petTypeId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAPostcodeTaxExemption]'
GO

/*
		   var exemptions = ZenithContext.PostcodeTaxExemptions.Where(f =>
                    f.Postcode == postcode &&
                    f.EffectiveFrom <= onThisDate);

                return exemptions.Any();
*/

CREATE PROCEDURE [Validation].[IsAPostcodeTaxExemption] 
	@postcode VARCHAR(100), 
	@onThisDate DATETIME
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.PostcodeTaxExemption p
		WHERE 
			p.Postcode = @postcode and
            p.EffectiveFrom <= @onThisDate
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsASalesChannel]'
GO

/*
		    var channels = ZenithContext.SalesChannels.Where(r => r.SalesChannelId == salesChannelId);

                return channels.Any();
*/

CREATE PROCEDURE [Validation].[IsASalesChannel] 
	@salesChannelId int
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.SalesChannel
		WHERE SalesChannelId = @salesChannelId
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Validation].[IsAVoluntaryExcess]'
GO

/*
		  var excess = ZenithContext.VoluntaryExcessFactors.Where(f =>
                    f.ExcessAmount == excessAmount &&
                    f.ExternalCoverId == externalCoverId &&
                    f.EffectiveFrom <= onThisDate);

                return excess.Any();
*/

CREATE PROCEDURE [Validation].[IsAVoluntaryExcess] 
	@excessAmount decimal(12,5), 
	@externalCoverId int, 
	@onThisDate DATETIME
AS
BEGIN
	
	IF EXISTS 
	(
		SELECT 
			TOP 1 1
		FROM dbo.VoluntaryExcessFactor f
		WHERE 
			f.ExcessAmount = @excessAmount and
            f.ExternalCoverId = @externalCoverId and
            f.EffectiveFrom <= @onThisDate
	)
		SELECT CAST(1 AS BIT) IsValid    
    ELSE 
		SELECT  CAST(0 AS BIT) IsValid
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AdminFee]'
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [FK_dbo.AdminFee_dbo.AdminFeeRecipient_AdminFeeRecipientId] FOREIGN KEY ([AdminFeeRecipientId]) REFERENCES [dbo].[AdminFeeRecipient] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [FK_dbo.AdminFee_dbo.Currency_CurrencyId] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [FK_dbo.AdminFee_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [FK_dbo.AdminFee_dbo.SalesChannel_SalesChannelId] FOREIGN KEY ([SalesChannelId]) REFERENCES [dbo].[SalesChannel] ([SalesChannelId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdminFee] ADD CONSTRAINT [FK_dbo.AdminFee_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AgeAtInceptionFactor]'
GO
ALTER TABLE [dbo].[AgeAtInceptionFactor] ADD CONSTRAINT [FK_dbo.AgeAtInceptionFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AgeAtInceptionFactor] ADD CONSTRAINT [FK_dbo.AgeAtInceptionFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AgeFactor]'
GO
ALTER TABLE [dbo].[AgeFactor] ADD CONSTRAINT [FK_dbo.AgeFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AgeFactor] ADD CONSTRAINT [FK_dbo.AgeFactor_dbo.LifeSpan_LifeSpanId] FOREIGN KEY ([LifeSpanId]) REFERENCES [dbo].[LifeSpan] ([LifeSpanId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AgeFactor] ADD CONSTRAINT [FK_dbo.AgeFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AreaRatingFactor]'
GO
ALTER TABLE [dbo].[AreaRatingFactor] ADD CONSTRAINT [FK_dbo.AreaRatingFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[BaseRate]'
GO
ALTER TABLE [dbo].[BaseRate] ADD CONSTRAINT [FK_dbo.BaseRate_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BaseRate] ADD CONSTRAINT [FK_dbo.BaseRate_dbo.Currency_CurrencyId] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BaseRate] ADD CONSTRAINT [FK_dbo.BaseRate_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[BreedGroupAreaRate]'
GO
ALTER TABLE [dbo].[BreedGroupAreaRate] ADD CONSTRAINT [FK_BreedGroupAreaRate_ExternalCoverId_Ref_ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[BreedGroupFactor]'
GO
ALTER TABLE [dbo].[BreedGroupFactor] ADD CONSTRAINT [FK_dbo.BreedGroupFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BreedGroupFactor] ADD CONSTRAINT [FK_dbo.BreedGroupFactor_dbo.BreedGroup_BreedGroup_BreedFactorId] FOREIGN KEY ([BreedGroup_BreedFactorId]) REFERENCES [dbo].[BreedGroup] ([BreedFactorId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[BreedGroup]'
GO
ALTER TABLE [dbo].[BreedGroup] ADD CONSTRAINT [FK_dbo.BreedGroup_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[BreedLifeSpan]'
GO
ALTER TABLE [dbo].[BreedLifeSpan] ADD CONSTRAINT [FK_BreedLifeSpan_Breed] FOREIGN KEY ([BreedId]) REFERENCES [dbo].[Breed] ([BreedId])
GO
ALTER TABLE [dbo].[BreedLifeSpan] ADD CONSTRAINT [FK_BreedLifeSpan_LifeSpan] FOREIGN KEY ([LifeSpanId]) REFERENCES [dbo].[LifeSpan] ([LifeSpanId])
GO
ALTER TABLE [dbo].[BreedLifeSpan] ADD CONSTRAINT [FK_BreedLifeSpan_ExternalCover] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Breed]'
GO
ALTER TABLE [dbo].[Breed] ADD CONSTRAINT [FK_dbo.Breed_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[CappingRate]'
GO
ALTER TABLE [dbo].[CappingRate] ADD CONSTRAINT [FK_dbo.CappingRate_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CappingRate] ADD CONSTRAINT [FK_dbo.CappingRate_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ClaimsLoadingFactor]'
GO
ALTER TABLE [dbo].[ClaimsLoadingFactor] ADD CONSTRAINT [FK_dbo.ClaimsLoadingFactor_dbo.ReoccuranceLikelihood_ReoccuranceLikelihoodId] FOREIGN KEY ([ReoccuranceLikelihoodId]) REFERENCES [dbo].[ReoccuranceLikelihood] ([ReoccuranceLikelihoodId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClaimsLoadingFactor] ADD CONSTRAINT [FK_dbo.ClaimsLoadingFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[CollaringRate]'
GO
ALTER TABLE [dbo].[CollaringRate] ADD CONSTRAINT [FK_dbo.CollaringRate_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CollaringRate] ADD CONSTRAINT [FK_dbo.CollaringRate_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[CommissionFactor]'
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [FK_dbo.CommissionFactor_dbo.CommissionType_CommissionTypeId] FOREIGN KEY ([CommissionTypeId]) REFERENCES [dbo].[CommissionType] ([CommissionTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [FK_dbo.CommissionFactor_dbo.CommissionRecipient_CommissionRecipientId] FOREIGN KEY ([CommissionRecipientId]) REFERENCES [dbo].[CommissionRecipient] ([CommissionRecipientId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [FK_dbo.CommissionFactor_dbo.SalesChannel_SalesChannelId] FOREIGN KEY ([SalesChannelId]) REFERENCES [dbo].[SalesChannel] ([SalesChannelId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [FK_dbo.CommissionFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommissionFactor] ADD CONSTRAINT [FK_dbo.CommissionFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[DiscountRate]'
GO
ALTER TABLE [dbo].[DiscountRate] ADD CONSTRAINT [FK_dbo.DiscountRate_dbo.CommissionRecipient_CommissionRecipientId] FOREIGN KEY ([CommissionRecipientId]) REFERENCES [dbo].[CommissionRecipient] ([CommissionRecipientId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DiscountRate] ADD CONSTRAINT [FK_dbo.DiscountRate_dbo.Discount_DiscountId] FOREIGN KEY ([DiscountId]) REFERENCES [dbo].[Discount] ([DiscountId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[HelplineCharge]'
GO
ALTER TABLE [dbo].[HelplineCharge] ADD CONSTRAINT [FK_dbo.HelplineCharge_dbo.Currency_CurrencyId] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HelplineCharge] ADD CONSTRAINT [FK_dbo.HelplineCharge_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HelplineCharge] ADD CONSTRAINT [FK_dbo.HelplineCharge_dbo.Helpline_HelplineId] FOREIGN KEY ([HelplineId]) REFERENCES [dbo].[Helpline] ([HelplineId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TplCharge]'
GO
ALTER TABLE [dbo].[TplCharge] ADD CONSTRAINT [FK_dbo.TplCharge_dbo.Currency_CurrencyId] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TplCharge] ADD CONSTRAINT [FK_dbo.TplCharge_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TplCharge] ADD CONSTRAINT [FK_dbo.TplCharge_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Discount]'
GO
ALTER TABLE [dbo].[Discount] ADD CONSTRAINT [FK_dbo.Discount_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Discount] ADD CONSTRAINT [FK_dbo.Discount_dbo.SalesChannel_SalesChannelId] FOREIGN KEY ([SalesChannelId]) REFERENCES [dbo].[SalesChannel] ([SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ExternalCoverSalesType]'
GO
ALTER TABLE [dbo].[ExternalCoverSalesType] ADD CONSTRAINT [FK_ExternalCoverSalesType_ExternalCover] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId])
GO
ALTER TABLE [dbo].[ExternalCoverSalesType] ADD CONSTRAINT [FK_ExternalCoverSalesType_SalesChannel] FOREIGN KEY ([SalesChannelId]) REFERENCES [dbo].[SalesChannel] ([SalesChannelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[FeePercentage]'
GO
ALTER TABLE [dbo].[FeePercentage] ADD CONSTRAINT [FK_dbo.FeePercentage_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FeePercentage] ADD CONSTRAINT [FK_dbo.FeePercentage_dbo.Fee_FeeId] FOREIGN KEY ([FeeId]) REFERENCES [dbo].[Fee] ([FeeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[MicroChipFactor]'
GO
ALTER TABLE [dbo].[MicroChipFactor] ADD CONSTRAINT [FK_dbo.MicroChipFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MicroChipFactor] ADD CONSTRAINT [FK_dbo.MicroChipFactor_dbo.MicroChipStatus_MicroChipStatusId] FOREIGN KEY ([MicroChipStatusId]) REFERENCES [dbo].[MicroChipStatus] ([MicroChipStatusId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[NeuteringFactor]'
GO
ALTER TABLE [dbo].[NeuteringFactor] ADD CONSTRAINT [FK_dbo.NeuteringFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NeuteringFactor] ADD CONSTRAINT [FK_dbo.NeuteringFactor_dbo.Gender_GenderId] FOREIGN KEY ([GenderId]) REFERENCES [dbo].[Gender] ([GenderId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NeuteringFactor] ADD CONSTRAINT [FK_dbo.NeuteringFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PostcodeAreaRating]'
GO
ALTER TABLE [dbo].[PostcodeAreaRating] ADD CONSTRAINT [FK_dbo.PostcodeAreaRating_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PostcodeAreaRating] ADD CONSTRAINT [FK_dbo.PostcodeAreaRating_Pricing.SalesType_SalesTypeId] FOREIGN KEY ([SalesTypeId]) REFERENCES [dbo].[SalesType] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PremiumMinimum]'
GO
ALTER TABLE [dbo].[PremiumMinimum] ADD CONSTRAINT [FK_dbo.PremiumMinimum_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PremiumMinimum] ADD CONSTRAINT [FK_dbo.PremiumMinimum_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PriceFactor]'
GO
ALTER TABLE [dbo].[PriceFactor] ADD CONSTRAINT [FK_dbo.PriceFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PriceFactor] ADD CONSTRAINT [FK_dbo.PriceFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[SalesChannelFactor]'
GO
ALTER TABLE [dbo].[SalesChannelFactor] ADD CONSTRAINT [FK_dbo.SalesChannelFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SalesChannelFactor] ADD CONSTRAINT [FK_dbo.SalesChannelFactor_dbo.SalesChannel_SalesChannelId] FOREIGN KEY ([SalesChannelId]) REFERENCES [dbo].[SalesChannel] ([SalesChannelId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ScopeConfiguration]'
GO
ALTER TABLE [dbo].[ScopeConfiguration] ADD CONSTRAINT [FK_dbo.ScopeConfiguration_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TaxRate]'
GO
ALTER TABLE [dbo].[TaxRate] ADD CONSTRAINT [FK_dbo.TaxRate_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TaxRate] ADD CONSTRAINT [FK_dbo.TaxRate_dbo.Tax_TaxId] FOREIGN KEY ([TaxId]) REFERENCES [dbo].[Tax] ([TaxId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[VoluntaryExcessFactor]'
GO
ALTER TABLE [dbo].[VoluntaryExcessFactor] ADD CONSTRAINT [FK_dbo.VoluntaryExcessFactor_dbo.ExternalCover_ExternalCoverId] FOREIGN KEY ([ExternalCoverId]) REFERENCES [dbo].[ExternalCover] ([ExternalCoverId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VoluntaryExcessFactor] ADD CONSTRAINT [FK_dbo.VoluntaryExcessFactor_dbo.PetType_PetTypeId] FOREIGN KEY ([PetTypeId]) REFERENCES [dbo].[PetType] ([PetTypeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[AdminFee]'
GO
GRANT SELECT ON  [dbo].[AdminFee] TO [m-user]
GO
GRANT SELECT ON  [dbo].[AdminFee] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[BreedGroup]'
GO
GRANT SELECT ON  [dbo].[BreedGroup] TO [h-framework-user]
GO
GRANT SELECT ON  [dbo].[BreedGroup] TO [m-user]
GO
GRANT SELECT ON  [dbo].[BreedGroup] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[Breed]'
GO
GRANT SELECT ON  [dbo].[Breed] TO [m-user]
GO
GRANT SELECT ON  [dbo].[Breed] TO [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[CommissionFactor]'
GO
GRANT SELECT ON  [dbo].[CommissionFactor] TO [m-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[DiscountRate]'
GO
GRANT SELECT ON  [dbo].[DiscountRate] TO [h-framework-user]
GO
GRANT SELECT ON  [dbo].[DiscountRate] TO [m-user]
GO
GRANT SELECT ON  [dbo].[DiscountRate] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[DiscountRate] TO [s-user]
GO
GRANT SELECT ON  [dbo].[DiscountRate] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[Discount]'
GO
GRANT SELECT ON  [dbo].[Discount] TO [h-framework-user]
GO
GRANT SELECT ON  [dbo].[Discount] TO [m-user]
GO
GRANT SELECT ON  [dbo].[Discount] TO [ssrs-user]
GO
GRANT SELECT ON  [dbo].[Discount] TO [s-user]
GO
GRANT SELECT ON  [dbo].[Discount] TO [ult-admin]
GO
GRANT SELECT ON  [dbo].[Discount] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[HelplineCharge]'
GO
GRANT SELECT ON  [dbo].[HelplineCharge] TO [m-user]
GO
GRANT SELECT ON  [dbo].[HelplineCharge] TO [ult-admin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[TaxRate]'
GO
GRANT SELECT ON  [dbo].[TaxRate] TO [x-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Core]'
GO
GRANT EXECUTE ON SCHEMA:: [Core] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Discount]'
GO
GRANT EXECUTE ON SCHEMA:: [Discount] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Management]'
GO
GRANT EXECUTE ON SCHEMA:: [Management] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Rating]'
GO
GRANT EXECUTE ON SCHEMA:: [Rating] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on SCHEMA:: [Validation]'
GO
GRANT EXECUTE ON SCHEMA:: [Validation] TO [h-framework-user]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
