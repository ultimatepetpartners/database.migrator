﻿using FluentMigrator;

namespace uispet.Migrations.Scripts
{
    [Migration(201912290940)]
    public class M201912290940_Baseline : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("01-20191229-ZenithPricingEngineDatabaseSchemaBaseLine-Down.sql");
        }

        public override void Up()
        {
            Execute.EmbeddedScript("01-20191229-ZenithPricingEngineDatabaseSchemaBaseLine-Up.sql");
        }
    }
}
